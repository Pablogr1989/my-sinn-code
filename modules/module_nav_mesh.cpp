#include "mcv_platform.h"
#include "module_nav_mesh.h"

void CModuleNavMesh::start()
{
    navmesh = CNavmesh();
    navmesh.loadMesh("data/navmeshes/milestone5.bin");
    //navmesh.loadMesh("data/navmeshes/milestone2.bin");
    if (navmesh.m_navMesh) {
        navmesh.prepareQueries();
    }
    else {
        fatal("Error when creating navmesh\n");
    }

}

void CModuleNavMesh::stop()
{
	navmesh.destroy();
}

void CModuleNavMesh::renderDebug()
{
}

void CModuleNavMesh::update(float elapsed)
{
}

void CModuleNavMesh::renderInMenu()
{
}

vector<VEC3> CModuleNavMesh::calculePath(VEC3 pos, VEC3 dest, float step, float slop)
{
    return navmeshQuery.findPath(pos, dest, step, slop);
}

vector<VEC3> CModuleNavMesh::calculePath(VEC3 pos, VEC3 dest)
{
    return navmeshQuery.findPath(pos, dest, step_size, slope);
}

float CModuleNavMesh::wallDistance(VEC3 pos)
{
    return navmeshQuery.wallDistance(pos);
}

bool CModuleNavMesh::raycast(VEC3 start, VEC3 end, VEC3& hitPos)
{
	return navmeshQuery.raycast(start, end, hitPos);  //Si es true, es que hay algo en medio, si no, no hay nada
}

VEC3 CModuleNavMesh::findNearestNavPoint(VEC3 start)
{
    return navmeshQuery.closestNavmeshPoint(start);
}

//Este metodo nos encuentra el punto mas cercano a una zona que tiene un flag especifico (por ejemplo un precipicio al que lanzar al player)
VEC3 CModuleNavMesh::findNearestPointFilterPoly(VEC3 pos, PolyFlags filter)
{
    return navmeshQuery.nearestPointFilterPoly(pos, filter);
}

VEC3 CModuleNavMesh::findRandomPointAroundCircle(VEC3 pos, float maxDist)
{
    return navmeshQuery.findRandomPointAroundCircle(pos, maxDist);
}

VEC3 CModuleNavMesh::findRandomPoint()
{
    return navmeshQuery.findRandomPoint();
}
