#pragma once
#include "modules/module.h"
#include "sinn/navmesh/navmesh.h"
#include "sinn/navmesh/navmesh_query.h"

using namespace std;

class CModuleNavMesh : public CModule
{
private:
    CNavmesh navmesh;
    CNavmeshQuery navmeshQuery = CNavmeshQuery(&navmesh);

    float step_size = 2.0f;
    float slope = 0.01f;

public:
    CModuleNavMesh(const std::string& name) : CModule(name) {}
    void start() override;
    void stop() override;
    void renderDebug() override;
    void update(float elapsed) override;
    void renderInMenu() override;

    vector<VEC3> calculePath(VEC3 pos, VEC3 dest, float step, float slop);
    vector<VEC3> calculePath(VEC3 pos, VEC3 dest);
    float wallDistance(VEC3 pos);
    bool raycast(VEC3 start, VEC3 end, VEC3& hitPos);
    VEC3 findNearestNavPoint(VEC3 start);
    VEC3 findNearestPointFilterPoly(VEC3 pos, PolyFlags filter);
    VEC3 findRandomPointAroundCircle(VEC3 pos, float maxDist);
    VEC3 findRandomPoint();

    void setStepSize(float size) { step_size = size; }
    void setSlope(float size) { slope = size; }



};

