#pragma once

#include "geometry/geometry.h"
#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "render/render.h"
#include "game_core_skeleton.h"
#include "engine.h"
#include "sinn/modules/module_physics_sinn.h"
#include "components/common/comp_transform.h"

class CGameCoreSkeleton;
class CalModel;

using namespace std;

struct TCompSkeleton : public TCompBase {

    struct TAutoRotator {
        bool is_enabled = false;
        bool rot_yaw = false;
        bool rot_pitch = false;
        float total_time;
        float curr_time = 0.0f;
        float yaw_rotation_speed;
        float pitch_rotation_speed;
        void applyYawRotation(float total_angle, float new_total_time);
        void applyPitchRotation(float total_angle, float new_total_time);
        void update(TCompTransform* ct, float dt);
        void reset();
    };

    struct TMov {
        int frame = 0;
        VEC3 despl = VEC3::Zero;
        float yaw = 0;
        float pitch = 0;
        float roll = 0;
        void clear();
    };

    struct TSaveAnimMovs {
        int frame = 0;
        string name_ani = "";
        float yaw_to_Point = 0;
        vector<TMov> ani_movs;
        VEC3 sum_mov = VEC3::Zero;
        float sum_yaw = 0;
        float sum_pitch = 0;
        float sum_roll = 0;

        void insert(VEC3 mov, float y, float p, float r);
        void printAll();
        void init(string name, float yaw);
    };

    struct TSaveRootMov {
        int frame = 0;
        int id_bone = 0;
        string name_ani = "";
        vector<TMov> root_movs;
        TMov last_mov;

        VEC3 sum_mov = VEC3::Zero;
        float sum_yaw = 0;
        float sum_pitch = 0;
        float sum_roll = 0;
        void insert(VEC3 pos, float y, float p, float r);
        void printAll();
        void init(string name, int id, VEC3 pos, float y, float p, float r);
    };

  TCompSkeleton();
  ~TCompSkeleton();

  TAutoRotator autoRot;

  //Variables DEBUG
  TSaveRootMov debug_root_movs;
  TSaveAnimMovs debug_anim_movs;
  bool use_save_root_movs = false;
  bool use_save_anim_movs = false;
  float debug_sum_yaw = 0;
  float debug_sum_pitch = 0;
  float debug_sum_roll = 0;
  VEC3 debug_sum_deltaMove = VEC3::Zero;
  /*************************************/

  int id_head_bone = -1;
  int id_left_foot = -1;
  int id_right_foot = -1;

  string head_bone_name = "";
  string left_foot_name = "";
  string right_foot_name = "";

  MAT44 mat_bone_head;
  VEC3 pos_bone_head;

  bool initAction = false;
  QUAT initial_rotation = QUAT(0, 0, 0, 0);

  VEC3 delta_move; //Movimiento que extraemos de la animacion
  float delta_yaw=0; //Rotacion yaw que extraemos de la animacion
  float delta_pitch = 0; //Rotacion pitch que extraemos de la animacion
  float delta_roll = 0;

  //Variables para corregir la posicion y rotacion del root bone
  VEC3 add_root_move = VEC3::Zero;
  QUAT add_root_rot = QUAT(0, 0, 0, -1);
  bool correct_root = false;


  CalModel* model = nullptr;
  const CGameCoreSkeleton* getGameCore() const;
  CCteBuffer<CteSkinBones> cb_bones;


  //void updateCtesBones();
  void renderDebug();
  void debugInMenu();
  void debugBone(int id);
  
  void updateSkeleton(float dt);
  void updatePostSkeleton(float dt);
  void update(float dt);
  void updateMultithread(float dt);
  void updatePostMultithread(float dt);
  
  void load(const json& j, TEntityParseContext& ctx);
  void onEntityCreated();
  void updateCtesBones();
  void updateAABB();
  VEC3 getFront(MAT44 mat);
  void correctYawAngle(float angle, float time);
  void correctPitchAngle(float angle, float time);
  void resetCorrectAngle() { autoRot.reset(); }
  void initialize();

  DECL_SIBLING_ACCESS();

public:
    bool enable_root_motion = false;

    CalModel* getModel();
    VEC3 getDeltaMove();
    float getDeltaYaw();
    float getDeltaPitch();
    float getDeltaRoll();
    void setApplyActionYawRotation(bool a);
    void setApplyActionPitchRotation(bool a);
    void setApplyCycleYawRotation(bool a);
    void setApplyCyclePitchRotation(bool a);
    void setApplyActionMovement(bool a);
    void setApplyCycleMovement(bool a);
    void setUseRootMotion(bool r);

    bool hasRightOrLeftFootInFloor(bool rightOrLeft, float floorPos, float maxDistance);
    int getLegForward();

    VEC3 getPosBoneHead();
    VEC3 getFrontBoneHead();
    VEC3 getLeftBoneHead();
    VEC3 getUpBoneHead();

    int getIdBone(string nameBone);
    QUAT getAbsRotationBone(int id);
    VEC3 getAbsTraslationBone(int id);
    CalMixer::vecT getTypeFront();
    CalMixer::vecT getTypeLeft();
    CalMixer::vecT getTypeUp();

    VEC3 returnVector(CalMixer::vecT type, MAT44 mtx);
    void insertMovBone();

    void addCorrectMoveRoot(VEC3 delta_move) { add_root_move += delta_move; }
    void addCorrectRotRoot(QUAT delta_rot) { add_root_rot *= delta_rot; }
    void setCorrectMoveRoot(VEC3 delta_move) { dbg("El root ahora esta deplazado [%.2f %.2f %.2f]\n", delta_move.x, delta_move.y, delta_move.z); add_root_move = delta_move; }
    void setCorrectRotRoot(QUAT delta_rot) { add_root_rot = delta_rot; }
    void clearCorrectRoot();
    void setCorrectRoot(bool b) { correct_root = b; }

};
