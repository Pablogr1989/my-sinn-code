#include "mcv_platform.h"
#include "comp_skeleton.h"
#include "cal3d/cal3d.h"
#include "game_core_skeleton.h"
#include "render/draw_primitives.h"      // for the max_skeletons_ctes
#include "components/common/comp_transform.h"
#include "components/common/comp_aabb.h"
#include "comp_skel_lookat.h"
#include "cal3d2engine.h"
#include <sinn\characters\animator.h>
#include <windows\application.h>
#include <components\common\comp_name.h>

// Changed name from skeleton to force to be parsed before the comp_mesh
DECL_OBJ_MANAGER("armature", TCompSkeleton);

// ---------------------------------------------------------------------------------------
// Cal2DX conversions, VEC3 are the same, QUAT must change the sign of w
CalVector DX2Cal(VEC3 p) {
  return CalVector(p.x, p.y, p.z);
}
CalQuaternion DX2Cal(QUAT q) {
  return CalQuaternion(q.x, q.y, q.z, -q.w);
}
VEC3 Cal2DX(CalVector p) {
  return VEC3(p.x, p.y, p.z);
}
QUAT Cal2DX(CalQuaternion q) {
  return QUAT(q.x, q.y, q.z, -q.w);
}
MAT44 Cal2DX(CalVector trans, CalQuaternion rot) {
  return
    MAT44::CreateFromQuaternion(Cal2DX(rot))
    * MAT44::CreateTranslation(Cal2DX(trans))
    ;
}

// ---------------------------------------------------------------------------------------
void TCompSkeleton::load(const json& j, TEntityParseContext& ctx) {

  std::string src = j["src"];

  if (!j.contains("headBone")) {
    head_bone_name = "";
  }
  else {
    head_bone_name = j["headBone"];
  }

  left_foot_name = j.value("leftFoot", "");
  right_foot_name = j.value("rightFoot", "");

  auto core = Resources.get(src)->as<CGameCoreSkeleton>();

  model = new CalModel((CalCoreModel*)core);
  model->getMixer()->blendCycle(0, 1.0f, 0.f);
  // Do a time zero update just to have the bones in a correct place
  model->update(0.f);
  bool free_root_movement = j.value("freeRootMovement", false);
  if (free_root_movement) {
    model->getMixer()->setRemoveActionMovement(false);
    model->getMixer()->setRemoveCycleMovement(false);
    model->getMixer()->setApplyActionMovement(false);
    model->getMixer()->setApplyCycleMovement(false);
  }
  if (!j.contains("move_bone_id")) {
    model->getMixer()->setVectors();
  }
  else {
    model->getMixer()->setIdRootBone(j["move_bone_id"]);
    model->getMixer()->setVectors();
  }

}

TCompSkeleton::TCompSkeleton()
{
}

TCompSkeleton::~TCompSkeleton() {
  cb_bones.destroy();
  if (model)
    delete model;
  model = nullptr;
}

void TCompSkeleton::updateMultithread(float dt)
{
	updateSkeleton(dt);
}

void TCompSkeleton::updatePostMultithread(float dt)
{
	updatePostSkeleton(dt);
}

void TCompSkeleton::updateSkeleton(float dt) {
	PROFILE_FUNCTION("updateSkeleton");
	assert(model);

  TCompTransform* c_trans = get<TCompTransform>();
  TCompCollider* c_col = get<TCompCollider>();
  autoRot.update(c_trans, dt);

  CEntity* e = CHandle(this).getOwner();
  std::string name = e->getName();
  static bool collision_down = false;

  VEC3 world_pos = c_trans->getPosition();
  QUAT world_rot = c_trans->getRotation();

  if (correct_root) {
      world_pos += add_root_move;
      world_rot *= add_root_rot;
  }

  model->getMixer()->setWorldTransform(DX2Cal(world_pos), DX2Cal(world_rot));
  {
    PROFILE_FUNCTION("Mixer");
    model->update(dt);
  }



  float yaw, pitch, roll;
  c_trans->getEulerAngles(&yaw, &pitch, &roll);
  //dbg("Frame %d Rotacion Yaw:%.4f, Pitch:%.4f\n", CApplication::get().getFrameCount(), yaw, pitch);

  //OBTENER LA YAW DE LA ROTACION DE LA ANIMACION
  QUAT q = model->getMixer()->getFuckingQUAT();
  VEC3 f = model->getMixer()->getFuckingFront();
  VEC3 left = model->getMixer()->getFuckingLeft();
  VEC3 up = model->getMixer()->getFuckingUp();
  MAT44 mat = model->getMixer()->getFuckingMAT44();

  //CalBone* cal_bone = model->getSkeleton()->getBone(0);
  
  /////////////////////////////////////////////////////////////
  //OBTENER EL VECTOR DE DESPLAZAMIENTO Y ROTACION DE LA ANIMACION ACTION
  //////////////////////////////////////////////////////////////////
  if (model->getMixer()->is_animAction()) {

    if (!initAction) {
      initial_rotation = c_trans->getRotation();

      debug_sum_yaw = 0;
      debug_sum_pitch = 0;
      debug_sum_roll = 0;
      debug_sum_deltaMove = VEC3::Zero;

      initAction = true;
    }

    delta_pitch = model->getMixer()->getAndClearRootBonePitch();
    delta_yaw = model->getMixer()->getAndClearRootBoneYaw();
    delta_roll = model->getMixer()->getAndClearRootBoneRoll();
    delta_move = Cal2DX(model->getMixer()->getAndClearDeltaLocalRootMotion());

    /*dbg("Frame %d Mi pitch %f \n", CApplication::get().getFrameCount(), rad2deg(pitch));
    dbg("----------------------------------------------------------------------------------\n");
    dbg("SKELETON\n");
    dbg("Frame %d Delta Move X:%f Y:%f Z:%f \n", CApplication::get().getFrameCount(), delta_move.x, delta_move.y, delta_move.z);
    dbg("Frame %d Le voy aplicar: pitch %f \n", CApplication::get().getFrameCount(), rad2deg(delta_pitch));
    dbg("Frame %d Le voy aplicar: yaw %f \n", CApplication::get().getFrameCount(), rad2deg(delta_yaw));*/


    /*dbg("Frame %d QUAT X:%.4f, Y:%.4f, Z:%.4f W:%.4f \n", CApplication::get().getFrameCount(), q.x, q.y, q.z, q.w);
    dbg("Frame %d Front X:%.4f, Y:%.4f, Z:%.4f\n", CApplication::get().getFrameCount(), f.x, f.y, f.z);
    dbg("Frame %d Left X:%.4f, Y:%.4f, Z:%.4f\n", CApplication::get().getFrameCount(), left.x, left.y, left.z);
    dbg("Frame %d Up X:%.4f, Y:%.4f, Z:%.4f\n\n", CApplication::get().getFrameCount(), up.x, up.y, up.z);*/


    CalBone* cal_bone = model->getSkeleton()->getBone(model->getMixer()->getRootId());
    QUAT rot = Cal2DX(cal_bone->getRotationAbsolute());
    VEC3 pos = Cal2DX(cal_bone->getTranslationAbsolute());
    MAT44 mtx;
    mtx = MAT44::CreateFromQuaternion(rot) * MAT44::CreateTranslation(pos);
    VEC3 front = -mtx.Forward();
    left = -mtx.Left();
    up = mtx.Up();

    VEC3 v;

    switch (model->getMixer()->getTypeFront()) {
    case CalMixer::vecT::FRONT:
      v = -mtx.Forward();
      break;
    case CalMixer::vecT::NFRONT:
      v = mtx.Forward();
      break;
    case CalMixer::vecT::LEFT:
      v = -mtx.Left();
      break;
    case CalMixer::vecT::NLEFT:
      v = mtx.Left();
      break;
    case CalMixer::vecT::UP:
      v = mtx.Up();
      break;
    case CalMixer::vecT::NUP:
      v = -mtx.Up();
      break;
    }

    float yaw, pitch, roll = 0.f;
    float mdo = sqrtf(v.x * v.x + v.z * v.z);
    yaw = atan2f(v.x, v.z);
    pitch = atan2f(-v.y, mdo);

    //dbg("Frame %d Bone pitch %f \n", CApplication::get().getFrameCount(), rad2deg(pitch));
    //dbg("Frame %d Bone yaw %f \n", CApplication::get().getFrameCount(), rad2deg(yaw));

    if (use_save_root_movs) {
        insertMovBone();
    }

    if (use_save_anim_movs) {
        VEC3 abs_mov = VEC3::Transform(delta_move, c_trans->getRotation());
        debug_anim_movs.insert(abs_mov, delta_yaw, delta_pitch, delta_roll);
        dbg("\n");
    }


  }
  else {

      
    model->getMixer()->getAndClearRootBonePitch();
    model->getMixer()->getAndClearRootBoneYaw();
    model->getMixer()->getAndClearRootBoneRoll();
    model->getMixer()->getAndClearDeltaLocalRootMotion();

    if (initAction) {
      initAction = false;

      if (use_save_root_movs) {
          debug_root_movs.printAll();
      }

      if (use_save_anim_movs) {
          debug_anim_movs.printAll();
      }
    }
  }



  {
    PROFILE_FUNCTION("RootMotion");
    if (enable_root_motion) {
      dbg("root motion\n");

      if (model->getMixer()->is_animAction()) {

           VEC3 Pos = getAbsTraslationBone(0);

           /*QUAT Rot = QUAT::CreateFromYawPitchRoll(delta_yaw, delta_pitch, delta_roll);
           VEC3 Rel_Pos = c_trans->getPosition() - Pos;
           MAT44 Rot_Matrix = MAT44::CreateFromQuaternion(Rot);
           c_trans->setRotation(Rot* c_trans->getRotation());
           MAT44 Player_Mat = MAT44::CreateTranslation(Rel_Pos);
           MAT44 new_trans = Player_Mat * Rot_Matrix;*/
           //VEC3 delta = VEC3(new_trans._41, new_trans._42, new_trans._43) - Rel_Pos;


            VEC3 abs_mov = VEC3::Transform(delta_move, initial_rotation);
            //VEC3 mov = (delta / dt) + abs_mov;
            //c_trans->addPosition(abs_mov);
            c_trans->addPosition(abs_mov);


           c_trans->getEulerAngles(&yaw, &pitch, &roll);
            yaw += delta_yaw;
            pitch += delta_pitch;
            roll += delta_roll;
            c_trans->setEulerAngles(yaw, pitch, roll);

            debug_sum_yaw += rad2deg(delta_yaw);
            debug_sum_pitch += rad2deg(delta_pitch);
            debug_sum_roll += rad2deg(delta_roll);
            debug_sum_deltaMove += abs_mov;

            delta_move = VEC3::Zero;
            delta_yaw = 0;
            delta_pitch = 0;
            delta_roll = 0;

            //CEngine::get().getPhysics().moveEnemy(abs_mov, dt, get<TCompCollider>(), collision_down);

            c_col->controller->setFootPosition(PxExtendedVec3(c_trans->getPosition().x, c_trans->getPosition().y, c_trans->getPosition().z));

      }
    }
  }


  if (id_head_bone >= 0) {
      CalBone* head_bone = model->getSkeleton()->getBone(id_head_bone);
      QUAT rot = Cal2DX(head_bone->getRotationAbsolute());
      VEC3 pos = Cal2DX(head_bone->getTranslationAbsolute());
      mat_bone_head = MAT44::CreateFromQuaternion(rot) * MAT44::CreateTranslation(pos);
      pos_bone_head = pos;
  }
  //dbg("----------------------------------------------------------------------------------\n");

  /*CalBone* root_bone = model->getSkeleton()->getBone(0);
  VEC3 posAbs = Cal2DX(root_bone->getTranslationAbsolute());
  CalVector traslation = DX2Cal(posAbs + pos_correction);
  root_bone->blendState(1, traslation, root_bone->getRotationAbsolute());*/

  updateCtesBones();
  updateAABB();
}

void TCompSkeleton::updatePostSkeleton(float dt)
{
	PROFILE_FUNCTION("updatePostSkeleton");
	TCompSkelLookAt* lookat = get< TCompSkelLookAt>();
	if (lookat)
		lookat->update(dt);

	updateAABB();
}

void TCompSkeleton::update(float dt) {
  PROFILE_FUNCTION("updateCompSkeleton");
  updateSkeleton(dt);
  //updatePostSkeleton(dt);
}

const CGameCoreSkeleton* TCompSkeleton::getGameCore() const {
  return (CGameCoreSkeleton*)model->getCoreModel();
}

void TCompSkeleton::updateAABB() {
  PROFILE_FUNCTION("updateAABB");
  TCompAbsAABB* aabb = get<TCompAbsAABB>();
  if (aabb) {
    VEC3 points[MAX_SUPPORTED_BONES];
    CalSkeleton* skel = model->getSkeleton();
    auto& cal_bones = skel->getVectorBone();
    assert(cal_bones.size() < MAX_SUPPORTED_BONES);

    auto& bone_ids_for_aabb = getGameCore()->bone_ids_for_aabb;
    if (bone_ids_for_aabb.size() < 3)return;
    int out_idx = 0;
    for (int bone_idx : bone_ids_for_aabb) {
      CalBone* bone = cal_bones[bone_idx];
      points[out_idx] = Cal2DX(bone->getTranslationAbsolute());
      ++out_idx;
    }
    {
      PROFILE_FUNCTION("CreateFromPoints");
      AABB::CreateFromPoints(*aabb, out_idx, points, sizeof(VEC3));
    }
    aabb->Extents = getGameCore()->aabb_extra_factor * VEC3(aabb->Extents);
  }
}

VEC3 TCompSkeleton::getFront(MAT44 mat)
{
    return -mat.Forward();
}

void TCompSkeleton::correctYawAngle(float angle, float time)
{
    autoRot.applyYawRotation(deg2rad(angle), time);
}

void TCompSkeleton::correctPitchAngle(float angle, float time)
{
    autoRot.applyPitchRotation(deg2rad(angle), time);
}

void TCompSkeleton::initialize()
{
    auto core = (CGameCoreSkeleton*)model->getCoreModel();
    auto bones = core->getCoreSkeleton()->getVectorCoreBone();
    for (int bone_id = 0; bone_id < bones.size(); ++bone_id) {
        string name = bones[bone_id]->getName();
        if (head_bone_name == name) {
            id_head_bone = bone_id;
        }
        if (right_foot_name == name) {
            id_right_foot = bone_id;
        }
        if (left_foot_name == name) {
            id_left_foot = bone_id;
        }
    }

}

void TCompSkeleton::debugInMenu() {
  static int anim_id = 0;
  static float in_delay = 0.3f;
  static float out_delay = 0.3f;
  static bool auto_lock = false;
  static int animOut_id = 0;
  static int animIn_id = 0;

  if (ImGui::TreeNode("Root bone correction")) {
      static float add_yaw = 0, add_pitch = 0, add_roll = 0;

      ImGui::Checkbox("Coorect Root Bone", &correct_root);
      ImGui::DragFloat3("Add root move", &add_root_move.x, 0.1, -1000, 1000);
      ImGui::DragFloat("Add Yaw rot", &add_yaw, 0.1f, -180.0f, 180.0f);
      float max_pitch = 90.0f - 1e-3f;
      ImGui::DragFloat("Add Pitch rot", &add_pitch, 0.1f, -max_pitch, max_pitch);
      ImGui::DragFloat("Add Roll rot", &add_roll, 0.1f, -180.0f, 180.0f);

      add_root_rot = QUAT::CreateFromYawPitchRoll(add_yaw, add_pitch, add_roll);
      ImGui::DragFloat4("Add root rot", &add_root_rot.x);

      static float dist_front = 0.f;
      static float dist_left = 0.f;
      ImGui::DragFloat("Distancia front", &dist_front, 0.1, -1000.f, 1000.f);
      if (ImGui::SmallButton("Add move from the front ")) {
          TCompTransform* trans = get<TCompTransform>();
          VEC3 pos = trans->getPosition() + trans->getFront() * dist_front;
          add_root_move += ( pos - trans->getPosition());
      }
      ImGui::DragFloat("Distancia left", &dist_left, 0.1, -1000.f, 1000.f);
      if (ImGui::SmallButton("Add move from the left ")) {
          TCompTransform* trans = get<TCompTransform>();
          VEC3 pos = trans->getPosition() + trans->getLeft() * dist_left;
          add_root_move += (pos - trans->getPosition()) ;
      }


  }

  ImGui::Text("Hueso cabeza        %s[%d]", head_bone_name.c_str(), id_head_bone);
  ImGui::Text("Hueso pie izquierdo %s[%d]", left_foot_name.c_str(), id_left_foot);
  ImGui::Text("Hueso pie derecho   %s[%d]", right_foot_name.c_str(), id_right_foot);

  if (ImGui::SmallButton("Clear sum moves")) {
    debug_sum_yaw = 0;
    debug_sum_pitch = 0;
    debug_sum_roll = 0;
    debug_sum_deltaMove = VEC3::Zero;
  }

  ImGui::Text("Suma yaw: %.4f", debug_sum_yaw);
  ImGui::Text("Suma pitch: %.4f", debug_sum_pitch);
  ImGui::Text("Suma roll: %.4f", debug_sum_roll);
  ImGui::Text("Suma delta_move: X:%.4f Y:%.4f Z:%.4f", debug_sum_deltaMove.x, debug_sum_deltaMove.y, debug_sum_deltaMove.z);

  ImGui::Text("Pos bone head %2.f %2.f %2.f", pos_bone_head.x, pos_bone_head.y, pos_bone_head.z);
  ImGui::Checkbox("Enable Root Motion", &enable_root_motion);
  ImGui::Checkbox("Save Root Movs in Action", &use_save_root_movs);
  ImGui::Checkbox("Save Action Anim Movs", &use_save_anim_movs);
  

  // Play actions/cycle from the menu
  ImGui::DragInt("Anim Id", &anim_id, 0.1f, 0, model->getCoreModel()->getCoreAnimationCount() - 1);
  auto core_anim = model->getCoreModel()->getCoreAnimation(anim_id);
  if (core_anim)
    ImGui::Text("%s", core_anim->getName().c_str());
  ImGui::DragFloat("In Delay", &in_delay, 0.01f, 0, 1.f);
  ImGui::DragFloat("Out Delay", &out_delay, 0.01f, 0, 1.f);
  ImGui::Checkbox("Auto lock", &auto_lock);
  if (ImGui::SmallButton("As Cycle")) {
    model->getMixer()->blendCycle(anim_id, 1.0f, in_delay);

  }
  if (ImGui::SmallButton("As Action")) {
    dbg("Frame %d Voy a reproducir la action %s\n", CApplication::get().getFrameCount(), core_anim->getName().c_str());
    if (use_save_root_movs) {
        QUAT rot = getAbsRotationBone(0);
        VEC3 pos = getAbsTraslationBone(0);

        MAT44 mtx;
        mtx = MAT44::CreateFromQuaternion(rot) * MAT44::CreateTranslation(pos);

        VEC3 v = returnVector(model->getMixer()->getTypeFront(), mtx);

        float yaw, pitch, roll = 0.f;

        yaw = atan2f(v.x, v.z);
        float mdo = sqrtf(v.x * v.x + v.z * v.z);
        pitch = atan2f(-v.y, mdo);

        debug_root_movs.init(core_anim->getName().c_str(), 0, pos, yaw, pitch, 0);
    }

    if (use_save_anim_movs) {
        QUAT rot = getAbsRotationBone(0);
        VEC3 pos = getAbsTraslationBone(0);

        MAT44 mtx;
        mtx = MAT44::CreateFromQuaternion(rot) * MAT44::CreateTranslation(pos);

        VEC3 v = returnVector(model->getMixer()->getTypeFront(), mtx);

        float yaw = atan2f(v.x, v.z);

        debug_anim_movs.init(core_anim->getName().c_str(), yaw);
    }
    model->getMixer()->executeAction(anim_id, in_delay, out_delay, 1.0f, auto_lock);
  }

  auto mixer = model->getMixer();

  ImGui::Text("Numero de animaciones acciones %d", mixer->getAnimationActionList().size());
  ImGui::Text("Numero de animaciones ciclos %d", mixer->getAnimationCycle().size());

  // Dump Mixer
  for (auto a : mixer->getAnimationActionList()) {
    ImGui::PushID(a);
    ImGui::Text("Action %s S:%d W:%1.2f Time:%1.4f/%1.4f"
      , a->getCoreAnimation()->getName().c_str()
      , a->getState()
      , a->getWeight()
      , a->getTime()
      , a->getCoreAnimation()->getDuration()
    );
    ImGui::SameLine();
    if (ImGui::SmallButton("X")) {
      auto core = (CGameCoreSkeleton*)model->getCoreModel();
      int id = core->getCoreAnimationId(a->getCoreAnimation()->getName());
      if (a->getState() == CalAnimation::State::STATE_STOPPED)
        mixer->removeAction(id, 0);
      else
        a->remove(out_delay);
      ImGui::PopID();
      break;
    }
    ImGui::PopID();
  }
  static float tf = 1;

  /*ImGui::Text("Mixer Time: %f/%f", mixer->getAnimationTime(), mixer->getAnimationDuration());
  ImGui::DragFloat("Time factor", &tf, 0.1, 0, 10, "%.2f");
  if (ImGui::SmallButton("Cambiar Time Factor")) {
    mixer->setTimeFactor(tf);
  }*/

  for (auto a : mixer->getAnimationCycle()) {
    ImGui::PushID(a);
    ImGui::Text("Cycle %s S:%d W:%1.4f Time:%1.4f"
        , a->getCoreAnimation()->getName().c_str()
        , a->getState()
        , a->getWeight()
        , a->getCoreAnimation()->getDuration()
    );
    ImGui::SameLine();
    if (ImGui::SmallButton("X")) {
      auto core = (CGameCoreSkeleton*)model->getCoreModel();
      int id = core->getCoreAnimationId(a->getCoreAnimation()->getName());
      mixer->clearCycle(id, out_delay);
    }
    ImGui::PopID();
  }

  // Test mixing two animations as cycles
  if (mixer->getAnimationCycle().size() == 2) {      
    static bool unit_weight = false;
    ImGui::Checkbox("Unit Weight", &unit_weight);
    if (unit_weight) {
      static float w1 = 0.5f;
      auto anim1 = mixer->getAnimationCycle().front();
      auto anim2 = mixer->getAnimationCycle().back();
      float wsmall = 1e-3f;      
      if (ImGui::DragFloat("Weight", &w1, 0.005f, wsmall, 1.0f - wsmall)) {
        int anim1_id = model->getCoreModel()->getCoreAnimationId(anim1->getCoreAnimation()->getName());
        int anim2_id = model->getCoreModel()->getCoreAnimationId(anim2->getCoreAnimation()->getName());
        model->getMixer()->blendCycle(anim1_id, w1, 0.1f);
        model->getMixer()->blendCycle(anim2_id, 1.0f - w1, 0.1f);        
      }      
    }
  }

  ImGui::Checkbox("Not remove Y axis", &mixer->remove_y);

  ImGui::Checkbox("Remove Action movement", &mixer->removeActionMovement);
  ImGui::Checkbox("Remove Action Pitch Rotation", &mixer->removeActionPitchRotation);
  ImGui::Checkbox("Remove Action Yaw Rotation", &mixer->removeActionYawRotation);
  ImGui::Checkbox("Remove Action Roll Rotation", &mixer->removeActionRollRotation);

  ImGui::Checkbox("Apply Action movement", &mixer->applyActionMovement);
  ImGui::Checkbox("Apply Action Pitch Rotation", &mixer->applyActionPitchRotation);
  ImGui::Checkbox("Apply Action Yaw Rotation", &mixer->applyActionYawRotation);
  ImGui::Checkbox("Apply Action Roll Rotation", &mixer->applyActionRollRotation);

  ImGui::Checkbox("Remove Cycle movement", &mixer->removeCycleMovement);
  ImGui::Checkbox("Remove Cycle Pitch Rotation", &mixer->removeCyclePitchRotation);
  ImGui::Checkbox("Remove Cycle Yaw Rotation", &mixer->removeCycleYawRotation);
  ImGui::Checkbox("Remove Cycle Roll Rotation", &mixer->removeCycleRollRotation);

  ImGui::Checkbox("Apply Cycle movement", &mixer->applyCycleMovement);
  ImGui::Checkbox("Apply Cycle Pitch Rotation", &mixer->applyCyclePitchRotation);
  ImGui::Checkbox("Apply Cycle Yaw Rotation", &mixer->applyCycleYawRotation);
  ImGui::Checkbox("Apply Cycle Roll Rotation", &mixer->applyCycleRollRotation);

  if (ImGui::SmallButton("Prueba Constrict Front")) {
      correctYawAngle(-180.0f, 0.3f);
      model->getMixer()->executeAction(21, 0.3, 0.3, 1.0f, true);
  }

  //ImGui::Checkbox("Root motion", &mixer->useRootMotion);

  // Show Skeleton Resource
  if (ImGui::TreeNode("Core")) {
    auto core_skel = (CGameCoreSkeleton*)model->getCoreModel();
    if (core_skel)
      core_skel->renderInMenu();
    ImGui::TreePop();
  }



  /*if (core_anim) {
    if (ImGui::SmallButton("Animacion bucle")) {
      TCompAnimator* animator = get<TCompAnimator>();
      animator->playParcialActionBucle(core_anim->getName());
    }
  }*/

  auto core = (CGameCoreSkeleton*)model->getCoreModel();
  if (ImGui::TreeNode("Debug bone list...")) {
    auto& bones_to_debug = core->bone_ids_to_debug;
    if (!bones_to_debug.empty()) {
      for (auto it : bones_to_debug) {
        debugBone(it);
      }
    }
    ImGui::TreePop();
  }

  
  ImGui::Text("");


}

void TCompSkeleton::debugBone(int id)
{
  CalBone* cal_bone = model->getSkeleton()->getBone(id);
  if (ImGui::TreeNode(cal_bone->getCoreBone()->getName().c_str())) {
    QUAT rot = Cal2DX(cal_bone->getRotationAbsolute());
    VEC3 pos = Cal2DX(cal_bone->getTranslationAbsolute());
    MAT44 mtx;
    mtx = MAT44::CreateFromQuaternion(rot) * MAT44::CreateTranslation(pos);
    VEC3 front = -mtx.Forward();
    VEC3 left = -mtx.Left();
    VEC3 up = mtx.Up();
    VEC3 newUp = -up;

    VEC3 v;

    switch (model->getMixer()->getTypeFront()) {
    case CalMixer::vecT::FRONT:
      v = -mtx.Forward();
      break;
    case CalMixer::vecT::NFRONT:
      v = mtx.Forward();
      break;
    case CalMixer::vecT::LEFT:
      v = -mtx.Left();
      break;
    case CalMixer::vecT::NLEFT:
      v = mtx.Left();
      break;
    case CalMixer::vecT::UP:
      v = mtx.Up();
      break;
    case CalMixer::vecT::NUP:
      v = -mtx.Up();
      break;
    }

    ImGui::DragFloat3("Pos", &pos.x, 0.025f, -200.f, 200.f);

    float yaw, pitch, roll = 0.f;
    ///////////////////////////////////////
    //getEulerAngles(&yaw, &pitch, &roll);
    yaw = atan2f(v.x, v.z);
    float mdo = sqrtf(v.x * v.x + v.z * v.z);
    pitch = atan2f(-v.y, mdo);
    //////////////////////////////////////////

    float yaw_deg = rad2deg(yaw);
    if (ImGui::DragFloat("Yaw", &yaw_deg, 0.1f, -180.0f, 180.0f)) {
      yaw = deg2rad(yaw_deg);
    }

    float pitch_deg = rad2deg(pitch);
    float max_pitch = 90.0f - 1e-3f;
    if (ImGui::DragFloat("Pitch", &pitch_deg, 0.1f, -max_pitch, max_pitch)) {
      pitch = deg2rad(pitch_deg);
    }

    float roll_deg = rad2deg(roll);
    if (ImGui::DragFloat("Roll", &roll_deg, 0.1f, -180.0f, 180.0f)) {
      roll = deg2rad(roll_deg);
    }


    if (ImGui::TreeNode("Axis...")) {
      ImGui::LabelText("Left", "%f %f %f", left.x, left.y, left.z);
      ImGui::LabelText("Up", "%f %f %f", up.x, up.y, up.z);
      ImGui::LabelText("Front", "%f %f %f", front.x, front.y, front.z);

      MAT44 mtx = MAT44::CreateFromQuaternion(rot) * MAT44::CreateTranslation(pos);
      MAT44 inv = mtx.Invert();
      for (int i = 0; i < 4; ++i)
        ImGui::LabelText("Mtx", "mtxRow[%d] = %f %f %f %f", i, mtx(i, 0), mtx(i, 1), mtx(i, 2), mtx(i, 3));
      ImGui::Separator();
      for (int i = 0; i < 4; ++i)
        ImGui::LabelText("Inv", "invRow[%d] = %f %f %f %f", i, inv(i, 0), inv(i, 1), inv(i, 2), inv(i, 3));
      ImGui::Separator();
      float s = 1.0f;
      MAT44 recInv = MAT44::Identity;
      float tx = -left.Dot(pos);
      float ty = -up.Dot(pos);
      float tz = -front.Dot(pos);
      recInv(0, 0) = left.x * s;
      recInv(1, 0) = left.y * s;
      recInv(2, 0) = left.z * s;
      recInv(3, 0) = tx * s;
      recInv(0, 1) = up.x * s;
      recInv(1, 1) = up.y * s;
      recInv(2, 1) = up.z * s;
      recInv(3, 1) = ty * s;
      recInv(0, 2) = front.x * s;
      recInv(1, 2) = front.y * s;
      recInv(2, 2) = front.z * s;
      recInv(3, 2) = tz * s;
      for (int i = 0; i < 4; ++i)
        ImGui::LabelText("RecInv", "recRow[%d] = %f %f %f %f", i, recInv(i, 0), recInv(i, 1), recInv(i, 2), recInv(i, 3));
      ImGui::TreePop();
    }
    ImGui::TreePop();
  }
 
}

void TCompSkeleton::onEntityCreated() {
  PROFILE_FUNCTION("onEntityCreated");
  bool is_ok = cb_bones.create(CB_SLOT_SKIN_BONES, "Bones");
  assert(is_ok);
  initialize();
}

void TCompSkeleton::renderDebug() {
  assert(model);

  VEC3 lines[MAX_SUPPORTED_BONES][2];
  int nrLines = model->getSkeleton()->getBoneLines(&lines[0][0].x);
  TCompTransform* transform = get<TCompTransform>();
  float scale = transform->getScale();
  for (int currLine = 0; currLine < nrLines; currLine++)
    drawLine(lines[currLine][0] * scale, lines[currLine][1] * scale, Color::White);


  // Show list of bones
  auto mesh = Resources.get("axis.mesh")->as<CMesh>();
  auto core = (CGameCoreSkeleton*)model->getCoreModel();
  auto& bones_to_debug = core->bone_ids_to_debug;
  for (auto it : bones_to_debug) {
    CalBone* cal_bone = model->getSkeleton()->getBone(it);
    QUAT rot = Cal2DX(cal_bone->getRotationAbsolute());
    VEC3 pos = Cal2DX(cal_bone->getTranslationAbsolute());
    MAT44 mtx;
    mtx = MAT44::CreateFromQuaternion(rot) * MAT44::CreateTranslation(pos);
    VEC3 front = -mtx.Forward();
    VEC3 left = -mtx.Left();
    VEC3 up = mtx.Up();
    drawMesh(mesh, mtx, Color::White);

    /*drawLine(pos, pos+front * 5, Color::Red);
    drawLine(pos, pos+left * 5, Color::Green);
    drawLine(pos, pos+up * 5, Color::Blue);*/
  }
}

void TCompSkeleton::updateCtesBones() {
  PROFILE_FUNCTION("updateCtesBones");

  // Pointer to the first float of the array of matrices
   //float* fout = &cb_bones.Bones[0]._11;
  float* fout = &cb_bones.Bones[0]._11;

  CalSkeleton* skel = model->getSkeleton();
  auto& cal_bones = skel->getVectorBone();
  assert(cal_bones.size() < MAX_SUPPORTED_BONES);

  // For each bone from the cal model
  for (size_t bone_idx = 0; bone_idx < cal_bones.size(); ++bone_idx) {
    CalBone* bone = cal_bones[bone_idx];

    const CalMatrix& cal_mtx = bone->getTransformMatrix();
    const CalVector& cal_pos = bone->getTranslationBoneSpace();

    *fout++ = cal_mtx.dxdx;
    *fout++ = cal_mtx.dydx;
    *fout++ = cal_mtx.dzdx;
    *fout++ = 0.f;
    *fout++ = cal_mtx.dxdy;
    *fout++ = cal_mtx.dydy;
    *fout++ = cal_mtx.dzdy;
    *fout++ = 0.f;
    *fout++ = cal_mtx.dxdz;
    *fout++ = cal_mtx.dydz;
    *fout++ = cal_mtx.dzdz;
    *fout++ = 0.f;
    *fout++ = cal_pos.x;
    *fout++ = cal_pos.y;
    *fout++ = cal_pos.z;
    *fout++ = 1.f;
  }

  // Send CPU data to the GPU
  {
    PROFILE_FUNCTION("updateGPU");
    cb_bones.updateGPU();
  }
}

CalModel* TCompSkeleton::getModel()
{
	return model;
}

VEC3 TCompSkeleton::getDeltaMove()
{
    VEC3 new_deltaMove = delta_move;
    delta_move = VEC3::Zero;
    return new_deltaMove;
}

float TCompSkeleton::getDeltaYaw()
{
    float new_deltaYaw = delta_yaw;
    delta_yaw = 0;
    return new_deltaYaw;
}

float TCompSkeleton::getDeltaPitch()
{
  float new_deltaPitch = delta_pitch;
  delta_pitch = 0;
  return new_deltaPitch;
}

float TCompSkeleton::getDeltaRoll()
{
  float new_deltaRoll = delta_roll;
  delta_roll = 0;
  return new_deltaRoll;
}

void TCompSkeleton::setApplyActionYawRotation(bool a)
{
  CalMixer* mixer = model->getMixer();
  mixer->setApplyActionYawRotation(a);
}

void TCompSkeleton::setApplyActionPitchRotation(bool a)
{
  CalMixer* mixer = model->getMixer();
  mixer->setApplyActionPitchRotation(a);
}

void TCompSkeleton::setApplyCycleYawRotation(bool a)
{
  CalMixer* mixer = model->getMixer();
  mixer->setApplyCycleYawRotation(a);
}

void TCompSkeleton::setApplyCyclePitchRotation(bool a)
{
  CalMixer* mixer = model->getMixer();
  mixer->setApplyCyclePitchRotation(a);
}

void TCompSkeleton::setApplyActionMovement(bool a)
{
    CalMixer* mixer = model->getMixer();
    mixer->setApplyActionMovement(a);
}

void TCompSkeleton::setApplyCycleMovement(bool a)
{
    CalMixer* mixer = model->getMixer();
    mixer->setApplyCycleMovement(a);
}

void TCompSkeleton::setUseRootMotion(bool r)
{
  /*CalMixer* mixer = model->getMixer();
  mixer->setUseRootMotion(r);*/
}

bool TCompSkeleton::hasRightOrLeftFootInFloor(bool rightOrLeft, float floorPos, float maxDistance)
{
    if (id_right_foot == -1) return false;
    if (id_left_foot == -1) return false;

    CalBone* right_foot_bone = model->getSkeleton()->getBone(id_right_foot);
    VEC3 right_foot_pos = Cal2DX(right_foot_bone->getTranslationAbsolute());

    CalBone* left_foot_bone = model->getSkeleton()->getBone(id_left_foot);
    VEC3 left_foot_pos = Cal2DX(left_foot_bone->getTranslationAbsolute());

    //SI ES TRUE SIGNIFICA QUE ES EL PIE IZQUIERDO EL QUE QUEREMOS COMPROBAR
    if (rightOrLeft) {
        //Comprobamos si el pie izquierdo esta cerca del suelo
        float distLeft = abs(floorPos - left_foot_pos.y);
        bool left_foot_inFloor = distLeft <= maxDistance;
        //Si lo estuviera comprobamos tambien si la distancia al suelo es menor que la distancia del otro pie al suelo
        if (left_foot_inFloor) {
            float distRight = abs(floorPos - right_foot_pos.y);
            return distLeft < distRight;
        }
        else {
            return false;
        }
    }
    //SINO ES EL PIE DERECHO
    else {
        //Comprobamos si el pie derecho esta cerca del suelo
        float distRight = abs(floorPos - right_foot_pos.y);
        bool right_foot_inFloor = distRight <= maxDistance;

        //Si lo estuviera comprobamos tambien si la distancia al suelo es menor que la distancia del otro pie al suelo
        if (right_foot_inFloor) {
            float distLeft = abs(floorPos - left_foot_pos.y);
            return distRight < distLeft;
        }
        else {
            return false;
        }
    }
}

int TCompSkeleton::getLegForward()
{

    int forwardLeg = -1; // Si devuelve 0 es la derecha, si devuelve 1 es la izquierda

    if (id_right_foot == -1) {
        auto core = (CGameCoreSkeleton*)model->getCoreModel();
        auto bones = core->getCoreSkeleton()->getVectorCoreBone();
        for (int bone_id = 0; bone_id < bones.size(); ++bone_id) {
            string name = bones[bone_id]->getName();
            if (right_foot_name == name) {
                id_right_foot = bone_id;
                break;
            }
        }
    }

    if (id_left_foot == -1) {
        auto core = (CGameCoreSkeleton*)model->getCoreModel();
        auto bones = core->getCoreSkeleton()->getVectorCoreBone();
        for (int bone_id = 0; bone_id < bones.size(); ++bone_id) {
            string name = bones[bone_id]->getName();
            if (left_foot_name == name) {
                id_left_foot = bone_id;
                break;
            }
        }
    }

    VEC3 pos = getAbsTraslationBone(0);
    QUAT rot = getAbsRotationBone(0);
    MAT44 mtx = MAT44::CreateFromQuaternion(rot) * MAT44::CreateTranslation(pos);

    VEC3 front = returnVector(getTypeFront(), mtx);
    VEC3 posInFront = pos + front * 3;

    CalBone* left_foot_bone = model->getSkeleton()->getBone(id_left_foot);
    VEC3 left_foot_pos = Cal2DX(left_foot_bone->getTranslationAbsolute());
    float distLeft = VEC3::DistanceSquared(posInFront, left_foot_pos);

    CalBone* right_foot_bone = model->getSkeleton()->getBone(id_right_foot);
    VEC3 right_foot_pos = Cal2DX(right_foot_bone->getTranslationAbsolute());
    float distRight = VEC3::DistanceSquared(posInFront, right_foot_pos);

    if (distRight < distLeft) {
        return 0;
    }
    else {
        return 1;
    }

}

VEC3 TCompSkeleton::getPosBoneHead()
{
    return pos_bone_head;
}

VEC3 TCompSkeleton::getFrontBoneHead()
{
  //Ya que el hueso de la cabeza esta rotado de manera diferente, se devuelve UP en vez de front
    return mat_bone_head.Up();
}

VEC3 TCompSkeleton::getLeftBoneHead()
{
  return -mat_bone_head.Forward();
}

VEC3 TCompSkeleton::getUpBoneHead()
{
  return -mat_bone_head.Left();
}

int TCompSkeleton::getIdBone(string nameBone)
{
    auto core = (CGameCoreSkeleton*)model->getCoreModel();
    auto bones = core->getCoreSkeleton()->getVectorCoreBone();
    for (int bone_id = 0; bone_id < bones.size(); ++bone_id) {
        string name = bones[bone_id]->getName();
        if (nameBone == name) {
            return bone_id;
        }
    }
    return 0;
}

QUAT TCompSkeleton::getAbsRotationBone(int id)
{
  CalBone* cal_bone = model->getSkeleton()->getBone(id);

  return Cal2DX(cal_bone->getRotationAbsolute());
}

VEC3 TCompSkeleton::getAbsTraslationBone(int id)
{
  CalBone* cal_bone = model->getSkeleton()->getBone(id);
  return Cal2DX(cal_bone->getTranslationAbsolute());
}

CalMixer::vecT TCompSkeleton::getTypeFront()
{
  return model->getMixer()->getTypeFront();
}

CalMixer::vecT TCompSkeleton::getTypeLeft()
{
  return model->getMixer()->getTypeLeft();
}

CalMixer::vecT TCompSkeleton::getTypeUp()
{
  return model->getMixer()->getTypeUp();
}

VEC3 TCompSkeleton::returnVector(CalMixer::vecT type, MAT44 mtx)
{
    switch (type) {
    case CalMixer::vecT::FRONT:
        return -mtx.Forward();
    case CalMixer::vecT::NFRONT:
        return mtx.Forward();
    case CalMixer::vecT::LEFT:
        return -mtx.Left();
    case CalMixer::vecT::NLEFT:
        return mtx.Left();
    case CalMixer::vecT::UP:
        return mtx.Up();
    case CalMixer::vecT::NUP:
        return -mtx.Up();
    }
}

void TCompSkeleton::TAutoRotator::applyYawRotation(float total_angle, float new_total_time)
{
    total_time = new_total_time;
    yaw_rotation_speed = total_angle / total_time;
    curr_time = 0.f;
    is_enabled = true;
}

void TCompSkeleton::TAutoRotator::applyPitchRotation(float total_angle, float new_total_time)
{
    total_time = new_total_time;
    pitch_rotation_speed = total_angle / total_time;
    curr_time = 0.f;
    is_enabled = true;
}

void TCompSkeleton::TAutoRotator::update(TCompTransform* ct, float dt)
{
    if (!is_enabled) return;

    if (curr_time + dt > total_time) {
        dt = total_time - curr_time;
        assert(dt >= 0);
        is_enabled = false;
    }
    else curr_time += dt;

    if (rot_yaw) {
        float angle_rotated = dt * yaw_rotation_speed;
        QUAT q = ct->getRotation();
        QUAT dq = QUAT::CreateFromAxisAngle(VEC3(0, 1, 0), angle_rotated);
        q = q * dq;
        ct->setRotation(q);
    }

    if (rot_pitch) {
        float angle_rotated = dt * pitch_rotation_speed;
        QUAT q = ct->getRotation();
        QUAT dq = QUAT::CreateFromAxisAngle(VEC3(1, 0, 0), angle_rotated);
        q = q * dq;
        ct->setRotation(q);
    }


}

void TCompSkeleton::TAutoRotator::reset()
{
    is_enabled = false;
    total_time = 0.f;
    curr_time = 0.f;
    yaw_rotation_speed = 0.f;
    pitch_rotation_speed = 0.f;
    rot_yaw = false;
    rot_pitch = false;

}

void TCompSkeleton::TSaveAnimMovs::insert(VEC3 mov, float y, float p, float r)
{
    TMov newMov;
    frame = frame + 1;

    newMov.frame = frame;
    newMov.despl = mov;
    newMov.yaw = y;
    newMov.pitch = p;
    newMov.roll = r;

    dbg("TSaveAnimMovs Frame %d -> Movimiento [ %.3f %.3f %.3f ] Yaw: %.2f Pitch: %.2f Roll: %.2f\n", newMov.frame, newMov.despl.x, newMov.despl.y, newMov.despl.z, newMov.yaw, newMov.pitch, newMov.roll);

    sum_mov += mov;
    sum_yaw += y;
    sum_pitch += p;
    sum_roll += r;

    ani_movs.push_back(newMov);
}

void TCompSkeleton::TSaveAnimMovs::printAll()
{
    dbg("== DEBUG DE ANIMACIONES ==\n");

    dbg("Animacion: %s\n", name_ani.c_str());
    for (auto a : ani_movs) {
        dbg("Frame %d -> Movimiento [ %.3f %.3f %.3f ] Yaw: %.2f Pitch: %.2f Roll: %.2f\n", a.frame, a.despl.x, a.despl.y, a.despl.z, a.yaw, a.pitch, a.roll);
    }
    dbg("Suma -> Movimiento [ %.3f %.3f %.3f ] Yaw: %.2f Pitch: %.2f Roll: %.2f\n", sum_mov.x, sum_mov.y, sum_mov.z, sum_yaw, sum_pitch, sum_roll);

    dbg("==========================\n");

}

void TCompSkeleton::TSaveAnimMovs::init(string name, float yaw)
{
    frame = 0;
    name_ani = name;
    yaw_to_Point = yaw;
    ani_movs.clear();
    sum_mov = VEC3::Zero;
    sum_yaw = 0;
    sum_pitch = 0;
    sum_roll = 0;
}

void TCompSkeleton::insertMovBone()
{
    CalBone* cal_bone = model->getSkeleton()->getBone(debug_root_movs.id_bone);

    QUAT rot = Cal2DX(cal_bone->getRotationAbsolute());
    VEC3 pos = Cal2DX(cal_bone->getTranslationAbsolute());
    MAT44 mtx;
    mtx = MAT44::CreateFromQuaternion(rot) * MAT44::CreateTranslation(pos);

    VEC3 v = returnVector(model->getMixer()->getTypeFront(), mtx);

    float yaw, pitch, roll = 0.f;

    yaw = atan2f(v.x, v.z);
    float mdo = sqrtf(v.x * v.x + v.z * v.z);
    pitch = atan2f(-v.y, mdo);

    debug_root_movs.insert(pos, yaw, pitch, 0.f);

}

void TCompSkeleton::clearCorrectRoot()
{
    add_root_move = VEC3::Zero;
    add_root_rot = QUAT(0, 0, 0, -1);
}

void TCompSkeleton::TSaveRootMov::insert(VEC3 pos, float y, float p, float r)
{
    TMov newMov;

    frame = frame + 1;

    newMov.frame = frame;
    newMov.despl = pos - last_mov.despl;
    newMov.yaw = y - last_mov.yaw;
    newMov.pitch = p - last_mov.pitch;
    newMov.roll = 0;

    dbg("TSaveRootMov Frame %d -> Movimiento [ %.2f %.2f %.2f ] Yaw: %.2f Pitch: %.2f Roll: %.2f\n", newMov.frame, newMov.despl.x, newMov.despl.y, newMov.despl.z, newMov.yaw, newMov.pitch, newMov.roll);

    sum_mov += newMov.despl;
    sum_yaw += newMov.yaw;
    sum_pitch += newMov.pitch;
    sum_roll += 0;

    root_movs.push_back(newMov);

    last_mov.despl = pos;
    last_mov.yaw = y;
    last_mov.pitch = p;
    last_mov.roll = 0;

}

void TCompSkeleton::TSaveRootMov::printAll()
{
    dbg("== DEBUG DE MOVIMIENTOS DEL ROOT ==\n");

    dbg("Animacion: %s\n", name_ani.c_str());
    for (auto a : root_movs) {
        dbg("Frame %d -> Movimiento [ %.2f %.2f %.2f ] Yaw: %.2f Pitch: %.2f Roll: %.2f\n", a.frame, a.despl.x, a.despl.y, a.despl.z, a.yaw, a.pitch, a.roll);
    }
    dbg("Suma -> Movimiento [ %.2f %.2f %.2f ] Yaw: %.2f Pitch: %.2f Roll: %.2f\n", sum_mov.x, sum_mov.y, sum_mov.z, sum_yaw, sum_pitch, sum_roll);

    dbg("==========================\n");


}

void TCompSkeleton::TSaveRootMov::init(string name, int id, VEC3 pos, float y, float p, float r)
{
    int id_bone = id;
    string name_ani = name;

    root_movs.clear();

    frame = 0;
    sum_mov = VEC3::Zero;
    sum_yaw = 0;
    sum_pitch = 0;
    sum_roll = 0;

    last_mov.despl = pos;
    last_mov.yaw = y;
    last_mov.pitch = p;
    last_mov.roll = 0;


}

void TCompSkeleton::TMov::clear()
{
    frame = 0;
    despl = VEC3::Zero;
    yaw = 0;
    pitch = 0;
    roll = 0;
}
