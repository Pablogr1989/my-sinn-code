#include "mcv_platform.h"
#include "animator.h"
#include "windows/application.h"

DECL_OBJ_MANAGER("animator", TCompAnimator);

TVariableValue TCompAnimator::DefaultValue(string type)
{
    TVariableValue value;
    if (type == "bool")
    {
        value = false;
    }
    else if (type == "int")
    {
        value = 0;
    }
    else if (type == "float")
    {
        value = 0.f;
    }
    else
    {
        value = false;
    }

    return value;

}

BlendTransition TCompAnimator::createBlendTransition(string fa, string ba, string faVal, string baVal, string compVar, string type)
{
    BlendTransition new_blend;
    new_blend.backAnimation = ba;
    new_blend.backValue = baVal;
    new_blend.frontAnimation = fa;
    new_blend.frontValue = faVal;
    new_blend.compareVariable = compVar;
    new_blend.typeVar = type;

    return new_blend;
}


void TCompAnimator::renderVariable(string name, TVariableValue& _value)
{
    if (std::holds_alternative<bool>(_value))
    {
        bool& value = std::get<bool>(_value);
        ImGui::Checkbox(name.c_str(), &value);
    }
    else if (std::holds_alternative<int>(_value))
    {
        int& value = std::get<int>(_value);
        ImGui::DragInt(name.c_str(), &value, 1, 0, 100);
    }
    else if (std::holds_alternative<float>(_value))
    {
        float& value = std::get<float>(_value);
        ImGui::DragFloat(name.c_str(), &value, 0.01f, 0.01, 10.0f);
    }
    else if (std::holds_alternative<std::string>(_value))
    {
        std::string& value = std::get<std::string>(_value);
        static char buffer[64];
        snprintf(buffer, 63, value.c_str());
        if (ImGui::InputText(name.c_str(), buffer, 63))
        {
            value = buffer;
        }
    }
}

void TCompAnimator::blendAnimations()
{

    //Si no hay dos animaciones no blendeamos nada
    if (numAnimCycles != 2)  return;

    string name_blend = front_cycle.name + "_to_" + back_cycle.name;

    //Si no existe la transicion en el map de transiciones que blendear volvemos
    if (bTransitions.find(name_blend) == bTransitions.end())   return;

    auto bl = bTransitions.find(name_blend);
    float w = 0;

    if (bl->second.typeVar == "bool")
    {
        w = calculateBlendBool(bl->second);
    }
    else if (bl->second.typeVar == "int")
    {
        w = calculateBlendInt(bl->second);
    }
    else if (bl->second.typeVar == "float")
    {
        w = calculateBlendFloat(bl->second);
    }

    TCompSkeleton* skel = get<TCompSkeleton>();
    CalModel* model = skel->getModel();
    CalMixer* mixer = model->getMixer();

    mixer->blendCycle(front_cycle.id, w, front_cycle.in_delay);
    mixer->blendCycle(back_cycle.id, 1-w, back_cycle.in_delay);

}

float TCompAnimator::calculateBlendFloat(BlendTransition b)
{

    float val1 = std::get<float>( getValue(b.frontValue));
    float val2 = std::get<float>(getValue(b.backValue));
    float var = std::get<float>(getValue(b.compareVariable));
    float w = 0.01;

    w = (var - val2) / (val1 - val2);

    if (w < 0)  return 0.01;
    if (w > 1)  return 0.99;

    return w;
}

float TCompAnimator::calculateBlendInt(BlendTransition b)
{
    int val1 = std::get<int>(getValue(b.frontValue));
    int val2 = std::get<int>(getValue(b.backValue));
    int var = std::get<int>(getValue(b.compareVariable));
    float w = 0.01;

    w = (var - val2) / (val1 - val2);

    return w;
}

float TCompAnimator::calculateBlendBool(BlendTransition b)
{

    bool val1 = std::get<bool>(getValue(b.frontValue));
    bool var = std::get<bool>(getValue(b.compareVariable));

    if (val1 == var) return 0.99;
    else return 0.01;

}

TVariableValue TCompAnimator::getValue(string varName)
{
    return list_variables.find(varName)->second;
}

void TCompAnimator::updateBucleActions()
{

    TCompSkeleton* skel = get<TCompSkeleton>();
    CalModel* model = skel->getModel();
    CalMixer* mixer = model->getMixer();

    for (auto animName : bucle_anim) {
        if (isActionAnimStop(animName)) {
            AnimationStruct anim = getAnim(animName);
            mixer->executeAction(anim.id, anim.in_delay, anim.out_delay, 1.0f, false);
        }
    }
}

void TCompAnimator::updateListAnimations()
{

    if (list_animations.size() > 0) {
        TCompSkeleton* skel = get<TCompSkeleton>();
        CalModel* model = skel->getModel();
        CalMixer* mixer = model->getMixer();

        int i = 0;
        for (auto& a : list_animations) {
            //Si esta activo comprobamos si hay que quitarlo ya
            if (a.active) {
                if (a.end_time < timer_list_animations) {
                    if (a.anim.type == "action") {
                        if (printLog) dbg("Voy a terminar la accion %s\n", a.anim.name.c_str());
                        mixer->removeAction(a.anim.id, a.anim.out_delay);
                        list_animations.erase(list_animations.begin() + i);
                    }
                    else if(a.anim.type == "cycle") {
                        if (printLog) dbg("Voy a terminar el ciclo %s\n", a.anim.name.c_str());
                        mixer->clearCycle(a.anim.id, a.anim.out_delay);
                        list_animations.erase(list_animations.begin() + i);
                    }
                    
                }
            }
            //Sino, comprobamos si hay que acivarlo
            else {
                if (timer_list_animations >= a.begin_time) {
                    a.active = true;
                    if (a.anim.type == "action") {
                        if(printLog) dbg("Voy a poner la accion %s\n", a.anim.name.c_str());
                        mixer->executeAction(a.anim.id, a.anim.in_delay, a.anim.out_delay, 1, false);
                    }
                    else if (a.anim.type == "cycle") {
                        if (printLog) dbg("Voy a poner el ciclo %s\n", a.anim.name.c_str());
                        mixer->blendCycle(a.anim.id, 1, a.anim.in_delay);
                    }
                }
            }
            i++;
        }
    }

}

void TCompAnimator::debugInMenu()
{

    ImGui::Checkbox("Print All Anims", &printAllAnims);
    ImGui::Checkbox("Print log", &printLog);

    TCompSkeleton* skel = get<TCompSkeleton>();
    CalModel* model = skel->getModel();
    CalMixer* mixer = model->getMixer();
    static int anim_id1 = 0;
    static int anim_id2 = 0;
    static float time = 0;

    //ImGui::Text("Frame animation %d", frames);
    //ImGui::Text("Frame Start Action %d", start_frameAction);

    ImGui::Text("Numero de animaciones ciclo : %d", numAnimCycles);

    if (ImGui::TreeNode("Animations...")) {
        for (auto& [name, value] : list_variables)
        {
            renderVariable(name, value);
        }

        ImGui::DragInt("Anim1 Id", &anim_id1, 0.1f, 0, model->getCoreModel()->getCoreAnimationCount() - 1);
        auto core_anim1 = model->getCoreModel()->getCoreAnimation(anim_id1);
        if (core_anim1)
            ImGui::Text("%s", core_anim1->getName().c_str());

        ImGui::DragInt("Anim2 Id", &anim_id2, 0.1f, 0, model->getCoreModel()->getCoreAnimationCount() - 1);
        auto core_anim2 = model->getCoreModel()->getCoreAnimation(anim_id2);
        if (core_anim2)
            ImGui::Text("%s", core_anim2->getName().c_str());

        ImGui::DragFloat("Tiempo", &time, 0.05f, 0.1, 10);

        if (core_anim1 && core_anim2) {
            if (ImGui::SmallButton("Blend")) {
                manualBlendAnimations(core_anim1->getName().c_str(), core_anim2->getName().c_str(), time);
            }
        }


        ImGui::Text("Mixer Time: %f/%f", mixer->getAnimationTime(), mixer->getAnimationDuration());
        for (auto a : mixer->getAnimationCycle()) {
            ImGui::PushID(a);
            ImGui::Text("Cycle %s S:%d W:%1.4f Time:%1.4f"
                , a->getCoreAnimation()->getName().c_str()
                , a->getState()
                , a->getWeight()
                , a->getCoreAnimation()->getDuration()
            );
            ImGui::SameLine();
            if (ImGui::SmallButton("X")) {
                auto core = (CGameCoreSkeleton*)model->getCoreModel();
                int id = core->getCoreAnimationId(a->getCoreAnimation()->getName());
                mixer->clearCycle(id, 0);
            }
            ImGui::PopID();
        }

        if (mixer->getAnimationCycle().size() == 2) {
            static bool unit_weight = false;
            ImGui::Checkbox("Unit Weight", &unit_weight);
            if (unit_weight) {
                static float w1 = 0.5f;
                auto anim1 = mixer->getAnimationCycle().front();
                auto anim2 = mixer->getAnimationCycle().back();
                float wsmall = 1e-3f;
                if (ImGui::DragFloat("Weight", &w1, 0.005f, wsmall, 1.0f - wsmall)) {
                    int anim1_id = model->getCoreModel()->getCoreAnimationId(anim1->getCoreAnimation()->getName());
                    int anim2_id = model->getCoreModel()->getCoreAnimationId(anim2->getCoreAnimation()->getName());
                    model->getMixer()->blendCycle(anim1_id, w1, 0.1f);
                    model->getMixer()->blendCycle(anim2_id, 1.0f - w1, 0.1f);
                }
            }
        }
        ImGui::TreePop();
    }


    static int anim_id = 0;

    ImGui::Text("Timer Animation List : %.2f", timer_list_animations);

    if (ImGui::TreeNode("Animation Items")) {
        ImGui::Text("Insertar animacion...");

        ImGui::DragInt("Animation", &anim_id, 0.1f, 0, model->getCoreModel()->getCoreAnimationCount() - 1);
        auto core_anim = model->getCoreModel()->getCoreAnimation(anim_id);
        string ani_name = "";
        if (core_anim) {
            ImGui::Text("%s", core_anim->getName().c_str());
            ani_name = core_anim->getName().c_str();
        }
            

        static ImGuiComboFlags flags = 0;
        const char* types[] = { "cycle", "action" };
        static const char* current_type = types[0];            
        if (ImGui::BeginCombo("combo animation list", current_type, flags))
        {
            for (int n = 0; n < IM_ARRAYSIZE(types); n++)
            {
                bool is_selected = (current_type == types[n]);
                if (ImGui::Selectable(types[n], is_selected))
                    current_type = types[n];
                if (is_selected)
                    ImGui::SetItemDefaultFocus();
            }
            ImGui::EndCombo();
        }


        static float begin = 0;
        static float end = 0;
        string type = current_type;        
        static float inDelay = 0;
        static float outDelay = 0;

        ImGui::DragFloat("Begin time", &begin, 0.1, 0, 1000);
        ImGui::DragFloat("End time", &end, 0.1, 0, 1000);
        ImGui::DragFloat("in Delay", &inDelay, 0.1, 0, 1000);
        ImGui::DragFloat("out Delay", &outDelay, 0.1, 0, 1000);

        if (ImGui::SmallButton("Insertar")) {
            insertAnimInList(begin, end, ani_name, type, inDelay, outDelay);
            begin = 0;
            end = 0;
            inDelay = 0;
            outDelay = 0;
            current_type = types[0];
        }

        if (list_animations.size() > 0) {
            for (auto a : list_animations) {
                ImGui::PushID(a.anim.id);
                ImGui::Text("Animation name %s", a.anim.name.c_str());
                ImGui::Text("Begin Time : %.2f", a.begin_time);
                ImGui::Text("End Time : %.2f", a.end_time);
                ImGui::Text("In delay : %.2f", a.anim.in_delay);
                ImGui::Text("Out delay : %.2f", a.anim.out_delay);
                ImGui::Text("Type : %s", a.anim.type.c_str());
                ImGui::Checkbox("Active", &a.active);
            }
        }

        ImGui::TreePop();
    }


}

void TCompAnimator::load(const json& j, TEntityParseContext& ctx)
{

    if (!j.contains("blendTransitions")) return;

    for (const auto& jTransition : j["blendTransitions"])
    {
        string type_values = jTransition.value("type_values", "bool");

        string anim1 = jTransition.value("anim1", "");

        string anim1_value_name = jTransition.value("anim1_value", "");
        TVariableValue anim1_value = DefaultValue(type_values);
        if (list_variables.find("anim1_value_name") == list_variables.end()) {
            list_variables[anim1_value_name] = anim1_value;
        }

        string anim2 = jTransition.value("anim2", "");
        string anim2_value_name = jTransition.value("anim2_value", "");
        TVariableValue anim2_value = DefaultValue(type_values);
        if (list_variables.find("anim1_value_name") == list_variables.end()) {
            list_variables[anim2_value_name] = anim2_value;
        }

        string compare_variable = jTransition.value("compare_variable", "");
        TVariableValue compare_value = DefaultValue(type_values);
        if (list_variables.find("anim1_value_name") == list_variables.end()) {
            list_variables[compare_variable] = compare_value;
        }
        

        //Creamos dos transiciones, una para cada direccion, por si estamos en el estado de la animacion 1 o en el estado de la animacion 2 que sigamos blendeando               
        bTransitions[anim1+"_to_"+anim2] = createBlendTransition(anim1, anim2, anim1_value_name, anim2_value_name, compare_variable, type_values);
        bTransitions[anim2+"_to_"+anim1] = createBlendTransition(anim2, anim1, anim2_value_name, anim1_value_name, compare_variable, type_values);
    }
}

void TCompAnimator::update(float dt)
{

    if (!initialized) {
        TCompSkeleton* skel = get<TCompSkeleton>();
        CalModel* model = skel->getModel();
        CalMixer* mixer = model->getMixer();

        if (mixer->getAnimationCycle().size() > 0) {
            auto animBack = mixer->getAnimationCycle().back();
            AnimationStruct anim = getAnim(animBack->getCoreAnimation()->getName());
            back_cycle = anim;
            numAnimCycles = 1;
        }
        initialized = true;
    }




    blendAnimations();
    if (bucle_anim.size() > 0)   updateBucleActions();

    if (manualBlend) {
      TCompSkeleton* skel = get<TCompSkeleton>();
      CalModel* model = skel->getModel();
      CalMixer* mixer = model->getMixer();
      laspsedTime += dt;
      if (laspsedTime <= blendTime) {
        float w = laspsedTime / blendTime;

        mixer->blendCycle(back_cycle.id, 1-w, 0);
        mixer->blendCycle(front_cycle.id, w, 0);
      }
      else {
        mixer->clearCycle(back_cycle.id, back_cycle.out_delay);
        manualBlend = false;
      }
    }

    timer_list_animations += dt;
    updateListAnimations();

    if (printAllAnims) {
        TCompSkeleton* skel = get<TCompSkeleton>();
        CalModel* model = skel->getModel();
        CalMixer* mixer = model->getMixer();
        for (auto a : mixer->getAnimationActionList()) {
            string state = "";                
            switch (a->getState())
            {
            case 0:
                state = "STATE_NONE";
                break;
            case 1:
                state = "STATE_SYNC";
                break;
            case 2:
                state = "STATE_ASYNC";
                break;
            case 3:
                state = "STATE_IN";
                break;
            case 4:
                state = "STATE_STEADY";
                break;
            case 5:
                state = "STATE_OUT";
                break;
            case 6:
                state = "STATE_STOPPED";
                break;
            }
            dbg("Action %s S:%s W:%1.2f Time:%1.4f/%1.4f\n"
                , a->getCoreAnimation()->getName().c_str()
                , state.c_str()
                , a->getWeight()
                , a->getTime()
                , a->getCoreAnimation()->getDuration()
            );
        }

        for (auto a : mixer->getAnimationCycle()) {
            string state = "";
            switch (a->getState())
            {
            case 0:
                state = "STATE_NONE";
                break;
            case 1:
                state = "STATE_SYNC";
                break;
            case 2:
                state = "STATE_ASYNC";
                break;
            case 3:
                state = "STATE_IN";
                break;
            case 4:
                state = "STATE_STEADY";
                break;
            case 5:
                state = "STATE_OUT";
                break;
            case 6:
                state = "STATE_STOPPED";
                break;
            }
            dbg("Cycle %s S:%s W:%1.4f Time:%1.4f\n"
                , a->getCoreAnimation()->getName().c_str()
                , state.c_str()
                , a->getWeight()
                , a->getCoreAnimation()->getDuration()
            );
        }
    }
}

AnimationStruct TCompAnimator::getAnim(string anim_name)
{
    TCompSkeleton* skel = get<TCompSkeleton>();
    CGameCoreSkeleton* ck = (CGameCoreSkeleton*) skel->getModel()->getCoreModel();

    auto it = ck->animations_list.find(anim_name);
    return it != ck->animations_list.cend() ? it->second : empty;
}

void TCompAnimator::playActionAnim(string anim_name, bool lock, bool with_yaw, bool with_pitch)
{
    AnimationStruct anim = getAnim(anim_name);
    
    int tframe = CApplication::get().getFrameCount();

    if (anim.id >= 0) {
        if (printLog) dbg("Frame %d -> Play animation %s con lock %d, yaw %d y pitch %d\n", tframe, anim_name.c_str(), lock, with_yaw, with_pitch);

      TCompSkeleton* skel = get<TCompSkeleton>();
      CalModel* model = skel->getModel();
      CalMixer* mixer = model->getMixer();

      //Si hay alguna otra accion la eliminamos
      if (mixer->getAnimationActionList().size() > 0) {
          auto animBack = mixer->getAnimationActionList().back();
          int animback_id = model->getCoreModel()->getCoreAnimationId(animBack->getCoreAnimation()->getName());
          mixer->removeAction(animback_id, 0.3);
      }   

      //Por ultimo ejecutamos la accion
      mixer->setApplyActionYawRotation(with_yaw);
      mixer->setRemoveActionYawRotation(with_yaw);
      mixer->setApplyActionPitchRotation(with_pitch);
      mixer->setRemoveActionPitchRotation(with_pitch);
      mixer->executeAction(anim.id, anim.in_delay, anim.out_delay, 1.0f, lock);
    }
}

void TCompAnimator::playActionAnim(string anim_name, bool lock, bool with_yaw, bool with_pitch, bool with_move)
{
    AnimationStruct anim = getAnim(anim_name);

    int tframe = CApplication::get().getFrameCount();

    dbg("Anim_name %s en frame %d\n", anim_name.c_str(), tframe);

    if (anim.id >= 0) {

        TCompSkeleton* skel = get<TCompSkeleton>();
        CalModel* model = skel->getModel();
        CalMixer* mixer = model->getMixer();


        playActionAnim(anim_name, lock, with_yaw, with_pitch);

        //Por ultimo ejecutamos la accion
        mixer->setApplyActionMovement(with_move);
        mixer->setRemoveActionMovement(with_move);
    }
}

void TCompAnimator::playActionAnim(string anim_name, bool lock, bool with_yaw, bool with_pitch, bool with_roll, bool with_move)
{
    AnimationStruct anim = getAnim(anim_name);

    int tframe = CApplication::get().getFrameCount();

    if (anim.id >= 0) {

        TCompSkeleton* skel = get<TCompSkeleton>();
        CalModel* model = skel->getModel();
        CalMixer* mixer = model->getMixer();


        playActionAnim(anim_name, lock, with_yaw, with_pitch);

        //Por ultimo ejecutamos la accion
        mixer->setApplyActionMovement(with_move);
        mixer->setRemoveActionMovement(with_move);
        mixer->setapplyActionRollRotation(with_roll);
        mixer->setRemoveActionRollRotation(with_roll);
    }
}

void TCompAnimator::playCycleAnim(string anim_name)
{
    AnimationStruct anim = getAnim(anim_name);
    int tframe = CApplication::get().getFrameCount();

    if (anim.id >= 0) {

        TCompSkeleton* skel = get<TCompSkeleton>();
        CalModel* model = skel->getModel();
        CalMixer* mixer = model->getMixer();

        if (!not_remove_action) {
          //Si hay alguna otra accion la eliminamos
          if (mixer->getAnimationActionList().size() > 0) {
            auto animBack = mixer->getAnimationActionList().back();
            int animback_id = model->getCoreModel()->getCoreAnimationId(animBack->getCoreAnimation()->getName());
            mixer->removeAction(animback_id, 0.3);
            float timeAnim = CApplication::get().getTimeStamp();
          }
        }

        if (numAnimCycles == 0) {
            //Si no hay ningun ciclo, lo a�ado y lo convierto en el back (mas antiguo)
            mixer->blendCycle(anim.id, 1, anim.in_delay);
            if (printLog) dbg("Frame %d -> Play animation %s cycle 1\n", tframe, anim.name.c_str());
            back_cycle = anim;
            numAnimCycles++;
            return;
        }

        if (numAnimCycles == 1) {

            //Si la animacion ya esta reproduciendose, no hago nada
            if (back_cycle.name == anim.name)    return;

            //Si ya hay uno, el back lo blendeo a 0.01 y a�ado el nuevo a front
            mixer->blendCycle(anim.id, 0.99, anim.in_delay);
            mixer->blendCycle(back_cycle.id, 0.01, back_cycle.in_delay);
            if (printLog) dbg("Frame %d -> Play animation %s cycle 0.01\n", tframe, back_cycle.name.c_str());
            if (printLog) dbg("Frame %d -> Play animation %s cycle 0.99\n", tframe, anim.name.c_str());
            front_cycle = anim;
            numAnimCycles++;
            return;
        }

        if (numAnimCycles == 2) {

            //Si la animacion que queremos reproducir, ya es la nueva, no hago nada
            if (front_cycle.name == anim.name)   return;

            //Si la animacion que queremos reproducir, no es la vieja, entonces la quitamos
            if (back_cycle.name != anim.name) {
                mixer->clearCycle(back_cycle.id, back_cycle.out_delay);
                if (printLog) dbg("Frame %d -> Stop animation %s cycle\n", tframe, back_cycle.name.c_str());
            }

            //Ahora el front se convierte en el back y a�adimos el nuevo al front            
            mixer->blendCycle(front_cycle.id, 0.01, front_cycle.in_delay);
            mixer->blendCycle(anim.id, 0.99, anim.in_delay);
            if (printLog) dbg("Frame %d -> Play animation %s cycle 0.01\n", tframe, front_cycle.name.c_str());
            if (printLog) dbg("Frame %d -> Play animation %s cycle 0.99\n", tframe, anim.name.c_str());
            back_cycle = front_cycle;
            front_cycle = anim;
            return;
        }        
    }
}

void TCompAnimator::playParcialActionAnim(string anim_name, bool lock)
{

    AnimationStruct anim = getAnim(anim_name);
    //dbg("Me han pedido que ponga %s\n", anim_name.c_str());

    if (anim.id >= 0) {

        TCompSkeleton* skel = get<TCompSkeleton>();
        CalModel* model = skel->getModel();
        CalMixer* mixer = model->getMixer();

        //Por ultimo ejecutamos la accion
        skel->getModel()->getMixer()->executeAction(anim.id, anim.in_delay, anim.out_delay, 1.0f, lock);
    }

}

void TCompAnimator::playParcialActionBucle(string anim_name)
{

    //Si ya esta en el bucle no la volvemos a meter
    for (auto it : bucle_anim) {
      if (it == anim_name) {
        return;
      }
    }

    playParcialActionAnim(anim_name, false);

    bucle_anim.push_back(anim_name);
}

void TCompAnimator::removeAllBucleActions()
{
    bucle_anim.clear();
}

void TCompAnimator::removeBucleAction(string anim_name)
{
    int pos=0;
    for (auto it : bucle_anim) {
        if (it == anim_name) {
            bucle_anim.erase(bucle_anim.begin() + pos);
            break;
        }
        pos++;
    }
}

void TCompAnimator::removeCycleAnim(string anim_name)
{
  TCompSkeleton* skel = get<TCompSkeleton>();
  CalModel* model = skel->getModel();
  CalMixer* mixer = model->getMixer();

  if (front_cycle.name == anim_name) {
    mixer->clearCycle(front_cycle.id, front_cycle.out_delay);
    front_cycle = empty;
    mixer->blendCycle(back_cycle.id, 1, back_cycle.in_delay);
    numAnimCycles--;
  }
  else if (back_cycle.name == anim_name) {
    mixer->clearCycle(back_cycle.id, back_cycle.out_delay);
    if (numAnimCycles == 2) {
      mixer->blendCycle(front_cycle.id, 1, front_cycle.in_delay);
      back_cycle = front_cycle;
      front_cycle = empty;
    }
    else {
      back_cycle = empty;
    }
    numAnimCycles--;
  }

}

void TCompAnimator::removeBackCycle()
{
  TCompSkeleton* skel = get<TCompSkeleton>();
  CalModel* model = skel->getModel();
  CalMixer* mixer = model->getMixer();

  if (back_cycle.id != empty.id) {
    mixer->clearCycle(back_cycle.id, 0.1);
    back_cycle = empty;
    if (numAnimCycles == 2) {
      mixer->blendCycle(front_cycle.id, 1, front_cycle.in_delay);
      back_cycle = front_cycle;
      front_cycle = empty;
    }
    numAnimCycles--;
  }
}

void TCompAnimator::removeFrontCycle()
{
  TCompSkeleton* skel = get<TCompSkeleton>();
  CalModel* model = skel->getModel();
  CalMixer* mixer = model->getMixer();

  if (back_cycle.id != empty.id) {
    mixer->clearCycle(front_cycle.id, 0.1);
    front_cycle = empty;
    mixer->blendCycle(back_cycle.id, 1, back_cycle.in_delay);
    numAnimCycles--;
  }
}

void TCompAnimator::removeAllCycles()
{
  TCompSkeleton* skel = get<TCompSkeleton>();
  CalModel* model = skel->getModel();
  CalMixer* mixer = model->getMixer();

  mixer->clearCycle(front_cycle.id, 0.1);
  mixer->clearCycle(back_cycle.id, 0.1);

  numAnimCycles = 0;

  front_cycle = empty;
  back_cycle = empty;
}

void TCompAnimator::blendToWeightNow(string anim_name, float w)
{

  TCompSkeleton* skel = get<TCompSkeleton>();
  CalModel* model = skel->getModel();
  CalMixer* mixer = model->getMixer();

  if (w < 0 || w > 1) return;

  if (front_cycle.name == anim_name) {
    mixer->blendCycle(front_cycle.id, w, 0);
  }
  else if (back_cycle.name == anim_name) {
    mixer->blendCycle(back_cycle.id, w, 0);
  }

}

void TCompAnimator::manualBlendAnimations(string sanim1, string sanin2, float t)
{
  TCompSkeleton* skel = get<TCompSkeleton>();
  CalModel* model = skel->getModel();
  CalMixer* mixer = model->getMixer();


  removeAllCycles();

  blendTime = t;
  laspsedTime = 0;
  manualBlend = true;

  AnimationStruct anim1 = getAnim(sanim1);
  AnimationStruct anim2 = getAnim(sanin2);
  
  back_cycle = anim1;
  front_cycle = anim2;

  mixer->blendCycle(back_cycle.id, 0.99, back_cycle.in_delay);
  mixer->blendCycle(front_cycle.id, 0.01, front_cycle.in_delay);

  numAnimCycles = 2;

}

void TCompAnimator::setVariable(string name, TVariableValue value)
{
    auto it = list_variables.find(name);

    if (it == list_variables.end())    return;

    it->second = value;
}

bool TCompAnimator::isActionAnimStop(string anim)
{
    TCompSkeleton* skel = get<TCompSkeleton>();
    CalModel* model = skel->getModel();
    CalMixer* mixer = model->getMixer();
    auto core = (CGameCoreSkeleton*)model->getCoreModel();

    if (mixer->getAnimationActionList().size() == 0) {
      return true;
    }

    for (auto a : mixer->getAnimationActionList()) {        
        string name = a->getCoreAnimation()->getName();

        //Si la animacion no tiene el id, no es la que buscamos
        if (name != anim)    continue;

        // Si la animacion esta en estado "STOP" le devolvemos true, en caso contrario le decimos que aun se esta reproduciendo
        if (a->getState() == CalAnimation::State::STATE_OUT) {
          return true;
        }
        else return false;

    }

    return true;
}

float TCompAnimator::getTimeAnimation(string name)
{
    TCompSkeleton* skel = get<TCompSkeleton>();
    CalModel* model = skel->getModel();
    CalMixer* mixer = model->getMixer();

    auto core = (CGameCoreSkeleton*)model->getCoreModel();
    int id = core->getCoreAnimationId(name);

    if (id < 0)  return -1;

    return core->getCoreAnimation(id)->getDuration();
}

void TCompAnimator::removeCurrentAction()
{
  TCompSkeleton* skel = get<TCompSkeleton>();
  CalModel* model = skel->getModel();
  CalMixer* mixer = model->getMixer();

  if (mixer->getAnimationActionList().size() > 0) {
    auto animBack = mixer->getAnimationActionList().back();
    int animback_id = model->getCoreModel()->getCoreAnimationId(animBack->getCoreAnimation()->getName());
    mixer->removeAction(animback_id, 0);
  }
}

void TCompAnimator::removeCurrentAction(float outDelay)
{
    TCompSkeleton* skel = get<TCompSkeleton>();
    CalModel* model = skel->getModel();
    CalMixer* mixer = model->getMixer();

    if (mixer->getAnimationActionList().size() > 0) {
        auto animBack = mixer->getAnimationActionList().back();
        //int animback_id = model->getCoreModel()->getCoreAnimationId(animBack->getCoreAnimation()->getName());
        animBack->remove(outDelay);
    }
}

void TCompAnimator::removeAction(string name, float outDelay)
{
    TCompSkeleton* skel = get<TCompSkeleton>();
    CalModel* model = skel->getModel();
    CalMixer* mixer = model->getMixer();

    if (mixer->getAnimationActionList().size() > 0) {
        int animback_id = model->getCoreModel()->getCoreAnimationId(name);
        if(animback_id > 0) mixer->removeAction(animback_id, outDelay);
        
    }
}

void TCompAnimator::resetAllAnims()
{
  TCompSkeleton* skel = get<TCompSkeleton>();
  CalModel* model = skel->getModel();
  CalMixer* mixer = model->getMixer();

  mixer->clearCycle(front_cycle.id, 0);
  mixer->clearCycle(back_cycle.id, 0);

  numAnimCycles = 0;

  front_cycle = empty;
  back_cycle = empty;

  if (mixer->getAnimationActionList().size() > 0) {
    auto animBack = mixer->getAnimationActionList().back();
    int animback_id = model->getCoreModel()->getCoreAnimationId(animBack->getCoreAnimation()->getName());
    mixer->removeAction(animback_id, 0);
  }

}

void TCompAnimator::insertAnimInList(float begin, float end, string anim_name, string type, float inDelay, float outDelay)
{
    TCompSkeleton* skel = get<TCompSkeleton>();
    CalModel* model = skel->getModel();
    CalMixer* mixer = model->getMixer();

    AnimationItem a;
    AnimationStruct anim;

    auto core = (CGameCoreSkeleton*)model->getCoreModel();
    int id = core->getCoreAnimationId(anim_name);

    anim.id = id;
    anim.in_delay = inDelay;
    anim.out_delay = outDelay;
    anim.type = type;
    anim.name = anim_name;

    a.anim = anim;
    a.begin_time = timer_list_animations + begin;
    a.end_time = timer_list_animations + end;
    a.active = false;

    if (printLog) dbg("Voy a insertar la animacion %s de tipo %s que empieza en %.2f y termina en %.2f\n", anim_name, type, begin, end);

    list_animations.push_back(a);

}

