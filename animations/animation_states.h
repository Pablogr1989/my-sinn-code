#pragma once
#include "engine.h"
#include "fsm/fsm_state.h"
#include "skeleton/comp_skeleton.h"

namespace fsm {

    class CState_Animation : public IState {
        std::string animation_name;
        std::string state_name;
        std::string type_anim;
        bool action_lock;
        bool with_yaw;
        bool with_pitch;
        bool with_move;

    public:
        virtual void load(const json& jData);
        void start(CContext& ctx) const override;
        void update(CContext& ctx, float dt) const override;
        virtual void finish(CContext& ctx) const {}
        virtual void renderInMenu() const {}
    };

}

