#include "mcv_platform.h"
#include "animation_states.h"
#include "entity/common_msgs.h"
#include "fsm/fsm_context.h"
#include "sinn/characters/animator.h"
#include "windows/application.h"

namespace fsm {
    void CState_Animation::load(const json& jData)
    {
        animation_name = jData["name_anim"];
        state_name = jData["name"];
        type_anim = jData["type_anim"];


        action_lock = jData.value("action_lock", false);
        with_yaw = jData.value("with_yaw", false);
        with_pitch = jData.value("with_pitch", false);
        with_move = jData.value("with_move", false);
        
    }
    void CState_Animation::start(CContext& ctx) const {
        CEntity* ent = ctx.getOwner();
        TCompAnimator* anim = ent->get<TCompAnimator>();
        //dbg("Frame %d -> Start FSM : %s\n",CApplication::get().getFrameCount(), state_name.c_str());
        
        
        if (type_anim == "action") {
            anim->playActionAnim(animation_name, action_lock, with_yaw, with_pitch, with_move);
        }
        else {            
            anim->playCycleAnim(animation_name);            
        }        
    }

    void CState_Animation::update(CContext& ctx, float dt) const {
        std::string msg = "Update en la animacion : " + state_name + "\n";
        //dbg(msg.c_str());

    }



}
