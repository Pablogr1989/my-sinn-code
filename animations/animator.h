#pragma once
#include <mcv_platform.h>
#include "engine.h"
#include "components/common/comp_base.h"
#include "skeleton/comp_skeleton.h"

using TVariableValue = std::variant<bool, int, float, std::string>;

struct BlendTransition {
	string backAnimation = "";
	string frontAnimation = "";
	string backValue = "";
	string frontValue = "";
	string compareVariable = "";
	string typeVar = "";
};

struct AnimationItem {
	AnimationStruct anim;
	float begin_time = 0;
	float end_time = 0;
	bool active = false;
};

class TCompAnimator : public TCompBase
{
	DECL_SIBLING_ACCESS();

	std::map<std::string, TVariableValue> list_variables;
	std::map<std::string, BlendTransition> bTransitions;

	//VARIABLES
	bool initialized = false;
	int numAnimCycles = 0;
	AnimationStruct empty;
	AnimationStruct front_cycle;
	AnimationStruct back_cycle;
	int start_frameAction = 0;
	vector<string> bucle_anim;


	float laspsedTime = 0.f;
	float blendTime = 0.f;
	bool manualBlend = false;
	bool not_remove_action = false;

	bool printLog = false;

	//Debug
	bool printAllAnims = false;
	vector< AnimationItem> list_animations;
	float timer_list_animations = 0;

	//FUNCIONES
	AnimationStruct getAnim(string anim_name);
	TVariableValue DefaultValue(string type);
	BlendTransition createBlendTransition(string fa, string ba, string faVal, string baVal, string compVar, string type);

	void renderVariable(string name, TVariableValue &_value);
	void blendAnimations();
	float calculateBlendFloat(BlendTransition b);
	float calculateBlendInt(BlendTransition b);
	float calculateBlendBool(BlendTransition b);
	TVariableValue getValue(string varName);
	void updateBucleActions();

	void updateListAnimations();

public:
	//FUNCIONES
	void debugInMenu();
	void load(const json& j, TEntityParseContext& ctx);
	void renderDebug() {};
	void onEntityCreated() {};
	void update(float dt);

	void playActionAnim(string anim_name, bool lock, bool with_yaw, bool with_pitch);
	void playActionAnim(string anim_name, bool lock, bool with_yaw, bool with_pitch, bool with_move);
	void playActionAnim(string anim_name, bool lock, bool with_yaw, bool with_pitch, bool with_roll, bool with_move);
	void playCycleAnim(string anim_name);
	void playParcialActionAnim(string anim_name, bool lock);
	void playParcialActionBucle(string anim_name);
	void removeAllBucleActions();
	void removeBucleAction(string anim_name);
	bool hasBucleAction() { return bucle_anim.size() > 0; }
	void removeCycleAnim(string anim_name);
	void removeBackCycle();
	void removeFrontCycle();
	void removeAllCycles();
	void blendToWeightNow(string anim_name, float w);
	void manualBlendAnimations(string anim1, string anin2, float t);


	void setVariable(string name, TVariableValue value);
	bool isActionAnimStop(string anim);
	float getTimeAnimation(string name);
	void removeCurrentAction();
	void removeCurrentAction(float outDelay);
	void removeAction(string name, float outDelay);
	void resetAllAnims();
	void setNotRemoveAction(bool r) { not_remove_action = r; }

	void insertAnimInList(float begin, float end, string anim, string type,  float inDelay, float outDelay);




};

