#include "mcv_platform.h"
#include "ai_controller.h"
#include "sinn/characters/player/comp_player.h"
#include "ai_blackboard.h"
#include "sinn/components/comp_audio.h"
#include "sinn/modules/module_sound.h"

DECL_OBJ_MANAGER("ai_controller", CAI_Controller);

void CAI_Controller::suspiciusNoiseEvent(const TMsgSuspiciusEvent& msg)
{

	//dbg("He recibido un event de %.2f de ruido\n", msg.intensity);

	for (auto& enemies : info_enemies) {
		VEC3 ePos = enemies.second.pos;
		float dist = VEC3::Distance(ePos, msg.pos);
		//dbg("La distancia del evento a %s es %.2f\n", enemies.first.c_str(), dist);
		if (dist < rad_det_noise) {
			//dbg("Le mando a un guadian el sonido porque esta dentro de su ratio\n");
			CEntity* ce = enemies.second.h_enemy;
			ce->sendMsg(msg);
		}
		else {
			//dbg("A este guardian no le llega porque esta demasiado lejos\n");
			//dbg("========================================================================\n");
		}
	}
}

void CAI_Controller::regEnemy(const TMsgEnemyRegister& msgReg)
{

	CEntity* e_enemy = msgReg.h_sender;

	string name = e_enemy->getName();
	enemy aux;
	aux.h_enemy = msgReg.h_sender;
	aux.player_seen = false;
	aux.pos = VEC3::Zero;

	info_enemies.insert(std::pair<string, enemy>(name, aux));
	numEnemies++;

}

void CAI_Controller::removeEnemy(string name)
{
	/*std::map<string, enemy>::iterator it;

	it = info_enemies.find(name);
	info_enemies.erase(it);
	numEnemies--;*/

	enemies_for_remove.push_back(name);

}

void CAI_Controller::sendResetTree(CHandle h_enemy)
{
	//Enviamos un mensaje de reseteo de arbol a ambos para que ejecuten su nuevo rol
	TMsgResetGame msg;
	h_enemy.sendMsg(msg);
}

void CAI_Controller::resetGame(const TMsgResetGame& msgReset)
{
	playerDead = false;

	for (auto& enemies : info_enemies) {
		CEntity* ce = enemies.second.h_enemy;
		TCompEnemy3D* enemy = ce->get<TCompEnemy3D>();
		enemy->resetEnemy();
	}
	if (isPlayerInRangeForMusic()) {
		CEngine::get().getSound().playBSO("guardiansBSO");
		setGuardiansBSO(CEngine::get().getSound().getBSO());
	}

	CAI_BlackBoard::get().reset();

}

void CAI_Controller::setPlayerDead(const TMsgDeath& msgDeath)
{
	playerDead = true;
	if (soundtrack) {
		soundtrack->stop();
		soundtrack = nullptr;
		constrictTimer = 0;
	}

}

void CAI_Controller::updateInfoPlayer()
{

	//Actualizamos la informacion del player

	if (h_player == h_null)	return;

	CEntity* c_ent = h_player;
	TCompTransform* c_tplayer = c_ent->get<TCompTransform> ();
	TCompPlayer* c_player = c_ent->get<TCompPlayer>();

	//TCompPlayer* c_player = c_ent->get<TCompPlayer>();
	CAI_BlackBoard::get().updateInfoPlayer(c_tplayer->getFront(), -c_tplayer->getLeft(), c_tplayer->getPosition(), c_player->getModVelocity()/*, c_player->getNoise()*/);

	//Calculamos las posiciones de bloqueo a traves de la informacion del player
	VEC3 posBlock1 = c_tplayer->getPosition() + (c_tplayer->getFront() * 2);  // Arriba
	VEC3 posBlock2 = c_tplayer->getPosition() + (-c_tplayer->getLeft() * 2);  //Derecha
	VEC3 posBlock3 = c_tplayer->getPosition() + (c_tplayer->getLeft() * 2);   //Izquierda
	VEC3 posBlock4 = c_tplayer->getPosition() + (-c_tplayer->getFront() * 2);  //Abajo
	CAI_BlackBoard::get().updatePosBlockPlace(posBlock1, 0);
	CAI_BlackBoard::get().updatePosBlockPlace(posBlock2, 1);
	CAI_BlackBoard::get().updatePosBlockPlace(posBlock3, 2);
	CAI_BlackBoard::get().updatePosBlockPlace(posBlock4, 3);
}

void CAI_Controller::updateInfoEnemies()
{
	CHandle aux_captor;
	string name_captor;
	float min_dist = std::numeric_limits<float>::max();
	bool checkAssign = false;
	playerDetected = false;
	vector<string> names_haunters;

	float max_suspicius = -1;

	if (numEnemies <= 0)	return;

	generalGuardiansState = enemyState::PATROL;

	for (auto& it : info_enemies) {

		//Actualizamos la posicion del enemigo en nuestro map
		CEntity* aux_entity = it.second.h_enemy;
		if (aux_entity == nullptr) {
			removeEnemy(it.first);
			continue;
		}
		TCompEnemy3D* aux_e = aux_entity->get<TCompEnemy3D>();

		if (aux_e->isDestroyed()) {
			//removeEnemy(it.first);
			continue;
		}

		if (aux_e->isDead())	continue;

		it.second.pos = aux_e->getMyPos();
		it.second.type = aux_e->getType();
		it.second.suspicius_bar = aux_e->getSuspiciusBarValue();
		it.second.state = aux_e->getGuardianState();

		// Actualizamos la informacion general de estos valores con cada uno de los enemigos
		suspiciurBar_alert_value = aux_e->getAlertValue();
		suspiciurBar_alarm_value = aux_e->getAlarmValue();
		suspiciurBar_suspicius_value = aux_e->getSuspiciusValue();

		//Obtenemos si esta en alerta para reproducir un sonido localizado en su posicion
		it.second.in_alert = it.second.suspicius_bar >= aux_e->getAlertValue();

		if (it.second.suspicius_bar > max_suspicius) {
			max_suspicius = it.second.suspicius_bar;
		}

		// Si ha sonado la alarma en cualquier guardian, avisamos que hemos sido detectados
		if (max_suspicius >= suspiciurBar_alarm_value) {
			playerDetected = true;
		}

		if (it.second.state > generalGuardiansState) {
			generalGuardiansState = it.second.state;
		}
	
		//Comrprobamos si este enemigo tiene al player a la vista
		it.second.player_seen = aux_e->getPlayerSeen();
		if (it.second.player_seen) {
			//playerInSight = true;
			checkAssign = true;
			if (it.second.type == ENEMY_PATROLLER)	names_haunters.push_back(it.first);
			//Si lo tiene, comprobamos su distancia a �l
			float dist = aux_e->getDistToPlayer();
			if (dist < min_dist) {
				min_dist = dist;
				name_captor = it.first;
				aux_captor = it.second.h_enemy;
			}
		}
	}

	//Solo comprobamos esto si ha salido que al menos un enemigo ha visto al player
	if (checkAssign) {
		checkIsAssignToCapture(aux_captor);
		checkBlockingPlaces(names_haunters, name_captor);
	}

	max_suspicius_value = max_suspicius;


	if (enemies_for_remove.size() > 0) {
		for (auto name_enemy : enemies_for_remove) {
			std::map<string, enemy>::iterator it;

			it = info_enemies.find(name_enemy);
			info_enemies.erase(it);
			numEnemies--;
		}
	}
}
bool bso = false;
void CAI_Controller::updateSoundEvents(float dt)
{
	float state = calculateSoundState();
	CEntity * c_ent = h_player;
	TCompPlayer* c_player = c_ent->get<TCompPlayer>();
	if (c_player->isInCutscene()) {
		state = 0;
		if (soundtrack)soundtrack->setParameter("distance", 20);
	}
	if(soundtrack)soundtrack->setParameter("guardianState",state);

	if (playerDetected && !soundPlayerSeenActivated) {
		soundPlayerSeenActivated = true;
		CEngine::get().get().getSound().playEvent("Alarm");
	}

	
	float distance=25;
	bool allEnemiesRelax = true;
	for (auto& it : info_enemies) {
		// En cuanto un enemigo tenga su sospecha mayor que 0, no podemos reproducir el sonido de detectado
		if (it.second.suspicius_bar > 0)	allEnemiesRelax = false;
		CEntity* enemy = it.second.h_enemy;
		TCompEnemy3D* e = enemy->get<TCompEnemy3D>();

		float enemy_distance = e->getDistToPlayer();
		if (enemy_distance <= distance)distance = enemy_distance;
		if (it.second.in_alert && !it.second.sound_player_alert_activated) {
			CEntity* enemy = it.second.h_enemy;
				TCompAudio* audio = enemy->get<TCompAudio>();
				audio->playEvent("Alert");
			it.second.sound_player_alert_activated = true;
		}
		//Esto ahora se hace al recibir el mensaje de que el player esta muerto
		/*if (generalGuardiansState == enemyState::CONSTRICT) {
			constrictTimer += dt;
			if (constrictTimer > 4 && soundtrack) {
				soundtrack->stop();
				soundtrack = nullptr;
				constrictTimer = 0;
			}
		}
		else {
			constrictTimer = 0;
		}*/

		if (it.second.sound_player_alert_activated && it.second.suspicius_bar == 0) {
			// Se desactiva el sonido de alerta porque ya ha pasado a estar totalmente de relax
			it.second.sound_player_alert_activated = false;
		}

	}
	if (generalGuardiansState == enemyState::PATROL && !c_player->isInCutscene()) {
		if(soundtrack)	soundtrack->setParameter("distance", distance);
	}
	//Si todos los enemigos estan a 0 de sospecha, ninguno ve al player (que con la barra de sospecha es suficiente pero porsi)
	//y se activ� hace poco el sonido, entonces lo desactivamos
	if (allEnemiesRelax && !playerDetected && soundPlayerSeenActivated) {
		soundPlayerSeenActivated = false;
	}

}

float CAI_Controller::calculateSoundState()
{

	if (playerDead)	return 0.f;

	switch (generalGuardiansState)
	{
	case enemyState::PATROL:
		return 0.f;
		break;
	case enemyState::ALERT:
		return 1.f;
		break;
	case enemyState::SUSPICIUS:
		return 2.f;
		break;
	case enemyState::PURSUER:
		return 3.f;
		break;
	case enemyState::ATTACK:
		return 4.f;
		break;
	case enemyState::CONSTRICT:
		return 5.f;
		break;
	case enemyState::CAUGHT:
		return 5.f;
		break;
	case enemyState::SURRENDER:
		return 1.f;
		break;
	case enemyState::CONSTRICT_RELEASE:
		return 2.f;
		break;
	case enemyState::CONSTRICT_GETUP:
		return 1.f;
		break;
	default:
		break;
	}
}

void CAI_Controller::checkIsAssignToCapture(CHandle h_enemy)
{
	old_captorEnemy = CAI_BlackBoard::get().whoCaughtPlayer();
	if (old_captorEnemy != h_enemy) {
		//Enviamos un mensaje de reseteo de arbol a ambos para que ejecuten su nuevo rol
		sendResetTree(old_captorEnemy);
		sendResetTree(h_enemy);
		CAI_BlackBoard::get().setEnemyCaptor(h_enemy);
	}	
}

float CAI_Controller::calculeVolBSO()
{
	// Vol = 0% -> Max_suspicius_value = 30
	// Vol = 100% -> Max_suspocius_value = 100

	float vol = ((max_suspicius_value - 30) / 70) * 100;

	if (vol < 0)	vol = 0;

	return vol;
}

void CAI_Controller::checkBlockingPlaces(vector<string> haunters, string name_captor)
{
	//Primero eliminamos al haunter que se ha convertido en el captor
	removeHaunter(haunters, name_captor);

	//Comprobamos los 4 lugares de bloqueo
	for (int i = 0; i < 4; i++) {
		CHandle h_blocker = CAI_BlackBoard::get().getBlocker(i);

		// �Esta libre el lugar de bloqueo?
		if (isBlockingPlaceFree(h_blocker, haunters)) {
			// �Quedan aun haunters por asignar o ya no queda ninguno?
			if(haunters.size() > 0) {
				//Si hay, cogemos el que este mas cerca de ese punto de bloqueo
				string s_enemy = getNearestHaunter(i, haunters);
				CHandle h_enemy = getHandleEnemyByName(s_enemy);
				//Ahora vamos a comprobar si ese enemigo tenia otro punto de bloqueo asignado
				int nbp = CAI_BlackBoard::get().whoBlockerIAm(h_enemy);
				//Si el numer del punto de bloqueo (Num block place) es diferente de 0, es que si tenia uno asignado
				//Tambien comprobamos que sea diferente de i, aunque en teoria eso no deberia ocurrir
				if (nbp != 0 && nbp != i) {
					//Si tenia uno asignado, lo liberamos ya que se pasa a otro diferente
					CAI_BlackBoard::get().freeBlockPlace(nbp);
				}
				//Asignamos este punto de bloqueo al enemigo que hemos escogido
				CAI_BlackBoard::get().setBlocker(h_enemy, i);
				//Reseteamos su BT para que vaya al nuevo punto
				sendResetTree(h_enemy);
				//Y por ultimo quitamos el haunter del vector
				removeHaunter(haunters, s_enemy);
			}
			else {
				//Si ya no hay mas haunters disponiles liberamos el punto de bloqueo por si no lo estuviera ya
				CAI_BlackBoard::get().freeBlockPlace(i);
			}

		}
		else {
			//Si no lo esta, vamos a excluir al enemigo que esta ahi de la lista de haunters
			CEntity* eaux = h_blocker;
			removeHaunter(haunters, eaux->getName());
		}
	}

}

bool CAI_Controller::isBlockingPlaceFree(CHandle h_blocker, vector<string> haunters)
{

	if (h_blocker == h_null) return true;

	for (int i = 0; i < haunters.size(); i++) {
		if (h_blocker == getHandleEnemyByName(haunters[i]))	return false;
	}

	return true;
}

string CAI_Controller::getNearestHaunter(int blockPlace, vector<string> haunters)
{

	float min_dist = std::numeric_limits<float>::max();
	string nearEnemy;
	VEC3 pos_bp = CAI_BlackBoard::get().getPosBlockPlace(blockPlace);

	for (int i = 0; i < haunters.size(); i++) {
		enemy aux = info_enemies.find(haunters[i])->second;
		float dist = VEC3::Distance(pos_bp, aux.pos);
		if (dist < min_dist) {
			nearEnemy = haunters[i];
			min_dist = dist;
		}
	}
	return nearEnemy;
}

void CAI_Controller::resetAssignBB()
{	
	CAI_BlackBoard::get().reset();
}

CHandle CAI_Controller::getHandleEnemyByName(string name)
{
	return info_enemies.find(name)->second.h_enemy;
}

void CAI_Controller::removeHaunter(vector<string>& haunters, string name)
{
	for (int i = 0; i < haunters.size(); i++) {
		if (haunters[i] == name) {
			haunters.erase(haunters.begin() + i );
			break;
		}
	}
}


void CAI_Controller::debugInMenu()
{
	ImGui::DragInt("Detection", &max_suspicius_value, 0, 0, 100);
	ImGui::LabelText("Not in chase %s", notInChase() ? "true" : "false");
}

void CAI_Controller::load(const json& j, TEntityParseContext& ctx)
{
}

void CAI_Controller::update(float dt)
{
	updateInfoPlayer();
	updateInfoEnemies();
	updateSoundEvents(dt);
}

void CAI_Controller::renderDebug()
{
	/*
	//////////////////////////////////////////////////////////////////////////////////////
	// VENTANA CON ESTADO DEL PERSONAJE Y GRAFICO DE RUIDO
	ImGui::Begin("Ai Controller", &windowStatus_active);
	ImGuiIO& io = ImGui::GetIO();
	ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / io.Framerate, io.Framerate);
	if (ImGui::TreeNode("Enemigos"))
	{
		int i = 1;
		for (auto& it : info_enemies) {
			//Actualizamos la posicion del enemigo en nuestro map
			CEntity* aux_entity = it.second.h_enemy;
			TCompEnemy3D* aux_e = aux_entity->get<TCompEnemy3D>();
			ImGui::PushID(i);
			aux_e->debugInMenu();
			ImGui::PopID();
			ImGui::NewLine();			
			i++;
		}
		ImGui::TreePop();
	}
	ImGui::End();
	*/
}

void CAI_Controller::renderEnemy(string name, enemy e)
{

	ImGui::Text("Nombre: %s", name.c_str());
	ImGui::DragFloat("Sospecha",&e.suspicius_bar, 1, 0, 100);
	ImGui::Checkbox("Ha visto al Player", &e.player_seen);

}

IA_state CAI_Controller::getStateDetection()
{

	if (generalGuardiansState == enemyState::PATROL) {
		return IA_state::PATROL;
	}
	else if (generalGuardiansState == enemyState::ALERT) {
		return IA_state::ALERT;
	}
	else if (generalGuardiansState == enemyState::SUSPICIUS) {
		return IA_state::SUSPICIUS;
	}
	else {
		return IA_state::ALARM;
	}
}

bool CAI_Controller::isPlayerInRangeForMusic()
{
	float distance = 100000.f;

	TCompEnemy3D* closest_enemy=nullptr;
	for (auto& enemies : info_enemies) {
		CEntity* ce = enemies.second.h_enemy;
		TCompEnemy3D* enemy = ce->get<TCompEnemy3D>();
		if (enemy->getDistToPlayer() <= distance) {
			distance = enemy->getDistToPlayer();
			closest_enemy = enemy;
		}
	}
	if (distance <= closest_enemy->getMaxDistanceSound())return true;
	return false;
}

void CAI_Controller::onEntityCreated()
{
	h_player = getEntityByName("Player");
}

void CAI_Controller::registerMsgs()
{
	DECL_MSG(CAI_Controller, TMsgEnemyRegister, regEnemy);
	DECL_MSG(CAI_Controller, TMsgResetGame, resetGame);
	DECL_MSG(CAI_Controller, TMsgDeath, setPlayerDead);
	DECL_MSG(CAI_Controller, TMsgSuspiciusEvent, suspiciusNoiseEvent);	
	
}
