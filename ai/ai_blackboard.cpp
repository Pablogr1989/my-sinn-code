#include "mcv_platform.h"
#include "ai_blackboard.h"

CAI_BlackBoard* CAI_BlackBoard::singleton = nullptr;

void CAI_BlackBoard::updateInfoPlayer(VEC3 dir, VEC3 rDir, VEC3 pos, float vel/*, float noise*/)
{
	dir_player = dir;
	rightDir_player = rDir;
	pos_player = pos;
	//noise_player = noise;
	vel_player = vel;
}

void CAI_BlackBoard::reset()
{
	player_caught = false;
	pos_player = VEC3(FLT_MAX, FLT_MAX, FLT_MAX);
	dir_player = VEC3::Zero;
	rightDir_player = VEC3::Zero;
	//noise_player = 0.f;
	h_captor = h_null;
	vel_player = 0.f;
	for (int i = 0; i < 4; i++) {
		blockPlace aux;
		aux.free = true;
		aux.h_blocker = h_null;
		aux.pos = VEC3::Zero;
		blockingPlaces.push_back(aux);
	}
}

void CAI_BlackBoard::setEnemyCaptor(CHandle enemy)
{
	h_captor = enemy;
}

VEC3 CAI_BlackBoard::getPosPlayer()
{
	return pos_player;
}

VEC3 CAI_BlackBoard::getDirPlayer()
{
	return dir_player;
}

VEC3 CAI_BlackBoard::getRightDirPlayer()
{
	return rightDir_player;
}

float CAI_BlackBoard::getVelocityPlayer()
{
	return vel_player;
}

//DEPRECATED
/*float CAI_BlackBoard::getNoisePlayer()
{
	return noise_player;
}*/

bool CAI_BlackBoard::isPlayerCaught()
{
	return player_caught;
}

CHandle CAI_BlackBoard::whoCaughtPlayer()
{
	return h_captor;
}

CHandle CAI_BlackBoard::getBlocker(int num)
{
	return blockingPlaces[num].h_blocker;
}

void CAI_BlackBoard::setBlocker(CHandle h_enemy, int num)
{
	if (num > 4) return;

	blockingPlaces[num].h_blocker = h_enemy;
	blockingPlaces[num].free = false;
}

int CAI_BlackBoard::whoBlockerIAm(CHandle h_enemy)
{

	for (int i = 0; i < 4; i++) {
		if (blockingPlaces[i].h_blocker == h_enemy)	return i;
	}

	return 0;
}

bool CAI_BlackBoard::isFreeBlockPlace(int num)
{
	return blockingPlaces[num].free;
}

void CAI_BlackBoard::freeBlockPlace(int num)
{
	blockingPlaces[num].h_blocker = h_null;
	blockingPlaces[num].free = true;
}

void CAI_BlackBoard::updatePosBlockPlace(VEC3 pos, int num)
{
	blockingPlaces[num].pos = pos;
}

VEC3 CAI_BlackBoard::getPosBlockPlace(int num)
{
	return blockingPlaces[num].pos;
}

CAI_BlackBoard& CAI_BlackBoard::get()
{
	if (singleton == nullptr) {
		CAI_BlackBoard cb;
	}

	return *singleton;
}

CAI_BlackBoard::CAI_BlackBoard()
{
	assert(singleton == nullptr);
	singleton = this;
}
