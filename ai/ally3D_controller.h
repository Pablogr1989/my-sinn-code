#pragma once

#include "sinn/behavior/behavior_tree.h"
#include "sinn/characters/enemies/comp_enemy3D.h"

class CAlly3DController : public TBehaviorTree
{

	//Mapas con las acciones y condiciones precargadas
	map<string, btaction> ally3d_actions;
	map<string, btcondition> ally3d_conditions;
	TCompEnemy3D* getC_enemy();  //Funcion que devuelve el componente TCompEnemy3D

	void create(string s);
	void loadNodes(const json& j, TEntityParseContext& ctx);
	void loadManualNodes(string node);

	bool waitingForExit = false;

	///////////////////////////////////////////////////
	//CONDICIONES
	bool conditionDeath();
	bool conditionDesactivated();
	bool conditionEternity();
	bool conditionTempDeath();
	bool conditionAlert();
	bool conditionSuspicius();
	bool conditionPursuer();
	bool conditionCombat();
	bool conditionWait();
	bool conditionAttack();

	//Condiciones Patrulleros
	bool conditionPatroller();
	bool conditionPatrol();
	bool conditionResetWpt();
	bool conditionFollowWpt();
	bool conditionCliff();
	bool conditionPatrollerConstrict();
	bool conditionSurrender();


	//Condiciones GateKeeper
	bool conditionGatekeeper();
	bool conditionGuard();

	///////////////////////////////////////////////////


	///////////////////////////////////////////////////
	//ACCIONES COMUNES
	// Muerte
	int actionGoToEternity(float dt);
	int actionInHell(float dt);
	int actionRebirth(float dt);
	int actionWhatHappened(float dt);
	// Desactivado
	int actionGoToInitPoint(float dt);
	int actionWaitToActivated(float dt);


	//Acciones Patroller
	//Patrulla
	int actionResetWpt(float dt);
	int actionSeekWpt(float dt);
	int actionRotateToLook(float dt);
	int actionLookAround(float dt);
	int actionNextWpt(float dt);

	//Alert
	int actionSearchAround(float dt);

	//Suspicius
	int actionGoToNoise(float dt);
	int actionSearchSomething(float dt);

	//Pursuer
	int actionPursue(float dt);

	// - Comunes Attack
	int actionWait(float dt);
	int actionWaitPlayerDead(float dt);
	int actionTryToCatch(float dt);
	int actionReposition(float dt);

	// - Constrict
	int actionConstrict(float dt);
	int actionReleaseConstrict(float dt);
	int actionGetUp(float dt);
	// - Thrown
	int actionReachCliff(float dt);
	int actionThrowPlayer(float dt);
	int actionWaitThrow(float dt);

	//Surrender
	int actionChooseWpt(float dt);
	int actionGoBackWpt(float dt);





	//Exclusivos GateKeeper
	int actionGuard(float dt);
	int actionDefend(float dt);
	int actionRotateToNoise(float dt);
	int actionRotateToPlayer(float dt);
	int actionMoveToPlayer(float dt);
	int actionOrbitAroundGP(float dt);
	int actionHit(float dt);



	///////////////////////////////////////////////////
};

