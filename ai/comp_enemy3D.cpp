#include "mcv_platform.h"
#include "comp_enemy3D.h"
#include "sinn/behavior/ai/ai_blackboard.h"
#include "input/input_module.h"
#include "sinn/behavior/behavior_tree.h"
#include "sinn/characters/animator.h"
#include "sinn/modules/module_nav_mesh.h"
#include "windows/application.h"
#include "components/common/comp_render.h"
#include "sinn/components/comp_audio.h"
#include "sinn/modules/module_scripting.h"

DECL_OBJ_MANAGER("enemy3D", TCompEnemy3D);
using namespace physx;

void TCompEnemy3D::debugInMenu()
{

	if (ImGui::TreeNode("Add Rotation Transform")) {
		static float add_yaw = 0.f, add_pitch = 0.f, add_roll = 0.f;
		ImGui::DragFloat("Yaw col", &add_yaw, 0.1f, -180.0f, 180.0f);
		float max_pitch = 90.0f - 1e-3f;
		ImGui::DragFloat("Pitch col", &add_pitch, 0.1f, -max_pitch, max_pitch);
		ImGui::DragFloat("Roll col", &add_roll, 0.1f, -180.0f, 180.0f);

		if (ImGui::SmallButton("Add rotation")) {
			TCompTransform* trans = get<TCompTransform>();
			QUAT rot = QUAT::CreateFromYawPitchRoll(deg2rad(add_yaw), deg2rad(add_pitch), deg2rad(add_roll));
			trans->setRotation(trans->getRotation() * rot);
			add_yaw = 0.f, add_pitch = 0.f, add_roll = 0.f;
		}
	}

	if(ImGui::TreeNode("Move only collider")) {
		static VEC3 manual_setPosition = VEC3::Zero;
		ImGui::DragFloat3("Additional Collider Position", &manual_setPosition.x, 0.1, -1000, 1000);
		static float col_yaw = 0.f, col_pitch = 0.f, col_roll = 0.f;
		ImGui::DragFloat("Yaw col", &col_yaw, 0.1f, -180.0f, 180.0f);
		float max_pitch = 90.0f - 1e-3f;
		ImGui::DragFloat("Pitch col", &col_pitch, 0.1f, -max_pitch, max_pitch);
		ImGui::DragFloat("Roll col", &col_roll, 0.1f, -180.0f, 180.0f);

		if (ImGui::SmallButton("Add collider position")) {
			TCompCollider* col = get<TCompCollider>();

			PxExtendedVec3 pxvec = col->controller->getFootPosition();

			VEC3 current_Colpos = VEC3(pxvec.x, pxvec.y, pxvec.z);
			current_Colpos += manual_setPosition;
			col->controller->setFootPosition(PxExtendedVec3(current_Colpos.x, current_Colpos.y, current_Colpos.z));


			VEC3 UP = VEC3(0, 1, 0);
			MAT44 UP_Mat = MAT44::CreateTranslation(UP);
			MAT44 Rot = MAT44::CreateFromYawPitchRoll(deg2rad(col_yaw), deg2rad(col_pitch), deg2rad(col_roll));
			MAT44 new_trans = UP_Mat * Rot;
			col->controller->setUpDirection(PxVec3(new_trans._41, new_trans._42, new_trans._43));
		}
	}


	CEntity* player = getEntityByName("Player");
	TCompTransform* trans_player = player->get<TCompTransform>();
	TCompAnimator* anim_player = player->get< TCompAnimator>();
	TCompCollider* col_player = player->get<TCompCollider>();
	TCompSkeleton* skel_player = player->get<TCompSkeleton>();

	TCompTransform* my_trans = get<TCompTransform>();
	TCompAnimator* my_anim = get< TCompAnimator>();
	TCompCollider* my_col = get<TCompCollider>();
	TCompSkeleton* my_skel = get<TCompSkeleton>();

	static float dist_sincr = 1.5f;
	static bool inSincr = false;
	static bool in_reposition = false;
	static bool in_StartConstrict = false;
	static bool in_HugCycle = false;
	static bool in_HugScape = false;
	static bool in_rebirth = false;
	static bool fall_sinc = false;
	static bool phase1 = false;
	static bool phase2 = false;
	static bool front_Start = false;

	float time_reposition = 0.1;
	float time_startConstrict = 0.63;
	static float time_hugCycle = 7;
	float time_hugScape = 1.5;
	float time_rebirth = 5;

	static VEC3 start_pos_guardian = VEC3::Zero;
	static VEC3 end_pos_guardian = VEC3::Zero;
	static QUAT start_rot_guardian = QUAT(0, 0, 0, -1);
	static QUAT end_rot_guardian = QUAT(0, 0, 0, -1);
	static VEC3 start_root_pos_guardian = VEC3::Zero;
	static VEC3 end_root_pos_guardian = VEC3::Zero;
	static QUAT start_rot_col_guardian = QUAT(0, 0, 0, -1);
	static QUAT end_rot_col_guardian = QUAT(0, 0, 0, -1);

	static VEC3 start_pos_seele = VEC3::Zero;
	static VEC3 end_pos_seele = VEC3::Zero;
	static QUAT start_rot_seele = QUAT(0, 0, 0, -1);
	static QUAT end_rot_seele = QUAT(0, 0, 0, -1);
	static VEC3 start_pos_root_seele = VEC3::Zero;
	static VEC3 end_pos_root_seele = VEC3::Zero;

	if (ImGui::TreeNode("Constrict manual sincronizations")) {

		VEC3 pos_root_guardian = my_skel->getAbsTraslationBone(0);
		VEC3 pos_root_player = skel_player->getAbsTraslationBone(0);

		ImGui::Text("Root Bone Guardian [%.3f %.3f %.3f]", pos_root_guardian.x, pos_root_guardian.y, pos_root_guardian.z);
		ImGui::Text("Root Bone Player   [%.3f %.3f %.3f]", pos_root_player.x, pos_root_player.y, pos_root_player.z);
		ImGui::Text("Distancia Entre roots %.3f", VEC3::Distance(pos_root_guardian, pos_root_player));

		ImGui::DragFloat("Distancia inicial sincronizacion", &dist_sincr, 0.1, 0, 100);
		ImGui::DragFloat("Tiempo Constrict Hug", &time_hugCycle, 0.1, 1.8, 7);

		if (ImGui::SmallButton("Iniciar Constrict")) {

			start_pos_seele = trans_player->getPosition();
			start_rot_seele = trans_player->getRotation();

			end_pos_seele = my_trans->getPosition() + my_trans->getFront() * dist_sincr;

			VEC3 front_guardian = my_trans->getFront();
			VEC3 front_seele = trans_player->getFront();
			float res = front_guardian.Dot(front_seele);

			if (res >= 0) {
				end_rot_seele = my_trans->getRotation();
				front_Start = false;
			}
			else {
				QUAT rot180 = QUAT(0, -1, 0, 0);
				end_rot_seele = my_trans->getRotation() * rot180;
				front_Start = true;
			}			

			timer_sincr = 0;
			in_reposition = true;

		}

		if (ImGui::SmallButton("Manual Fall Guardian")) {
			not_auto_move = true;

			//Rotacion que hara la capsula del guardian al caer
			float y, p;
			my_trans->getEulerAngles(&y, &p, nullptr);
			end_rot_col_guardian = QUAT::CreateFromYawPitchRoll(y + (deg2rad(-180)), deg2rad(89.f), deg2rad(0.f));
			float aux_y, aux_p, aux_r;
			ToEulerAngles(start_rot_col_guardian, &aux_y, &aux_p, &aux_r);
			dbg("Inicio rotacion capsula: Yaw:%.2f Pitch:%.2f Roll:%.2f\n", rad2deg(aux_y), rad2deg(aux_p), rad2deg(aux_r));
			ToEulerAngles(end_rot_col_guardian, &aux_y, &aux_p, &aux_r);
			dbg("Fin rotacion capsula: Yaw:%.2f Pitch:%.2f Roll:%.2f\n", rad2deg(aux_y), rad2deg(aux_p), rad2deg(aux_r));


			my_anim->playActionAnim("constrictHugScape", true, false, false, false, false);
			//my_skel->enable_root_motion = true;

			start_pos_guardian = my_trans->getPosition();
			end_pos_guardian = start_pos_guardian - my_trans->getFront() * 0.9;

			fall_sinc = true;
			timer_sincr = 0;
		}

		if (in_reposition && !CApplication::get().getInPause()) {
			if (timer_sincr <= time_reposition) {

				float ratio = timer_sincr / time_reposition;

				VEC3 pos_seele = lerp(start_pos_seele, end_pos_seele, ratio);
				QUAT rot_seele = QUAT::Slerp(start_rot_seele, end_rot_seele, ratio);

				//trans_player->setPosition(pos_seele);
				trans_player->setRotation(rot_seele);
				col_player->controller->setFootPosition(PxExtendedVec3(pos_seele.x, pos_seele.y, pos_seele.z));
			}
			else {

				trans_player->setRotation(end_rot_seele);
				trans_player->setPosition(end_pos_seele);
				col_player->controller->setFootPosition(PxExtendedVec3(end_pos_seele.x, end_pos_seele.y, end_pos_seele.z));

				if (front_Start) {
					anim_player->playActionAnim("frontStartConstrictIni", true, false, false, true);
					start_rot_seele = trans_player->getRotation();
					end_rot_seele = my_trans->getRotation();

					start_pos_seele = trans_player->getPosition();
					end_pos_seele = start_pos_seele - trans_player->getFront() * 0.12;

				}
				else {
					anim_player->playActionAnim("backStartConstrictIni", true, false, false, true);

					start_pos_seele = trans_player->getPosition();
					end_pos_seele = start_pos_seele + trans_player->getFront() * 0.12;
				}


				anim_player->playActionAnim("backStartConstrictIni", true, false, false, true);
				my_anim->playActionAnim("backStartConstrictIni", true, false, false, true);
				timer_sincr = 0;
				in_reposition = false;
				in_StartConstrict = true;

				start_pos_guardian = my_trans->getPosition();
				end_pos_guardian = my_trans->getPosition() + my_trans->getFront() * 0.63;


			}
		}


		if (in_StartConstrict && !CApplication::get().getInPause()) {

			if (timer_sincr <= time_startConstrict) {

				if (timer_sincr <= 0.3) {
					float ratio = timer_sincr / 0.3;

					VEC3 pos_guardian = lerp(start_pos_guardian, end_pos_guardian, ratio);
					VEC3 pos_seele = lerp(start_pos_seele, end_pos_seele, ratio);
					//dbg("Posicion Lerp en %.2f ratio [%.2f %.2f %.2f]\n",ratio, pos.x, pos.y, pos.z);

					if (front_Start) {
						QUAT rot_seele = QUAT::Slerp(start_rot_seele, end_rot_seele, ratio);
						trans_player->setRotation(rot_seele);
					}

					my_trans->setPosition(pos_guardian);
					my_col->controller->setFootPosition(PxExtendedVec3(pos_guardian.x, pos_guardian.y, pos_guardian.z));

					trans_player->setPosition(pos_seele);
					col_player->controller->setFootPosition(PxExtendedVec3(pos_seele.x, pos_seele.y, pos_seele.z));

				}

			}
			else {

				my_trans->setPosition(end_pos_guardian);
				my_col->controller->setFootPosition(PxExtendedVec3(end_pos_guardian.x, end_pos_guardian.y, end_pos_guardian.z));

				trans_player->setPosition(end_pos_seele);
				col_player->controller->setFootPosition(PxExtendedVec3(end_pos_seele.x, end_pos_seele.y, end_pos_seele.z));

				if (front_Start) {
					trans_player->setRotation(end_rot_seele);
				}


				anim_player->removeCurrentAction();
				anim_player->playActionAnim("constrictHug", true, false, false, false);
				my_anim->removeCurrentAction();
				my_anim->playActionAnim("constrictHug", true, false, false, false);

				in_StartConstrict = false;
				in_HugCycle = true;
				timer_sincr = 0;

			}

		}

		if (in_HugCycle && !CApplication::get().getInPause()) {
			if (timer_sincr > time_hugCycle) {

				anim_player->removeCurrentAction();
				my_anim->removeCurrentAction();
				anim_player->playActionAnim("constrictHugScape", true, false, false, true);
				my_anim->playActionAnim("constrictHugScape", true, false, false, false, false);
				timer_animation = 0;

				skel_player->setCorrectRoot(true);
				skel_player->clearCorrectRoot();

				start_pos_root_seele = trans_player->getPosition();
				end_pos_root_seele = start_pos_root_seele - trans_player->getFront() * 0.422;
				skel_player->setCorrectMoveRoot(end_pos_root_seele - start_pos_root_seele);

				start_pos_root_seele = end_pos_root_seele - start_pos_root_seele;
				end_pos_root_seele = VEC3::Zero;

				// Movimiento que hara el player
				start_pos_seele = trans_player->getPosition();
				end_pos_seele = trans_player->getPosition() + trans_player->getFront() * 1.607;

				not_auto_move = true;

				//Rotacion que hara la capsula del guardian al caer
				float y, p;
				my_trans->getEulerAngles(&y, &p, nullptr);
				end_rot_col_guardian = QUAT::CreateFromYawPitchRoll(y + (deg2rad(-180)), deg2rad(89.f), deg2rad(0.f));
				/*float aux_y, aux_p, aux_r;
				ToEulerAngles(start_rot_col_guardian, &aux_y, &aux_p, &aux_r);
				dbg("Inicio rotacion capsula: Yaw:%.2f Pitch:%.2f Roll:%.2f\n", rad2deg(aux_y), rad2deg(aux_p), rad2deg(aux_r));
				ToEulerAngles(end_rot_col_guardian, &aux_y, &aux_p, &aux_r);
				dbg("Fin rotacion capsula: Yaw:%.2f Pitch:%.2f Roll:%.2f\n", rad2deg(aux_y), rad2deg(aux_p), rad2deg(aux_r));*/

				//Peque�o desplazamiento del root para que encaje donde se quedo el constrictHug
				my_skel->setCorrectRoot(true);
				my_skel->clearCorrectRoot();
				start_root_pos_guardian = my_trans->getPosition();
				end_root_pos_guardian = my_trans->getPosition() + my_trans->getFront() * 0.378;
				my_skel->setCorrectMoveRoot(end_root_pos_guardian - start_root_pos_guardian);

				start_root_pos_guardian = end_root_pos_guardian - start_root_pos_guardian;
				end_root_pos_guardian = VEC3::Zero;

				start_pos_guardian = my_trans->getPosition();
				end_pos_guardian = my_trans->getPosition() - my_trans->getFront() * 0.378;

				in_HugCycle = false;
				in_HugScape = true;
				timer_sincr = 0;

			}
		}

		if (in_HugScape && !CApplication::get().getInPause()) {
			if (timer_sincr <= time_hugScape) {

				setConstrictRender(enemyState::CONSTRICT_RELEASE);

				float ratio = timer_sincr / time_hugScape;

				//Rotamos la capsula
				QUAT rot_guardian = QUAT::Slerp(start_rot_col_guardian, end_rot_col_guardian, ratio);

				VEC3 UP = VEC3(0, 1, 0);
				MAT44 UP_Mat = MAT44::CreateTranslation(UP);
				MAT44 Rot = MAT44::CreateFromQuaternion(rot_guardian);
				MAT44 new_trans = UP_Mat * Rot;
				my_col->controller->setUpDirection(PxVec3(new_trans._41, new_trans._42, new_trans._43));

				VEC3 pos_root_guardian = lerp(start_root_pos_guardian, end_root_pos_guardian, ratio);
				my_skel->setCorrectMoveRoot(pos_root_guardian);

				VEC3 pos_guardian = lerp(start_pos_guardian, end_pos_guardian, ratio);
				my_trans->setPosition(pos_guardian);
				my_col->controller->setFootPosition(PxExtendedVec3(pos_guardian.x, pos_guardian.y, pos_guardian.z));
				CEngine::get().getPhysics().moveEnemy(VEC3::Zero, 0.1, my_col, collision_down);


				if (timer_sincr < 0.5) {
					float ratio_seele = timer_sincr / 0.5;
					VEC3 pos_root_seele = lerp(start_pos_root_seele, end_pos_root_seele, ratio_seele);
					skel_player->setCorrectMoveRoot(pos_root_seele);
				}else if (timer_sincr >= 0.5 && timer_sincr <= 1.0) {
					skel_player->setCorrectRoot(false);
					float max = 1.0 - 0.5;
					float ratio_seele = (timer_sincr - 0.5) / max;
					//Movemos al player
					VEC3 pos_seele = lerp(start_pos_seele, end_pos_seele, ratio_seele);
					trans_player->setPosition(end_pos_seele);
					col_player->controller->setFootPosition(PxExtendedVec3(pos_seele.x, pos_seele.y, pos_seele.z));
				}
			}
			else {
				VEC3 UP = VEC3(0, 1, 0);
				MAT44 UP_Mat = MAT44::CreateTranslation(UP);
				MAT44 Rot = MAT44::CreateFromQuaternion(end_rot_col_guardian);
				MAT44 new_trans = UP_Mat * Rot;
				my_col->controller->setUpDirection(PxVec3(new_trans._41, new_trans._42, new_trans._43));

				my_trans->setPosition(end_pos_guardian);
				my_col->controller->setFootPosition(PxExtendedVec3(end_pos_guardian.x, end_pos_guardian.y, end_pos_guardian.z));
				CEngine::get().getPhysics().moveEnemy(VEC3::Zero, 0.1, my_col, collision_down);

				// Procedimientos para la siguiente fase
				dbg("Iniciamos la resureccion\n");
				float y, p;
				my_trans->getEulerAngles(&y, &p, nullptr);
				y += deg2rad(-180);
				my_trans->setEulerAngles(y, p, 0.f);

				my_skel->setCorrectRoot(true);
				my_skel->clearCorrectRoot();
				start_root_pos_guardian = my_trans->getPosition();
				end_root_pos_guardian = start_root_pos_guardian + my_trans->getFront() * 0.9;
				my_skel->setCorrectMoveRoot(end_root_pos_guardian - start_root_pos_guardian);


				my_anim->removeCurrentAction();
				my_anim->playActionAnim("getUp", true, false, false, false, false);
				timer_animation = 0;

				start_rot_col_guardian = QUAT::CreateFromYawPitchRoll(y, deg2rad(89.f), deg2rad(0.f));
				end_rot_col_guardian = QUAT::CreateFromYawPitchRoll(y, deg2rad(70.f), deg2rad(0.f));

				/*float aux_y, aux_p, aux_r;
				ToEulerAngles(start_rot_col_guardian, &aux_y, &aux_p, &aux_r);
				dbg("Inicio rotacion capsula: Yaw:%.2f Pitch:%.2f Roll:%.2f\n", rad2deg(aux_y), rad2deg(aux_p), rad2deg(aux_r));
				ToEulerAngles(end_rot_col_guardian, &aux_y, &aux_p, &aux_r);
				dbg("Fin rotacion capsula: Yaw:%.2f Pitch:%.2f Roll:%.2f\n", rad2deg(aux_y), rad2deg(aux_p), rad2deg(aux_r));

				VEC3 pos_root_guardian = my_skel->getAbsTraslationBone(0);
				dbg("Root Bone Guardian despues de iniciar animacion [%.3f %.3f %.3f]\n", pos_root_guardian.x, pos_root_guardian.y, pos_root_guardian.z);*/

				skel_player->setCorrectRoot(false);
				skel_player->clearCorrectRoot();

				in_rebirth = true;

				timer_sincr = 0;
				in_HugScape = false;


			}
		}
		ImGui::TreePop();
	}


	if (fall_sinc && !CApplication::get().getInPause()) {
		if (timer_sincr <= time_hugScape) {

			float ratio = timer_sincr / time_hugScape;

			//Rotamos la capsula
			QUAT rot_guardian = QUAT::Slerp(start_rot_col_guardian, end_rot_col_guardian, ratio);

			VEC3 UP = VEC3(0, 1, 0);
			MAT44 UP_Mat = MAT44::CreateTranslation(UP);
			MAT44 Rot = MAT44::CreateFromQuaternion(rot_guardian);
			MAT44 new_trans = UP_Mat * Rot;
			my_col->controller->setUpDirection(PxVec3(new_trans._41, new_trans._42, new_trans._43));
			VEC3 pos_guardian = my_trans->getPosition();
			my_trans->setPosition(pos_guardian);
			my_col->controller->setFootPosition(PxExtendedVec3(pos_guardian.x, pos_guardian.y, pos_guardian.z));

		}
		else {
			/*my_trans->setPosition(end_pos_guardian);
			my_col->controller->setFootPosition(PxExtendedVec3(end_pos_guardian.x, end_pos_guardian.y, end_pos_guardian.z));*/

			//my_skel->setCorrectMoveRoot(end_root_pos_guardian);

			VEC3 UP = VEC3(0, 1, 0);
			MAT44 UP_Mat = MAT44::CreateTranslation(UP);
			MAT44 Rot = MAT44::CreateFromQuaternion(end_rot_col_guardian);
			MAT44 new_trans = UP_Mat * Rot;
			my_col->controller->setUpDirection(PxVec3(new_trans._41, new_trans._42, new_trans._43));

			fall_sinc = false;

			// Procedimientos para la siguiente fase
			dbg("Iniciamos la resureccion\n");
			float y, p;
			my_trans->getEulerAngles(&y, &p, nullptr);
			y += deg2rad(-180);
			my_trans->setEulerAngles(y, p, 0.f);

			my_skel->setCorrectRoot(true);
			my_skel->clearCorrectRoot();
			start_root_pos_guardian = my_trans->getPosition();
			end_root_pos_guardian = start_root_pos_guardian + my_trans->getFront() * 0.9;
			my_skel->setCorrectMoveRoot(end_root_pos_guardian - start_root_pos_guardian);


			my_anim->removeCurrentAction();
			my_anim->playActionAnim("rebirth", true, false, false, false, false);

			start_rot_col_guardian = QUAT::CreateFromYawPitchRoll(y, deg2rad(89.f), deg2rad(0.f));
			end_rot_col_guardian = QUAT::CreateFromYawPitchRoll(y, deg2rad(70.f), deg2rad(0.f));

			float aux_y, aux_p, aux_r;
			ToEulerAngles(start_rot_col_guardian, &aux_y, &aux_p, &aux_r);
			dbg("Inicio rotacion capsula: Yaw:%.2f Pitch:%.2f Roll:%.2f\n", rad2deg(aux_y), rad2deg(aux_p), rad2deg(aux_r));
			ToEulerAngles(end_rot_col_guardian, &aux_y, &aux_p, &aux_r);
			dbg("Fin rotacion capsula: Yaw:%.2f Pitch:%.2f Roll:%.2f\n", rad2deg(aux_y), rad2deg(aux_p), rad2deg(aux_r));

			VEC3 pos_root_guardian = my_skel->getAbsTraslationBone(0);
			dbg("Root Bone Guardian despues de iniciar animacion [%.3f %.3f %.3f]\n", pos_root_guardian.x, pos_root_guardian.y, pos_root_guardian.z);

			in_rebirth = true;

			timer_sincr = 0;
		}
	}

	if (in_rebirth && !CApplication::get().getInPause()) {
		dbg("=================================================================\n");
		dbg("Timer Sincr: %.2f\n", timer_sincr);
		if (timer_sincr <= time_rebirth) {

			setConstrictRender(enemyState::CONSTRICT_GETUP);

			if (timer_sincr >= 0 && timer_sincr <= 1) {
				dbg("Estoy haciendo las cosas entre 0 y 1 segundo\n");

				float ratio = timer_sincr / 1;

				//Elevamos la capsula
				VEC3 pos_guardian = my_trans->getPosition();
				QUAT rot_guardian = QUAT::Slerp(start_rot_col_guardian, end_rot_col_guardian, ratio);

				float aux_y, aux_p, aux_r;
				ToEulerAngles(rot_guardian, &aux_y, &aux_p, &aux_r);
				dbg("Rotacion capsula: Yaw:%.2f Pitch:%.2f Roll:%.2f\n", rad2deg(aux_y), rad2deg(aux_p), rad2deg(aux_r));


				VEC3 UP = VEC3(0, 1, 0);
				MAT44 UP_Mat = MAT44::CreateTranslation(UP);
				MAT44 Rot = MAT44::CreateFromQuaternion(rot_guardian);
				MAT44 new_trans = UP_Mat * Rot;
				my_col->controller->setUpDirection(PxVec3(new_trans._41, new_trans._42, new_trans._43));
				my_trans->setPosition(pos_guardian);
				my_col->controller->setFootPosition(PxExtendedVec3(pos_guardian.x, pos_guardian.y, pos_guardian.z));
				CEngine::get().getPhysics().moveEnemy(VEC3::Zero, 0.1, my_col, collision_down);
			}
			else {
				if (!phase1) {
					dbg("Finalizamos la primera fase\n");
					float y, p;
					my_trans->getEulerAngles(&y, &p, nullptr);

					start_rot_col_guardian = QUAT::CreateFromYawPitchRoll(y, deg2rad(70.f), deg2rad(0.f));
					end_rot_col_guardian = QUAT::CreateFromYawPitchRoll(y, deg2rad(0.f), deg2rad(0.f));
					/*float aux_y, aux_p, aux_r;
					ToEulerAngles(start_rot_col_guardian, &aux_y, &aux_p, &aux_r);
					dbg("Inicio rotacion capsula: Yaw:%.2f Pitch:%.2f Roll:%.2f\n", rad2deg(aux_y), rad2deg(aux_p), rad2deg(aux_r));
					ToEulerAngles(end_rot_col_guardian, &aux_y, &aux_p, &aux_r);
					dbg("Fin rotacion capsula: Yaw:%.2f Pitch:%.2f Roll:%.2f\n", rad2deg(aux_y), rad2deg(aux_p), rad2deg(aux_r));*/

					start_root_pos_guardian = (my_trans->getPosition() + my_trans->getFront() * 0.9) - my_trans->getPosition();
					end_root_pos_guardian = VEC3::Zero;

					phase1 = true;

				}
			}

			if (timer_sincr >= 2.8 && timer_sincr <= 4) {
				dbg("Estoy haciendo las cosas entre los segundos 2.8 y 4 segundo\n");

				float max = 4 - 2.8;
				float ratio = (timer_sincr - 2.8) / max;

				// Elevamos la capsula
				VEC3 pos_guardian = my_trans->getPosition();
				QUAT rot_guardian = QUAT::Slerp(start_rot_col_guardian, end_rot_col_guardian, ratio);

				float aux_y, aux_p, aux_r;
				ToEulerAngles(rot_guardian, &aux_y, &aux_p, &aux_r);
				dbg("Rotacion capsula: Yaw:%.2f Pitch:%.2f Roll:%.2f\n", rad2deg(aux_y), rad2deg(aux_p), rad2deg(aux_r));

				VEC3 UP = VEC3(0, 1, 0);
				MAT44 UP_Mat = MAT44::CreateTranslation(UP);
				MAT44 Rot = MAT44::CreateFromQuaternion(rot_guardian);
				MAT44 new_trans = UP_Mat * Rot;
				my_col->controller->setUpDirection(PxVec3(new_trans._41, new_trans._42, new_trans._43));
				my_trans->setPosition(pos_guardian);
				my_col->controller->setFootPosition(PxExtendedVec3(pos_guardian.x, pos_guardian.y, pos_guardian.z));
				CEngine::get().getPhysics().moveEnemy(VEC3::Zero, 0.1, my_col, collision_down);

				//A�adimos margen de altura al root
				VEC3 pos_root = lerp(start_root_pos_guardian, end_root_pos_guardian, ratio);
				my_skel->setCorrectMoveRoot(pos_root);
			}
			if (timer_sincr > 4) {
				if (!phase2) {
					dbg("Finalizamos la segunda fase\n");
					VEC3 pos_guardian = my_trans->getPosition();

					VEC3 UP = VEC3(0, 1, 0);
					MAT44 UP_Mat = MAT44::CreateTranslation(UP);
					MAT44 Rot = MAT44::CreateFromQuaternion(end_rot_col_guardian);
					MAT44 new_trans = UP_Mat * Rot;
					my_col->controller->setUpDirection(PxVec3(new_trans._41, new_trans._42, new_trans._43));
					my_trans->setPosition(pos_guardian);
					my_col->controller->setFootPosition(PxExtendedVec3(pos_guardian.x, pos_guardian.y, pos_guardian.z));
					CEngine::get().getPhysics().moveEnemy(VEC3::Zero, 0.1, my_col, collision_down);

					my_skel->setCorrectMoveRoot(end_root_pos_guardian);
					phase2 = true;
				}
			}
		}
		else {
			phase1 = phase2 = false;
			timer_sincr = 0;
			in_rebirth = false;
			my_anim->removeCurrentAction();
			not_auto_move = false;
			my_skel->setCorrectRoot(false);
		}
	}



	if (ImGui::TreeNode("Dead Sincr")) {
		static bool dead_sinc = false;

		static VEC3 pos_start = VEC3::Zero;
		static VEC3 pos_root_start = VEC3::Zero;
		static VEC3 pos_end = VEC3::Zero;
		static VEC3 pos_root_end = VEC3::Zero;
		static QUAT rot_start = QUAT(0, 0, 0, 0);
		static QUAT rot_end = QUAT(0, 0, 0, 0);
		static float time_dead = 0.6f;
		static float add_dist = 0.7;
		static float add_root_move = 0.5f;

		ImGui::DragFloat("Time dead", &time_dead, 0.1, 0, 100);
		ImGui::DragFloat("Add Dist", &add_dist, 0.1, 0, 100);
		ImGui::DragFloat("Add root move", &add_root_move, 0.1, 0, 100);
		ImGui::Checkbox("not_auto_move", &not_auto_move);
		ImGui::DragFloat3("Pos Start", &pos_start.x, 0.1, -1000, 1000);
		ImGui::DragFloat3("Pos End", &pos_end.x, 0.1, -1000, 1000);
		ImGui::DragFloat4("Rot Start", &rot_start.x, 0.1, -1000, 1000);
		ImGui::DragFloat4("Rot End", &rot_end.x, 0.1, -1000, 1000);

		if (ImGui::SmallButton("Dead Sincr")) {
			TCompAnimator* anim = get< TCompAnimator>();
			TCompTransform* trans = get<TCompTransform>();
			TCompCollider* col = get<TCompCollider>();
			TCompSkeleton* skel = get<TCompSkeleton>();

			pos_start = trans->getPosition();
			rot_start = trans->getRotation();

			float y, p;
			trans->getEulerAngles(&y, &p, nullptr);

			pos_end = pos_start + trans->getFront() * add_dist;
			pos_end.y += 0.2;

			pos_root_start = VEC3::Zero;
			VEC3 aux = pos_start + trans->getFront() * add_root_move;
			pos_root_end = aux - pos_start;


			rot_end = QUAT::CreateFromYawPitchRoll(y, deg2rad(89.f), 0);

			anim->removeCurrentAction();
			anim->playActionAnim("dead", true, false, true, false, true);

			dead_sinc = true;

			timer_sincr = 0;

			not_auto_move = true;

			skel->setCorrectRoot(true);
			skel->clearCorrectRoot();

		}

		if (dead_sinc) {

			/*if (timer_sincr > 0.6f) {
				TCompTransform* my_trans = get<TCompTransform>();
				TCompCollider* my_col = get<TCompCollider>();

				VEC3 pos_guardian = my_trans->getPosition();
				pos_guardian.y += 0.2;

				my_trans->setPosition(pos_guardian);
				my_col->controller->setFootPosition(PxExtendedVec3(pos_guardian.x, pos_guardian.y, pos_guardian.z));

				dead_sinc = false;

			}*/

			if (timer_sincr <= time_dead) {

				TCompTransform* my_trans = get<TCompTransform>();
				TCompCollider* my_col = get<TCompCollider>();
				TCompSkeleton* skel = get<TCompSkeleton>();

				float ratio = timer_sincr / time_dead;

				VEC3 pos_guardian = lerp(pos_start, pos_end, ratio);
				QUAT rot_guardian = QUAT::Slerp(rot_start, rot_end, ratio);

				my_trans->setPosition(pos_guardian);
				my_trans->setRotation(rot_guardian);
				my_col->controller->setFootPosition(PxExtendedVec3(pos_guardian.x, pos_guardian.y, pos_guardian.z));

				VEC3 UP = VEC3(0, 1, 0);
				MAT44 UP_Mat = MAT44::CreateTranslation(UP);
				MAT44 Rot = MAT44::CreateFromQuaternion(my_trans->getRotation());
				MAT44 new_trans = UP_Mat * Rot;
				my_col->controller->setUpDirection(PxVec3(new_trans._41, new_trans._42, new_trans._43));

				VEC3 pos_root = lerp(pos_root_start, pos_root_end, ratio);

				skel->setCorrectMoveRoot(pos_root);
			}
			else {
				dead_sinc = false;
			}

		}
		ImGui::TreePop();
	}

	if (ImGui::SmallButton("Set Current BTNode")) {
		TCompBehaviourTree* bt = get<TCompBehaviourTree>();
		bt->setBTNode("WaitPlayerDead");
	}

	/*ImGui::Text("Frame enemy %d", frames);
	ImGui::Text("Frame Start Rot180 %d", start_framerotate180);
	ImGui::Text("Frame Stop Rot180 %d", stop_framerotate180);
	ImGui::Text("Frame Start Rot90 %d", start_framerotate90);
	ImGui::Text("Frame Stop Rot90 %d", stop_framerotate90);*/

	/*int i = 0;
	for (auto p : way_points) {
		string wtext = "WayPoint " + to_string(i) + " :";
		ImGui::DragFloat3(wtext.c_str(), &p.x, 0.1, -100, 100);
	}*/
	//ImGui::Text("Suspicius %.2f", suspicius_bar);
	if (suspicius_bar < 30) {
		ProgressBar("Normal       ", suspicius_bar, 0, 100, "%.2f", ImVec2(250, 15), c_noSos, ImVec4(0.758, 0.758, 0.758, 1), ImVec4(0, 0, 0, 0));
	}
	if (suspicius_bar >= 30 && suspicius_bar < 51) {
		ProgressBar("Alert        ", suspicius_bar, 0, 100, "%.2f", ImVec2(250, 15), c_alert, ImVec4(0.758, 0.758, 0.758, 1), ImVec4(0, 0, 0, 0));
	}
	else if(suspicius_bar >=51 && suspicius_bar < 80) {
		ProgressBar("Suspicius", suspicius_bar, 0, 100, "%.2f", ImVec2(250, 15), c_soslv1, ImVec4(0.758, 0.758, 0.758, 1), ImVec4(0, 0, 0, 0));
	}
	else if (suspicius_bar >= 80) {
		ProgressBar("Pursuer", suspicius_bar, 0, 100, "%.2f", ImVec2(250, 15), c_alarm, ImVec4(0.758, 0.758, 0.758, 1), ImVec4(0, 0, 0, 0));
	}


	if (ImGui::SmallButton("Activar")) {
		desactivated = false;
		TCompBehaviourTree* bt = get<TCompBehaviourTree>();
		bt->onActivated(true);
	}

	if (ImGui::SmallButton("Desactivar")) {
		desactivated = true;
		TCompBehaviourTree* bt = get<TCompBehaviourTree>();
		bt->onActivated(false);
	}

	ImGui::Checkbox("Not update", &not_update);

	ImGui::Checkbox("Sin Vista & Oido", &desactivated);
	ImGui::Text("Estado: %s", name_state.c_str());
	ImGui::Checkbox("Te veo!", &vPlayer);
	ImGui::Checkbox("Dentro del cono!", &playerIn);
	ImGui::Checkbox("Inside Cone Capture", &is_in_cone_contact);
	ImGui::Text("Noise detected %.2f", noise_detec);
	ImGui::DragFloat3("My Init Pos", &my_init_pos.x, 0.1, -1000, 1000);
	ImGui::Text("My pos: X:%.2f Y:%.2f Z:%.2f", my_Pos.x, my_Pos.y, my_Pos.z);
	ImGui::Text("Voy hacia: X:%.2f Y:%.2f Z:%.2f", p.x, p.y, p.z);
	float d = VEC3::Distance(my_Pos, p);
	ImGui::LabelText("Distancia hacia donde voy", "%f", d);
	ImGui::Text("Distancia al player %.2f\n", dist_ToPlayer);
	ImGui::Text("Distancia por segundo %.2f\n", distxsec);
	ImGui::Text("Current Velocity %.2f\n", current_velocity);
	ImGui::Text("Last velocity %.2f\n", last_velocity);
	ImGui::Checkbox("Collision Down", &collision_down);

	ImGui::DragFloat("Tope Maximo Suspicius Bar", &tope_suspicius_bar, 0.1, 0, 100);
	ImGui::DragFloat("Suspicius bar", &suspicius_bar, 1, 0, 100);
	ImGui::DragFloat3("Suspicius Point", &pos_suspicius.x, 0.1, -1000, 1000);
	ImGui::DragFloat("Angulo Cono", &angle_detec, 1, 0, 359);
	ImGui::DragFloat("Constante ruido", &constNoiseDetec, 0.1, 1, 100);
	ImGui::Text("Tiempo desde la ultima vez contact player %.2f", timer_contact_player);

	if (ImGui::TreeNode("LOG")) {
		ImGui::Checkbox("Print log Frame a Frame", &printCurrentLog);
		ImGui::Checkbox("notSaveFSMLog", &notSaveFSMLog);
		ImGui::Checkbox("notSaveVarsLog", &notSaveVarsLog);

		if (ImGui::SmallButton("Print All Log")) {
			printAllLog();
		}

		if (ImGui::SmallButton("Write file log")) {
			CEntity* en = CHandle(this).getOwner();
			CApplication::get().writeFile(en->getName(), logEnemy);
		}
		ImGui::TreePop();
	}

	ImGui::Checkbox("Out of Patrol", &outOfPatrol);

	ImGui::Checkbox("Manual Pos Player", &manualPosPlayer);
	if (manualPosPlayer) {
		ImGui::DragFloat3("Pos player", &pos_player.x, 0.1, -1000, 1000);
		ImGui::SameLine();
		if (ImGui::SmallButton("Copy")) {
			pos_player = my_Pos;
		}
	}
	ImGui::DragFloat("Time Waiting Angry", &time_waitAngry, 0.1, 0, 100);
	ImGui::Checkbox("Waiting Angry!", &waiting_angry);
	if (waiting_angry) {
		ImGui::Text("Timer waiting Angry %.2f", timer_waitAngry);
	}
	ImGui::Checkbox("Surrender", &surrender);

	bool rot = isRotating();
	ImGui::Checkbox("Rotating", &rot);
	ImGui::Checkbox("See Anim/Root Movs in Actions", &use_save_anim_movs);

	ImGui::DragFloat3("Manual Point", &manual_point.x, 0.1, -1000, 1000);
	ImGui::SameLine();
	if (ImGui::SmallButton("<-")) {
		manual_point = my_Pos;
	}

	static float step_size = 0.5f;
	static float slope = 0.01f;
	if (ImGui::SmallButton("Calcule path to Manual Point")) {
		navPath = CEngine::get().getNavMesh().calculePath(my_Pos, manual_point, step_size, slope);
	}
	ImGui::DragFloat("Step size", &step_size, 0.1, 0, 100);
	ImGui::DragFloat("Slope", &slope, 0.1, 0, 100);

	static bool debug_hit = false;
	ImGui::DragFloat("Dist Raycast", &dist_ManualRaycast, 0.1, 1, 100);
	if (ImGui::SmallButton("Raycast To ManualPoint")) {
		VEC3 debug_pos = my_Pos;
		debug_pos.y += 0.5;
		debug_hit = manualRaycast(debug_pos, manual_point, dist_ManualRaycast);
	}
	ImGui::Text("Hit Raycast To ManualPoint: %s", debug_hit ? "True" : "False");

	static float random_dist = 1;
	ImGui::DragFloat("Dist Max Random Point", &random_dist);
	ImGui::Checkbox("Using Random WayPoint", &using_rand_wayPoint);
	if (using_rand_wayPoint) {
		if (ImGui::SmallButton("Generate Random Point")) {
			random_way_point = generateRandomPoint(manual_point, random_dist);
		}
	}
	ImGui::DragFloat3("Random WayPoint", &random_way_point.x);

	static bool walkorRunVelocity = true;
	ImGui::Checkbox("Manual Move", &b_manualMove);
	ImGui::Checkbox("Walk or Run Velocity", &walkorRunVelocity);

	if (ImGui::SmallButton("Move to Manual Point")) {
		if (isThereWayNearToPoint(my_Pos, manual_point)) {
			navPath = CEngine::get().getNavMesh().calculePath(my_Pos, manual_point);
			b_manualMove = true;
			if (walkorRunVelocity) {
				last_velocity = walk_velocity;
				current_velocity = walk_velocity;
			}
			else {
				last_velocity = run_velocity;
				current_velocity = run_velocity;
			}			
		}
	}
	/*static float tiempoMuerte = 1;
	static bool applyRot = true;
	static bool inHell = false;
	static bool rebirth = false;
	static bool WhatFuck = false;
	static bool removeDeadLoop = true;*/

	/*ImGui::DragFloat("Tiempo para morir", &tiempoMuerte, 0.1, 0.1, 10, "%.2f");
	if (ImGui::Checkbox("Apply rotation", &applyRot)) {
		TCompSkeleton* skel = get<TCompSkeleton>();
		skel->setApplyActionYawRotation(applyRot);
	}*/

	if (ImGui::TreeNode("Timers")) {
		ImGui::DragFloat("timer_animation", &timer_animation);
		ImGui::DragFloat("timerNewRot", &timerNewRot);
		ImGui::DragFloat("timer_waitingDead", &timer_waitingDead);
		ImGui::DragFloat("timer_delay_dead", &timer_delay_dead);
		ImGui::DragFloat("timer_fall", &timer_fall);
		ImGui::DragFloat("timer_contact_player", &timer_contact_player);
		ImGui::DragFloat("timer_change_suspicius_node", &timer_change_suspicius_node);
		ImGui::DragFloat("timer_waitAnim", &timer_waitAnim);
		ImGui::DragFloat("timer_exitNode", &timer_exitNode);
		ImGui::DragFloat("timer_waitAngry", &timer_waitAngry);
		ImGui::DragFloat("timer_distXsec", &timer_distXsec);
		ImGui::TreePop();
	}


	ImGui::Text("Tiempo: %.2f", tiempo.elapsed());
	//ImGui::Text("Waiting Dead %.2f", timer_waitingDead);
	//ImGui::Text("Delay time %.2f", timer_delay_dead);


	/*if (ImGui::SmallButton("Muerte por columna")) {
		tiempo.reset();
		delay_time_dead = 1.f;
		timer_delay_dead = 0;
		delayDead = true;
	}*/

	if (ImGui::SmallButton("Destroy")) {
		destroyEnemy();
	}

	if (ImGui::SmallButton("Reset")) {
		resetEnemy();
	}

	if (ImGui::TreeNode("Waypoints")) {
		int i = 0;
		for (auto& w : wps) {
			ImGui::PushID(i);			
			CEntity* w_ent = w;
			TCompWaypoint* c_wps = w_ent->get<TCompWaypoint>();
			VEC3 posW = c_wps->getPos();
			ImGui::Text("Waypoint %d", (i+1));
			ImGui::SameLine();
			if (ImGui::DragFloat3("", &posW.x, 0.1, -1000, 1000)) {
				TCompTransform* trans_w = w_ent->get<TCompTransform>();
				trans_w->setPosition(posW);
			}
			ImGui::PopID();
			i++;
		}
		ImGui::TreePop();
	}

	/*if (dead && inHell) {
		if (timer_waitingDead >= rebirthTime) {
			setRebirth();
			rebirth = true;
			inHell = false;
		}
	}

	if (!dead && rebirth) {
		float mitadAnim = anim_wait_time / 2;
		if (removeDeadLoop && timer_waitingDead >= mitadAnim) {
			TCompAnimator* anim = get<TCompAnimator>();
			anim->removeCycleAnim("dead-loop");
			removeDeadLoop = false;
		}
		if (timer_waitingDead >= anim_wait_time) {
			rebirth = false;
			WhatFuck = true;
		}
	}

	if (WhatFuck) {
		prepareLookAt();
		if(turn_timer > anim_wait_time) {
			WhatFuck = false;
		}
	}	*/

	/*
	if (ImGui::SmallButton("Eliminar dead-loop")) {
		TCompAnimator* anim = get<TCompAnimator>();
		anim->removeCycleAnim("dead-loop");
	}*/
	ImGui::NewLine();
	if (ImGui::TreeNode("Fsm Variables")) {

		ImGui::Checkbox("turn180", &turn180);
		ImGui::Checkbox("turn_left", &turn_left);
		ImGui::Checkbox("turn_right", &turn_right);
		ImGui::DragFloat("velocity", &current_velocity, 0.1, 0, 100);
		ImGui::Checkbox("searching", &searching);
		ImGui::Checkbox("player_caught", &player_caught);
		ImGui::Checkbox("collision_down", &collision_down);
		ImGui::Checkbox("looking", &looking);
		ImGui::Checkbox("releaseConstrict", &releaseConstrict);
		ImGui::Checkbox("getUp", &getUp);
		ImGui::Checkbox("constrict", &constrict);
		ImGui::Checkbox("back_start_constrict", &back_start_constrict);
		ImGui::Checkbox("front_start_constrict", &front_start_constrict);
		ImGui::Checkbox("dead", &dead);
		ImGui::Checkbox("chase_player", &chase_player);

		ImGui::TreePop();
	}	


	/*ImGui::DragFloat("Tiempo espera nueva rotacion", &timeToNewRot, 0.1, 0, 10, "%.2f");
	ImGui::Text("Timer nueva rotacion %.2f\n", timerNewRot);*/

	/*ImGui::Text("Distancia escuchado: %.2f\n", hearedDist);
	if (ImGui::SmallButton("Borrar Distancia")) {
		hearedDist = 0;
	}*/
	/*ImGui::DragFloat("Radio", &rad_det_noise, 0.1, 0, 100, "%.2f");
	ImGui::DragFloat("Umbral deteccion", &noise_threshold, 0.1, 0, 100, "%.2f");
	ImGui::DragFloat("Angulo cono", &angle_detec, 1, 0, 360, "%.2f");
	ImGui::DragFloat("Min dist cono", &min_cone_dist, 0.1, 0, 100, "%.2f");
	ImGui::DragFloat("Max dist cono", &max_cone_dist, 0.1, 0, 100, "%.2f");*/

	//ImGui::Text("Punto destino: X:%.2f Y:%.2f Z:%.2f", p.x, p.y, p.z);
	//ImGui::Text("Yaw hasta destino: %.5f", yawToPoint);

	//ImGui::Text("Movimiento Animacion: X:%.5f Y:%.5f Z:%.5f", animDeltaMove.x, animDeltaMove.y, animDeltaMove.z);	
	//ImGui::Text("Yaw Animacion: %.5f", animDeltaYaw);

	//ImGui::Text("Rotate timer: %.3f", rotate_timer);
	//ImGui::Text("Time Walk Time:%1.4f/1.03", time);
	//ImGui::Text("Last point: X:%.2f Y:%.2f Z:%.2f", last_point.x, last_point.y, last_point.z);
	//ImGui::Text("Distance x Anim %.3f", distxsec);

	//ImGui::Checkbox("View Point", &viewPoint);


	/*if (ImGui::SmallButton("Generate Cliff Point")) {
		VEC3 point = CEngine::get().getNavMesh().findNearestPointFilterPoly(myPos, POLYFLAGS_CLIFF);
		CEntity* hpA = getEntityByName("PointA");
		TCompTransform* trans_pa = hpA->get<TCompTransform>();
		trans_pa->setPosition(point);
	}

	if (ImGui::SmallButton("Fool: Go to Point")) {
		b_manualMove = true;
		current_velocity = walk_velocity;
	}

	*/

	//ImGui::DragFloat3("Nav Point", &navPoint.x, 0.1, -1000, 1000);

	/*TCompTransform* trans = get<TCompTransform>();

	if (ImGui::SmallButton("Calcule Nav Path")) {		
		hitWithSomething = CEngine::get().getNavMesh().raycast(trans->getPosition(), randomPoint, hit);
		navPath = CEngine::get().getNavMesh().calculePath(trans->getPosition(), randomPoint);
	}
	ImGui::Checkbox("Hit with something?", &hitWithSomething);

	if (!hitWithSomething) {
		hit = VEC3::Zero;
	}

	static float distToLastPoint = 0;
	*/

	if (navPath.size() > 0) {
		for (int i = 0; i < navPath.size(); i++) {
			ImGui::Text("Punto %d [%.2f, %.2f, %.2f]", i, navPath[i].x, navPath[i].y, navPath[i].z);
		}
		if(ImGui::SmallButton("Remove First Point")){
			navPath.erase(navPath.begin());
		}
	}

	//ImGui::Checkbox("Left Foot in Floor", &rightOrLeftFootInFloor);
	//ImGui::DragFloat("Margen al suelo", &distStepToFloor, 0.01, 0, 100);

	/*ImGui::DragFloat("Distancia Last Point Nav To Random", &distToLastPoint);

	static float maxDist;
	ImGui::DragFloat("Max Dist", &maxDist, 0.1, 0, 100);

	ImGui::DragFloat3("Random Point", &randomPoint.x, 0.1, -1000, 1000);

	if (ImGui::SmallButton("Random Point")) {
		//randomPoint = CEngine::get().getNavMesh().findRandomPointAroundCircle(trans->getPosition(), maxDist);
		randomPoint = generateRandomPoint(trans->getPosition(),maxDist);
	}*/

}

void TCompEnemy3D::load(const json& j, TEntityParseContext& ctx)
{
	max_distance_sound = j.value("max_distance_sound", 20);
	/*if (type == ENEMY_PATROLLER) {
		//Cargamos los WayPoints
		vector<string> auxWps = j["wps"].get<vector<string>>();
		for (auto e : auxWps) {
			addWaypoint(loadVEC3(e));
		}
		current_way_point = 0;
	}
	else {
		guard_point = loadVEC3(j, "guard_point");
	}*/


}

void TCompEnemy3D::update(float dt)
{

	if (dt > 0.2) {
		return;
	}


	if (not_update)	return;

	addStatusLog();


	if (destroyed) {
		//if (!inPause)		updateTimers(dt);
		return;
	}

	if (timer_distXsec >= 1) {
		distxsec = VEC3::Distance(my_Pos, last_point);
		timer_distXsec = 0;
		last_point = my_Pos;
	}

	if (b_manualMove) {
		if (turnIdle) {
			//dbg("Estoy girando en idle\n");
			turnInIdle(dt);
		}
		else {
			if (seekNavPath(dt)) {
				b_manualMove = false;
				current_velocity = 0;
			}
		}

	}

	if (!inPause) {
		getInfoBlackBoard();
		commonsCalcs(dt);
		updateSuspiciusBar(dt);
		updateTimers(dt);
		updateAnimationVars();
	}

	if (suspicius_bar > tope_suspicius_bar) {
		suspicius_bar = tope_suspicius_bar;
	}


}

void TCompEnemy3D::renderDebug()
{
	TCompTransform* my_trans = get<TCompTransform>();

	drawWiredSphere(randomPoint, 0.2, Color::Blue);

	drawWiredSphere(manual_point, 0.1, Color::LimeGreen);

	//Pintamos el manual Raycast
	VEC3 debug_pos = my_Pos;
	debug_pos.y += 0.5;
	VEC3 debug_dir = manual_point - debug_pos;
	debug_dir.Normalize();
	VEC3 debug_destiny = debug_pos + debug_dir * dist_ManualRaycast;
	drawLine(debug_pos, debug_destiny, Color::LimeGreen);


	drawWiredSphere(my_init_pos, 0.2, Color::White);

	VEC3 current_pos = my_Pos;
	current_pos.y += 0.5;

	VEC3 my_front_pos = current_pos + (my_frontRoot * 2);
	drawLine(current_pos, my_front_pos, Color::Grey);
	if (manualMovePos.size() > 0) {
		drawLine(my_front_pos, manualMovePos[0], Color::Grey);
		drawWiredSphere(manualMovePos[0], 0.3, Color::Grey);
	}

	VEC3 my_back_pos = current_pos - (my_frontRoot * 2);
	drawLine(current_pos, my_back_pos, Color::Grey);
	if (manualMovePos.size() > 1) {
		drawLine(my_back_pos, manualMovePos[1], Color::Grey);
		drawWiredSphere(manualMovePos[1], 0.3, Color::Grey);
	}

	VEC3 my_left_pos = current_pos + (my_leftRoot * 2);
	drawLine(current_pos, my_left_pos, Color::Grey);
	if (manualMovePos.size() > 2) {
		drawLine(my_left_pos, manualMovePos[2], Color::Grey);
		drawWiredSphere(manualMovePos[2], 0.3, Color::Grey);
	}

	VEC3 my_right_pos = current_pos - (my_leftRoot * 2);
	drawLine(current_pos, my_right_pos, Color::Grey);
	if (manualMovePos.size() > 3) {
		drawLine(my_right_pos, manualMovePos[3], Color::Grey);
		drawWiredSphere(manualMovePos[3], 0.3, Color::Grey);
	}


	if (hitWithSomething) {
		drawLine(my_posHead, hit, Color::Purple);
		drawWiredSphere(hit, 0.2, Color::Purple);
	}

	//Dibujo del path de nav mesh
	if (navPath.size() > 0) {
		for (int i = 0; i < navPath.size(); i++) {
			drawWiredSphere(navPath[i], 0.2, Color::Green);
		}
	}

	for (auto w : wps) {
		CEntity* e_wp = w;
		TCompWaypoint* c_wp = e_wp->get<TCompWaypoint>();
		if (c_wp != nullptr) {
			drawWiredSphere(c_wp->getPos(), 0.2, Color::Red);
		}
	}

	if (using_rand_wayPoint) {
		drawWiredSphere(random_way_point, 0.2, Color::Orange);
	}

	// ESFERA DISTANCIA RUIDO
	drawWiredSphere(my_trans->getPosition(), rad_det_noise, Color::Blue);

	//Radio maximo de vision
	drawWiredSphere(my_trans->getPosition(), max_cone_dist, Color::White);

	//Radio minimo de vision
	drawWiredSphere(my_trans->getPosition(), min_cone_dist, Color::Red);

	drawWiredSphere(pos_suspicius, 0.2, Color::Purple);

	//drawWiredSphere(pos_player, 0.5, Color::Yellow);

	// LINEA PUNTO DE VISION
	// Si lo veo el raycast va hacia el player, sino, solo al frente del enemigo

	if (player_seen) {
		VEC3 pos = pos_player;
		VEC3 uniDir = pos - my_posHead;
		uniDir.Normalize();
		VEC3 destiny = my_posHead + uniDir * max_cone_dist;
		drawLine(my_posHead, destiny, Color::Red); // Pintamos la linea del raycast
	}
	else if (suspicius_bar > 50) {
		VEC3 pos = pos_suspicius;
		VEC3 uniDir = pos - my_posHead;
		uniDir.Normalize();
		VEC3 destiny = my_posHead + uniDir * max_cone_dist;
		drawLine(my_posHead, destiny, Color::Red); // Pintamos la linea del raycast
	}
	else {
		VEC3 destiny = my_posHead + my_frontHead * max_cone_dist;
		drawLine(my_posHead, destiny, Color::Red); // Pintamos la linea del raycast
	}

	//Dibujo el cono de vision de deteccion
	VEC3 v = my_frontHead;
	float a = deg2rad(angle_detec);

	//Lado izquierdo
	VEC3 vIzq = VEC3(v.x * cos(a) - v.z * sin(a), 0, v.x * sin(a) + v.z * cos(a));
	VEC3 pIzq = my_posHead + vIzq * 100;
	drawLine(my_posHead, pIzq, Color::White);

	//Lado derecho
	VEC3 vDer = VEC3(v.x * cos(-a) - v.z * sin(-a), 0, v.x * sin(-a) + v.z * cos(-a));
	VEC3 pDer = my_posHead + vDer * 100;
	drawLine(my_posHead, pDer, Color::White);


	//Dibujo el cono de captura
	v = my_frontRoot;
	a = deg2rad(angle_to_capture);

	//Lado izquierdo
	vIzq = VEC3(v.x * cos(a) - v.z * sin(a), 0, v.x * sin(a) + v.z * cos(a));
	pIzq = my_Pos + vIzq * 1.5;
	drawLine(my_Pos, pIzq, Color::Orange);

	//Lado derecho
	vDer = VEC3(v.x * cos(-a) - v.z * sin(-a), 0, v.x * sin(-a) + v.z * cos(-a));
	pDer = my_Pos + vDer * 1.5;
	drawLine(my_Pos, pDer, Color::Orange);

	//Pintamos el pie dominante
	/*if (!rightOrLeftFootInFloor) {
		TCompSkeleton* skel = get<TCompSkeleton>();
		VEC3 posLFoot = skel->getAbsTraslationBone(56);
		drawWiredSphere(posLFoot, 0.1, Color::Purple);
	}
	else {
		TCompSkeleton* skel = get<TCompSkeleton>();
		VEC3 posRFoot = skel->getAbsTraslationBone(61);
		drawWiredSphere(posRFoot, 0.1, Color::Purple);
	}*/


}

void TCompEnemy3D::onEntityCreated()
{
	TCompTransform* my_trans = get<TCompTransform>();
	preCalculate(); //Realizamos todos los calculos que no queremos hacer en cada frame

	//Nos registramos en el AI Controller
	CHandle h_aicontroller = getEntityByName("Boss");

	assert(h_aicontroller.isValid());  //Si da error, significa que no se ha metido la entidad Boss en el fichero de los guardianes

	TMsgEnemyRegister msg;
	CHandle my = CHandle(this).getOwner();
	msg.h_sender = my;
	h_aicontroller.sendMsg(msg);

	//Inicializamos la varible walk_velocity de la fsm
	TCompFSM* fsm = get<TCompFSM>();
	if(fsm)	fsm->setVariable("walk_velocity", walk_velocity);

	my_Pos = my_init_pos = my_trans->getPosition();
	my_init_rot = my_trans->getRotation();

	CEntity* ce = my;
	string name = ce->getName();

	addStringLog("- Guardian", name, "creado");

}
void TCompEnemy3D::registerMsgs()
{
	DECL_MSG(TCompEnemy3D, TMsgEnemyDeath, onDeathByOthers);
	DECL_MSG(TCompEnemy3D, TMsgControllersOnContact, onContact);
	DECL_MSG(TCompEnemy3D, TMsgActiveGuardian, onActivated);
	DECL_MSG(TCompEnemy3D, TMsgPlayerRelease, onPlayerRelease);
	DECL_MSG(TCompEnemy3D, TMsgSuspiciusEvent, onSuspiciusEvent);
}

bool TCompEnemy3D::rotateToNoise(float dt)
{
	//return rotate(pos_suspicius, dt);
	return true;
}

bool TCompEnemy3D::nearGuardPoint()
{
	return VEC3::Distance(my_Pos,guard_point) < maxDist_ToGP;
}

bool TCompEnemy3D::victory(float dt)
{
	current_velocity = 0;
	chase_player = false;
	//return rotate(pos_player, dt);
	return true;
}

void TCompEnemy3D::hitPlayer()
{
	/*if (h_OnContact == h_null) {
		TMsgHitPlayer msg;

		msg.h_sender = CHandle(this).getOwner();
		msg.damage = (rand() % 10) + 10;
		msg.force = (rand() % 15) + 5;
		h_OnContact.sendMsg(msg);
		h_OnContact = h_null;
	}*/
}


void TCompEnemy3D::calculeOrbitPoint()
{
	VEC3 player_line = pos_player - guard_point;
	player_line.Normalize();
	orbit_point = guard_point + player_line * maxDist_ToGP;
}

bool TCompEnemy3D::rotateToOrbitPoint(float dt)
{
	//return rotate(orbit_point, dt);
	return true;
}

bool TCompEnemy3D::goToOrbitPoint(float dt)
{

	/*if (VEC3::Distance(myPos, orbit_point) < 1)	return true;

	goToPoint(orbit_point, dt);*/

	return false;

}

void TCompEnemy3D::moveToOrbitPoint(float dt)
{
	//moveToPoint(orbit_point, dt);
}


void TCompEnemy3D::preparePatrol()
{
	if (node_ready)	return;

	addLog("= Prepare Patrol = ");

	VEC3 my_way_pos = getPosWayPoint();

	if (isThereWayNearToPoint(my_Pos, my_way_pos)) {

		addFloat3Log("Puedo llegar hasta mi punto de wayPoint [", my_way_pos, "]");
		//dbg("Puedo llegar hasta mi punto de wayPoint [%.2f %.2f %.2f]\n", my_way_pos.x, my_way_pos.y, my_way_pos.z);

		navPath = CEngine::get().getNavMesh().calculePath(my_Pos, my_way_pos);

		/*for (int i = 0; i < aux.size(); i++) {
			if (i % 2 == 0)	navPath.push_back(aux[i]);
		}*/

		//move_with_navPath = true;

		/*if (!checkIfIdleTurn(navPath[0])) {
			current_velocity = walk_velocity;
		}
		else {
			current_velocity = 0;
		}

		last_velocity = walk_velocity;*/
		last_velocity = walk_velocity;

		node_ready = true;

	}
	else {
		dbg("No puedo llegar a mi punto de patrulla\n");
	}
}

void TCompEnemy3D::prepareLookAt()
{
	if (node_ready)	return;

	addLog("= Prepare Look At = ");

	timer_animation = 0;

	time_animation = getAnimationTime(LOOKAT);

	addFloatLog("Voy a tardar ", time_animation, "s en realizar la animacion");

	looking = true;
	current_velocity = 0;

	node_ready = true;
	/*TCompRender* enemy_rend = get<TCompRender>();
	enemy_rend->activateState(1);*/
}

void TCompEnemy3D::prepareGoNoise()
{
	if (node_ready)	return;

	/*if (prepareGoTo(pos_suspicius)) {
		if (suspicius_bar > 70)	current_velocity = last_velocity = run_velocity;
		else current_velocity = last_velocity = walk_velocity;
	}
	else {
		if (suspicius_bar > 70)	last_velocity = run_velocity;
		else last_velocity = walk_velocity;
		current_velocity = 0;
	}*/

	addLog("= Prepare Go to Noise = ");

	addFloat3Log("Voy hacia el punto de sospecha [", pos_suspicius, "] andando");

	/*if (checkIfIdleTurn(pos_suspicius)) {
		addLog("Antes de ir al punto de sospecha me voy a girar en idle");
		current_velocity = 0;
		last_velocity = walk_velocity;
	}
	else {
		addLog("No me hace falta girar, voy andando hacia el punto de sospecha");
		current_velocity = last_velocity = walk_velocity;
	}*/
	last_velocity = walk_velocity;

	node_ready = true;
	/*TCompRender* enemy_rend = get<TCompRender>();
	enemy_rend->activateState(1);*/
}

void TCompEnemy3D::prepareGoToPlayer()
{
	if (node_ready)	return;

	addLog("= Prepare Go to Player = ");

	/*if (prepareGoTo(pos_player)) {
		current_velocity = run_velocity;
	}
	else {
		current_velocity = 0;
	}*/


	/*if (checkIfIdleTurn(pos_player)) {
		addLog("Antes de ir hacia el player me voy a girar en idle");
		current_velocity = 0;
		last_velocity = run_velocity;
	}
	else {
		addLog("No me hace falta girar, voy corriendo a por el player");
		current_velocity = last_velocity = run_velocity;
	}*/

	last_velocity = run_velocity;

	node_ready = true;
}

void TCompEnemy3D::prepareTryToCatch()
{
	if (node_ready)	return;

	addLog("= Prepare Try to Catch = ");

	/*if (checkIfIdleTurn(pos_player)) {
		addLog("Antes de ir atrapar al player me voy a girar en idle");
		current_velocity = 0;
		last_velocity = run_velocity;
	}
	else {
		addLog("No me hace falta girar voy corriendo a por el player");
		current_velocity = last_velocity = run_velocity;
	}*/

	last_velocity = run_velocity;

	chase_player = true;
	node_ready = true;

}

void TCompEnemy3D::resetPatrol()
{
	addLog("= Reset Patrol = ");

	resetGoTo();
	resetTurn();
	node_ready = false;
}

void TCompEnemy3D::resetGoToNoise()
{
	addLog("= Reset Go to Noise = ");

	resetGoTo();
	resetTurn();
	last_pos_suspicius = VEC3(std::numeric_limits<float>::min(), std::numeric_limits<float>::min(), std::numeric_limits<float>::min());
	randomPoint = VEC3(std::numeric_limits<float>::min(), std::numeric_limits<float>::min(), std::numeric_limits<float>::min());
	node_ready = false;
	outOfPatrol = true;
}

void TCompEnemy3D::resetGoToPlayer()
{
	/*TCompRender* enemy_rend = get<TCompRender>();
	enemy_rend->activateState(2);*/
	addLog("= Reset Go to Player = ");
	resetGoTo();
	resetTurn();
	node_ready = false;
	outOfPatrol = true;
}

void TCompEnemy3D::resetLookAt()
{
	addLog("= Reset Look at = ");
	node_ready = false;
	time_animation = 0;
	looking = false;
	timer_exitNode = 0;
}

void TCompEnemy3D::resetTryToCatch()
{
	addLog("= Reset Try to catch = ");
	resetGoTo();
	node_ready = false;
	outOfPatrol = true;
	chase_player = false;
}

void TCompEnemy3D::resetTryToCatch(bool chase)
{
	addLog("= Reset Try to catch Manual = ");
	resetGoTo();
	node_ready = false;
	outOfPatrol = true;
	chase_player = chase;
}

bool TCompEnemy3D::isOutOfPatrol()
{
	return outOfPatrol;
}

bool TCompEnemy3D::isPlayerClose()
{
	return dist_ToPlayer <= (min_cone_dist);
}

bool TCompEnemy3D::resetWayPoint()
{
	addLog("= Reset WayPoint = ");

	current_velocity = 0;
	
	//Primero probamos a coger el punto mas cercano desde nuestra posicion actual
	current_way_point = getNearWayPoint(my_Pos);
	addIntLog("Mi waypoint desde mi posicion actual es", current_way_point, "");
	if (current_way_point >= 0) {
		navPath = generatePath(my_Pos, getPosWayPoint());
		outOfPatrol = false;
		return true;
	}

	manualMovePos.clear();

	float dist = 0.5;

	//Si no tenemos punto de patrulla desde nuestra posicion, cogemos desde nuestra posicion un poco hacia el frente
	if (current_way_point < 0) {
		VEC3 my_front_pos = my_Pos + (my_frontRoot * dist);
		VEC3 aux_pos = VEC3::Zero;
		bool b = CEngine::get().getPhysics().checkRayWithHit(my_front_pos, VEC3(0, -1, 0), CModulePhysicsSinn::FilterGroup::Floor, 100, aux_pos);
		if (b) {
			my_front_pos = aux_pos;
			manualMovePos.push_back(aux_pos);
			addLog("He usado el hitPoint");
		}
		current_way_point = getNearWayPoint(my_front_pos);
		addIntLog("Mi waypoint desde el front es", current_way_point, "");

		if (current_way_point > 0) {
			navPath = generatePath(my_front_pos, getPosWayPoint());
			return true;
		}
		
	}

	//Si no tenemos punto de patrulla desde nuestra posicion, cogemos desde nuestra posicion un poco hacia atras
	if (current_way_point < 0) {
		VEC3 my_back_pos = my_Pos - (my_frontRoot * dist);
		VEC3 aux_pos = VEC3::Zero;
		bool b = CEngine::get().getPhysics().checkRayWithHit(my_back_pos, VEC3(0, -1, 0), CModulePhysicsSinn::FilterGroup::Floor, 100, aux_pos);
		if (b) {
			my_back_pos = aux_pos;
			manualMovePos.push_back(aux_pos);
			addLog("He usado el hitPoint");
		}
		current_way_point = getNearWayPoint(my_back_pos);
		addIntLog("Mi waypoint desde el back es", current_way_point, "");

		if (current_way_point > 0) {
			navPath = generatePath(my_back_pos, getPosWayPoint());
			return true;
		}

	}

	//Si no tenemos punto de patrulla desde nuestra posicion, cogemos desde nuestra posicion un poco hacia la izquierda
	if (current_way_point < 0) {
		VEC3 my_left_pos = my_Pos + (my_leftRoot * dist);
		VEC3 aux_pos = VEC3::Zero;
		bool b = CEngine::get().getPhysics().checkRayWithHit(my_left_pos, VEC3(0, -1, 0), CModulePhysicsSinn::FilterGroup::Floor, 100, aux_pos);
		if (b) {
			my_left_pos = aux_pos;
			manualMovePos.push_back(aux_pos);
			addLog("He usado el hitPoint");
		}
		current_way_point = getNearWayPoint(my_left_pos);
		addIntLog("Mi waypoint desde el left es", current_way_point, "");

		if (current_way_point > 0) {
			navPath = generatePath(my_left_pos, getPosWayPoint());
			return true;
		}
	}

	//Si no tenemos punto de patrulla desde nuestra posicion, cogemos desde nuestra posicion un poco hacia la izquierda
	if (current_way_point < 0) {
		VEC3 my_right_pos = my_Pos - (my_leftRoot * dist);
		VEC3 aux_pos = VEC3::Zero;
		bool b = CEngine::get().getPhysics().checkRayWithHit(my_right_pos, VEC3(0, -1, 0), CModulePhysicsSinn::FilterGroup::Floor, 100, aux_pos);
		if (b) {
			my_right_pos = aux_pos;
			manualMovePos.push_back(aux_pos);
			addLog("He usado el hitPoint");
		}
		current_way_point = getNearWayPoint(my_right_pos);
		addIntLog("Mi waypoint desde el right es", current_way_point, "");

		if (current_way_point > 0) {
			navPath = generatePath(my_right_pos, getPosWayPoint());
			return true;
		}
	}

	if (current_way_point < 0) {
		addLog("No tengo aun punto de waypoint al que volver");
		return false;
	}



	////Si no existiera ningun waypoint valido entonces generaremos un punto aleatorio al que ir hacer patrulla
	//if (current_way_point < 0) {
	//	point_withOutPath = false;
	//	addLog("No puedo ir a ningun waypoint voy a generar uno aleatorio");
	//	//Si no se genero, lo generamos
	//	for (int i = 0; i < 10; i++) {
	//		VEC3 point = generateRandomPoint(my_Pos, (10-i));
	//		addFloat3Log("Voy a probar a ir al random waypoint [", point, "]");
	//		if (isThereWayNearToPoint(my_Pos, point)) {
	//			random_way_point = point;
	//			using_rand_wayPoint = true;
	//			outOfPatrol = false;
	//			addFloat3Log("Voy al punto aleatorio ", random_way_point , " en vez de ir al waypoint");
	//			return true;
	//		}
	//		else {
	//			if (!isThereObstacleToPoint(my_Pos, point)) {
	//				random_way_point = point;
	//				using_rand_wayPoint = true;
	//				outOfPatrol = false;
	//				point_withOutPath = true;
	//				addFloat3Log("Voy al punto aleatorio ", random_way_point, " en vez de ir al waypoint pero sin path");
	//				return true;
	//			}
	//		}
	//	}
	//	addLog("Tampoco he podido generar uno aleatorio");
	//	return false;
	//}
	//else {
	//	addFloatLog("Puedo llegar hasta el waypoint ", (current_way_point+1), "");
	//	addFloat3Log("WayPoint coords [", getPosWayPoint(), "]");
	//	using_rand_wayPoint = false;
	//	outOfPatrol = false;
	//	return true;
	//}

	return false;
}

void TCompEnemy3D::resetGoBackWpt()
{
	random_way_point = VEC3::Zero;
	using_rand_wayPoint = false;
	point_withOutPath = false;
	surrender = false;
	resetGoTo();
	resetTurn();
	node_ready = false;
}

void TCompEnemy3D::resetGoToInitPoint()
{
	last_velocity = 0;
	current_velocity = 0;
	resetGoTo();
	resetTurn();
	node_ready = false;

}

void TCompEnemy3D::resetConstrict()
{
	node_ready = false;
}

void TCompEnemy3D::resetStartConstrict()
{

	/*current_velocity = 0;
	last_velocity = 0;
	back_start_constrict = true;
	timer_animation = 0;
	time_animation = getAnimationTime(BACK_START_CONSTRICT);*/

	TCompTransform* my_trans = get<TCompTransform>();
	TCompCollider* my_col = get<TCompCollider>();

	my_trans->setPosition(end_pos);
	my_col->controller->setFootPosition(PxExtendedVec3(end_pos.x, end_pos.y, end_pos.z));

	start_pos = VEC3::Zero;
	end_pos = VEC3::Zero;


	node_ready = false;
}

void TCompEnemy3D::resetReleaseConstrict()
{
	TCompTransform* my_trans = get<TCompTransform>();
	TCompCollider* my_col = get<TCompCollider>();
	TCompSkeleton* my_skel = get<TCompSkeleton>();

	VEC3 UP = VEC3(0, 1, 0);
	MAT44 UP_Mat = MAT44::CreateTranslation(UP);
	MAT44 Rot = MAT44::CreateFromQuaternion(end_rot_col);
	MAT44 new_trans = UP_Mat * Rot;
	my_col->controller->setUpDirection(PxVec3(new_trans._41, new_trans._42, new_trans._43));

	my_trans->setPosition(end_pos);
	my_col->controller->setFootPosition(PxExtendedVec3(end_pos.x, end_pos.y, end_pos.z));

	playerRelease = false;
	node_ready = false;
}

void TCompEnemy3D::resetGetUp()
{
	TCompSkeleton* my_skel = get<TCompSkeleton>();
	TCompAnimator* my_anim = get<TCompAnimator>();

	constrict = false;
	back_start_constrict = false;
	front_start_constrict = false;
	releaseConstrict = false;
	getUp = false;
	getUp_phase1 = false;
	getUp_phase2 = false;
	my_skel->setCorrectRoot(false);

	my_anim->removeCurrentAction();
	not_auto_move = false;

	suspicius_bar = alert_value + 10;

	node_ready = false;
}

void TCompEnemy3D::resetWait()
{
	node_ready = false;
}

void TCompEnemy3D::resetVictory()
{
	if (victory_event) {
		victory_event->stop();
		victory_event = nullptr;
		initVictory = false;
	}
}

bool TCompEnemy3D::goToWayPoint(float dt)
{
	if (getDistWayPoint() < 1) return true;

	//Si estamos girandonos bruscamente en el sitio, esperamos a que termine
	if (turnIdle) {
		//dbg("Estoy girando para ir al punto de patrulla\n");
		turnInIdle(dt);
		return false;
	}

	current_velocity = walk_velocity;

	if (!isNearPoint(getPosWayPoint())) {
		VEC3 my_way_pos = getPosWayPoint();
		if (isThereWayToPoint(my_Pos, my_way_pos)) {
			navPath = CEngine::get().getNavMesh().calculePath(my_Pos, my_way_pos);
		}
	}

	return seekNavPath(dt);
}

bool TCompEnemy3D::goToBlockPoint(float dt)
{
	/*int blocker = CAI_BlackBoard::get().whoBlockerIAm(CHandle(this).getOwner());
	VEC3 b_point = CAI_BlackBoard::get().getPosBlockPlace(blocker);

	if (VEC3::Distance(myPos, b_point) < 1)	return true;

	goToPoint(b_point, dt);*/

	return false;
}

bool TCompEnemy3D::goToInitPoint(float dt)
{
	addLog("== Go to Init Point ==");

	if (turnIdle) {
		addLog("Estoy girando en idle para volver a mi punto Inicial");
		turnInIdle(dt);
		return false;
	}

	if (VEC3::Distance(my_Pos, my_init_pos) < 1)	return true;

	//Recalculamos el camino por si no se gener� adecuadamente
	if (!isNearPoint(my_init_pos)) {
		if (isThereWayToPoint(my_Pos, my_init_pos)) {
			navPath = CEngine::get().getNavMesh().calculePath(my_Pos, my_init_pos);
		}
	}

	current_velocity = walk_velocity;
	if (seekNavPath(dt)) {
		addLog("Estaba yendo a mi punto Inicial y ya he terminado");
		return true;
	}
	else {
		addLog("Aun no he llegado hasta mi punto Inicial");
		return false;
	}
}

void TCompEnemy3D::actionStartConstrict()
{

	if (timer_constrict <= 0.3) {
		float ratio = timer_constrict / 0.3;
		VEC3 pos_guardian = lerp(start_pos, end_pos, ratio);

		TCompTransform* my_trans = get<TCompTransform>();
		TCompCollider* my_col = get<TCompCollider>();

		my_trans->setPosition(pos_guardian);
		my_col->controller->setFootPosition(PxExtendedVec3(pos_guardian.x, pos_guardian.y, pos_guardian.z));
	}

}

void TCompEnemy3D::actionReleaseConstrict()
{
	TCompTransform* my_trans = get<TCompTransform>();
	TCompCollider* my_col = get<TCompCollider>();
	TCompSkeleton* my_skel = get<TCompSkeleton>();

	float max = getAnimationTime(RELEASE_CONSCTRICT);
	float ratio = timer_constrict / max;

	VEC3 pos_guardian = lerp(start_pos, end_pos, ratio);
	VEC3 pos_root = lerp(start_root_pos, end_root_pos, ratio);
	QUAT rot_guardian = QUAT::Slerp(start_rot_col, end_rot_col, ratio);

	VEC3 UP = VEC3(0, 1, 0);
	MAT44 UP_Mat = MAT44::CreateTranslation(UP);
	MAT44 Rot = MAT44::CreateFromQuaternion(rot_guardian);
	MAT44 new_trans = UP_Mat * Rot;
	my_col->controller->setUpDirection(PxVec3(new_trans._41, new_trans._42, new_trans._43));

	my_skel->setCorrectMoveRoot(pos_root);

	my_trans->setPosition(pos_guardian);
	my_col->controller->setFootPosition(PxExtendedVec3(pos_guardian.x, pos_guardian.y, pos_guardian.z));

}

void TCompEnemy3D::actionGetUp()
{

	TCompTransform* my_trans = get<TCompTransform>();
	TCompCollider* my_col = get<TCompCollider>();
	TCompSkeleton* my_skel = get<TCompSkeleton>();

	if (timer_constrict >= 0 && timer_constrict <= 1) {
		float ratio = timer_constrict / 1;

		//Elevamos la capsula
		VEC3 pos_guardian = my_trans->getPosition();
		QUAT rot_guardian = QUAT::Slerp(start_rot_col, end_rot_col, ratio);


		VEC3 UP = VEC3(0, 1, 0);
		MAT44 UP_Mat = MAT44::CreateTranslation(UP);
		MAT44 Rot = MAT44::CreateFromQuaternion(rot_guardian);
		MAT44 new_trans = UP_Mat * Rot;
		my_col->controller->setUpDirection(PxVec3(new_trans._41, new_trans._42, new_trans._43));
		my_trans->setPosition(pos_guardian);
		my_col->controller->setFootPosition(PxExtendedVec3(pos_guardian.x, pos_guardian.y, pos_guardian.z));
	}

	if (timer_constrict >= 2.8 && timer_constrict <= 4) {
		if (!getUp_phase1) {
			float y, p;
			my_trans->getEulerAngles(&y, &p, nullptr);

			start_rot_col = QUAT::CreateFromYawPitchRoll(y, deg2rad(70.f), deg2rad(0.f));
			end_rot_col = QUAT::CreateFromYawPitchRoll(y, deg2rad(0.f), deg2rad(0.f));


			start_root_pos = (my_trans->getPosition() + my_trans->getFront() * 0.9) - my_trans->getPosition();
			end_root_pos = VEC3::Zero;

			getUp_phase1 = true;
		}

		float max = 4 - 2.8;
		float ratio = (timer_constrict - 2.8) / max;

		// Elevamos la capsula
		VEC3 pos_guardian = my_trans->getPosition();
		QUAT rot_guardian = QUAT::Slerp(start_rot_col, end_rot_col, ratio);

		VEC3 UP = VEC3(0, 1, 0);
		MAT44 UP_Mat = MAT44::CreateTranslation(UP);
		MAT44 Rot = MAT44::CreateFromQuaternion(rot_guardian);
		MAT44 new_trans = UP_Mat * Rot;
		my_col->controller->setUpDirection(PxVec3(new_trans._41, new_trans._42, new_trans._43));
		my_trans->setPosition(pos_guardian);
		my_col->controller->setFootPosition(PxExtendedVec3(pos_guardian.x, pos_guardian.y, pos_guardian.z));

		//A�adimos margen de altura al root
		VEC3 pos_root = lerp(start_root_pos, end_root_pos, ratio);
		my_skel->setCorrectMoveRoot(pos_root);
	}
	
	if(timer_constrict > 4){
		if (!getUp_phase2) {
			VEC3 pos_guardian = my_trans->getPosition();

			VEC3 UP = VEC3(0, 1, 0);
			MAT44 UP_Mat = MAT44::CreateTranslation(UP);
			MAT44 Rot = MAT44::CreateFromQuaternion(end_rot_col);
			MAT44 new_trans = UP_Mat * Rot;
			my_col->controller->setUpDirection(PxVec3(new_trans._41, new_trans._42, new_trans._43));
			my_trans->setPosition(pos_guardian);
			my_col->controller->setFootPosition(PxExtendedVec3(pos_guardian.x, pos_guardian.y, pos_guardian.z));

			my_skel->setCorrectMoveRoot(end_root_pos);
			getUp_phase2 = true;
		}
	}
}

void TCompEnemy3D::setNextWP()
{
	current_way_point += 1;

	if (current_way_point > wps.size() - 1) {
		current_way_point = 0;
		dbg("Elijo el primer waypoint\n");
	}
	else {
		VEC3 posw = getPosWayPoint();
		dbg("Voy al waypoint[%d] %.2f %.2f %.2f\n", (current_way_point+1), posw.x, posw.y, posw.z);
	}
}

void TCompEnemy3D::sendConstrict()
{
	CHandle hplayer = getEntityByName("Player");

	TMsgConstrict msg;
	msg.h_sender = CHandle(this).getOwner();
	msg.attemptsToFree = (rand() % 20) + 10;
	hplayer.sendMsg(msg);
	playerRelease = false;
	timer_reposition = 0;

}

void TCompEnemy3D::sendPlayerDead()
{
	if (!player_dead) {
		CHandle hplayer = getEntityByName("Player");
		CHandle hBoss = getEntityByName("Boss");

		TMsgDeath msg;
		hplayer.sendMsg(msg);
		hBoss.sendMsg(msg);

		player_dead = true;


	}
}

void TCompEnemy3D::sendGoToCliff()
{
	if (h_OnContact == h_null) {
		TMsgGoToCliff msg;
		msg.h_sender = CHandle(this).getOwner();
		msg.attemptsToFree = (rand() % 20) + 10;
		h_OnContact.sendMsg(msg);
	}
}

void TCompEnemy3D::sendThrowPlayer()
{
	TCompTransform* t_enemy = get<TCompTransform>();

	if (h_OnContact == h_null) {
		TMsgThrowPlayer msg;
		msg.h_sender = CHandle(this).getOwner();
		msg.dir = t_enemy->getFront();
		msg.force = (rand() % 10) + 10;
		h_OnContact.sendMsg(msg);
	}
}

bool TCompEnemy3D::isDead()
{
	return dead;
}

bool TCompEnemy3D::isDestroyed()
{
	return destroyed;
}

VEC3 TCompEnemy3D::getMyPos()
{
	return my_Pos;
}

bool TCompEnemy3D::getPlayerSeen()
{
	return player_seen || dist_ToPlayer <= min_cone_dist;
}

float TCompEnemy3D::getDistToPlayer()
{
	return dist_ToPlayer;
}

bool TCompEnemy3D::isType(int t)
{
	return type == t;
}

bool TCompEnemy3D::isNormal()
{
	return suspicius_bar < alert_value && !player_seen /*&& !player_seen_far*/;
}

bool TCompEnemy3D::isAlert()
{
	return suspicius_bar >= alert_value && suspicius_bar < suspicius_value && !player_seen /*&& !player_seen_far*/;
}

bool TCompEnemy3D::isSuspicius()
{
	return suspicius_bar >= suspicius_value && suspicius_bar < alarm_value && !player_seen /*&& !player_seen_far*/;
}

bool TCompEnemy3D::isAlarm()
{
	return suspicius_bar >= alarm_value && !getPlayerSeen() /*&& !player_seen_far*/;
}

float TCompEnemy3D::getSuspiciusBarValue()
{
	return suspicius_bar;
}

bool TCompEnemy3D::isSearching()
{
	return searching;
}

bool TCompEnemy3D::isRotating()
{
	return turnIdle || turnMovement;
}

int TCompEnemy3D::getType()
{
	return type;
}

bool TCompEnemy3D::isOnContactWithPlayer()
{
	addLog("= Is On Contact With Player =");


	bool contac = contact_with_player && timer_contact_player >= time_contact_player;

	if (contac) {
		addLog("He estado el suficiente tiempo en contacto con el player para poder capturarlo");
		return true;
	}
	else {
		addLog("Aun no he estado el suficiente tiempo en contacto con el player para poder capturarlo");
		return false;
	}

}

bool TCompEnemy3D::goToPlayer(float dt)
{
	addLog("== Go to Player ==");

	if (turnIdle) {
		addLog("Estoy girando en idle para ir a por el player");
		turnInIdle(dt);
		return false;
	}

	if (turnMovement) {
		addLog("Estoy girando en movimiento para ir a por el player");
		turnInMovement(dt);
		return false;
	}

	current_velocity = run_velocity;
	if (seekNavPath(dt)) {
		addLog("Estaba yendo a por el player pero ya no puedo llegar");
		return true;
	}
	else {
		addLog("Aun no he llegado hasta el player");
		return false;
	}
	
}


bool TCompEnemy3D::goToTryToCatch(float dt)
{

	addLog("== Go to try catch ==");

	/*if (turnIdle) {
		addLog("Estoy girando en idle para ir a por el player");
		turnInIdle(dt);
		return false;
	}

	if (turnMovement) {
		addLog("Estoy girando en movimiento para ir a por el player");
		turnInMovement(dt);
		return false;
	}

	current_velocity = run_velocity;
	chase_player = true;
	addLog("Estoy yendo a intentar atrapar al player");
	goToPoint(pos_player, dt);

	if (isNotDisplacement()) {
		return true;
	}

	return false;*/

	//Copia del Go To Player ya que ahora queremos que SIEMPRE siga la navmesh

	if (turnIdle) {
		addLog("Estoy girando en idle para ir a por el player");
		turnInIdle(dt);
		return false;
	}

	if (turnMovement) {
		addLog("Estoy girando en movimiento para ir a por el player");
		turnInMovement(dt);
		return false;
	}

	current_velocity = run_velocity;
	if (seekNavPath(dt)) {
		addLog("Estaba yendo a por el player pero ya no puedo llegar");
		return true;
	}
	else {
		addLog("Aun no he llegado hasta el player");
		return false;
	}

}

bool TCompEnemy3D::goToNoise(float dt)
{
	/*TCompRender* enemy_rend = get<TCompRender>();
	enemy_rend->activateState(2);*/
	if (VEC3::Distance(my_Pos, pos_suspicius) < 1) return true;

	//Si estamos girandonos bruscamente en el sitio, esperamos a que termine
	if (turnIdle) {
		turnInIdle(dt);
		return false;
	}

	current_velocity = walk_velocity;
	/*if (suspicius_bar > 70)	current_velocity = run_velocity;
	else current_velocity = walk_velocity;*/

	//Si no podemos llegar al punto nos quedamos parados
	/*if (!checkNeedNavMesh(pos_suspicius)) {
		last_velocity = current_velocity;
		current_velocity = 0;
		return false;
	}*/

	//Si ya hemos llegado al punto final del path, devolvemos true
	return seekNavPath(dt);

	/*if (move_with_navPath)	seekNavPath(dt);
	else goToPoint(pos_suspicius, dt);*/

	//return false;
}

bool TCompEnemy3D::goToBackWpt(float dt)
{
	return false;
}

bool TCompEnemy3D::checkNeedNavMesh(VEC3 point)
{
	VEC3 hit;
	//Si el raycast es true, significa 
	if (CEngine::get().getNavMesh().raycast(my_Pos, point, hit)) {
		navPath = CEngine::get().getNavMesh().calculePath(my_Pos, point);
		if (navPath.empty()) {
			//navPath.push_back(my_Pos);
			return false;
		}
		else {
			if (navPath.size() > 2)	navPath.erase(navPath.begin());
		}
		//move_with_navPath = true;
		return true;
	}
	else {
		//move_with_navPath = false;
		return true;
	}

}

bool TCompEnemy3D::seekNavPath(float dt)
{
	addLog("Vamos a comprobar el NavPath");	
	if (navPath.size() > 0) {
		addLog("Aun queda camino");
		//move_with_navPath = true;
		if(VEC3::Distance(my_Pos, navPath[0]) > 1){
			addLog("La distancia al punto actual es mayor que 1");
			goToPoint(navPath[0], dt);
			return false;
		}
		else {
			addLog("La distancia al ultimo punto ya es menor que 1, asi que elimino este punto");
			navPath.erase(navPath.begin());

			if (navPath.size() > 0) {
				addLog("Como aun tengo puntos, voy al siguiente");
				goToPoint(navPath[0], dt);
				return false;
			}
			else {
				addLog("Ya no me quedan puntos al que ir");
				return true;
			}
			//else move_with_navPath = false;
		}
	}
	else {
		addLog("El navpath esta vacio");
		return true;
	}

	//return move_with_navPath;
}

void TCompEnemy3D::moveToBlockPoint(float dt)
{
	int blocker = CAI_BlackBoard::get().whoBlockerIAm(CHandle(this).getOwner());
	VEC3 b_point = CAI_BlackBoard::get().getPosBlockPlace(blocker);

	moveToPoint(b_point, dt);

}

bool TCompEnemy3D::checkGoToCliff()
{
	VEC3 point = CEngine::get().getNavMesh().findNearestPointFilterPoly(my_Pos, POLYFLAGS_CLIFF);

	if (VEC3::Distance(my_Pos, point) > max_dist_cliff) return false;
	else return true;
}

bool TCompEnemy3D::canDestroyEnemy()
{
	return !waitingDead;
}

void TCompEnemy3D::resetEnemy()
{
	dbg("Me han reseteado\n");

	TCompCollider* col = get<TCompCollider>();
	TCompAnimator* anim = get<TCompAnimator>();
	TCompFSM* fsm = get<TCompFSM>();
	TCompTransform* my_trans = get<TCompTransform>();
	TCompSkeleton* my_skel = get<TCompSkeleton>();


	/////////////////////////////////
	//			VARIABLES
	// - Variables de animaciones
	animDeltaMove = VEC3::Zero;
	animDeltaYaw = 0;

	if(fsm)		fsm->changeState("idle");
	resetFSM();

	// - Variables del player
	pos_player = CAI_BlackBoard::get().getPosPlayer();
	dir_player = VEC3::Zero;
	player_caught = false;

	// - Variables basicas
	floor_value = 0;
	my_Pos = my_init_pos;
	my_posHead = VEC3::Zero;
	my_frontHead = VEC3::Zero;
	my_frontRoot = VEC3::Zero;
	my_leftRoot = VEC3::Zero;
	my_leftHead = VEC3::Zero;
	my_upHead = VEC3::Zero;
	h_OnContact = h_null;
	dist_ToPlayer = VEC3::Distance(my_Pos, pos_player);
	current_velocity = 0.f;
	last_velocity = 0;
	node_ready = false;

	// - Variables de muerte
	dead = false;
	destroyed = false;
	collision_down = true;
	timer_fall = 0;
	falling = false;
	waitingDead = false;
	delayDead = false;
	delay_time_dead = 0;
	timer_waitingDead = 0;
	timer_delay_dead = 0;

	// - Variables de la rotacion
	timer_animation = 0;
	time_animation = 0;
	timerNewRot = 0;
	searching = false;
	looking = false;
	turnIdle = false;
	turnMovement = false;
	init_rot_turn = QUAT(0, 0, 0, 0);
	stopping_of_turning = false;
	timer_stopTurn = 0.f;

	//Variables del GateKeeper
	orbit_point = VEC3::Zero;

	//Variables del Patroller
	outOfPatrol = false;
	current_way_point = 0;
	random_way_point = VEC3(std::numeric_limits<float>::min(), std::numeric_limits<float>::min(), std::numeric_limits<float>::min());
	using_rand_wayPoint = false;
	point_withOutPath = false;

	///////////////////////////////////////////////////////////
	// - Variables para la barra de sospechas
	player_seen = false;
	//player_seen_far = false;
	suspicius_bar = 0.f;
	pos_suspicius = VEC3::Zero;
	contact_with_player = false;
	timer_contact_player = 0;
	distxsec = 0;
	timer_change_suspicius_node = 0;
	timer_goToNoise = 0;
	last_pos_suspicius = VEC3(std::numeric_limits<float>::min(), std::numeric_limits<float>::min(), std::numeric_limits<float>::min());
	timer_exitNode = 0;
	randomPoint = VEC3(std::numeric_limits<float>::min(), std::numeric_limits<float>::min(), std::numeric_limits<float>::min());
	timer_waitAngry = 0;
	waiting_angry = false;
	not_displacement = false;
	surrender = false;

	//Variables de la NavMesh
	navPath.clear();
	//move_with_navPath = false;

	//Variables de contacto con el player
	contact_with_player = false;
	timer_contact_player = 0;
	frames_to_remove_contact = 1;
	is_in_cone_contact = false;
	constrict = false;
	back_start_constrict = false;
	front_start_constrict = false;
	releaseConstrict = false;
	getUp = false;
	player_dead = false;
	timer_constrict = 0;
	timer_reposition = 0;
	not_auto_move = false;
	getUp_phase1 = false;
	getUp_phase2 = false;

	// - Variables de debugger
	inPause = false;
	p = VEC3::Zero;  
	yawToPoint = 0;
	last_point = VEC3::Zero;
	tiempo.reset();
	b_manualMove = false;
	noise_detec = 0;
	vPlayer = false;
	playerIn = false;


	//Reseteo la posicion
	VEC3 UP = VEC3(0, 1, 0);
	MAT44 UP_Mat = MAT44::CreateTranslation(UP);
	MAT44 Rot = MAT44::CreateFromQuaternion(my_init_rot);
	MAT44 new_trans = UP_Mat * Rot;
	my_trans->setRotation(my_init_rot);
	col->controller->setUpDirection(PxVec3(new_trans._41, new_trans._42, new_trans._43));
	my_trans->setPosition(my_Pos);
	col->controller->setFootPosition(PxExtendedVec3(my_Pos.x, my_Pos.y, my_Pos.z));

	//Borro desplazamiento extra en el root_bone
	my_skel->clearCorrectRoot();
	my_skel->setCorrectRoot(false);

	//Elimino todas las animaciones
	anim->resetAllAnims();

	resetBT();

	updateAnimationVars();	

}

void TCompEnemy3D::getInfoBlackBoard()
{
	addLog(" = Get Info BlackBoard = ");

	if(!manualPosPlayer) pos_player = CAI_BlackBoard::get().getPosPlayer();
	dir_player = CAI_BlackBoard::get().getDirPlayer();
	//noise_player = CAI_BlackBoard::get().getNoisePlayer();
	player_caught = CAI_BlackBoard::get().isPlayerCaught();
	vel_player = CAI_BlackBoard::get().getVelocityPlayer();

	addFloat3Log("Posicion Player: [", pos_player, "]");
	addFloatLog("Velocidad Player:", vel_player, "");
}

void TCompEnemy3D::preCalculate()
{
	//Precalculamos las dos distancias que vamos a utilizar para comparar con la distancia al player
	// para saber que porcentaje de la barra subira
	// V1 es la maxima distancia
	// V2 es la distancia al 70%=porc1 del total
	// V3 es la distancia al 30%=porc2 del total
	// V4 es la minima distancia
	float v1, v2, v3, v4;
	int porc1=70, porc2=30;
	v1 = max_cone_dist;	
	v4 = min_cone_dist;
	v2 = ((v1 - v4) * porc1 / 100) + v4;
	v3 = ((v1 - v4) * porc2 / 100) + v4;
	view_compare_dist.push_back(v1);
	porc_values_view.push_back(2);  //Aumentara la barra un 2% por segundo
	view_compare_dist.push_back(v2);
	porc_values_view.push_back(5);  //Aumentara la barra un 5% por segundo
	view_compare_dist.push_back(v3);
	porc_values_view.push_back(10);  //Aumentara la barra un 10% por segundo
	view_compare_dist.push_back(v4);
 
}

void TCompEnemy3D::commonsCalcs(float dt)
{
	addLog("== Common Calcs ==");
	TCompTransform* my_trans = get<TCompTransform>();
	TCompSkeleton* skel = get<TCompSkeleton>();

	my_frontHead = skel->getFrontBoneHead();
	my_leftHead = skel->getLeftBoneHead();
	my_upHead = skel->getUpBoneHead();
	my_posHead = skel->getPosBoneHead();
	my_posHead.y += constConeVision;
	VEC3 old_pos = my_Pos;
	my_Pos = my_trans->getPosition();
	my_frontRoot = my_trans->getFront();
	my_leftRoot = my_trans->getLeft();
	addFloatLog("Me he movido", VEC3::Distance(old_pos, my_Pos), "desde el frame anterior");
	//dbg("Mi posicion actual es X:%.2f X:%.2f X:%.2f \n", my_Pos.x, my_Pos.y, my_Pos.z);
	dist_ToPlayer = VEC3::Distance(my_Pos, pos_player);

	//Actualizamos el movimiento y rotacion proviniente de la animacion

	animDeltaMove = skel->getDeltaMove();
	animDeltaYaw = skel->getDeltaYaw();
	animDeltaPitch = skel->getDeltaPitch();
	addFloat3Log("Obtengo el vector", animDeltaMove, "para movimiento por animacion");
	addFloatLog("Obtengo", rad2deg(animDeltaYaw), "� para girar en yaw por animacion");
	addFloatLog("Obtengo", rad2deg(animDeltaPitch), "� para girar en pitch por animacion");

	if (collision_down)	floor_value = my_Pos.y;

	if(!destroyed && !not_auto_move)	CEngine::get().getPhysics().moveEnemy(VEC3::Zero, dt, get<TCompCollider>(), collision_down);

	checkDeadStuffs();

	calculeLastFootInFloor();

}

//void TCompEnemy3D::updateSuspiciusBar(float dt)
//{
//	addLog("== UPDATE SUSPICIUS BAR ==");
//
//	//Si estamos muertos ni vemos ni oimos
//	if (dead || desactivated)	return;
//
//	bool headOrViewPlayer = false;
//	contact_with_player = false;
//	player_seen = false;
//	/*float noise = heardPlayer();
//	float distViewPlayer = viewPlayer();*/
//
//
//	if (checkContactPlayer(dt)) {
//		suspicius_bar = max_value_suspicius;
//		player_seen = true;
//		//return;
//	}
//
//	// Veo al player?
//	if (distViewPlayer > 0) {
//		headOrViewPlayer = true;
//		if (distViewPlayer < min_cone_dist) {
//			suspicius_bar = max_value_suspicius;
//			player_seen = true;
//		}
//		else {
//			//player_seen_far = true;
//			if (distViewPlayer < view_compare_dist[0] && distViewPlayer >= view_compare_dist[1]) suspicius_bar += porc_values_view[0] * dt;
//			if (distViewPlayer < view_compare_dist[1] && distViewPlayer >= view_compare_dist[2]) suspicius_bar += porc_values_view[1] * dt;
//			if (distViewPlayer < view_compare_dist[2] && distViewPlayer >= view_compare_dist[3]) suspicius_bar += porc_values_view[2] * dt;
//		}
//	}
//
//	//Oigo al player
//	if (noise > 0) {
//		headOrViewPlayer = true;
//		suspicius_bar += noise * 50 * dt * constNoiseDetec; // La constance 50 es porque el maximo de ruido 1 sube la barra un 50% de golpe
//	}
//
//	//Si no lo he escuchado ni lo he visto, reduzco la barra
//	if (!headOrViewPlayer) {
//		suspicius_bar -= (max_value_suspicius / time_to_reduce_suspicius) * dt;
//		if (suspicius_bar < 0)	suspicius_bar = 0;
//	}
//	
//	if (suspicius_bar > max_value_suspicius)	suspicius_bar = max_value_suspicius;
//
//
//}


/*float TCompEnemy3D::viewPlayer()
{
	addLog("== View Player ==");

	//Si la distancia es mayor que nuestra vision, devolvemos 0 porque no podemos verlo
	if (dist_ToPlayer > max_cone_dist) {
		addLog("El player esta fuera de mi rango de vision");
		return 0.f;
	}

	vPlayer = false;
	playerIn = false;

	TCompTransform* my_trans = get<TCompTransform>();
	float half_cone = deg2rad(angle_detec);

	if (isInHeadCone(half_cone)) {
		playerIn = true;

		addLog("El player esta dentro de mi cono de vision");

		//Calculamos el origen del raycast
		VEC3 origin = my_posHead;
		//origin.y += 1; // Pongo la "y" a 1 para que no lance el rayo desde el suelo, sino el centro del cuerpo

		// Calculamos la direccion hacia donde esta el player
		VEC3 copy_pos_player = pos_player;
		copy_pos_player.y += 1; // Pongo la "y" a 1 para que el rayo no vaya hacia el suelo, sino el centro del cuerpo
		VEC3 uniDir = copy_pos_player - origin;
		uniDir.Normalize();  // Normalizamos la direccion para usarla en el raycast
		bool v = CEngine::get().getPhysics().checkRay(origin, uniDir, CModulePhysicsSinn::FilterGroup::World ^ CModulePhysicsSinn::FilterGroup::Enemy_3D, dist_ToPlayer*2);

		if (v) {
			addLog("Tengo linea directa con el Player");
			vPlayer = true;
			pos_suspicius = pos_player;
			return dist_ToPlayer;
		}
		else {
			addLog("No tengo linea directa con el player, algo me obstaculiza");
			return 0.f;
		}

	}
	else {
		addLog("El player no esta dentro de mi cono de vision");
		return 0.f;
	}
}*/


/*float TCompEnemy3D::heardPlayer()
{
	addLog("== Heard Player ==");

	//Comprobamos si hay eventos de ruidos de sospecha
	if (suspicius_events.size() == 0)	return 0.f;

	suspicius_noise sn = calculateSuspiciusNoise();
	//Primero compruebo la intensidad del ruido
	if (sn.intensity < 0.01)	return 0.f;

	addFloatLog("He escuchado", sn.intensity, " de ruido");

	//Despues compruebo que el evento este dentro de la burbuja de deteccion de ruido del enemigo
	float dist_toEvent = VEC3::Distance(my_Pos, sn.position);
	if (dist_toEvent > rad_det_noise)	return 0.f;

	//Calculamos el ruido que le llega al enemigo
	noise_detec = noise_detec = ((sn.intensity * constant_noise) / (dist_toEvent));

	//Si el ruido supera el umbral del enemigo, devolvemos lo que hemos escuchado, sino, devolvemos 0
	if (noise_detec > noise_threshold) {
		addLog("El ruido detectado es mayor que el umbral");
		pos_suspicius = sn.position;
		return noise_detec;
	}
	else return 0.f;
}*/

void TCompEnemy3D::updateSuspiciusBar(float dt)
{

	addLog("== UPDATE SUSPICIUS BAR ==");
	
	//Si estamos muertos ni vemos ni oimos
	if (dead || desactivated)	return;
	
	bool headOrViewPlayer = false;
	player_seen = false;
	/*float noise = heardPlayer();
	float distViewPlayer = viewPlayer();*/
	
	
	if (checkContactPlayer(dt)) {
		suspicius_bar = max_value_suspicius;
		player_seen = true;
		return;
	}

	if (seePlayer(dt)) {
		headOrViewPlayer = true;
		return;
	}

	if (hearSomething(dt)) {
		headOrViewPlayer = true;
		return;
	}
		
	//Si no lo he escuchado ni lo he visto, reduzco la barra
	if (!headOrViewPlayer) {
		suspicius_bar -= (max_value_suspicius / time_to_reduce_suspicius) * dt;
		if (suspicius_bar < 0)	suspicius_bar = 0;
	}
		
	if (suspicius_bar > max_value_suspicius)	suspicius_bar = max_value_suspicius;

}

bool TCompEnemy3D::seePlayer(float dt)
{
	addLog("== View Player ==");

	//Si la distancia es mayor que nuestra vision, devolvemos 0 porque no podemos verlo
	if (dist_ToPlayer > max_cone_dist) {
		addLog("El player esta fuera de mi rango de vision");
		return false;
	}

	vPlayer = false;
	playerIn = false;

	TCompTransform* my_trans = get<TCompTransform>();
	float half_cone = deg2rad(angle_detec);

	if (isInHeadCone(half_cone)) {
		playerIn = true;

		addLog("El player esta dentro de mi cono de vision");

		//Calculamos el origen del raycast
		VEC3 origin = my_posHead;
		//origin.y += 1; // Pongo la "y" a 1 para que no lance el rayo desde el suelo, sino el centro del cuerpo

		// Calculamos la direccion hacia donde esta el player
		VEC3 copy_pos_player = pos_player;
		copy_pos_player.y += 1; // Pongo la "y" a 1 para que el rayo no vaya hacia el suelo, sino el centro del cuerpo
		VEC3 uniDir = copy_pos_player - origin;
		uniDir.Normalize();  // Normalizamos la direccion para usarla en el raycast
		bool v = CEngine::get().getPhysics().checkRay(origin, uniDir, CModulePhysicsSinn::FilterGroup::World ^ CModulePhysicsSinn::FilterGroup::Enemy_3D, dist_ToPlayer * 2);

		if (v) {
			addLog("Tengo linea directa con el Player");
			vPlayer = true;
			pos_suspicius = pos_player;

			if (dist_ToPlayer < min_cone_dist) {
				suspicius_bar = max_value_suspicius;
				player_seen = true;
			}
			else {
				//player_seen_far = true;
				if (dist_ToPlayer < view_compare_dist[0] && dist_ToPlayer >= view_compare_dist[1]) suspicius_bar += porc_values_view[0] * dt;
				if (dist_ToPlayer < view_compare_dist[1] && dist_ToPlayer >= view_compare_dist[2]) suspicius_bar += porc_values_view[1] * dt;
				if (dist_ToPlayer < view_compare_dist[2] && dist_ToPlayer >= view_compare_dist[3]) suspicius_bar += porc_values_view[2] * dt;
			}

			return true;
		}
		else {
			addLog("No tengo linea directa con el player, algo me obstaculiza");
			return false;
		}

	}
	else {
		addLog("El player no esta dentro de mi cono de vision");
		return false;
	}
}

bool TCompEnemy3D::hearSomething(float dt)
{
	addLog("== Heard Something ==");

	//Comprobamos si hay eventos de ruidos de sospecha
	if (suspicius_events.size() == 0)	return false;

	suspicius_noise sn = calculateSuspiciusNoise();
	//Primero compruebo la intensidad del ruido
	if (sn.intensity < 0.01)	return false;

	addFloatLog("He escuchado", sn.intensity, " de ruido");

	//Despues compruebo que el evento este dentro de la burbuja de deteccion de ruido del enemigo
	float dist_toEvent = VEC3::Distance(my_Pos, sn.position);
	if (dist_toEvent > rad_det_noise)	return false;

	//Calculamos el ruido que le llega al enemigo
	noise_detec = ((sn.intensity * constant_noise) / (dist_toEvent));

	//Si el ruido supera el umbral del enemigo, devolvemos lo que hemos escuchado, sino, devolvemos 0
	if (noise_detec > noise_threshold) {
		addLog("El ruido detectado es mayor que el umbral");
		pos_suspicius = sn.position;

		switch (sn.type)
		{
		case suspicius_noise_type::PLAYER_NOISE:
			suspicius_bar += noise_detec * 50 * dt * constNoiseDetec; // La constance 50 es porque el maximo de ruido 1 sube la barra un 50% de golpe
			break;
		case suspicius_noise_type::STONE_NOISE:
			suspicius_bar = suspicius_value + 10;
			break;
		}

	}
	else return false;
}

suspicius_noise TCompEnemy3D::calculateSuspiciusNoise()
{
	int ind = 0;
	float pmax = -1000000;
	suspicius_noise sn;

	for (int i = 0; i < suspicius_events.size(); i++) {
		float dist = VEC3::Distance(my_Pos, suspicius_events[i].position);
		float p = suspicius_events[i].intensity * 2 + dist * -1;
		dbg("Evento [%d] -[Int:%.2f, Dist:%.2f ", i, suspicius_events[i].intensity, dist);
		switch (suspicius_events[i].type)
		{
		case suspicius_noise_type::PLAYER_NOISE:
			dbg("Tipo: Player]");
			break;
		case suspicius_noise_type::STONE_NOISE:
			dbg("Tipo: Stone]");
			break;
		default:
			dbg("Tipo: Uknown]");
			break;
		}
		dbg("-> Puntuacion de sospecha: %.2f\n", p);
		if (p > pmax) {
			dbg("P[%.2f] es mayor que PMax[%.2f]\n", p, pmax);
			pmax = p;
			ind = i;
		}
		else {
			dbg("P[%.2f] es menor que PMax[%.2f]\n", p, pmax);
		}
	}

	sn = suspicius_events[ind];
	dbg("Se mandara el evento %d\n", ind);

	suspicius_events.clear();
	return sn;
}

void TCompEnemy3D::checkDeadStuffs()
{
	isDeathByFall(); //Comprobamos si hemos muerto por caida
	isWaitingToDie(); //Comprobamos si estamos esperando a morir (un delay para sincronizar)
}

void TCompEnemy3D::isDeathByFall()
{
	//Si estamos en el aire...
	if (!collision_down) {
		//Si acabamos de empezar a caer...
		if (!falling) {
			//dbg("Frame %d-> No estoy tocando el suelo\n", getFrameCount());
			falling = true;
			timer_fall = 0;
		}
		//Si ya llevamos bastantes segundos cayendo...
		else if (timer_fall > time_falling_before_dead) {
			//dbg("Frame %d-> No estoy tocando el suelo y han pasado 5 segundos\n", getFrameCount());
			destroyEnemy();
			falling = false;
		}
	}
	//Si estamos tocando el suelo...
	else {
		//Si estabamos cayendo y ademas el tiempo de caida es mayor de 1 (no es un peque�o desnivel)
		if (falling && timer_fall >= 1) {
			//dbg("Frame %d-> Ya estoy tocando el suelo, pero ha sido una caida de 1 segundo\n", getFrameCount());
			setDead();
			destroyed = true;
			falling = false;
		}
		else {
			//Aqui no pasa nada, porque estamos en el suelo y en ningun momento estamos cayendo
			timer_fall = 0;
		}

	}
}

void TCompEnemy3D::isWaitingToDie()
{
	if (delayDead) {
		if (timer_delay_dead >= delay_time_dead) {
			delayDead = false;
			setDead();
		}
	}
}

bool TCompEnemy3D::isStillDead()
{
	if (timer_waitingDead >= rebirthTime) {
		setRebirth();
		return false;
	}
	return true;
}

void TCompEnemy3D::resetFSM()
{
	turn180 = false;
	turn_left = false;
	turn_right = false;
	searching = false;
	current_velocity = 0;
	dead = false;
	player_caught = false;
	looking = false;
	back_start_constrict = false;
	front_start_constrict = false;
	constrict = false;
	releaseConstrict = false;
	getUp = false;
	chase_player = false;
}

bool TCompEnemy3D::canChangeSuspiciusNode()
{
	if (timer_change_suspicius_node > 1) {
		timer_change_suspicius_node = 0;
		return true;
	}
	else return false;
}

bool TCompEnemy3D::waitAnim()
{

	if (timer_waitAnim >= time_animation) {
		return true;
	}

	return false;
}

void TCompEnemy3D::playSteps()
{
	if (rightOrLeftFootInFloor) {
		//PASO DERECHO
		if (last_value_rightorleft != rightOrLeftFootInFloor) {
			TCompAudio* audio = get<TCompAudio>();
			steps_event = audio->playEvent("guardianstep");
			//steps_event = audio->playEvent("Test1/OnStep");
			last_value_rightorleft = rightOrLeftFootInFloor;
		}
	}
	else {
		//PASO IZQUIERDO
		if (last_value_rightorleft != rightOrLeftFootInFloor) {
			TCompAudio* audio = get<TCompAudio>();
			//dbg("pie izquierdo \n");
			steps_event = audio->playEvent("guardianstep");
			//steps_event = audio->playEvent("Test1/OnStep");
			last_value_rightorleft = rightOrLeftFootInFloor;
		}
	}
}

void TCompEnemy3D::calculeLastFootInFloor()
{
	if (collision_down) {
		TCompTransform* t_player = get<TCompTransform>();
		TCompSkeleton* skel = get<TCompSkeleton>();

		//Si es true significa que fue el pie derecho, si es false el izquierdo
		if (rightOrLeftFootInFloor) {
			//Como el ultimo fue el derecho, comprobamos ahora si el izquierdo toca el suelo
			if (skel->hasRightOrLeftFootInFloor(rightOrLeftFootInFloor, t_player->getPosition().y, distStepToFloor)) {
				rightOrLeftFootInFloor = false;
			}
		}
		else {
			if (skel->hasRightOrLeftFootInFloor(rightOrLeftFootInFloor, t_player->getPosition().y, distStepToFloor)) {
				rightOrLeftFootInFloor = true;
			}
		}
	}
}

bool TCompEnemy3D::checkContactPlayer(float dt)
{
	addLog("== Check Contact Player ==");

	contact_with_player = false;
	is_in_cone_contact = false;

	if (dist_ToPlayer <= dist_toCapture) {

		float half_cone = deg2rad(angle_to_capture);
		pos_suspicius = pos_player;
		addLog("Estamos cerca para capturarlo");

		if (isInCone(half_cone)) {
			timer_contact_player += dt;
			contact_with_player = true;
			is_in_cone_contact = true;
			addLog("El player esta dentro del cono para atraparlo");
			return true;
		}
		else {
			addLog("El player no esta dentro del cono, asi que no puedo atraparlo");
			resetContactPlayer();
			return false;
		}

	}
	else {
		addLog("Estamos demasiado lejos para capturarlo");
		resetContactPlayer();
		return false;
	}


	//METODO ANTIGUO
	/*if (h_OnContact == h_null) {
		addLog("El handle de contacto es nulo");
		resetContactPlayer();
		return false;
	}
	else {
		CEntity* e = h_OnContact;

		if (e == nullptr) {
			addLog("El handle de contacto no es valido");
			resetContactPlayer();
			return false;
		}

		if (strcmp(e->getName(), "Player") == 0) {
			float half_cone = deg2rad(angle_to_capture);
			addLog("El handle es el del player");

			if (isInHeadCone(half_cone)){
				timer_contact_player += dt;
				h_OnContact = h_null;
				contact_with_player = true;
				frames_to_remove_contact = 5;
				addLog("El player esta dentro del cono para atraparlo");
				return true;
			}
			else {
				addLog("El player no esta dentro del cono, asi que no puedo atraparlo");
				resetContactPlayer();
				return false;
			}


		}
		else {
			addLog("El handle no es el del Player");
			resetContactPlayer();
			return false;
		}
	}*/


}

void TCompEnemy3D::resetContactPlayer()
{
	/*if (contact_with_player && frames_to_remove_contact > 0) {
		frames_to_remove_contact -= 1;
		return;
	}*/

	contact_with_player = false;
	timer_contact_player = 0;
}

//void TCompEnemy3D::prepareSearch(float rot_time, float wait_time)
//{
//	//rotation_duration = rot_time;
//	//watching_duration = wait_time;
//	//phase_duration = rotation_duration + watching_duration;
//	//rotate_timer.reset();
//	//searching = true;
//	//current_velocity = 0;
//}

void TCompEnemy3D::prepareSearch()
{
	if (node_ready)	return;

	timer_animation = 0;
	time_animation = getAnimationTime(SEARCH);

	searching = true;
	current_velocity = 0;

	node_ready = true;
}

void TCompEnemy3D::prepareGoBackWpt()
{
	if (node_ready)	return;

	//Como ya hemos dejado de esperar y nos volvemos, lo ponemos en falso por si volvieramos a ir detras del player
	//waiting_angry = false;

	/*VEC3 my_way_pos = getPosWayPoint();

	if (point_withOutPath) {
		navPath.push_back(my_way_pos);
	}
	else {
		navPath = CEngine::get().getNavMesh().calculePath(my_Pos, my_way_pos);
	}

	last_velocity = walk_velocity;*/


	node_ready = true;
}

void TCompEnemy3D::prepareGoToInitPoint()
{

	if (node_ready)	return;

	addLog("= Prepare Go to Init Point = ");

	resetSuspiciusBar();

	state = enemyState::PATROL;

	setRender();

	addFloat3Log("Voy hacia mi punto inicial [", my_init_pos, "] andando");

	if (isThereWayNearToPoint(my_Pos, my_init_pos)) {
		navPath = generatePath(my_Pos, my_init_pos);
	}
	else {
		if (!isDead()) {
			resetEnemy();
		}
		navPath.push_back(my_init_pos);
	}

	last_velocity = walk_velocity;

	node_ready = true;

}

void TCompEnemy3D::prepareStartConstrict()
{
	if (node_ready)	return;

	addLog("= Prepare Start Constrict = ");

	current_velocity = 0;
	last_velocity = 0;
	back_start_constrict = true;
	timer_animation = 0;
	timer_constrict = 0;
	time_animation = getAnimationTime(BACK_START_CONSTRICT);
	start_pos = my_Pos;
	end_pos = start_pos + my_frontRoot * 0.63;

	node_ready = true;
}

void TCompEnemy3D::prepareConstrict()
{
	if (node_ready)	return;

	addLog("= Prepare Constrict = ");

	current_velocity = 0;
	last_velocity = 0;
	constrict = true;
	timer_animation = 0;
	timer_constrict = 0;
	time_animation = getAnimationTime(CONSTRICT);
	chase_player = false;

	node_ready = true;


}

void TCompEnemy3D::prepareReleaseConstrict()
{
	if (node_ready)	return;

	addLog("= Prepare Release Constrict = ");

	TCompAnimator* anim = get<TCompAnimator>();
	TCompSkeleton* my_skel = get<TCompSkeleton>();
	TCompTransform* my_trans = get<TCompTransform>();

	anim->removeCurrentAction(0.1);
	releaseConstrict = true;
	not_auto_move = true;  // Con esto hacemos que la capsula pueda estar en el aire

	//Rotacion que hara la capsula del guardian al caer
	float y, p;
	my_trans->getEulerAngles(&y, &p, nullptr);
	start_rot_col = QUAT(0, 0, 0, -1);
	float yaw180 = y + deg2rad(-180);
	end_rot_col = QUAT::CreateFromYawPitchRoll(yaw180, deg2rad(89.f), deg2rad(0.f));


	//Peque�o desplazamiento del root para que encaje donde se quedo el constrictHug
	my_skel->setCorrectRoot(true);
	my_skel->clearCorrectRoot();
	my_skel->setCorrectMoveRoot((my_Pos + my_frontRoot * 0.378) - my_Pos);

	start_root_pos = (my_Pos + my_frontRoot * 0.378) - my_Pos;
	end_root_pos = VEC3::Zero;

	start_pos = my_Pos;
	end_pos = my_Pos - my_frontRoot * 0.378;

	timer_constrict = 0;
	timer_animation = 0;
	time_animation = getAnimationTime(RELEASE_CONSCTRICT);

	node_ready = true;
}

void TCompEnemy3D::prepareGetUp()
{
	if (node_ready)	return;

	addLog("= Prepare Get Up = ");

	TCompAnimator* my_anim = get<TCompAnimator>();
	TCompSkeleton* my_skel = get<TCompSkeleton>();
	TCompTransform* my_trans = get<TCompTransform>();

	float y, p;
	my_trans->getEulerAngles(&y, &p, nullptr);
	y += deg2rad(-180);
	my_trans->setEulerAngles(y, p, 0.f);

	my_skel->setCorrectRoot(true);
	my_skel->clearCorrectRoot();
	start_root_pos = my_trans->getPosition();
	end_root_pos = start_root_pos + my_trans->getFront() * 0.9;
	my_skel->setCorrectMoveRoot(end_root_pos - start_root_pos);

	getUp = true;

	start_rot_col = QUAT::CreateFromYawPitchRoll(y, deg2rad(89.f), deg2rad(0.f));
	end_rot_col = QUAT::CreateFromYawPitchRoll(y, deg2rad(70.f), deg2rad(0.f));

	timer_animation = 0;
	timer_constrict = 0;
	time_animation = getAnimationTime(GETUP);

	node_ready = true;
}

void TCompEnemy3D::prepareWait()
{
	if (node_ready)	return;

	last_velocity = current_velocity;
	current_velocity = 0;

	node_ready = true;
}

void TCompEnemy3D::prepareVictory()
{

	if (!initVictory) {
		victory_event = CEngine::get().get().getSound().playEvent("guardianVictory");
		initVictory = true;
	}

}

void TCompEnemy3D::resetSearch()
{
	node_ready = false;
	time_animation = 0;
	searching = false;
	timer_exitNode = 0;
	outOfPatrol = true;
}

void TCompEnemy3D::prepareSpasm()
{

	if (node_ready)	return;

	node_ready = true;
	timer_waitAnim = 0;

	last_velocity = current_velocity;
	current_velocity = 0;

	time_animation = getAnimationTime(SPASM);

}

void TCompEnemy3D::resetSpasm()
{
	node_ready = false;
	time_animation = 0;
}

bool TCompEnemy3D::prepareGoTo(VEC3 point)
{

	//YA NO SE UTILIZA ESTE METODO

	//HAY TRES OPCIONES PARA MOVERNOS
	// - No hay forma de llegar al punto -> Compruebo si necesito girar y me quedo quieto
	// - Puedo llegar sin NavMesh al punto -> Compruebo si necesito girar, sino, me muevo al punto
	// - Necesito la NavMesh para llegar al punto -> Compruebo si necesito girar, sino, me muevo al primer nodo de la navMesh

	//Si no hay ningun camino que seguir con la nav mesh, debemos quedarnos parados
	/*if (!checkNeedNavMesh(point)) {
		//Pero aunque debamos quedarnos parados podemos mirar si tenemos que girar o no
		checkIfIdleTurn(point);
		return false;
	}

	//Si necesito moverme con la navMesh
	if (move_with_navPath) {
		//Si es asi, comprobamos si tenemos que girar en idle hacia el primer punto de la navmesh
		if (!checkIfIdleTurn(navPath[0]))	return true;
		else return false;
	}
	//Si no necesito moverme con la navMesh
	else {
		//Si es asi, comprobamos si tenemos que girar en idle hacia el primer punto de la navmesh
		if (!checkIfIdleTurn(point))	return true;
		else return false;
	}*/

	return false;

}

void TCompEnemy3D::resetGoTo()
{
	navPath.clear();
	not_displacement = false;
	//move_with_navPath = false;
}

void TCompEnemy3D::resetTurn()
{
	/*TCompAnimator* anim = get<TCompAnimator>();
	anim->setRemoveLockAction();*/

	addLog("Reseteando Turn");
	if (use_save_anim_movs) {
		debug_root.printAll();
		debug_anim.printAll();
	}

	turn180 = false;
	turn_left = false;
	turn_right = false;
	turnIdle = false;
	turnMovement = false;
	current_velocity = last_velocity;
	timerNewRot = 0;
	init_rot_turn = QUAT(0, 0, 0, 0);

}

bool TCompEnemy3D::isInHeadCone(float half_cone)
{
	VEC3 pos_to_p = pos_player - my_posHead;
	float amount_of_front = my_frontHead.Dot(pos_to_p);
	float amount_of_left = my_leftHead.Dot(pos_to_p);

	return fabsf(atan2(amount_of_left, amount_of_front)) < half_cone;

	//return isInCone(my_posHead, my_frontHead, posPlayer, half_cone);

}

/*if (isInCone(my_Pos, trans->getFront(), pos_player, half_cone))*/

bool TCompEnemy3D::isInCone(float half_cone)
{
	TCompTransform* trans = get<TCompTransform>();

	VEC3 pos_to_p = pos_player - my_Pos;
	float amount_of_front = my_frontRoot.Dot(pos_to_p);
	float amount_of_left = my_leftRoot.Dot(pos_to_p);

	return fabsf(atan2(amount_of_left, amount_of_front)) < half_cone;
}

VEC3 TCompEnemy3D::getPosWayPoint()
{

	if (using_rand_wayPoint) {
		dbg("Devuelvo un punto waypoint random\n");
		return random_way_point;
	}

	CEntity* e_wps = wps[current_way_point];
	TCompWaypoint* c_wps = e_wps->get<TCompWaypoint>();

	while (!c_wps->isEnable()) {
		dbg("El waypoint %s no esta disponible, cogemos el siguiente\n", e_wps->getName());
		setNextWP();

		e_wps = wps[current_way_point];
		c_wps = e_wps->get<TCompWaypoint>();
	}

	return c_wps->getPos();
}

float TCompEnemy3D::getDistWayPoint()
{

	if (using_rand_wayPoint) {
		return VEC3::Distance(my_Pos, random_way_point);
	}

	CEntity* e_wps = wps[current_way_point];
	TCompWaypoint* c_wps = e_wps->get<TCompWaypoint>();

	return c_wps->getDistance(my_Pos);
}

int TCompEnemy3D::getNearWayPoint(VEC3 current_pos)
{

	addLog("== Get Near WayPoint ==");

	float minDistance = std::numeric_limits<float>::max();

	int wayPoint = -1;

	//Primero vamos a ver cual es el waypoint mas cercano y coger su indice en el vector
	int i = 0;
	for (auto& wp : wps) {
		CEntity* e_wps = wp;
		TCompWaypoint* c_wps = e_wps->get<TCompWaypoint>();

		if (!c_wps->isEnable()) continue;

		addIntLog("Vamos a mirar si hay camino hasta el waypoint ", (i + 1), "");

		if (isThereWayNearToPoint(current_pos, c_wps->getPos())) {
			addFloat3Log("Hay un camino hasta el waypoint [", c_wps->getPos(), "]");
			float distance = c_wps->getDistance(current_pos);
			if (distance < minDistance && distance > 1) {
				addLog("Y es el punto mas cercano hasta ahora");
				minDistance = distance;
				wayPoint = i;
			}
		}

		i++;
	}

	return wayPoint;
}

void TCompEnemy3D::resetBT()
{
	//Reseteo el behavior tree
	TCompBehaviourTree* bt = get<TCompBehaviourTree>();
	bt->resetGame();
}

void TCompEnemy3D::setRender(int render)
{
	addLog("== Set Render ==");
	TCompRender* enemy_rend = get<TCompRender>();

	if (render < 0 || render > 3)	return;

	addFloatLog("Render:", render, "");

	enemy_rend->activateState(render);
}

void TCompEnemy3D::setRender()
{
	addLog("== Set Render ==");
	TCompRender* enemy_rend = get<TCompRender>();

	switch (state)
	{
	case enemyState::PATROL:
		addLog("Render del patrol -> Azul");
		enemy_rend->activateState(ENEMY_RENDER_NORMAL);
		break;
	case enemyState::ALERT:
		addLog("Render del alert -> Morado");
		enemy_rend->activateState(ENEMY_RENDER_ALERT);
		break;
	case enemyState::SUSPICIUS:
		addLog("Render del suspicius -> Morado");
		enemy_rend->activateState(ENEMY_RENDER_ALERT);
		break;
	case enemyState::PURSUER:
		addLog("Render del Pursuer -> Rojo");
		enemy_rend->activateState(ENEMY_RENDER_ALARM);
		break;
	case enemyState::ATTACK:
		addLog("Render del Attack -> Rojo");
		enemy_rend->activateState(ENEMY_RENDER_ALARM);
		break;
	case enemyState::SURRENDER:
		addLog("Render del surrender -> Morado");
		enemy_rend->activateState(ENEMY_RENDER_ALERT);
		break;
	case enemyState::CONSTRICT:
		addLog("Render del Consrict -> Rojo");
		enemy_rend->activateState(ENEMY_RENDER_ALARM);
		break;
	default:
		break;
	}
}

vector<VEC3> TCompEnemy3D::generatePath(VEC3 src, VEC3 dest)
{
	vector<VEC3> newPath = CEngine::get().getNavMesh().calculePath(src, dest);

	//if (newPath.size() > 2)	newPath.erase(newPath.begin());

	return newPath;
}

VEC3 TCompEnemy3D::generateRandomPoint(VEC3 point, float maxDist)
{
	TCompTransform* trans = get<TCompTransform>();
	/*float dist = minDist + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (maxDist - minDist)));
	//float dist = (rand() % maxDist) + minDist;
	float ang = (rand() % 360);
	float dang = deg2rad(ang);
	QUAT q = QUAT::CreateFromAxisAngle(VEC3::Up, dang);
	VEC3 v_result = VEC3::Transform(VEC3::Forward * dist, q);
	return trans->getPosition() + v_result;*/
	return CEngine::get().getNavMesh().findRandomPointAroundCircle(point, maxDist);
}

VEC3 TCompEnemy3D::generateSuspiciusRandPoint()
{
	for (int i = 1; i < 30; i++) {
		VEC3 point = generateRandomPoint(pos_player, i);
		if (isThereWayNearToPoint(my_Pos, point))	return point;
	}
	return VEC3(std::numeric_limits<float>::min(), std::numeric_limits<float>::min(), std::numeric_limits<float>::min());
}

bool TCompEnemy3D::isThereWayNearToPoint(VEC3 src, VEC3 dest)
{

	addLog("== is There Way Near To Point ==");

	vector<VEC3> path = CEngine::get().getNavMesh().calculePath(src, dest);

	addIntLog("El path tiene ", path.size(), "puntos");

	return path.size() > 0;
}

bool TCompEnemy3D::isThereWayToPoint(VEC3 src, VEC3 dest)
{
	vector<VEC3> path = CEngine::get().getNavMesh().calculePath(src, dest);

	if (path.size() <= 0)	return false;

	VEC3 end_point = path.back();

	float d = VEC3::Distance(end_point, dest);

	return d < 0.2;

}

bool TCompEnemy3D::isThereObstacleToPoint(VEC3 src, VEC3 dest)
{

	VEC3 hit;
	bool obstacle = CEngine::get().getNavMesh().raycast(src, dest, hit);

	if (obstacle) {
		addLog("Desde mi posicion hay obstaculos para llegar al punto");
		return true;
	}
	else {
		addLog("Desde mi posicion puedo llegar al punto");
		return false;
	}
}

bool TCompEnemy3D::isAValidPos(VEC3 pos)
{
	VEC3 pos_noT_valid = VEC3(std::numeric_limits<float>::min(), std::numeric_limits<float>::min(), std::numeric_limits<float>::min());

	if (pos_noT_valid == pos)	return false;
}

bool TCompEnemy3D::isNearPoint(VEC3 dest_point)
{
	//Esta funcion se utiliza para cuando por alguna razon en vez de tener un path hasta el waypoint
	//tenemos un path a un punto cercano, en ese caso vamos andando hacia ese punto cercano y mientras,
	//vamos intentando calcular el path hasta el waypoint, no un punto cercano

	if (navPath.size() <= 0) return true;

	VEC3 end_point = navPath.back();

	float d = VEC3::Distance(end_point, dest_point);

	return d < 0.2;
}

void TCompEnemy3D::resetCheckDistXSec()
{
	timer_distXsec = 0;
}

bool TCompEnemy3D::canExitNode()
{

	if (timer_exitNode >= time_exitNode)	return true;

	return false;
}

void TCompEnemy3D::calculateState(enemyState behavior_state)
{
	addLog("== Calculate State ==");

	//Primero vamos a recalcular la barra de sospecha en caso de que haya bajado mas que el estado del behaviour
	//Es decir, si estamos en searchAround de Alert, la barra no deberia bajar menos de 30
	switch (behavior_state)
	{
	case enemyState::ALERT:
		addLog("Estamos calculando el estado del BT Alert");
		if (suspicius_bar < alert_value)	suspicius_bar = alert_value+5;
		break;
	case enemyState::SUSPICIUS:
		addLog("Estamos calculando el estado del BT Suspicius");
		if (suspicius_bar < suspicius_value)	suspicius_bar = suspicius_value+10;
		break;
	case enemyState::PURSUER:
		addLog("Estamos calculando el estado del BT Pursuer");
		if (waiting_angry) {
			if (suspicius_bar < alarm_value)	suspicius_bar = alarm_value+10;
		}
		break;
	case enemyState::ATTACK:
		addLog("Estamos calculando el estado del BT Attack");
		if (suspicius_bar < max_value_suspicius)	suspicius_bar = max_value_suspicius;
		break;
	case enemyState::SURRENDER:
		addLog("Estamos calculando el estado del BT Surrender");
		suspicius_bar = suspicius_value+10;
		break;

	case enemyState::CONSTRICT:
		addLog("Estamos calculando el estado del BT Constrict");
		state = enemyState::CONSTRICT;
		if (suspicius_bar < max_value_suspicius)	suspicius_bar = max_value_suspicius;
		break;
	case enemyState::PURSUER_WAIT:
		suspicius_bar = max_value_suspicius;
		break;
	default:
		break;
	}


	//Ahora dependiendo del valor de la suspicius bar y otros factores, estableceremos el estado del guardian
	if (behavior_state == enemyState::PURSUER_WAIT) {
		state = enemyState::PURSUER;
		setRender();
		return;
	}


	if (behavior_state == enemyState::CONSTRICT) {
		setRender();
		return;
	}

	if (surrender) {
		if (isThereWayToPoint(my_Pos, pos_player)) {
			addLog("SURRENDER: Estaba en surrender pero ahora ha un camino al player");
			suspicius_bar = max_value_suspicius;
		}
		addLog("SURRENDER: Estoy en surrender y aun no hay camino hasta el player, asi que sigo volviendo a mi punto de patrulla");
		state = enemyState::SURRENDER;
		setRender();
		return;
	}

	//COMBAT
	//Ahora ya no vamos a por el player sin navmesh cuando lo tenemos cerca, asi que usamos el Pursuer
	/*if (player_seen) {
		addLog("ATTACK: Estoy viendo al player, asi que voy atacarle");
		state = enemyState::ATTACK;
		setRender();
		return;
	}*/

	//PATRULLA
	if (suspicius_bar < alert_value) {
		addLog("PATROL: Voy a seguir haciendo patrulla");
		state = enemyState::PATROL;
		setRender();
		return;
	}

	//ALERTA
	if (suspicius_bar >= alert_value && suspicius_bar < suspicius_value) {
		addLog("ALERT: Estoy en alerta");
		state = enemyState::ALERT;
		setRender();
		return;
	}

	//SUSPICIUS
	if (suspicius_bar >= suspicius_value && suspicius_bar < alarm_value) {
		state = enemyState::SUSPICIUS;
		setRender();

		//Si hay camino hasta el punto de sospecha, entonces vamos hacia �l
		if (isThereWayNearToPoint(my_Pos, pos_suspicius)) {
			addLog("SUSPICIUS: Hay camino hacia el punto de sospecha o cerca de �l");
			last_pos_suspicius = pos_suspicius;
			navPath = generatePath(my_Pos, pos_suspicius);
			return;
		}
		else {
			//Si el nuevo punto de sospecha no es viable, miramos si tenemos un punto de sospecha anterior valido
			if (isAValidPos(last_pos_suspicius)) {
				//Si es asi, iremos hacia este anterior
				pos_suspicius = last_pos_suspicius;
				navPath = generatePath(my_Pos, pos_suspicius);
				addLog("SUSPICIUS: No se puede llegar al nuevo punto de sospecha, asi que voy al antiguo");
				return;
			}
			else {
				//Si no hubiera uno anterior valido, generaremos un punto random cerca del player al que ir

				//Si ya lo habiamos generado cuando entramos al estado de suspicius, no generamos uno diferente
				if (isAValidPos(randomPoint)) {					
					addLog("SUSPICIUS: Vamos al punto random de sospecha que ya habiamos calculado anteriormente");
					pos_suspicius = randomPoint;
					navPath = generatePath(my_Pos, pos_suspicius);
					return;
				}
				//Si no se genero, lo generamos
				else {					
					VEC3 pos = generateSuspiciusRandPoint();
					//Si hay camino hasta el punto generado, entonces iremos hacia �l
					if (isThereWayNearToPoint(my_Pos, pos)) {
						addLog("SUSPICIUS: Generamos un nuevo punto random de sospecha y vamos hacia �l");
						randomPoint = pos_suspicius = pos;
						navPath = generatePath(my_Pos, pos_suspicius);
						return;
					}
					//Pero si no lo es, generaremos uno nuevo cerca del guardian
					else {
						addLog("SUSPICIUS: No podemos generar un nuevo punto random de sospecha, asi que vamos a generar uno cerca del guardian");
						randomPoint = generateRandomPoint(my_Pos, 5);
						pos_suspicius = randomPoint;
						navPath = generatePath(my_Pos, pos_suspicius);
						return;
					}
	
				}
			}
		}
	}

	//PURSUE
	if (suspicius_bar >= alarm_value) {
		enemyState last_state = state;
		state = enemyState::PURSUER;

		if (isThereWayNearToPoint(my_Pos, pos_player)) {
			addLog("PURSUER: Tengo camino yendo cerca del player, asi que voy hacia alli");
			navPath = generatePath(my_Pos, pos_player);
			setRender();
			return;
		}
		else {
			//Si antes ya estabamos 
			if (last_state == enemyState::PURSUER) {
				addLog("PURSUER: No puede ir al nuevo punto del player, asi que voy al anterior");
				setRender();
				//Entonces el navPath sera el anterior, no generaremos ninguno nuevo e iremos a donde antes estaba el player
				return;
			}
			else {
				//Si fuera la primera vez que entramos a Pursuer pero no fueramos capaces de ir a por el player, entonces pasaremos a suspicius
				addLog("PURSUER: Como no podemos llegar al player, y antes no estabamos en Pursuer, pasamos a Suspicius");
				suspicius_bar = alarm_value - 1;
				calculateState(behavior_state);
				//setRender();
				return;
			}
		}
	}

}

void TCompEnemy3D::initWaitingAngry(bool b)
{
	if (b) {
		if (!waiting_angry) {
			addLog("Init Wait Angry -> True\n");
			current_velocity = 0;
			waiting_angry = true;
			timer_waitAngry = 0;
		}

	}
	else {
		if (waiting_angry) {
			addLog("Init Wait Angry -> False\n");
			waiting_angry = false;
			current_velocity = last_velocity;
		}
	}
}

bool TCompEnemy3D::isFinishedWaitAngry()
{

	bool b = waiting_angry && timer_waitAngry >= time_waitAngry;
	if (b) {
		addLog("Ya he terminado de esperar cabreado, me vuelvo a mi patrulla");
		return true;
	}
	else {
		addLog("Aun espero enfadado");
		return false;
	}
}

void TCompEnemy3D::checkDisplacement(VEC3 beforeMov, VEC3 afterMov, float amount, VEC3 point)
{
	bool displ = false;

	displ = VEC3::Distance(beforeMov, afterMov) > (amount / 2.f);

	if (!displ) {
		addLog("Me he quedado pillado, voy a ver si estoy contra algo");
		hitWithSomething = CEngine::get().getNavMesh().raycast(my_Pos, point, hit);
		if (hitWithSomething) {
			not_displacement = true;
			addLog("Me he quedado pillado contra alguna pared o algo");
		}
		else {
			not_displacement = false;
			addLog("Mi desplazamiento es menor que el amount pero no hay nada delante mia");
		}
	}
	else {
		not_displacement = false;
		addLog("No Me he quedado pillado porque mi velocidad de desplazamiento es mayor que la amount");
	}
}

bool TCompEnemy3D::canGoToTryCatch()
{
	return isThereWayToPoint(my_Pos, pos_player);
}

bool TCompEnemy3D::canGoToPlayer()
{
	return isThereWayToPoint(my_Pos, pos_player);
}

bool TCompEnemy3D::canExitAlertState()
{
	return timer_animation < 1;
}

void TCompEnemy3D::destroyEnemy()
{
	dbg("Me destruyen\n");

	resetBT();
	resetFSM();
	TCompCollider* col = get<TCompCollider>();
	col->controller->setPosition(PxExtendedVec3(my_Pos.x, my_Pos.y-2000, my_Pos.z));
	current_velocity = 0;
	dead = true;
	destroyed = true;
	waitingDead = false;
	resetSuspiciusBar();
}

void TCompEnemy3D::setDead()
{
	dbg("Me he muerto\n");

	TCompTransform* my_trans = get<TCompTransform>();

	resetBT();
	resetFSM();
	dead = true;
	current_velocity = 0;
	chase_player = false;
	waitingDead = true;
	timer_waitingDead = 0;
	init_rot_turn = my_trans->getRotation();
	time_animation = getAnimationTime(DEAD);
	resetSuspiciusBar();
}

void TCompEnemy3D::setRebirth()
{
	TCompTransform* my_trans = get<TCompTransform>();

	dead = false;
	current_velocity = 0;
	waitingDead = false;
	timer_waitingDead = 0;
	time_animation = getAnimationTime(REBIRTH);
	init_rot_turn = my_trans->getRotation();

}

void TCompEnemy3D::applyDeathMov(float dt)
{
	TCompTransform* my_trans = get<TCompTransform>();
	TCompAnimator* anim = get<TCompAnimator>();

	if (timer_waitingDead <= time_animation) {
		float yaw, pitch;
		// Calculamos la rotacion para llegar al punto
		my_trans->getEulerAngles(&yaw, &pitch, nullptr);
		//yaw += animDeltaYaw;
		pitch += animDeltaPitch;
		my_trans->setEulerAngles(yaw, pitch, 0.0f);
		//VEC3 abs_mov = VEC3::Transform(animDeltaMove, initial_rotation);
		//CEngine::get().getPhysics().moveEnemy(abs_mov, dt, get<TCompCollider>(), collision_down);
	}
	else {
		//dbg("He terminado la animacion de muerte\n");
	}

}

bool TCompEnemy3D::applyRebirthMov(float dt)
{
	static bool removed_dead_loop = false;

	TCompTransform* my_trans = get<TCompTransform>();
	TCompAnimator* anim = get<TCompAnimator>();

	if (timer_waitingDead < time_animation) {
		float yaw, pitch;
		// Calculamos la rotacion para llegar al punto
		my_trans->getEulerAngles(&yaw, &pitch, nullptr);
		//yaw += animDeltaYaw;
		pitch += animDeltaPitch;
		my_trans->setEulerAngles(yaw, pitch, 0.0f);

		float mitadAnim = time_animation / 2;
		if (!removed_dead_loop && timer_waitingDead >= mitadAnim) {
			TCompAnimator* anim = get<TCompAnimator>();
			anim->removeCycleAnim("dead-loop");
			removed_dead_loop = true;
		}

		return true;
	}
	else {
		float yaw, pitch;
		// Calculamos la rotacion para llegar al punto
		my_trans->getEulerAngles(&yaw, &pitch, nullptr);
		my_trans->setEulerAngles(yaw, 0, 0.0f);
		return false;
	}
}


bool TCompEnemy3D::isCurrentActionAnimFinished(float dt)
{
	return timer_animation > time_animation;
}

void TCompEnemy3D::checkTurn(VEC3 point)
{
	if (isRotating()) {
		addLog("Ya estoy rotando, asi que no voy a chequear");
		return;
	}

	addLog("== Check Turn ==");

	TCompTransform* my_trans = get<TCompTransform>();
	float yaw = my_trans->getDeltaYawToAimTo(point);

	double yaw_degrees = rad2deg(yaw);

	//Primero comprobamos la velocidad actual
	if (current_velocity == 0) {
		addLog("La velocidad actual es 0, asi que comprobamos si giramos en idle");
		checkIfIdleTurn(yaw_degrees);
	}
	else if (current_velocity == walk_velocity) {
		addLog("La velocidad actual es walk, asi que comprobamos si giramos en idle");
		if (checkIfIdleTurn(yaw_degrees)) {
			addLog("Como vamos a girar en idle, la velocidad actual ahora sera 0 y se guarda la walk");
			last_velocity = walk_velocity;
			current_velocity = 0;
		}
	}
	else if (current_velocity == run_velocity) {

		if (isZoneForceIdleTurn(my_Pos)) {
			addLog("Estoy en una zona donde debo comprobar si o si si giro pero en Idle\n");
			if (checkIfIdleTurn(yaw_degrees)) {
				addLog("Como vamos a girar en idle, la velocidad actual ahora sera 0 y se guarda la run");
				last_velocity = run_velocity;
				current_velocity = 0;
				chase_player = false;
			}
		}
		else if (vel_player < min_vel_to_idleRot && dist_ToPlayer < min_cone_dist) {
			addLog("Voy a comprobar si giro en Idle que el player esta parado o casi y muy cerca de mi\n");
			if (checkIfIdleTurn(yaw_degrees)) {
				addLog("Como vamos a girar en idle, la velocidad actual ahora sera 0 y se guarda la run");
				last_velocity = run_velocity;
				current_velocity = 0;
				chase_player = false;
			}
		}
		else {
			addLog("La velocidad actual es run, asi que comprobamos si giramos en movimiento");
			checkIfMovementTurn(yaw_degrees);
		}

	}

}

bool TCompEnemy3D::checkIfMovementTurn(float yaw_degrees)
{
	TCompSkeleton* skel = get<TCompSkeleton>();
	TCompAnimator* anim = get<TCompAnimator>();
	TCompTransform* trans = get<TCompTransform>();

	yawToPoint = yaw_degrees;   //Variable de debug

	if (abs(yaw_degrees) > min_angle_for180) {

		if (use_save_anim_movs) {
			debug_anim.init("Run Turn Back", yawToPoint);
			initDebugRootMovs("Run Turn Back");
		}

		turnMovement = true;
		timer_animation = 0;
		turn180 = true;
		addFloatLog("Tengo que girar en movimiento", yaw_degrees, "con turn_back");

		float aux = 180 - abs(yaw_degrees);
		float angle_correction;
		if (yaw_degrees > 0)	angle_correction = -aux;
		else angle_correction = aux;
		//dbg("La correccion del angulo es %2.f\n", angle_correction);
		time_animation = getAnimationTime(MOV180);
		skel->correctYawAngle(angle_correction, time_animation);
		init_rot_turn = trans->getRotation();
		return true;
	}

	if (yaw_degrees > 0 && abs(yaw_degrees) > min_angle_for90) {

		addFloatLog("Tengo que girar en movimiento", yaw_degrees, "hacia la izquierda");
		if (use_save_anim_movs) {
			debug_anim.init("Run Turn Left", yawToPoint);
			initDebugRootMovs("Run Turn Left");
		}

		turnMovement = true;
		timer_animation = 0;
		turn_left = true;
		time_animation = getAnimationTime(MOV90);

		float aux = 90 - abs(yaw_degrees); 
		float angle_correction = -aux;
		//dbg("La correccion del angulo es %2.f\n", angle_correction);
		skel->correctYawAngle(angle_correction, time_animation);
		init_rot_turn = trans->getRotation();
		return true;
	}

	if (yaw_degrees < 0 && abs(yaw_degrees) > min_angle_for90) {

		addFloatLog("Tengo que girar en movimiento", yaw_degrees, "hacia la derecha");

		if (use_save_anim_movs) {
			debug_anim.init("Run Turn Right", yawToPoint);
			initDebugRootMovs("Run Turn Right");
		}

		turnMovement = true;
		timer_animation = 0;
		turn_right = true;
		time_animation = getAnimationTime(MOV90);
		float aux = 90 - abs(yaw_degrees);
		float angle_correction = aux;
		//dbg("La correccion del angulo es %2.f\n", angle_correction);
		skel->correctYawAngle(angle_correction, time_animation);
		init_rot_turn = trans->getRotation();
		return true;
	}

	return false;

}

bool TCompEnemy3D::checkIfIdleTurn(float yaw_degrees)
{
	turnIdle = false;

	TCompSkeleton* skel = get<TCompSkeleton>();
	TCompAnimator* anim = get<TCompAnimator>();
	TCompTransform* my_trans = get<TCompTransform>();

	yawToPoint = yaw_degrees;   //Variable de debug

	addFloatLog("El punto al que me voy a girar esta a", yaw_degrees, "� en yaw de mi");

	if (timerNewRot < timeToNewRot) {
		addLog("No se si necesito girar, pero aun no puedo porque recien acabo de terminar de rotar");
		return false;
	}

	if (abs(yaw_degrees) > min_angle_for180) {
		addLog("Voy hacer un giro de 180� en idle");

		if (use_save_anim_movs) {
			debug_anim.init("Idle Turn Back", yawToPoint);
			initDebugRootMovs("Idle Turn Back");
		}

		timer_animation = 0;
		turn180 = true;
		turnIdle = true;
		time_animation = getAnimationTime(IDLE180);
		//dbg("Frame %d-> Giro 90 izquierda. Tengo que girar %.2f\n", getFrameCount(), yaw_degrees);
		//dbg("Tengo que girar %.2f a la izquierda\n", yaw_degrees);
		float aux = 180 - abs(yaw_degrees);
		float angle_correction;
		if (yaw_degrees > 0)	angle_correction = -aux;
		else angle_correction = aux;
		addFloatLog("La correccion del angulo es", angle_correction, "�");
		skel->correctYawAngle(angle_correction, time_animation);
		init_rot_turn = my_trans->getRotation();

		return turnIdle;
	}

	if (yaw_degrees > 0 && abs(yaw_degrees) > min_angle_for90) {
		addLog("Voy hacer un giro hacia la izquierda en idle");

		if (use_save_anim_movs) {
			debug_anim.init("Idle Turn Left", yawToPoint);
			initDebugRootMovs("Idle Turn Left");
		}

		timer_animation = 0;
		turn_left = true;
		turnIdle = true;
		time_animation = getAnimationTime(IDLE90);
		//dbg("Frame %d-> Giro 90 izquierda. Tengo que girar %.2f\n", getFrameCount(), yaw_degrees);
		//dbg("Tengo que girar %.2f a la izquierda\n", yaw_degrees);
		float aux = 90 - abs(yaw_degrees);
		float angle_correction = -aux;
		addFloatLog("La correccion del angulo es", angle_correction, "�");
		skel->correctYawAngle(angle_correction, time_animation);
		init_rot_turn = my_trans->getRotation();
	}

	if (yaw_degrees < 0 && abs(yaw_degrees) > min_angle_for90) {
		addLog("Voy hacer un giro hacia la derecha en idle");

		if (use_save_anim_movs) {
			debug_anim.init("Idle Turn Right", yawToPoint);
			initDebugRootMovs("Idle Turn Right");
		}

		timer_animation = 0;
		turn_right = true;
		turnIdle = true;
		//dbg("Frame %d-> Giro 90 derecha. Tengo que girar %.2f\n", getFrameCount(), yaw_degrees);
		time_animation = getAnimationTime(IDLE90);
		//dbg("Tengo que girar %.2f a la derecha\n", yaw_degrees);
		float aux = 90 - abs(yaw_degrees);
		float angle_correction = aux;
		addFloatLog("La correccion del angulo es", angle_correction, "�");
		skel->correctYawAngle(angle_correction, time_animation);
		init_rot_turn = my_trans->getRotation();
	}

	return turnIdle;
}

bool TCompEnemy3D::isZoneForceIdleTurn(VEC3 pos)
{

	//Hacer que 

	return false;
}

void TCompEnemy3D::stopTurn()
{
	addLog("== Stop Turn ==");

	if (!isRotating()) {
		addLog("No estaba rotando, asi que no hay nada que parar");
		return;
	}

	//Si queremos parar, comprobamos si ya lo hemos hecho
	if (!stopping_of_turning) {
		addLog("Ya he comenzado el proceso para detener el giro");

		turn180 = false;
		turn_right = false;
		turn_left = false;

		TCompAnimator* anim = get<TCompAnimator>();
		TCompSkeleton* skel = get<TCompSkeleton>();

		anim->removeCurrentAction(time_stopTurn);
		skel->resetCorrectAngle();
		current_velocity = 0;

		timer_stopTurn = 0;
		stopping_of_turning = true;
	}

}

bool TCompEnemy3D::isStoppingTurn()
{

	addLog("= Is Stopping Turn =");

	if (!stopping_of_turning) {
		addLog("No estoy parando");
		return false;
	}

	if (timer_stopTurn > time_stopTurn) {
		addLog("Estaba parando de girar, y ya he terminado");
		resetTurn();
		stopping_of_turning = false;
		return false;
	}
	else {
		addLog("Aun me queda algo de tiempo antes de poder parar de girar");
		return true;
	}
}

bool TCompEnemy3D::manualRaycast(VEC3 src, VEC3 dest, float dist)
{
	VEC3 direction = dest - src;	

	return CEngine::get().getPhysics().checkRay(src, direction, CModulePhysicsSinn::FilterGroup::Obstacles, dist);
}

bool TCompEnemy3D::canRecapture()
{
	return timer_waitToRecapture > time_waitToRecapture;
}

void TCompEnemy3D::setBTState(string state)
{
	TCompBehaviourTree* bt = get<TCompBehaviourTree>();
	bt->setBTNode(state);
}
void TCompEnemy3D::setConstrictRender(enemyState btState)
{
	TCompRender* enemy_rend = get<TCompRender>();

	int anim_frame = toFrame(timer_animation);
	int r_alarm = ENEMY_RENDER_ALARM;
	int r_alert = ENEMY_RENDER_ALERT;
	int r_desactivated = ENEMY_RENDER_DESACTIVATED;

	if (btState == enemyState::CONSTRICT_RELEASE) {

		state = enemyState::CONSTRICT_RELEASE;
		
		//TEXTURA ACTIVADA
		if (anim_frame >= 0 && anim_frame <=7)	setRender(r_alarm);
		if (anim_frame >= 14 && anim_frame <= 20)	setRender(r_alarm);
		if (anim_frame >= 55 && anim_frame <= 60)	setRender(r_alarm);
		if (anim_frame >= 76 && anim_frame <= 80)	setRender(r_alarm);

		//TEXTURA DESACTIVADA
		if (anim_frame >= 8 && anim_frame <= 13)	setRender(r_desactivated);
		if (anim_frame >= 21 && anim_frame <= 54)	setRender(r_desactivated);
		if (anim_frame >= 60 && anim_frame <= 75)	setRender(r_desactivated);
		if (anim_frame >= 81)						setRender(r_desactivated);

	}
	else if(btState == enemyState::CONSTRICT_GETUP){

		state = enemyState::CONSTRICT_GETUP;

		//TEXTURA ACTIVADA
		if (anim_frame >= 3 && anim_frame <= 4)		setRender(r_alert);
		if (anim_frame >= 7 && anim_frame <= 112)	setRender(r_alert);
		if (anim_frame >= 115 && anim_frame <= 116)	setRender(r_alert);
		if (anim_frame >= 119 && anim_frame <= 121)	setRender(r_alert);
		if (anim_frame >= 125 && anim_frame <= 129)	setRender(r_alert);
		if (anim_frame >= 132)						setRender(r_alert);

		if (anim_frame >= 0 && anim_frame <= 2)		setRender(r_desactivated);
		if (anim_frame >= 5 && anim_frame <= 6)		setRender(r_desactivated);
		if (anim_frame >= 113 && anim_frame <= 114)	setRender(r_desactivated);
		if (anim_frame >= 117 && anim_frame <= 118)	setRender(r_desactivated);
		if (anim_frame >= 122 && anim_frame <= 124)	setRender(r_desactivated);
		if (anim_frame >= 130 && anim_frame <= 131)	setRender(r_desactivated);

	}
}

//bool TCompEnemy3D::checkNeedRotate()
//{
//
//	if (node_ready)	return true;
//
//	addLog("Voy a comprobar si necesito rotar en idle");
//
//	TCompTransform* my_trans = get<TCompTransform>();
//
//	TCompSkeleton* skel = get<TCompSkeleton>();
//	//TCompAnimator* anim = get<TCompAnimator>();
//
//	turnIdle = false;
//
//	//Si estamos usando un punto aleatorio no es necesario rotar
//	/*if (using_rand_wayPoint) {
//		return false;
//	}*/
//
//	CEntity* e_wps = wps[current_way_point];
//	TCompWaypoint* c_wps = e_wps->get<TCompWaypoint>();
//	float yawWp = c_wps->getYaw();
//	double yawWp_degrees = rad2deg(yawWp);
//
//	float my_yaw, my_pitch;
//	my_trans->getEulerAngles(&my_yaw, &my_pitch, nullptr);
//	float myYaw_degrees = rad2deg(my_yaw);
//
//	float yaw_degrees = myYaw_degrees - yawWp_degrees;
//
//	if (abs(yaw_degrees) > min_angle_for180) {
//		timer_animation = 0;
//		turn180 = true;
//		turnIdle = true;
//		time_animation = getAnimationTime(IDLE90);
//		//dbg("Frame %d-> Giro 180 Tengo que girar %.2f\n", getFrameCount(), yaw_degrees);
//		float aux = 180 - abs(yaw_degrees);
//		float angle_correction;
//		if (yaw_degrees > 0)	angle_correction = -aux;
//		else angle_correction = aux;
//		//dbg("La correccion del angulo es %2.f\n", angle_correction);
//		skel->correctAngle(angle_correction, time_animation);
//		init_rot_turn = my_trans->getRotation();
//		current_velocity = 0;
//	}
//
//	if (yaw_degrees > 0 && abs(yaw_degrees) > min_angle_for90) {
//		timer_animation = 0;
//		turn_left = true;
//		turnIdle = true;
//		time_animation = getAnimationTime(IDLE90);
//		//dbg("Frame %d-> Giro 90 izquierda. Tengo que girar %.2f\n", getFrameCount(), yaw_degrees);
//		//dbg("Tengo que girar %.2f a la izquierda\n", yaw_degrees);
//		float aux = 90 - abs(yaw_degrees);
//		float angle_correction = -aux;
//		//dbg("La correccion del angulo es %2.f\n", angle_correction);
//		skel->correctAngle(angle_correction, time_animation);
//		init_rot_turn = my_trans->getRotation();
//		current_velocity = 0;
//	}
//
//	if (yaw_degrees < 0 && abs(yaw_degrees) > min_angle_for90) {
//		timer_animation = 0;
//		turn_right = true;
//		turnIdle = true;
//		//dbg("Frame %d-> Giro 90 derecha. Tengo que girar %.2f\n", getFrameCount(), yaw_degrees);
//		time_animation = getAnimationTime(IDLE90);
//		//dbg("Tengo que girar %.2f a la derecha\n", yaw_degrees);
//		float aux = 90 - abs(yaw_degrees);
//		float angle_correction = aux;
//		//dbg("La correccion del angulo es %2.f\n", angle_correction);
//		skel->correctAngle(angle_correction, time_animation);
//		init_rot_turn = my_trans->getRotation();
//		current_velocity = 0;
//	}
//
//	node_ready = true;
//	return turnIdle;
//}

float TCompEnemy3D::getAnimationTime(animType type)
{
	TCompAnimator* anim = get<TCompAnimator>();

	float aTime = 0;

	switch (type) {
		case IDLE180:
			aTime = anim->getTimeAnimation("idleTurnBack");
			break;
		case IDLE90:
			aTime = anim->getTimeAnimation("idleTurnRight");
			break;
		case MOV90:
			aTime = anim->getTimeAnimation("runTurnRight");
			break;
		case MOV180:
			aTime = anim->getTimeAnimation("runTurnBack");
			break;
		case LOOKAT:
			aTime = anim->getTimeAnimation("lookAt");
			break;
		case SEARCH:
			aTime = anim->getTimeAnimation("search");
			break;
		case REBIRTH:
			aTime = anim->getTimeAnimation("rebirth");
			break;
		case DEAD:
			aTime = anim->getTimeAnimation("dead");
			break;
		case FRONT_START_CONSTRICT:
			aTime = anim->getTimeAnimation("frontStartConstrictIni");
			break;
		case BACK_START_CONSTRICT:
			aTime = anim->getTimeAnimation("backStartConstrictIni");
			break;
		case CONSTRICT:
			aTime = anim->getTimeAnimation("constrictHug");
			break;
		case RELEASE_CONSCTRICT:
			aTime = anim->getTimeAnimation("constrictHugScape");
			break;
		case GETUP:
			aTime = anim->getTimeAnimation("getUp");
			break;
			
	}

	if (aTime == 0)	aTime = 0.1;  //Por si acaso no existe la animacion, que no pete la funcion de corregir el angulo

	return aTime;
}

int TCompEnemy3D::getFrameCount()
{
	return CApplication::get().getFrameCount();
}

void TCompEnemy3D::goToPoint(VEC3 point, float dt)
{
	addLog("== Go to Point ==");

	p = point;  //Variable de debug

	addFloat3Log("Voy hacia el punto [", p, "]");

	if (dt > 0.2) {
		return;
	}

	//Comprobamos si necesitamos girar antes de movernos hacia el punto
	checkTurn(point);
	if (isRotating())	return;

	playSteps();

	TCompTransform* my_trans = get<TCompTransform>();

	float amount_moved = current_velocity * dt;
	float delta_yaw = my_trans->getDeltaYawToAimTo(point);	

	float yaw, pitch;
	my_trans->getEulerAngles(&yaw, &pitch, nullptr);

	//Si estamos corriendo comprobamos si necesitamos hacer un giro en movimiento
	/*if (current_velocity == run_velocity) {
		if (checkIfMovementTurn(point, delta_yaw)) {
			addLog("Estoy corriendo y tengo que girar en movimiento");
			return;
		}
	}
	//En cambio, si estamos andando, comprobamos si tenemos que hacer el giro en idle
	else if (current_velocity == walk_velocity) {
		if (checkIfIdleTurn(point)) {
			addLog("Estoy andando y tengo que girar en idle, asi que me paro primero");
			last_velocity = current_velocity;
			current_velocity = 0;
			return;
		}
	}*/

	addLog("Estoy girando y moviendome normal");
	yaw += delta_yaw * dt;

	my_trans->setEulerAngles(yaw, pitch, 0.0f);
	VEC3 deltaMove = my_trans->getFront() * amount_moved;

	TCompCollider* col = get<TCompCollider>();

	VEC3 before_move = VEC3(col->controller->getFootPosition().x, col->controller->getFootPosition().y, col->controller->getFootPosition().z);
	CEngine::get().getPhysics().moveEnemy(deltaMove, dt, get<TCompCollider>(), collision_down);
	VEC3 after_move = VEC3(col->controller->getFootPosition().x, col->controller->getFootPosition().y, col->controller->getFootPosition().z);

	checkDisplacement(before_move, after_move, amount_moved, point);
}

void TCompEnemy3D::moveToPoint(VEC3 point, float dt)
{
	//CEngine::get().getPhysics().move(animDeltaMove, dt, get<TCompCollider>());
}

/*bool TCompEnemy3D::rotate()
{
	//dbg("Rotando...\n");
	TCompTransform* my_trans = get<TCompTransform>();

	if (turnIdle) {
		TCompAnimator* anim = get<TCompAnimator>();
		if (timer_animation < time_animation) {
			float yaw, pitch;
			// Calculamos la rotacion para llegar al punto
			my_trans->getEulerAngles(&yaw, &pitch, nullptr);
			yaw += animDeltaYaw;
			my_trans->setEulerAngles(yaw, pitch, 0.0f);
			VEC3 abs_mov = VEC3::Transform(animDeltaMove, init_rot_turn);
			//dbg("Frame %d-> Estoy rotando %.4f en idle\n\n", getFrameCount(), animDeltaYaw);
			return true;
		}
		else {
			//dbg("Tiempo %.2f -> He terminado de girar 180 en idle\n", pruebas.elapsed());
			//dbg("Frame %d-> He terminado de rotar\n", getFrameCount());
			return false;
		}
	}


	return false;
}*/

/*void TCompEnemy3D::resetRotate()
{
	addLog("Reseteo la rotacion");

	turn180 = false;
	turn_left = false;
	turn_right = false;
	turnIdle = false;
	timerNewRot = 0;

	init_rot_turn = QUAT(0, 0, 0, 0);
	node_ready = false;
	time_animation = 0;
	searching = false;
}*/

bool TCompEnemy3D::isNoiseDifferentPlace()
{
	//Si ha pasado el tiempo de estar buscando y el sonido que hemos escuchado esta a mas distancia
	//entonces dejamos de buscar y vamos hacia el sonido
	
	bool cond1 = timer_goToNoise >= time_to_GoDifferentNoise;
	bool cond2 = VEC3::Distance(pos_suspicius, last_pos_suspicius) >= dist_to_GoDifferentNoise;

	if (cond1 && cond2)	return true;

	return false;
}

bool TCompEnemy3D::turnInIdle(float dt)
{
	addLog("== Turn in Idle ==");
	TCompTransform* my_trans = get<TCompTransform>();
	TCompAnimator* anim = get<TCompAnimator>();

	if (timer_animation < time_animation) {
		VEC3 abs_mov = VEC3::Transform(animDeltaMove, init_rot_turn);

		addFloatLog("Estoy girando", rad2deg(animDeltaYaw), "� yaw en idle");
		addFloat3Log("Y moviendome", abs_mov, "a la vez");

		my_trans->addPosition(abs_mov);
		CEngine::get().getPhysics().moveEnemy(abs_mov, dt, get<TCompCollider>(), collision_down);

		float yaw, pitch;
		// Calculamos la rotacion para llegar al punto
		my_trans->getEulerAngles(&yaw, &pitch, nullptr);
		yaw += animDeltaYaw;
		my_trans->setEulerAngles(yaw, pitch, 0.0f);

		if (use_save_anim_movs) {
			insertDebugRootMovs();
			debug_anim.insert(animDeltaMove, animDeltaYaw, animDeltaPitch, 0);
		}

		return true;
	}
	else {
		CEngine::get().getPhysics().moveEnemy(VEC3::Zero, dt, get<TCompCollider>(), collision_down);
		addLog("He terminado de girar en idle");
		resetTurn();
		return false;
	}

	return false;
}

bool TCompEnemy3D::turnInMovement(float dt)
{
	addLog("== Turn In Movement ==");
	TCompTransform* my_trans = get<TCompTransform>();
	TCompAnimator* anim = get<TCompAnimator>();

	if (timer_animation < time_animation) {
		VEC3 abs_mov = VEC3::Transform(animDeltaMove, init_rot_turn);
		my_trans->addPosition(abs_mov);
		CEngine::get().getPhysics().moveEnemy(abs_mov, dt, get<TCompCollider>(), collision_down);

		float yaw, pitch;
		// Calculamos la rotacion para llegar al punto
		my_trans->getEulerAngles(&yaw, &pitch, nullptr);
		yaw += animDeltaYaw;
		my_trans->setEulerAngles(yaw, pitch, 0.0f);

		addFloatLog("Estoy girando ", rad2deg(animDeltaYaw), "� yaw en movimiento");
		addFloat3Log("Y moviendome", abs_mov, "a la vez");
		if (use_save_anim_movs) {
			insertDebugRootMovs();
			debug_anim.insert(animDeltaMove, animDeltaYaw, animDeltaPitch, 0);
		}
		return true;
	}
	else {
		addLog("He terminado de girar en movimiento");
		CEngine::get().getPhysics().moveEnemy(VEC3::Zero, dt, get<TCompCollider>(), collision_down);
		resetTurn();
		return false;
	}

	return false;
}

void TCompEnemy3D::updateAnimationVars()
{
	TCompFSM* fsm = get<TCompFSM>();

	if (fsm) {
		fsm->setVariable("velocity", current_velocity);
		fsm->setVariable("searching", searching);
		fsm->setVariable("dead", dead);
		fsm->setVariable("chase_player", chase_player);
		fsm->setVariable("collision_down", collision_down);
		fsm->setVariable("turn180", turn180);
		fsm->setVariable("turn_left", turn_left);
		fsm->setVariable("turn_right", turn_right);
		fsm->setVariable("looking", looking);
		fsm->setVariable("releaseConstrict", releaseConstrict);
		fsm->setVariable("getUp", getUp);
		fsm->setVariable("constrict", constrict);
		fsm->setVariable("back_start_constrict", back_start_constrict);
		fsm->setVariable("front_start_constrict", front_start_constrict);
	}
}

void TCompEnemy3D::updateTimers(float dt)
{

	last_dt = dt;

	timer_animation += dt;
	timerNewRot += dt;
	timer_waitingDead += dt;
	timer_delay_dead += dt;
	timer_fall += dt;
	timer_change_suspicius_node += dt;
	timer_waitAnim += dt;
	timer_goToNoise += dt;
	timer_exitNode += dt;
	timer_waitAngry += dt;
	timer_distXsec += dt;
	timer_stopTurn += dt;
	timer_waitToRecapture += dt;
	timer_reposition += dt;
	timer_sincr += dt;
	timer_constrict += dt;
}

void TCompEnemy3D::onContact(const TMsgControllersOnContact& msgContact)
{
	h_OnContact = msgContact.h_sender;
}

void TCompEnemy3D::onDeathByOthers(const TMsgEnemyDeath& msgDeath)
{
	delay_time_dead = msgDeath.time;
	timer_delay_dead = 0;
	delayDead = true;
}

void TCompEnemy3D::onActivated(const TMsgActiveGuardian& msg)
{
	if (msg.active) {
		dbg("He sido activado\n");
		desactivated = false;
		//TCompBehaviourTree* bt = get<TCompBehaviourTree>();
		//bt->resetGame();
		//bt->onActivated(true);
	}
	else {
		dbg("He sido desactivado\n");
		if (desactivated)	return;
		node_ready = false;
		desactivated = true;
		TCompBehaviourTree* bt = get<TCompBehaviourTree>();
		bt->resetGame();
		//bt->onActivated(false);
	}
}

void TCompEnemy3D::onPlayerRelease(const TMsgPlayerRelease& msg)
{
	playerRelease = true;
	timer_waitToRecapture = 0;
}

void TCompEnemy3D::onSuspiciusEvent(const TMsgSuspiciusEvent& msg)
{
	suspicius_noise sn;

	sn.intensity = msg.intensity;
	sn.position = msg.pos;
	sn.type = static_cast<suspicius_noise_type> (msg.type);

	dbg("He recibido un evento de rudio de sospecha en [%.2f %.2f %.2f] con intensidad %.2f\n", msg.pos.x, msg.pos.y, msg.pos.z, msg.intensity);
	dbg("======================================================================\n");
	
	suspicius_events.push_back(sn);
}

void TCompEnemy3D::addHeaderLog(string l)
{
	CEntity* ce = CHandle(this).getOwner();

	const char* name = ce->getName();
	double time = CApplication::get().getTimeInSecond();
	int frame = CApplication::get().getFrameCount();
	const char* sep = "=======================================================================\n";
	char log[512];
	snprintf(log, sizeof(log) - 1, "%sLOG[%s] %.2f[%d frames] -> %s\n%s", sep, name, time, frame, l.c_str(), sep);
	logEnemy.push_back(log);

	if (printCurrentLog)	dbg(log);
}

void TCompEnemy3D::addStatusLog()
{

	string log = "BEHAVIOR STATE - " + name_state;
	addHeaderLog(log);

	addFSMLog();
	addGeneralVarsLog();

}

void TCompEnemy3D::addFSMLog()
{
	if (notSaveFSMLog)	return;

	addLog("-----  FSM VARIABLES  -----");
	char buffer[64];

	snprintf(buffer, sizeof(buffer) - 1, "- Velocity %.2f", current_velocity);
	addLog(buffer);
	snprintf(buffer, sizeof(buffer) - 1, "- Searching %s ", searching ? "true" : "false");
	addLog(buffer);
	snprintf(buffer, sizeof(buffer)-1, "- Dead %s", dead ? "true" : "false");
	addLog(buffer);
	snprintf(buffer, sizeof(buffer)-1, "- Chase_player %s", chase_player ? "true" : "false");
	addLog(buffer);
	snprintf(buffer, sizeof(buffer)-1, "- Player_caught %s", player_caught ? "true" : "false");
	addLog(buffer);
	snprintf(buffer, sizeof(buffer)-1, "- Collision_down %s", collision_down ? "true" : "false");
	addLog(buffer);
	snprintf(buffer, sizeof(buffer)-1, "- Turn180 %s", turn180 ? "true" : "false");
	addLog(buffer);
	snprintf(buffer, sizeof(buffer)-1, "- Turn_left %s", turn_left ? "true" : "false");
	addLog(buffer);
	snprintf(buffer, sizeof(buffer)-1, "- Turn_right %s", turn_right ? "true" : "false");
	addLog(buffer);
	snprintf(buffer, sizeof(buffer)-1, "- Looking %s", looking ? "true" : "false");
	addLog(buffer);
	snprintf(buffer, sizeof(buffer) - 1, "- ReleaseConstrict %s", releaseConstrict ? "true" : "false");
	addLog(buffer);
	snprintf(buffer, sizeof(buffer) - 1, "- Constrict %s", constrict ? "true" : "false");
	addLog(buffer);
	snprintf(buffer, sizeof(buffer) - 1, "- Back_start_constrict %s", back_start_constrict ? "true" : "false");
	addLog(buffer);
	snprintf(buffer, sizeof(buffer) - 1, "- Front_start_constrict %s", front_start_constrict ? "true" : "false");
	addLog(buffer);
	snprintf(buffer, sizeof(buffer) - 1, "- Get Up %s", getUp ? "true" : "false");
	addLog(buffer);

	addLog("-------------------------");
}

void TCompEnemy3D::addGeneralVarsLog()
{

	if (notSaveVarsLog)	return;

	addLog(">>>>>  GENERAL VARIABLES  <<<<<");
	switch (state)
	{
	case enemyState::PATROL:
		addLog("-State : PATROL");
		break;
	case enemyState::ALERT:
		addLog("-State : ALERT");
		break;
	case enemyState::SUSPICIUS:
		addLog("-State : SUSPICIUS");
		break;
	case enemyState::PURSUER:
		addLog("-State : PURSUER");
		break;
	case enemyState::ATTACK:
		addLog("-State : ATTACK");
		break;
	case enemyState::CONSTRICT:
		addLog("-State : CONSTRICT");
		break;
	case enemyState::CAUGHT:
		addLog("-State : CAUGHT");
		break;
	case enemyState::SURRENDER:
		addLog("-State : SURRENDER");
		break;
	default:
		break;
	}

	addLog("\n      --SUSPICIUS SYSTEM--\n");

	addIntLog("- Suspicius Value: ", suspicius_bar, "");
	addBoolLog("- desactivated:", desactivated, "");
	addBoolLog("- player_seen:", player_seen, "");
	addBoolLog("- player release:", playerRelease, "");
	addBoolLog("- contact_with_player:", contact_with_player, "");
	addBoolLog("- Stopping of Turning:", stopping_of_turning, "");
	addBoolLog("- waiting_angry:", waiting_angry, "");
	addBoolLog("- surrender:", surrender, "");
	addFloatLog("- Distancia por segundo: ", distxsec, "");
	addFloat3Log("- Pos Suspicius: ", pos_suspicius, "");
	addFloat3Log("- Last Pos Suspicius: ", last_pos_suspicius, "");
	addFloat3Log("- Random Point: ", randomPoint, "");
	addFloatLog("- Timer Contact Player", timer_contact_player, "");
	addFloatLog("- Timer Stopping Turn", timer_stopTurn, "");
	addFloatLog("- Timer Change Suspicius Node", timer_change_suspicius_node, "");
	addFloatLog("- Timer Go To Noise", timer_goToNoise, "");
	addFloatLog("- Timer Exit Node", timer_exitNode, "");
	addFloatLog("- Timer Wait Angry", timer_waitAngry, "");
	addFloatLog("- Timer Dist por sec", timer_distXsec, "");
	addFloatLog("- Timer Wait To Recapture", timer_waitToRecapture, "");

	addLog("\n      --NAVMESH SYSTEM--\n");

	addLog("--Puntos navmesh");
	for (auto np : navPath) {
		addFloat3Log("-NavPoint [", np, "]");
	}


	addLog("\n      --ROTATION SYSTEM--\n");
	addFloatLog("- Yaw to Point", yawToPoint, "");
	addFloatLog("- Time Animation", time_animation, "");
	addFloatLog("- Timer Animation", timer_animation, "");
	addFloatLog("- Timer New Rot", timerNewRot, "");
	addBoolLog("- Searching: ", searching, "");
	addBoolLog("- Looking: ", looking, "");
	addBoolLog("- Turn Idle: ", turnIdle, "");
	addBoolLog("- Turn Movement: ", turnMovement, "");
	addFloat4Log("- Init Mat Root", init_rot_turn, "");


	addLog(">>>>>>>>>>>>>>><<<<<<<<<<<<<<<");


}

void TCompEnemy3D::addFloat4Log(string ini, VEC4 value, string end)
{
	char buffer[128];

	snprintf(buffer, sizeof(buffer) - 1, "%s %.2f %.2f %.2f %.2f %s", ini.c_str(), value.x, value.y, value.z, value.w, end.c_str());
	addLog(buffer);
}

void TCompEnemy3D::addFloat3Log(string ini, VEC3 value, string end)
{
	char buffer[128];

	snprintf(buffer, sizeof(buffer) - 1, "%s %.2f %.2f %.2f %s", ini.c_str(), value.x, value.y, value.z, end.c_str());
	addLog(buffer);

}

void TCompEnemy3D::addFloatLog(string ini, float value, string end)
{
	char buffer[64];

	snprintf(buffer, sizeof(buffer) - 1, "%s %.2f %s", ini.c_str(), value, end.c_str());
	addLog(buffer);
}

void TCompEnemy3D::addBoolLog(string ini, bool value, string end)
{
	char buffer[64];

	snprintf(buffer, sizeof(buffer) - 1, "%s %s %s", ini.c_str(), value ? "true" : "false", end.c_str());
	addLog(buffer);
}

void TCompEnemy3D::addIntLog(string ini, int value, string end)
{
	char buffer[64];

	snprintf(buffer, sizeof(buffer) - 1, "%s %d %s", ini.c_str(), value, end.c_str());
	addLog(buffer);
}

void TCompEnemy3D::addStringLog(string ini, string value, string end)
{
	char buffer[128];

	snprintf(buffer, sizeof(buffer) - 1, "%s %s %s", ini.c_str(), value.c_str(), end.c_str());
	addLog(buffer);
}

void TCompEnemy3D::addLog(string l)
{
	logEnemy.push_back(l+"\n");
	if (printCurrentLog)	dbg("%s\n", l.c_str());
}

void TCompEnemy3D::writeLogInFile()
{
}

void TCompEnemy3D::printAllLog()
{
	for (auto log : logEnemy) {
		dbg(log.c_str());
	}
}

void TCompEnemy3D::initDebugRootMovs(string name)
{

	TCompSkeleton* skel = get<TCompSkeleton>();

	QUAT rot = skel->getAbsRotationBone(0);
	VEC3 pos = skel->getAbsTraslationBone(0);

	MAT44 mtx;
	mtx = MAT44::CreateFromQuaternion(rot) * MAT44::CreateTranslation(pos);

	VEC3 v = skel->returnVector(skel->getTypeFront(), mtx);

	float yaw, pitch, roll = 0.f;

	yaw = atan2f(v.x, v.z);
	float mdo = sqrtf(v.x * v.x + v.z * v.z);
	pitch = atan2f(-v.y, mdo);


	debug_root.init(name, 0, pos, yaw, pitch, 0);
}

void TCompEnemy3D::insertDebugRootMovs()
{
	TCompSkeleton* skel = get<TCompSkeleton>();

	QUAT rot = skel->getAbsRotationBone(0);
	VEC3 pos = skel->getAbsTraslationBone(0);

	MAT44 mtx;
	mtx = MAT44::CreateFromQuaternion(rot) * MAT44::CreateTranslation(pos);

	VEC3 v = skel->returnVector(skel->getTypeFront(), mtx);

	float yaw, pitch, roll = 0.f;

	yaw = atan2f(v.x, v.z);
	float mdo = sqrtf(v.x * v.x + v.z * v.z);
	pitch = atan2f(-v.y, mdo);

	debug_root.insert(pos, yaw, pitch, 0.f);

}

void TCompEnemy3D::ToEulerAngles(QUAT q, float* yaw, float* pitch, float* roll)
{

	// roll (x-axis rotation)
	double sinr_cosp = 2 * (q.w * q.x + q.y * q.z);
	double cosr_cosp = 1 - 2 * (q.x * q.x + q.y * q.y);
	*roll = std::atan2(sinr_cosp, cosr_cosp);

	// pitch (y-axis rotation)
	double sinp = 2 * (q.w * q.y - q.z * q.x);
	if (std::abs(sinp) >= 1)
		*pitch = std::copysign(M_PI / 2, sinp); // use 90 degrees if out of range
	else
		*pitch = std::asin(sinp);

	// yaw (z-axis rotation)
	double siny_cosp = 2 * (q.w * q.z + q.x * q.y);
	double cosy_cosp = 1 - 2 * (q.y * q.y + q.z * q.z);
	*yaw = std::atan2(siny_cosp, cosy_cosp);

}


float TCompEnemy3D::ProgressBar(const char* optionalPrefixText, float value, const float minValue, const float maxValue, const char* format, const ImVec2& sizeOfBarWithoutTextInPixels, const ImVec4& colorLeft, const ImVec4& colorRight, const ImVec4& colorBorder) {
	if (value < minValue) value = minValue;
	else if (value > maxValue) value = maxValue;
	const float valueFraction = (maxValue == minValue) ? 1.0f : ((value - minValue) / (maxValue - minValue));
	const bool needsPercConversion = strstr(format, "%%") != NULL;

	ImVec2 size = sizeOfBarWithoutTextInPixels;
	if (size.x <= 0) size.x = ImGui::GetWindowWidth() * 0.25f;
	if (size.y <= 0) size.y = ImGui::GetTextLineHeightWithSpacing(); // or without

	const ImFontAtlas* fontAtlas = ImGui::GetIO().Fonts;

	if (optionalPrefixText && strlen(optionalPrefixText) > 0) {
		//ImGui::AlignFirstTextHeightToWidgets();
		ImGui::Text(optionalPrefixText);
		ImGui::SameLine();
	}

	if (valueFraction > 0) {
		ImGui::Image(fontAtlas->TexID, ImVec2(size.x * valueFraction, size.y), fontAtlas->TexUvWhitePixel, fontAtlas->TexUvWhitePixel, colorLeft, colorBorder);
	}
	if (valueFraction < 1) {
		if (valueFraction > 0) ImGui::SameLine(0, 0);
		ImGui::Image(fontAtlas->TexID, ImVec2(size.x * (1.f - valueFraction), size.y), fontAtlas->TexUvWhitePixel, fontAtlas->TexUvWhitePixel, colorRight, colorBorder);
	}
	ImGui::SameLine();

	ImGui::Text(format, needsPercConversion ? (valueFraction * 100.f + 0.0001f) : value);
	return valueFraction;
}
