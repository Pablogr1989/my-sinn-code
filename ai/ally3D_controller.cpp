#include "mcv_platform.h"
#include "ally3D_controller.h"
#include "ai_blackboard.h"

TCompEnemy3D* CAlly3DController::getC_enemy()
{
	CEntity* cen = h_owner;

	return cen->get<TCompEnemy3D>();
}

void CAlly3DController::create(string s)
{
	name = s;

	// ACCIONES
	ally3d_actions[""] = NULL;

	//Comunes
	// Muerte
	ally3d_actions["actionGoToEternity"] = (btaction)&CAlly3DController::actionGoToEternity;
	ally3d_actions["actionInHell"] = (btaction)&CAlly3DController::actionInHell;
	ally3d_actions["actionRebirth"] = (btaction)&CAlly3DController::actionRebirth;
	ally3d_actions["actionWhatHappened"] = (btaction)&CAlly3DController::actionWhatHappened;
	// Desactivado
	ally3d_actions["actionGoToInitPoint"] = (btaction)&CAlly3DController::actionGoToInitPoint;
	ally3d_actions["actionWaitToActivated"] = (btaction)&CAlly3DController::actionWaitToActivated;

	//Patroller
		//Patrol
	ally3d_actions["actionRotateToLook"] = (btaction)&CAlly3DController::actionRotateToLook;
	ally3d_actions["actionResetWpt"] = (btaction)&CAlly3DController::actionResetWpt;
	ally3d_actions["actionSeekWpt"] = (btaction)&CAlly3DController::actionSeekWpt;
	ally3d_actions["actionLookAround"] = (btaction)&CAlly3DController::actionLookAround;
	ally3d_actions["actionNextWpt"] = (btaction)&CAlly3DController::actionNextWpt;
		//Alert
	ally3d_actions["actionPatrollerAlert"] = (btaction)&CAlly3DController::actionSearchAround;
		//Suspiscius
	ally3d_actions["actionRotateToSearch"] = (btaction)&CAlly3DController::actionSearchSomething;
	ally3d_actions["actionGoToNoise"] = (btaction)&CAlly3DController::actionGoToNoise;
		//Pursue
	ally3d_actions["actionPatrollerPursuer"] = (btaction)&CAlly3DController::actionPursue;
		//Surrender
	ally3d_actions["actionChooseWpt"] = (btaction)&CAlly3DController::actionChooseWpt;
	ally3d_actions["actionGoBackWpt"] = (btaction)&CAlly3DController::actionGoBackWpt;
		//Attack
	ally3d_actions["actionWait"] = (btaction)&CAlly3DController::actionWait;
	ally3d_actions["actionTryToCatch"] = (btaction)&CAlly3DController::actionTryToCatch;

		// Constrict - Caught
	ally3d_actions["actionRepositionInCliff"] = (btaction)&CAlly3DController::actionReposition;
	ally3d_actions["actionRepositionInConstrict"] = (btaction)&CAlly3DController::actionReposition;
	ally3d_actions["actionConstrict"] = (btaction)&CAlly3DController::actionConstrict;
	ally3d_actions["actionReleaseConstrict"] = (btaction)&CAlly3DController::actionReleaseConstrict;
	ally3d_actions["actionGetUp"] = (btaction)&CAlly3DController::actionGetUp;	
	ally3d_actions["actionReachCliff"] = (btaction)&CAlly3DController::actionReachCliff;
	ally3d_actions["actionThrowPlayer"] = (btaction)&CAlly3DController::actionThrowPlayer;
	ally3d_actions["actionWaitThrow"] = (btaction)&CAlly3DController::actionWaitThrow;
	ally3d_actions["actionWaitPlayerDead"] = (btaction)&CAlly3DController::actionWaitPlayerDead;  //FUNCION MANUAL NO SALE EN EL GRAPH
	

	//GateKeeper	
	ally3d_actions["actionGuard"] = (btaction)&CAlly3DController::actionGuard;
	ally3d_actions["actionGKAlert"] = (btaction)&CAlly3DController::actionSearchAround;
	ally3d_actions["actionGKSuspicius"] = (btaction)&CAlly3DController::actionSearchSomething;
	ally3d_actions["actionDefend"] = (btaction)&CAlly3DController::actionDefend;
	ally3d_actions["actionHit"] = (btaction)&CAlly3DController::actionHit;	
	ally3d_actions["actionRotateToPlayer"] = (btaction)&CAlly3DController::actionRotateToPlayer;
	ally3d_actions["actionMoveToPlayer"] = (btaction)&CAlly3DController::actionMoveToPlayer;
	ally3d_actions["actionOrbitAroundGP"] = (btaction)&CAlly3DController::actionOrbitAroundGP;


	// CONDICIONES
	ally3d_conditions[""] = NULL;
	ally3d_conditions["conditionDeath"] = (btcondition)&CAlly3DController::conditionDeath;
	ally3d_conditions["conditionDesactivated"] = (btcondition)&CAlly3DController::conditionDesactivated;
	ally3d_conditions["conditionPatroller"] = (btcondition)&CAlly3DController::conditionPatroller;
	ally3d_conditions["conditionGatekeeper"] = (btcondition)&CAlly3DController::conditionGatekeeper;
	ally3d_conditions["conditionPatrol"] = (btcondition)&CAlly3DController::conditionPatrol;
	ally3d_conditions["conditionPatrollerAlert"] = (btcondition)&CAlly3DController::conditionAlert;
	ally3d_conditions["conditionPatrollerSuspicius"] = (btcondition)&CAlly3DController::conditionSuspicius;
	ally3d_conditions["conditionPatrollerCombat"] = (btcondition)&CAlly3DController::conditionCombat;
	ally3d_conditions["conditionGuard"] = (btcondition)&CAlly3DController::conditionGuard;
	ally3d_conditions["conditionGKAlert"] = (btcondition)&CAlly3DController::conditionAlert;
	ally3d_conditions["conditionGKSuspicius"] = (btcondition)&CAlly3DController::conditionSuspicius;
	ally3d_conditions["conditionGKCombat"] = (btcondition)&CAlly3DController::conditionCombat;
	ally3d_conditions["conditionDefend"] = (btcondition)&CAlly3DController::conditionWait;
	ally3d_conditions["conditionGKAttack"] = (btcondition)&CAlly3DController::conditionAttack;
	ally3d_conditions["conditionResetWpt"] = (btcondition)&CAlly3DController::conditionResetWpt;
	ally3d_conditions["conditionFollowWpt"] = (btcondition)&CAlly3DController::conditionFollowWpt;
	ally3d_conditions["conditionWait"] = (btcondition)&CAlly3DController::conditionWait;
	ally3d_conditions["conditionPatrollerAttack"] = (btcondition)&CAlly3DController::conditionAttack;
	ally3d_conditions["conditionCliff"] = (btcondition)&CAlly3DController::conditionCliff;
	ally3d_conditions["conditionConstrict"] = (btcondition)&CAlly3DController::conditionPatrollerConstrict;
	ally3d_conditions["conditionEternity"] = (btcondition)&CAlly3DController::conditionEternity;
	ally3d_conditions["conditionTempDeath"] = (btcondition)&CAlly3DController::conditionTempDeath;
	ally3d_conditions["conditionPatrollerPursuer"] = (btcondition)&CAlly3DController::conditionPursuer;
	ally3d_conditions["conditionSurrender"] = (btcondition)&CAlly3DController::conditionSurrender;
}

void CAlly3DController::loadNodes(const json& j, TEntityParseContext& ctx)
{

	bool first_time = true;
	string node_root;

	assert(j.is_array());
	// Para cada nodo en el json
	for (auto it = j.begin(); it != j.end(); ++it) {
		assert(it->is_object());

		//Cogemos el padre solo si no es la primera vez
		string node_parent;
		if (!first_time)
			node_parent = it->value("parent", "");

		//Cogemos el nombre del nodo
		string name = it->value("name", "");

		//Cogemos la accion del nodo y buscamos su funcion en nuestro mapa de acciones
		string sAction = it->value("action", "");
		btaction action = ally3d_actions[sAction];

		// Cogemos la condicion del nodo y buscamos su funcion en nuestro mapa de condiciones
		string sCondition = it->value("condition", "");
		btcondition condition = ally3d_conditions[sCondition];

		//Cogemos el tipo de nodo y buscamos su equivalencia en el vector de nodos
		string sType = it->value("type", "");
		int iType = getTypeNodes(sType);

		if (first_time) {
			first_time = false;
			createRoot(name, iType, condition, action);
			node_root = name;

		}
		else {
			auto node_created = addChild(node_parent, name, iType, condition, action);
		}
	}

	loadManualNodes(node_root);
}

void CAlly3DController::loadManualNodes(string node)
{
	addChild(node, "WaitPlayerDead", getTypeNodes("Action"), nullptr, ally3d_actions["actionWaitPlayerDead"]);
}

bool CAlly3DController::conditionDeath()
{
	//return false;

	return getC_enemy()->isDead();
}

bool CAlly3DController::conditionDesactivated()
{
	return getC_enemy()->isDesactivated();
}

bool CAlly3DController::conditionEternity()
{
	return getC_enemy()->isDestroyed();
}

bool CAlly3DController::conditionTempDeath()
{
	return !getC_enemy()->isDestroyed();
}

bool CAlly3DController::conditionAlert()
{

	//return false;

	return getC_enemy()->isAlert();
}

bool CAlly3DController::conditionSuspicius()
{

	//return false;

	return getC_enemy()->isSuspicius();
}

bool CAlly3DController::conditionPursuer()
{
	return getC_enemy()->isAlarm();
}

bool CAlly3DController::conditionWait()
{

	//return false;

	return CAI_BlackBoard::get().whoCaughtPlayer() != h_owner;
}

bool CAlly3DController::conditionAttack()
{

	//return false;

	return CAI_BlackBoard::get().whoCaughtPlayer() == h_owner;
}

bool CAlly3DController::conditionPatroller()
{
	return getC_enemy()->isType(ENEMY_PATROLLER);
}

bool CAlly3DController::conditionGatekeeper()
{

	return false;

	//return getC_enemy()->isType(TCompEnemy3D::ENEMY_GATEKEEPER);
}

bool CAlly3DController::conditionGuard()
{

	return false;

	//return getC_enemy()->isNormal();
}

bool CAlly3DController::conditionPatrol()
{
	return getC_enemy()->isNormal();
}

bool CAlly3DController::conditionResetWpt()
{
	return getC_enemy()->isOutOfPatrol();
}

bool CAlly3DController::conditionFollowWpt()
{
	return !getC_enemy()->isOutOfPatrol();
}

bool CAlly3DController::conditionCombat()
{
	//return false;

	return getC_enemy()->getPlayerSeen();
}

bool CAlly3DController::conditionCliff()
{
	return false;
	//return getC_enemy()->checkGoToCliff();
}

bool CAlly3DController::conditionPatrollerConstrict()
{
	return true;
	//return !getC_enemy()->checkGoToCliff();
}

bool CAlly3DController::conditionSurrender()
{
	return getC_enemy()->isSurrender();
}

int CAlly3DController::actionGoToEternity(float dt)
{
	TCompEnemy3D* c_enemy = getC_enemy();

	c_enemy->name_state = "Death Forever and Ever";

	c_enemy->resetSuspiciusBar();

	if (c_enemy->isDestroyed())	return STAY;

	if (c_enemy->canDestroyEnemy()) {
		c_enemy->destroyEnemy();
	}
	return STAY;
}

int CAlly3DController::actionInHell(float dt)
{
	TCompEnemy3D* c_enemy = getC_enemy();

	c_enemy->name_state = "In hell";

	if (c_enemy->isStillDead()) {
		c_enemy->applyDeathMov(dt);
	}
	else {
		return LEAVE;
	}


	return 0;
}

int CAlly3DController::actionRebirth(float dt)
{
	TCompEnemy3D* c_enemy = getC_enemy();

	c_enemy->name_state = "Rebirth";

	if (c_enemy->applyRebirthMov(dt)) {
		return STAY;
	}
	else {
		return LEAVE;
	}
}

int CAlly3DController::actionWhatHappened(float dt)
{
	getC_enemy()->name_state = "What the fuck!!!??";

	TCompEnemy3D* c_enemy = getC_enemy();

	//En el momento en el que haya una minima sospecha dejamos de hacer la patrulla
	// y pasamos a buscar aquello que nos inquieta
	if (!c_enemy->isNormal()) {
		resetTree();
		c_enemy->resetPatrol();
		return ABORT;
	}

	c_enemy->prepareLookAt();

	//Si ya hemos termionado la busqueda nos salimos del nodo
	if (getC_enemy()->isCurrentActionAnimFinished(dt)) {
		c_enemy->resetLookAt();
		return LEAVE;
	}

	return STAY;
}

int CAlly3DController::actionGoToInitPoint(float dt)
{
	getC_enemy()->name_state = "Desactivated - Go To Init Point";

	TCompEnemy3D* c_enemy = getC_enemy();

	c_enemy->addLog("== ACTION Go To Init Point ==");

	if (!c_enemy->isDesactivated()) {
		if (!c_enemy->isRotating()) {
			resetTree();
			c_enemy->resetGoToInitPoint();
			c_enemy->addLog("== ABORT Go To Init Point ==");
			return ABORT;
		}
	}

	c_enemy->prepareGoToInitPoint();

	if (c_enemy->goToInitPoint(dt)) {
		c_enemy->resetGoToInitPoint();
		c_enemy->addLog("== LEAVE Go To Init Point ==");
		return LEAVE;
	}
	c_enemy->addLog("== STAY Go To Init Point ==");
	return STAY;
}

int CAlly3DController::actionWaitToActivated(float dt)
{
	getC_enemy()->name_state = "Desactivated - Wait To Activated";

	TCompEnemy3D* c_enemy = getC_enemy();

	c_enemy->addLog("== ACTION Wait To Activated ==");

	if (!c_enemy->isDesactivated()) {
		c_enemy->resetGoToInitPoint();
		c_enemy->addLog("== ABORT Wait To Activated ==");
		return LEAVE;
	}

	return STAY;
}

int CAlly3DController::actionSearchAround(float dt)
{

	getC_enemy()->name_state = "Alert - Search Around";

	TCompEnemy3D* c_enemy = getC_enemy();

	c_enemy->calculateState(enemyState::ALERT);

	if (waitingForExit) {
		if (c_enemy->canExitNode()) {
			resetTree();
			waitingForExit = false;
			return ABORT;
		}
	}
	else{
		// Nos salimos del estado de alerta si hemos sido alarmados o hemos visto al player
		if (c_enemy->isAlarm() || c_enemy->getPlayerSeen()) {
			c_enemy->resetLookAt();
			waitingForExit = true;
			return STAY;
		}

		if (c_enemy->isSuspicius()) {
			//Solo podras irte a Suspicius desde Alert, si recien acabas de empezar el estado
			if (c_enemy->canExitAlertState()) {
				c_enemy->resetLookAt();
				waitingForExit = true;
				return STAY;
			}
		}

		c_enemy->prepareLookAt();
	}

	if (getC_enemy()->isCurrentActionAnimFinished(dt)) {
		c_enemy->resetLookAt();
		//Si no hemos tenido se�ales del player reseteamos la barra para volver a la patrulla directamente
		if (c_enemy->isSuspicius() || c_enemy->isAlarm() || c_enemy->getPlayerSeen()) {
			waitingForExit = false;
			return LEAVE;
		}
		else {
			c_enemy->resetSuspiciusBar();
			waitingForExit = false;
			return LEAVE;
		}

	}

	return STAY;
}

int CAlly3DController::actionSearchSomething(float dt)
{

	getC_enemy()->name_state = "Suspicius - Search Something";

	TCompEnemy3D* c_enemy = getC_enemy();

	c_enemy->calculateState(enemyState::SUSPICIUS);

	if (waitingForExit) {
		if (c_enemy->canExitNode()) {
			resetTree();
			waitingForExit = false;
			return ABORT;
		}
	}
	else{
		if (c_enemy->isAlarm() || c_enemy->getPlayerSeen()) {
			c_enemy->resetSearch();
			waitingForExit = true;
			return STAY;
		}

		c_enemy->prepareSearch();
	}

	/*if (c_enemy->isNoiseDifferentPlace()) {
		return LEAVE;
		c_enemy->resetSearch();
	}*/

	//Si ya hemos termionado la busqueda nos salimos del nodo
	if (c_enemy->isCurrentActionAnimFinished(dt)) {
		//Si no hemos tenido se�ales del player reseteamos la barra para volver a la patrulla directamente
		if (!c_enemy->isAlarm() && !c_enemy->getPlayerSeen()) {
			c_enemy->resetSuspiciusBar();
		}
		c_enemy->resetSearch();
		waitingForExit = false;
		return LEAVE;
	}

	return STAY;
}

int CAlly3DController::actionChooseWpt(float dt)
{

	TCompEnemy3D* c_enemy = getC_enemy();

	c_enemy->name_state = "Surrender - Choose Wpt";

	c_enemy->addLog("== ACTION Choose Wpt to Back ==");

	if (c_enemy->resetWayPoint()) {
		c_enemy->addLog("== LEAVE Choose Wpt to Back ==");
		return LEAVE;
	}
	else {
		c_enemy->addLog("== STAY Choose Wpt to Back ==");
		return STAY;
	}
}

int CAlly3DController::actionGoBackWpt(float dt)
{
	getC_enemy()->name_state = "Surrender - Go Back Wpt";

	TCompEnemy3D* c_enemy = getC_enemy();

	c_enemy->addLog("== ACTION Go Back Wpt ==");

	c_enemy->calculateState(enemyState::SURRENDER);

	//En el momento en el que haya una minima sospecha dejamos de ir a la patrulla
	// y pasamos a buscar aquello que nos inquieta
	if (c_enemy->isAlarm()) {
		//Si no estaba rotando podemos salir de este estado
		if (!c_enemy->isRotating()) {
			resetTree();
			c_enemy->resetGoBackWpt();
			c_enemy->addLog("== ABORT Go Back Wpt ==");
			return ABORT;
		}
	}

	c_enemy->prepareGoBackWpt();

	if (c_enemy->goToWayPoint(dt)) {
		c_enemy->resetGoBackWpt();
		c_enemy->resetSuspiciusBar();
		c_enemy->addLog("== LEAVE Go Back Wpt ==");
		return LEAVE;
	}
	c_enemy->addLog("== STAY Go Back Wpt ==");
	return STAY;
}

int CAlly3DController::actionTryToCatch(float dt)
{
	TCompEnemy3D* c_enemy = getC_enemy();
	c_enemy->name_state = "Attack - Try to Catch";

	c_enemy->addLog("== ACTION TRY TO CATCH ==");


	c_enemy->calculateState(enemyState::ATTACK);

	if (c_enemy->isStoppingTurn()) {
		c_enemy->addLog("== STAY TRY TO CATCH ==");
		return STAY;
	}

	//CONTACTO CON EL PLAYER
	if (c_enemy->isOnContactWithPlayer()) {  //HACER QUE CUANDO SE VAYA ENTRE EN ESTADO DE VUELTA A PATRULLA PASANDO DEL PLAYER
		if (c_enemy->isRotating()) {
			c_enemy->stopTurn();
			c_enemy->addLog("== STAY TRY TO CATCH ==");
			return STAY;
		}
		else {
			//dbg("He tocado al player\n");
			c_enemy->initWaitingAngry(false);
			c_enemy->resetTryToCatch(true);
			c_enemy->sendConstrict();
			c_enemy->addLog("== LEAVE TRY TO CATCH ==");
			return LEAVE;
		}
	}

	// ESPERANDO PORQUE NO PODEMOS LLEGAR AL PLAYER
	if (c_enemy->isWaitingAngry()) {

		if (c_enemy->canGoToTryCatch()) {
			//dbg("Estaba esperando enfadado pero ya puedo llegar hasta el player\n");
			c_enemy->initWaitingAngry(false);
			c_enemy->setChase(true);
			c_enemy->addLog("== STAY TRY TO CATCH ==");
			return STAY;
		}

		if (c_enemy->isFinishedWaitAngry()) {
			//dbg("Ya me he cansado de esperar enfadado, me rindo\n");
			c_enemy->setSurrender(true);
			resetTree();
			c_enemy->resetTryToCatch();
			c_enemy->initWaitingAngry(false);
			c_enemy->addLog("== ABORT TRY TO CATCH ==");
			return ABORT;
		}
	}
	// CORREMOS A POR EL PLAYER
	else {
		c_enemy->prepareTryToCatch();

		//Si se ha alejado un poco de mi, entonces salgo del estado
		if (!c_enemy->getPlayerSeen()) {
			//dbg("Ya no veo al player\n");
			//Si no estaba rotando podemos salir de este estado
			if (!c_enemy->isRotating()) {
				//dbg("Como no esta rotando podemos salir de Try to Catch\n");
				resetTree();
				c_enemy->resetTryToCatch();
				c_enemy->addLog("== ABORT TRY TO CATCH ==");
				return ABORT;
			}
		}

		//Corremos detras del player
		if (c_enemy->goToTryToCatch(dt)) {
		//Si devuelve True significa que ya ha llegado al final de su camino, y como no hay fin cuando perseguimos al player, significa que no podemos alcanzarlo
			if (!c_enemy->isRotating()) {
				//dbg("Iba de camino a coger al player y no llego, asi que me espero enfadado\n");
				c_enemy->initWaitingAngry(true);
				c_enemy->setChase(false);
			}
		}
	}

	c_enemy->addLog("== STAY TRY TO CATCH ==");
	return STAY;
}

int CAlly3DController::actionReposition(float dt)
{

	TCompEnemy3D* c_enemy = getC_enemy();
	c_enemy->name_state = "Reposition Constrict";

	c_enemy->addLog("== ACTION REPOSITION CONSRICT ==");

	c_enemy->calculateState(enemyState::CONSTRICT);

	if (!c_enemy->isRepositionFinished()) {
		c_enemy->addLog("Estoy esperando a que el player termine de reposicionarse");
		return STAY;
	}

	c_enemy->prepareStartConstrict();


	if (getC_enemy()->isCurrentActionAnimFinished(dt)) {
		c_enemy->resetStartConstrict();
		return LEAVE;
	}
	else {
		c_enemy->actionStartConstrict();
		return STAY;
	}


}

int CAlly3DController::actionConstrict(float dt)
{
	TCompEnemy3D* c_enemy = getC_enemy();

	c_enemy->name_state = "Constrict";

	c_enemy->addLog("== ACTION CONSRICT ==");

	c_enemy->calculateState(enemyState::CONSTRICT);

	c_enemy->prepareConstrict();

	if (c_enemy->isTimeToVictory()) {
		c_enemy->prepareVictory();
	}

	if (getC_enemy()->isConstrictHugFinished()) {
		c_enemy->sendPlayerDead();
		c_enemy->setBTState("WaitPlayerDead");
		c_enemy->addLog("El player va a morir, esperamos a que estire la pata");
		c_enemy->resetVictory();
		return STAY;
	}
	else {
		if (c_enemy->isPlayerRelease()) {
			c_enemy->resetConstrict();
			c_enemy->resetVictory();
			c_enemy->addLog("El player se ha soltado");
			return LEAVE;
		}
	}

	return STAY;
}

int CAlly3DController::actionReleaseConstrict(float dt)
{
	TCompEnemy3D* c_enemy = getC_enemy();

	c_enemy->name_state = "ReleaseConstrict";

	c_enemy->addLog("== ACTION RELEASE CONSRICT ==");

	c_enemy->setConstrictRender(enemyState::CONSTRICT_RELEASE);

	c_enemy->prepareReleaseConstrict();

	if (c_enemy->isCurrentActionAnimFinished(dt)) {
		if (c_enemy->canRecapture()) {
			c_enemy->resetReleaseConstrict();
			return LEAVE;
		}
		else {
			return STAY;
		}
	}
	else {
		c_enemy->actionReleaseConstrict();
		return STAY;
	}
}

int CAlly3DController::actionGetUp(float dt)
{
	TCompEnemy3D* c_enemy = getC_enemy();

	c_enemy->name_state = "GetUp";

	c_enemy->addLog("== ACTION GET UP ==");

	c_enemy->setConstrictRender(enemyState::CONSTRICT_GETUP);

	c_enemy->prepareGetUp();

	if (c_enemy->isCurrentActionAnimFinished(dt)) {
		c_enemy->resetGetUp();
		return LEAVE;
	}
	else {
		c_enemy->actionGetUp();
		return STAY;
	}
}

int CAlly3DController::actionPursue(float dt)
{
	getC_enemy()->name_state = "Pusuer";
	TCompEnemy3D* c_enemy = getC_enemy();

	c_enemy->addLog("== ACTION PURSUER ==");

	c_enemy->calculateState(enemyState::PURSUER);

	//Si estabamos esperando porque no podemos llegar hasta el player
	if (c_enemy->isWaitingAngry()) {
		//comprobamos si ya podemos llegar hasta el
		if (c_enemy->canGoToPlayer()) {
			//dbg("Estaba esperando enfadado pero ya puedo llegar hasta el player\n");
			c_enemy->initWaitingAngry(false);
			c_enemy->addLog("== STAY PURSUER ==");
			return STAY;
		}
		//Si aun nada, preguntamos si ya ha terminado el tiempo de espera antes de rendirnos
		if (c_enemy->isFinishedWaitAngry()) {
			//dbg("Ya me he cansado de esperar enfadado, me rindo\n");
			c_enemy->setSurrender(true);
			resetTree();
			c_enemy->resetGoToPlayer();
			c_enemy->initWaitingAngry(false);
			c_enemy->addLog("== ABORT PURSUER ==");
			return ABORT;
		}
	}
	//Si no estabamos esperando
	else {
		//Si ya no estoy en alarma, o lo estoy viendo de cerca
		if (!c_enemy->isAlarm() || c_enemy->getPlayerSeen()) {
			//dbg("Ya no estoy en alarma o ahora veo al player cerca \n");
			//Si no estaba rotando podemos salir de este estado
			if (!c_enemy->isRotating()) {
				//dbg("Como no esta rotando podemos salir de Pursuer\n");
				resetTree();
				c_enemy->resetGoToPlayer();
				c_enemy->addLog("== ABORT PURSUER ==");
				return ABORT;
			}
		}

		c_enemy->prepareGoToPlayer();

		//Nos vamos hacia el player
		if (c_enemy->goToPlayer(dt)) {
			//Si GoToPlayer devuelve true, significa que ya ha llegado al punto final del path y no puede seguir
			//Antes de iniciar el waitingAngry comprobamos si estabamos rotando
			if (!c_enemy->isRotating()) {
				//dbg("Iba persiguiendo al player y no llego, asi que me espero enfadado\n");
				c_enemy->initWaitingAngry(true);
				c_enemy->addLog("== STAY PURSUER ==");
				return STAY;
			}
		}
	}

	return STAY;
}

int CAlly3DController::actionResetWpt(float dt)
{
	getC_enemy()->name_state = "Patrol - Reset Wpt";

	if (getC_enemy()->resetWayPoint()) {
		return LEAVE;
	}
	else {
		return STAY;
	}
}

int CAlly3DController::actionSeekWpt(float dt)
{
	getC_enemy()->name_state = "Patrol - Seek Wpt";

	TCompEnemy3D* c_enemy = getC_enemy();

	c_enemy->calculateState(enemyState::PATROL);

	//En el momento en el que haya una minima sospecha dejamos de hacer la patrulla
	// y pasamos a buscar aquello que nos inquieta
	if (!c_enemy->isNormal()) {
		//Si no estaba rotando podemos salir de este estado
		if (!c_enemy->isRotating()) {
			resetTree();
			c_enemy->resetPatrol();
			return ABORT;
		}
	}

	c_enemy->preparePatrol();

	if (c_enemy->goToWayPoint(dt)) {
		c_enemy->resetPatrol();
		return LEAVE;
	}
	return STAY;
}

int CAlly3DController::actionRotateToLook(float dt)
{
	return LEAVE;

	//DEPRECATED!!
	//Los guardianes ya no se giran cuando llegan al punto de guardian

	/*getC_enemy()->name_state = "PATROL - Rotate to Look";

	TCompEnemy3D* c_enemy = getC_enemy();	

	if (c_enemy->checkNeedRotate()) {
		if (c_enemy->rotate()) {
			return STAY;
		}
	}
	c_enemy->resetRotate();
	return LEAVE;*/
}

int CAlly3DController::actionLookAround(float dt)
{
	getC_enemy()->name_state = "Patrol - Look Around";

	TCompEnemy3D* c_enemy = getC_enemy();

	c_enemy->calculateState(enemyState::PATROL);

	if (waitingForExit) {
		if (c_enemy->canExitNode()) {
			resetTree();
			waitingForExit = false;
			return ABORT;
		}
	}

	//En el momento en el que haya una minima sospecha dejamos de hacer la patrulla
	// y pasamos a buscar aquello que nos inquieta
	if (!c_enemy->isNormal() && !waitingForExit) {
		waitingForExit = true;
		c_enemy->resetLookAt();
		return STAY;
	}

	if(!waitingForExit)	c_enemy->prepareLookAt();

	if (c_enemy->isCurrentActionAnimFinished(dt)) {
		c_enemy->resetLookAt();
		waitingForExit = false;
		return LEAVE;
	}

	return STAY;
}

int CAlly3DController::actionNextWpt(float dt)
{
	getC_enemy()->name_state = "Patrol - Next Wpt";

	TCompEnemy3D* c_enemy = getC_enemy();

	if (c_enemy->goingRandPoint()) {
		dbg("Elijo un punto de patrulla random\n");
		c_enemy->setOutOfPatrol(true);
	}
	else {
		getC_enemy()->setNextWP();
	}

	return LEAVE;
}

int CAlly3DController::actionGoToNoise(float dt)
{

	getC_enemy()->name_state = "Suspicius - Go to Noise";

	TCompEnemy3D* c_enemy = getC_enemy();

	c_enemy->calculateState(enemyState::SUSPICIUS);

	//En el momento en el que haya una minima sospecha dejamos de hacer la patrulla
	// y pasamos a buscar aquello que nos inquieta
	if (c_enemy->isAlarm() || c_enemy->getPlayerSeen()) {

		//Si no estaba rotando podemos salir de este estado
		if (!c_enemy->isRotating()) {
			resetTree();
			c_enemy->resetGoToNoise();
			return ABORT;
		}
	}
	c_enemy->prepareGoNoise();

	if (c_enemy->goToNoise(dt)) {
		//dbg("He terminado de ir hacia el punto de sospecha\n");
		c_enemy->resetGoToNoise();
		return LEAVE;
	}

	return STAY;
}


int CAlly3DController::actionReachCliff(float dt)
{

	getC_enemy()->sendGoToCliff();

	return LEAVE;
}

int CAlly3DController::actionThrowPlayer(float dt)
{

	getC_enemy()->sendThrowPlayer();

	return LEAVE;
}

int CAlly3DController::actionGuard(float dt)
{
	TCompEnemy3D* c_enemy = getC_enemy();	

	//En el momento en el que haya una minima sospecha dejamos de hacer la patrulla
	// y pasamos a buscar aquello que nos inquieta
	if (!c_enemy->isNormal()) {
		resetTree();
		return ABORT;
	}

	//if (getC_enemy()->isSearching()) {
	//	getC_enemy()->prepareSearch(5, 5);
	//}

	c_enemy->prepareSearch();


	if (c_enemy->isCurrentActionAnimFinished(dt)) {
		c_enemy->resetSearch();
		return LEAVE;
	}

	return STAY;
}

int CAlly3DController::actionDefend(float dt)
{
	if (getC_enemy()->rotateToNoise(dt)) {
		return LEAVE;
	}

	return STAY;
}

int CAlly3DController::actionRotateToNoise(float dt)
{
	//Si ya estamos mirando hacia de donde provienen nuestra sospecha, nos salimos del nodo
	if (getC_enemy()->rotateToNoise(dt)) {
		return LEAVE;
	}

	return STAY;
}

int CAlly3DController::actionRotateToPlayer(float dt)
{
	/*if (getC_enemy()->rotateToPlayer(dt)) {
		return LEAVE;
	}*/
	return STAY;
}

int CAlly3DController::actionMoveToPlayer(float dt)
{
	TCompEnemy3D* c_enemy = getC_enemy();

	if (c_enemy->nearGuardPoint()) {
		c_enemy->goToOrbitPoint(dt);
		return STAY;
	}

	return LEAVE;
}

int CAlly3DController::actionOrbitAroundGP(float dt)
{
	return 0;
}

int CAlly3DController::actionHit(float dt)
{
	getC_enemy()->name_state = "Hit";

	getC_enemy()->hitPlayer();

	return LEAVE;
}

int CAlly3DController::actionWaitThrow(float dt)
{

	TCompEnemy3D* c_enemy = getC_enemy();

	c_enemy->calculateState(enemyState::CONSTRICT);

	if (c_enemy->isPlayerRelease()) {
		return LEAVE;
	}

	//getC_enemy()->victory(dt);

	return STAY;
}

int CAlly3DController::actionWait(float dt)
{

	TCompEnemy3D* c_enemy = getC_enemy();

	c_enemy->calculateState(enemyState::PURSUER_WAIT);

	c_enemy->prepareWait();

	if (!CAI_BlackBoard::get().isPlayerCaught()) {
		c_enemy->resetWait();
		return LEAVE;
	}

	return STAY;
}

int CAlly3DController::actionWaitPlayerDead(float dt)
{
	TCompEnemy3D* c_enemy = getC_enemy();

	c_enemy->calculateState(enemyState::CONSTRICT);

	dbg("Estoy esperando a que el player se muera!\n");

	return STAY;
}
