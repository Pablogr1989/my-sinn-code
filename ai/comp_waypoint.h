#pragma once
#include "components/common/comp_base.h"
#include "entity/common_msgs.h"
#include "entity/entity.h"

using namespace std;

class TCompWaypoint : public TCompBase
{

public:
  void onEntityCreated();
  void update(float dt);
  void load(const json& j, TEntityParseContext& ctx);
  void debugInMenu();
  void renderDebug();
  static void registerMsgs();

  void setIsEnable(bool e);

  void setEnable(const TMsgSetEnableWayPoint& msgWay);
  VEC3 getLookAt();
  VEC3 getPos();
  float getDistance(VEC3 p);
  bool isLongSearch() { return longSearch; }
  float getYaw();
  bool isEnable() { return enable; }


private:
  DECL_SIBLING_ACCESS();
  VEC3 lookAt = VEC3::Zero;
  string guardianName = "";
  CEntity* null;
  bool longSearch = false;
  bool registered = false;
  bool enable = true;

};
