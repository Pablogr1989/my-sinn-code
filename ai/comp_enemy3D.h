#pragma once
#include <mcv_platform.h>
#include "engine.h"
#include "components/common/comp_base.h"
#include "components/common/comp_transform.h"
#include "components/common/comp_fsm.h"
#include "sinn/modules/module_physics_sinn.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "sinn/characters/enemies/comp_waypoint.h"
#include "sound/soundEvent.h"
#include "sinn/modules/module_sound.h"
#include "skeleton/comp_skeleton.h"

using namespace std;

enum enemyType { ENEMY_PATROLLER, ENEMY_GATEKEEPER };
enum animType { IDLE180, IDLE90, MOV90, MOV180, SEARCH, LOOKAT, REBIRTH, DEAD, SPASM, FRONT_START_CONSTRICT, BACK_START_CONSTRICT, CONSTRICT, RELEASE_CONSCTRICT, GETUP };
enum enemyRender {ENEMY_RENDER_NORMAL, ENEMY_RENDER_ALERT, ENEMY_RENDER_ALARM, ENEMY_RENDER_DESACTIVATED};
enum class enemyState {PATROL, ALERT, SUSPICIUS, PURSUER, ATTACK, CONSTRICT, CAUGHT, SURRENDER, CONSTRICT_RELEASE, CONSTRICT_GETUP, PURSUER_WAIT};
enum class suspicius_noise_type {PLAYER_NOISE, STONE_NOISE};

struct suspicius_noise {
	float intensity;
	VEC3 position;
	suspicius_noise_type type;
};

class TCompEnemy3D : public TCompBase
{	
	////////////////////////////////////////////////
	// PRIVADAS
	////////////////////////////////////////////////
	DECL_SIBLING_ACCESS();
	/////////////////////////////////
	//			ATRIBUTOS

	// - Generales
	int type = 0;
	float walk_velocity = 1.5;
	float run_velocity = 4;
	VEC3 my_init_pos = VEC3::Zero;
	QUAT my_init_rot = QUAT(0, 0, 0, 0);
	float rebirthTime = 15;
	float destroyTime = 10;
	float max_distance_sound = 20;

	// - Especificos GateKeeper
	VEC3 guard_point = VEC3::Zero;
	float maxDist_ToGP = 5.f;  //Maxima distancia a la que se aleja un Gatekeeper de su punto de guardia

	// - Especificos Patroller
	float max_dist_cliff = 30.f;  //Maxima distancia a la que elegiria si ir para tirar al player


	// - Deteccion del enemigo
	float rad_det_noise = 12.f; //Radio de la esfera donde el enemigo comienza a escuchar al player
	float noise_threshold = 0.24f; // Umbral de ruido que el enemigo detecta
	float constant_noise = 2.f;  //Constante que se multiplica para ajustar los numeros del ruido como queramos
	float angle_detec = 40.f;
	float min_cone_dist = 3.75f;  //Distancia maxima del cono de vision cercano
	float max_cone_dist = 6.5f;  //Distancia maxima del cono de vision lejano
	float constConeVision = 0.1f; //Variable que se le a�ade al punto de la cabeza para que el cono empiece desde los ojos
	float constNoiseDetec = 10.f; //Una constante que utilizo para aumentar la barra de sospecha al escuchar un ruido
	// - Suspicius Bar
	int alert_value = 30;  // Al 30% de la barra entra en estado de alerta
	int suspicius_value = 50;  // Al 50% de la barra entra en estado de sospecha
	int alarm_value = 80; // Al 80% entra en estado de alarma

	// - Rotacion
	float min_angle_for90 = 50.f;  //Angulo minimo que debe girar de golpe para utilizar la animacion de rotacion de 90�
	float min_angle_for180 = 135.f;  //Angulo minimo que debe girar de golpe para utilizar la animacion de rotacion de 180�


	/////////////////////////////////
	//			VARIABLES
	// - Variables de animaciones
	float timer_waitAnim = 0;
	VEC3 animDeltaMove = VEC3::Zero;
	float animDeltaYaw = 0.f;
	float animDeltaPitch = 0.f;

	// VARIABLES FSM
	bool chase_player = false;
	bool turn180 = false;
	bool turn_left = false;
	bool turn_right = false;
	bool back_start_constrict = false;
	bool front_start_constrict = false;
	bool constrict = false;
	bool releaseConstrict = false;
	bool getUp = false;


	// - Variables del player
	float vel_player = 0.f;
	VEC3 pos_player = VEC3::Zero;
	VEC3 dir_player = VEC3::Zero;
	//float noise_player = 0.f;
	bool player_caught = false;
	bool player_dead = false;

	// - Variables basicas
	float last_dt = 0.f;
	enemyState state;
	float floor_value = 0.f;
	VEC3 my_Pos = VEC3::Zero;
	VEC3 my_posHead = VEC3::Zero;
	VEC3 my_frontHead = VEC3::Zero;
	VEC3 my_leftHead = VEC3::Zero;
	VEC3 my_upHead = VEC3::Zero;
	VEC3 my_frontRoot = VEC3::Zero;
	VEC3 my_leftRoot = VEC3::Zero;

	//Variables de sonidos
	bool last_value_rightorleft = false;  // Se usa para reproducir solo una vez el sonido del paso
	bool rightOrLeftFootInFloor = false; // Indica cual de los dos pies ha tocado la ultima vez el suelo en una animacion
	float distStepToFloor = 0.11; //Margen que tiene de maximo el pie para que se detecte que esta pisando el suelo
	SoundEvent* steps_event;


	//Variables cuando atrapa al Player
	CHandle h_null=CHandle();
	CHandle h_OnContact=CHandle();
	float dist_toCapture = 1.5f;
	bool contact_with_player = false;
	float timer_contact_player = 0.f;
	float angle_to_capture = 45.f;    //Angulo en el que tenemos que estar para que nos atrapen (Asi, si le tocamos la espalda no contara con que nos atrapa)
	int frames_to_remove_contact = 1;
	float time_contact_player = 0.05f;
	bool is_in_cone_contact = false;
	float timer_waitToRecapture = 0.f;
	float time_waitToRecapture = 7.0f;
	bool playerRelease = false;
	float time_reposition = 0.1f;
	float timer_reposition = 0;
	//Variables de las animaciones conjuntas
	bool not_auto_move = false;
	float timer_constrict = 0;
	VEC3 start_pos = VEC3::Zero;
	VEC3 end_pos = VEC3::Zero;
	QUAT start_rot = QUAT(0, 0, 0, -1);
	QUAT end_rot = QUAT(0, 0, 0, -1);
	VEC3 start_root_pos = VEC3::Zero;
	VEC3 end_root_pos = VEC3::Zero;
	QUAT start_rot_col = QUAT(0, 0, 0, -1);
	QUAT end_rot_col = QUAT(0, 0, 0, -1);
	float time_constrictHug = 6;
	bool getUp_phase1 = false;
	bool getUp_phase2 = false;
	SoundEvent* victory_event;
	float time_initVictory = 4;
	bool initVictory = false;



	float dist_ToPlayer = 0.f;
	float current_velocity = 0.f;
	float last_velocity = 0.f;
	bool node_ready = false;   //Variable que utilizo para inicialiazar los nodos del BT nada mas entrar (para hacerlo una sola vez)

	// - Variables de muerte
	bool dead = false;
	bool destroyed = false;
	bool collision_down = true;
	float timer_fall = 0;
	bool falling = false;
	bool time_falling_before_dead = 1;
	bool waitingDead = false;
	bool delayDead = false;
	float delay_time_dead = 0;
	float timer_waitingDead;  //Timer para cuanto tiempo esperaremos antes de resucitar o ser destruidos
	float timer_delay_dead;	//Timer para la espera antes de morir
	bool fallDead = false;
	float y_dead = 0.225;



	// - Variables de la rotacion
	float timer_animation = 0.f;
	float time_animation = 0.f;
	float timeToNewRot = 0.01f;  //Tiempo para volver hacer una rotacion y que no quede raro
	float timerNewRot = 0.f;
	bool searching = false;
	bool looking = false;
	bool turnIdle = false;
	bool turnMovement = false;
	bool stopping_of_turning = false;   //Variable que indica si estamos deteniendo la animacion de girar en mitad de esta
	float timer_stopTurn = 0.f;
	float time_stopTurn = 0.1;  //Tiempo que esperaremos sin hacer nada mientras se para la animacion de girar
	float min_vel_to_idleRot = 0.5f;   //Velocidad maxima que debe tener el player para que nos fuerce a girar en idle en vez de en run
	QUAT init_rot_turn=QUAT(0,0,0,0);  //Cuando una animacion se mueve y rota, necesitamos saber la rotacion inicial para aplicarsela al movimiento


	//Variables del GateKeeper
	VEC3 orbit_point = VEC3::Zero;

	//Variables del Patroller
	bool outOfPatrol = false;
	vector<CHandle> wps;
	int current_way_point = 0;
	VEC3 random_way_point;
	bool using_rand_wayPoint = false;
	bool point_withOutPath = false;  //Variable que indica que el punto al que vamos no es posible ir con navpath pero si yendo en linea recta


	///////////////////////////////////////////////////////////
	// BARRA DE SOSPECHA
	//	0-29%      : sin efecto
	//	30% -50% : alert (mira alrededor)
	//	51% - 80%  : Suspicius (mira hacia el origen y va andando precavido)
	//	81% - 100%  : Alarm (detecta al player y corre hacia el)
	///////////////////////////////////////////////////////////
	// - Variables para la barra de sospechas
	bool desactivated = true;  //Al ser true deshabilita por completo que vean u oigan al player
	bool player_seen = false;  // Esto es para cuando lo vemos a una distancia menor que nuestro cono mas peque�o
	//bool player_seen_far = false;  // Esto es cuando lo vemos a una distancia mayor que nuestro cono mas peque�o pero menos que nuestro cono mas grande
	float suspicius_bar = 0.f;	// Barra de sospecha cuando un enemigo ve o escucha al player (0 a 100%)	
	float max_value_suspicius = 100.f;
	float time_to_reduce_suspicius = 30.f; //Tiempo que tarda en reducirse la barra a 0 (en segundos)
	vector<float> view_compare_dist; //Estas son las distancias precalculadas de comparacion para la suma del porcentaje de la barra de sospecha
	vector<float> porc_values_view;
	VEC3 pos_suspicius = VEC3::Zero; //Posicion actual del punto de sospecha
	vector<suspicius_noise> suspicius_events;

	float timer_change_suspicius_node = 0;
	float timer_goToNoise = 0;  //Con este timer hago que si estoy buscando al player, pero el ruido esta en otro lado deje de buscar y vaya hacia ese ruido
	float time_to_GoDifferentNoise = 2; //Si han pasado 2 segundos
	float dist_to_GoDifferentNoise = 5; //Y si la distancia entre ambos puntos son de 5 metros, voy hacia ese nuevo ruido
	VEC3 last_pos_suspicius = VEC3(std::numeric_limits<float>::min(), std::numeric_limits<float>::min(), std::numeric_limits<float>::min()); //Ultima posicion valida del punto de sospecha
	float time_exitNode = 0.3;  // Tiempo para cuando interrumpimos un estado y tenemos que esperar para salir de este
	float timer_exitNode = 0;   //Timer para la variable anterior
	VEC3 randomPoint = VEC3(std::numeric_limits<float>::min(), std::numeric_limits<float>::min(), std::numeric_limits<float>::min());  // Punto random que generamos cuando no podemos llegar al punto de sospecha
	float time_waitAngry = 5; //Tiempo que espera el guardian cuando esta en Pursuer pero no puede llegar a �l
	float timer_waitAngry = 0; //Timer para la variable anterior
	bool waiting_angry = false; //Propiedad que indica si estamos esperando a tener un camino para ir detras del player
	bool not_displacement = false; //Variable que calculamos para saber si estamos andando pero en realidad no nos estamos moviendo nada
	bool surrender = false; //Variable que indica si el guardian se ha rendido de perseguir al player

	//Variables de la NavMesh
	vector<VEC3> navPath;
	//bool move_with_navPath = false;


	// - Variables de debugger
	bool not_update = false;
	bool inPause = false;
	VEC3 p = VEC3::Zero;  //Punto al que nos dirigimos
	float yawToPoint = 0.f;
	VEC3 last_point = VEC3::Zero;
	CTimer tiempo;
	VEC3 manual_point = VEC3::Zero;
	float dist_ManualRaycast = 1;
	bool b_manualMove = false;
	float noise_detec = 0.f;
	ImVec4 c_noSos = ImVec4(0, 0, 1, 1);
	ImVec4 c_alert = ImVec4(0.971, 1.0, 0.0, 1);
	ImVec4 c_soslv1 = ImVec4(1.0, 0.792, 0.412, 1);
	ImVec4 c_soslv2 = ImVec4(1.0, 0.588, 0.0, 1);
	ImVec4 c_alarm = ImVec4(1.0, 0.0, 0.0, 1);
	bool vPlayer = false;
	bool playerIn = false;
	bool hitWithSomething = false;
	VEC3 hit;
	bool printCurrentLog = false;
	bool notSaveFSMLog = false;
	bool notSaveVarsLog = false;
	bool manualPosPlayer = false;
	int current_frame = 0;
	bool use_save_anim_movs = false;
	float distxsec = 0.f;  //Desplazamiento por segundo que uso cuando estoy en TryToCatch
	float timer_distXsec = 0;  //Timer de la variable anterior
	vector<VEC3> manualMovePos;
	TCompSkeleton::TSaveAnimMovs debug_anim;
	TCompSkeleton::TSaveRootMov debug_root;
	float timer_sincr = 0;


	//Variables para videos de CV
	float tope_suspicius_bar = 100;


	/// VARIABLES DE LOG DEL ENEMIGO
	bool writeLogEnemy = true;
	vector<string> logEnemy;
	//std::ofstream fileEnemy;

	/////////////////////////////////
	//		FUNCIONES
	//- BASICAS O COMUNES
	void getInfoBlackBoard();
	void preCalculate();
	void commonsCalcs(float dt);
	void updateSuspiciusBar(float dt);
	suspicius_noise calculateSuspiciusNoise();
	void checkDeadStuffs();
	void isDeathByFall();
	void isWaitingToDie();
	//float viewPlayer();  // DEPRECATED
	bool seePlayer(float dt); //
	//float heardPlayer();  DEPRECATED
	bool hearSomething(float dt);
	bool checkContactPlayer(float dt);
	void resetContactPlayer();
	void goToPoint(VEC3 point, float dt);
	void moveToPoint(VEC3 point, float dt);
	void updateAnimationVars();
	void updateTimers(float dt);
	// - Rotaciones
	bool turnInIdle(float dt);
	bool turnInMovement(float dt);

	void checkTurn(VEC3 point);
	bool checkIfMovementTurn(float yaw);
	bool checkIfIdleTurn(float yaw);
	bool isZoneForceIdleTurn(VEC3 pos);
	//---------------------------------------
	float getAnimationTime(animType type);
	int getFrameCount();
	bool prepareGoTo(VEC3 point);
	void resetGoTo();
	void resetTurn();
	bool isInHeadCone(float half_cone);
	bool isInCone(float half_cone);
	VEC3 getPosWayPoint();
	float getDistWayPoint();	
	int getNearWayPoint(VEC3 current_pos);
	void resetBT();
	void setRender(int render);
	void setRender();
	vector<VEC3> generatePath(VEC3 src, VEC3 dest);
	VEC3 generateRandomPoint(VEC3 point, float maxDist);
	VEC3 generateSuspiciusRandPoint();
	bool isThereWayNearToPoint(VEC3 src, VEC3 dest);
	bool isThereWayToPoint(VEC3 src, VEC3 dest);
	bool isThereObstacleToPoint(VEC3 src, VEC3 dest);
	bool isAValidPos(VEC3 pos);
	bool isNearPoint(VEC3 dest_point);
	void resetCheckDistXSec();
	void playSteps();
	void calculeLastFootInFloor();

	//- Registro de mensajes
	void onContact(const TMsgControllersOnContact& msgContact);
	void onDeathByOthers(const TMsgEnemyDeath& msgDeath);
	void onActivated(const TMsgActiveGuardian& msg);
	void onPlayerRelease(const TMsgPlayerRelease& msg);
	void onSuspiciusEvent(const TMsgSuspiciusEvent& msg);

	// - DEBUG
	void addHeaderLog(string l);
	void addStatusLog();
	void addFSMLog();
	void addGeneralVarsLog();
	void addFloat4Log(string ini, VEC4 value, string end);
	void addFloat3Log(string ini, VEC3 value, string end);
	void addFloatLog(string ini, float value, string end);
	void addBoolLog(string ini, bool value, string end);
	void addIntLog(string ini, int value, string end);
	void addStringLog(string ini, string value, string end);
	void writeLogInFile();
	void printAllLog();
	void initDebugRootMovs(string name);
	void insertDebugRootMovs();
	void ToEulerAngles(QUAT q, float* yaw, float* pitch, float* roll);
	float ProgressBar(const char* optionalPrefixText, float value, const float minValue = 0.f, const float maxValue = 1.f, const char* format = "%1.0f%%", const ImVec2& sizeOfBarWithoutTextInPixels = ImVec2(-1, -1),
		const ImVec4& colorLeft = ImVec4(0, 1, 0, 0.8), const ImVec4& colorRight = ImVec4(0, 0.4, 0, 0.8), const ImVec4& colorBorder = ImVec4(0.25, 0.25, 1.0, 1));



////////////////////////////////////////////////
// PUBLICAS
////////////////////////////////////////////////
public:
	string name_state = "";

	//FUNCIONES DE TIPO COMPONENTE
	void debugInMenu();
	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);
	void renderDebug();
	void onEntityCreated();
	static void registerMsgs();
	void resetEnemy();
	void addLog(string l);

	//FUNCIONES DEL UMBRAL DE SOSPECHA
	bool rotateToNoise(float dt);

	//FUNCIONES DEL GATEKEEPER
	bool nearGuardPoint();
	bool victory(float dt);
	void hitPlayer();
	void calculeOrbitPoint();
	bool rotateToOrbitPoint(float dt);
	bool goToOrbitPoint(float dt);
	void moveToOrbitPoint(float dt);


	//FUNCIONES DEL PATROLLER
	void registerWayPoint(CHandle wp) { wps.push_back(wp); }

	void preparePatrol();
	void prepareLookAt();
	void prepareGoNoise();
	void prepareGoToPlayer();
	void prepareTryToCatch();
	void prepareSpasm();
	void prepareSearch();
	void prepareGoBackWpt();
	void prepareGoToInitPoint();
	void prepareStartConstrict();
	void prepareConstrict();
	void prepareReleaseConstrict();
	void prepareGetUp();
	void prepareWait();
	void prepareVictory();

	void resetSearch();
	void resetPatrol();
	void resetGoToNoise();
	void resetGoToPlayer();
	void resetLookAt();
	void resetTryToCatch();
	void resetTryToCatch(bool chase);
	//void resetRotate();
	void resetSpasm();
	bool resetWayPoint();
	void resetGoBackWpt();
	void resetGoToInitPoint();
	void resetConstrict();
	void resetStartConstrict();
	void resetReleaseConstrict();
	void resetGetUp();
	void resetWait();
	void resetVictory();


	bool goToTryToCatch(float dt);
	bool goToPlayer(float dt);
	bool goToNoise(float dt);
	bool goToBackWpt(float dt);
	bool goToWayPoint(float dt);
	bool goToBlockPoint(float dt);
	bool goToInitPoint(float dt);

	bool isOutOfPatrol();
	bool isPlayerClose();

	//bool playerInSight();

	//Funciones animacion conjunta Constrict
	void actionStartConstrict();
	void actionReleaseConstrict();
	void actionGetUp();


	void setNextWP();
	void sendConstrict();
	void sendPlayerDead();
	void sendGoToCliff();
	void sendThrowPlayer();
	void moveToBlockPoint(float dt);
	//bool checkNeedRotate();
	//bool rotate();
	bool isNoiseDifferentPlace();
	bool goingRandPoint() { return using_rand_wayPoint; }
	void setOutOfPatrol(bool b) { outOfPatrol = b; }


	//FUNCIONES PARA DEVOLVER VARIABLES
	bool isDead();
	bool isDestroyed();
	VEC3 getMyPos();
	bool getPlayerSeen();
	float getDistToPlayer();
	bool isType(int t);
	bool isNormal();
	bool isAlert();
	bool isSuspicius();
	bool isAlarm();
	float getSuspiciusBarValue();
	bool isSearching();
	bool isRotating();
	int getType();
	bool isOnContactWithPlayer();
	int getAlertValue() { return alert_value; }
	int getAlarmValue() { return alarm_value; }
	int getSuspiciusValue() { return suspicius_value; }


	//void prepareSearch(float rot_time, float wait_time);
	bool isCurrentActionAnimFinished(float dt);
	bool checkNeedNavMesh(VEC3 point);
	bool seekNavPath(float dt);
	bool checkGoToCliff();
	bool canDestroyEnemy();
	void destroyEnemy();
	void setDead();
	void setRebirth();
	void applyDeathMov(float dt);
	bool applyRebirthMov(float dt);
	bool isStillDead();
	void resetFSM();
	bool canChangeSuspiciusNode();
	bool waitAnim();
	bool canExitNode();
	bool isNodeReady() { return node_ready; }
	void resetSuspiciusBar() { suspicius_bar = 0; }
	void calculateState(enemyState behavior_state);
	bool isWaitingAngry() { return waiting_angry; }
	void initWaitingAngry(bool b);
	bool isFinishedWaitAngry();
	void setSurrender(bool b) { surrender = b;}
	bool isSurrender() { return surrender; }
	enemyState getGuardianState() { return state; }
	void checkDisplacement(VEC3 beforeMov, VEC3 afterMov, float amount, VEC3 point);
	bool isNotDisplacement() { return not_displacement; }
	bool canGoToTryCatch();
	bool canGoToPlayer();
	bool canExitAlertState();
	void setChase(bool b) { chase_player = b; }
	void stopTurn();
	bool isStoppingTurn();
	bool manualRaycast(VEC3 src, VEC3 dest, float dist);
	bool isDesactivated() { return desactivated; }
	void setNotUpdate(bool b) { not_update = b; }
	bool isPlayerRelease() { return playerRelease; }
	void setPlayerRelease(bool b) { playerRelease = b; }
	bool canRecapture();
	bool isRepositionFinished() { return timer_reposition > time_reposition; }
	bool isConstrictHugFinished() { return timer_constrict > time_constrictHug; }
	void setBTState(string state);
	void setConstrictRender(enemyState state);
	float toTime(int frame) { return frame / 60.0f; }
	int toFrame(float time) { return (int)(time * 60); }
	float getMaxDistanceSound() { return max_distance_sound; }
	bool isTimeToVictory() { return timer_constrict >= time_initVictory; }

};

