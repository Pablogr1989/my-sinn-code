#include "mcv_platform.h"
#include "comp_waypoint.h"
#include "comp_enemy3D.h"

DECL_OBJ_MANAGER("waypoint", TCompWaypoint);

void TCompWaypoint::onEntityCreated()
{
	CEntity* e_Guardian = getEntityByName(guardianName);
	if (e_Guardian != null) {
		TCompEnemy3D* enemy = e_Guardian->get<TCompEnemy3D>();
		enemy->registerWayPoint(CHandle(this).getOwner());
	}
}

void TCompWaypoint::update(float dt)
{
	/*if (!registered) {
		CEntity* e_Guardian = getEntityByName(guardianName);
		TCompEnemy3D* enemy = e_Guardian->get<TCompEnemy3D>();
		enemy->registerWayPoint(CHandle(this).getOwner());
		registered = true;
	}*/
}

void TCompWaypoint::load(const json& j, TEntityParseContext& ctx)
{
	guardianName = j["owner"];
	longSearch = j.value("longSearch", false);
}

void TCompWaypoint::debugInMenu()
{
	ImGui::Checkbox("Enable", &enable);
}

void TCompWaypoint::renderDebug()
{
}

void TCompWaypoint::registerMsgs()
{
	DECL_MSG(TCompWaypoint, TMsgSetEnableWayPoint, setEnable);
}

void TCompWaypoint::setIsEnable(bool e) {
	enable = e;
}

void TCompWaypoint::setEnable(const TMsgSetEnableWayPoint& msgWay)
{
	enable = msgWay.enable;
}

VEC3 TCompWaypoint::getLookAt()
{
	return VEC3();
}

VEC3 TCompWaypoint::getPos()
{
	return ((TCompTransform*)get<TCompTransform>())->getPosition();
}

float TCompWaypoint::getDistance(VEC3 p)
{
	return VEC3::Distance(p, ((TCompTransform*)get<TCompTransform>())->getPosition());
}

float TCompWaypoint::getYaw()
{
	TCompTransform* my_trans = get<TCompTransform>();
	float yaw, pitch;
	// Calculamos la rotacion para llegar al punto
	my_trans->getEulerAngles(&yaw, &pitch, nullptr);

	return yaw;
}
