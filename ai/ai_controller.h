#pragma once
#include <mcv_platform.h>
#include "components/common/comp_base.h"
#include "entity/common_msgs.h"
#include "sound/soundEvent.h"
#include "entity/entity.h"
#include <string>
#include <map>
#include "sinn/characters/enemies/comp_enemy3D.h"


enum class IA_state {
	PATROL = 0,
	ALERT = 1,
	SUSPICIUS = 2, 
	ALARM = 3
};

struct enemy {
	CHandle h_enemy;
	VEC3 pos = VEC3::Zero;
	bool player_seen = false;
	bool in_alert = false;
	bool sound_player_alert_activated = false;
	int type = 0;
	float suspicius_bar = 0;
	enemyState state;
};

using namespace std;

class CAI_Controller : public TCompBase
{

	DECL_SIBLING_ACCESS();

	//VARIABLES
	int suspiciurBar_alert_value = 30;
	int suspiciurBar_alarm_value = 80;
	int suspiciurBar_suspicius_value = 50;
	float rad_det_noise = 12; // Distancia maxima a la que escucha un guardian

	bool windowStatus_active = true;
	float dist_Bloq = 2.f;
	int numEnemies = 0;

	int max_suspicius_value = 0;
	bool playerDetected = false;
	bool playerDead = false;
	bool soundPlayerSeenActivated = false;

	enemyState generalGuardiansState;

	map<string, enemy> info_enemies;
	CHandle h_player = CHandle();
	CHandle h_null = CHandle();
	CHandle old_captorEnemy = CHandle();

	vector<string> enemies_for_remove;

	//FUNCIONES
	void suspiciusNoiseEvent(const TMsgSuspiciusEvent& msg);
	void regEnemy(const TMsgEnemyRegister& msgReg);
	void removeEnemy(string name);
	void sendResetTree(CHandle h_enemy);
	void resetGame(const TMsgResetGame& msgReset);
	void setPlayerDead(const TMsgDeath& msgDeath);

	void updateInfoPlayer();
	void updateInfoEnemies();
	void updateSoundEvents(float dt);
	float calculateSoundState();
	void checkIsAssignToCapture(CHandle h_enemy);
	float calculeVolBSO();

	void checkBlockingPlaces(vector<string> haunters, string name_captor);
	bool isBlockingPlaceFree(CHandle h_blocker, vector<string> haunters);
	string getNearestHaunter(int blockPlace, vector<string> haunters);
	void resetAssignBB();
	CHandle getHandleEnemyByName(string enemy);
	void removeHaunter(vector<string>& haunters, string name);

	

public:
	void debugInMenu();
	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);
	void onEntityCreated();
	static void registerMsgs();
	void renderDebug();
	void renderEnemy(string name, enemy e);

	int getValueDetection() { return max_suspicius_value; };
	bool getPlayerDetected() { return playerDetected; };
	IA_state getStateDetection();
	bool notInChase() { return generalGuardiansState != enemyState::PURSUER && generalGuardiansState != enemyState::ATTACK; };
	SoundEvent* soundtrack;
	float constrictTimer = 0;
	void setGuardiansBSO(SoundEvent* s) {
		soundtrack = s;
	};

	bool isPlayerInRangeForMusic();

};

