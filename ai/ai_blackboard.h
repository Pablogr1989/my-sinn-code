#pragma once
#include "mcv_platform.h"
#include <entity\entity.h>

using namespace std;

struct blockPlace {
    CHandle h_blocker;
    VEC3 pos;
    bool free;
};

class CAI_BlackBoard
{
private:

    static CAI_BlackBoard* singleton;
    bool player_caught = false;
    VEC3 pos_player = VEC3::Zero;
    VEC3 dir_player = VEC3::Zero;
    VEC3 rightDir_player = VEC3::Zero;
    //float noise_player = 0.f;  //DEPRECATED
    float vel_player = 0.f;

    CHandle h_null=CHandle();
    CHandle h_captor=CHandle();

    vector<blockPlace> blockingPlaces;
    //0 - Enemigo que bloquea por el frente
    //1 - Enemigo que bloquea por la derecha
    //2 - Enemigo que bloquea por la izquierda
    //3 - Enemigo que bloquea por atras



public:
    void updateInfoPlayer(VEC3 dir, VEC3 rDir, VEC3 pos, float vel/*, float noise*/);
    void reset();
    void setEnemyCaptor(CHandle enemy);
    VEC3 getPosPlayer();
    VEC3 getDirPlayer();
    VEC3 getRightDirPlayer();
    float getVelocityPlayer();

    //float getNoisePlayer(); //DEPRECATED
    bool isPlayerCaught();
    CHandle whoCaughtPlayer();

    CHandle getBlocker(int num);
    void setBlocker(CHandle h_enemy, int num);
    int whoBlockerIAm(CHandle h_enemy);
    bool isFreeBlockPlace(int num);
    void freeBlockPlace(int num);
    void updatePosBlockPlace(VEC3 pos, int num);
    VEC3 getPosBlockPlace(int num);



    static CAI_BlackBoard& get();
    CAI_BlackBoard();

};
