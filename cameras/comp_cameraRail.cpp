#include "mcv_platform.h"
#include "comp_cameraRail.h"
#include <components\common\comp_camera.h>
#include "input/input_module.h"
#include "sinn/cameras/comp_camera3D.h"
#include "sinn/characters/player/comp_direction.h"

DECL_OBJ_MANAGER("cameraRail", TCompCameraRail);

void TCompCameraRail::renderDebug()
{
	TCompTransform* camera_trans = get<TCompTransform>();
	VEC3 myPos = camera_trans->getPosition();
	TCompTransform* cTargetTransform = get<TCompTransform>();
	if (!cTargetTransform) return;

	const MAT44 rt = MAT44::CreateFromYawPitchRoll(0, 0.f, 0.f);
	const MAT44 world;


	if (drawAll || drawCurvePosCam) {
		curve_pos->renderDebug(world, 200, { 1.f, 1.f, 0.f, 1.f });
	}

	if (drawAll || drawCurveLookatCam) {
		if(!target_player) curve_lookat->renderDebug(world, 200, { 1.f, 1.f, 0.f, 1.f });
	}

	if (drawAll || drawCurveGuide) {
		if(guide_mov) curve_guide_mov->renderDebug(world, 200, { 1.f, 1.f, 0.f, 1.f });
	}


	if (drawAll || drawPointers) {
		//if(!target_player)	curve_lookat->renderDebug(world, 200, { 1.f, 1.f, 0.f, 1.f });

		CEntity* hplayer = (CEntity*)getEntityByName("Player");
		TCompPlayer* player = hplayer->get<TCompPlayer>();
		TCompTransform* htrans = hplayer->get<TCompTransform>();

		/*VEC3 projPos = curve_pos->projectPoint(world, htrans->getPosition(), nSamples);
		dbg("Proj Point : %f %f %f\n", projPos.x, projPos.y, projPos.z);*/
		drawWiredSphere(nextPosPlayer, 0.3, Color::Red);
		drawWiredSphere(projPos, 0.3, Color::White);		
		drawLine(htrans->getPosition(), nextPosPlayer, Color::Red);
	}

	TCompCamera* camera = get<TCompCamera>();

	//// xy between -1..1 and z betwee 0 and 1
	auto mesh = Resources.get("view_volume.mesh")->as<CMesh>();

	////// Sample several times to 'view' the z distribution along the 3d space
	if (drawAll || drawCamera) {
		const int nsamples = 10;
		for (int i = 1; i < nsamples; ++i) {
			float f = (float)i / (float)(nsamples - 1);
			MAT44 world = MAT44::CreateScale(1.f, 1.f, f) * camera->getInvertViewProjection();
			drawMesh(mesh, world, Color::Blue);
			//drawWiredSphere(oldLookAt, 0.2, Color::Blue);
		}
	}
	if (drawAll || drawRay) {
		drawLine(camera->getPosition(), camera->getPosition() + camera->getFront() * 100, Color::Red);
		drawWiredSphere(camera->getPosition() + camera->getFront() * 5, 0.2, Color::Blue);
	}
}

void TCompCameraRail::debugInMenu()
{
	ImGui::Checkbox("Draw All", &drawAll);
	ImGui::Checkbox("Draw camera", &drawCamera);
	ImGui::Checkbox("Draw Curve", &drawCurvePosCam);
	ImGui::Checkbox("Draw Lookat Curve", &drawCurveLookatCam);
	ImGui::Checkbox("Draw Guide Curve", &drawCurveGuide);
	ImGui::Checkbox("Draw Pointer", &drawPointers);
	ImGui::DragFloat("Sphere size", &sizeSphere, 0.1, 0, 10);
	ImGui::Checkbox("Draw Ray", &drawRay);
	ImGui::DragFloat("Ratio", &ratio, 0.1, 0, 1);
	ImGui::DragFloat("Offset", &offset, 0.1, 0, 1);
	ImGui::DragFloat("Next Pos Offset", &offsetNextPos, 0.1, 0, 1);	
	ImGui::DragFloat("CamDir Pos Offset", &offsetCamDir, 0.1, 0, 1);	
	ImGui::DragFloat("CamDir Pos Y Offset", &offsetYCamDir, 0.1, 0, 1);
	ImGui::Checkbox("Ratio in LookAt", &ratioInLookAt);
	ImGui::Checkbox("Ratio in Guide", &ratioInGuide);
	ImGui::Checkbox("Manual Ratio", &manualRatio);
	ImGui::Checkbox("Manual Pos Player", &manualPosPlayer);
	if (&manualPosPlayer) {
		ImGui::DragFloat3("Pos Player", &posPlayer.x);
	}
	ImGui::Checkbox("Quickly Blend", &quicklyBlend);
	ImGui::NewLine();



	ImGui::Text("Curva Pos");
	//curve_pos->renderInMenu();
	ImGui::NewLine();

	if (!target_player) {
		ImGui::Text("Curva LookAt");
		//curve_lookat->renderInMenu();
		for (int i = 0; i < curve_lookat->getSizeCurve(); i++) {
			VEC3 pos = curve_lookat->getPosKnot(i);
			ImGui::LabelText("", "Knot %d [%f %f %f] ", i, pos.x, pos.y, pos.z);
		}
	}
	
	if (guide_mov) {
		ImGui::Text("Curva Guide Move");
		curve_guide_mov->renderInMenu();
		/*for (int i = 0; i < curve_guide_mov->getSizeCurve(); i++) {
			VEC3 pos = curve_guide_mov->getPosKnot(i);
			ImGui::LabelText("", "Knot %d [%f %f %f] ", i, pos.x, pos.y, pos.z);
		}*/
	}
}

void TCompCameraRail::load(const json& j, TEntityParseContext& ctx)
{
	curvePosFilename = j["curve_pos"];
	curve_pos = Resources.get(curvePosFilename)->as<CCurve>();

	if (j.count("curve_lookAt")) {
		curveLookAtFilename = j["curve_lookAt"];
		curve_lookat = Resources.get(curveLookAtFilename)->as<CCurve>();

		ratioInLookAt = j.value("ratioInLookAt", ratioInLookAt);
	}
	else {
		target_player = true;
	}

	if (j.count("curve_guideMov")) {
		curveGuideMovFilename = j["curve_guideMov"];
		curve_guide_mov = Resources.get(curveGuideMovFilename)->as<CCurve>();
		guide_mov = true;

		ratioInGuide = j.value("ratioInGuide", ratioInGuide);
	}

	std::string t = j.value("typePath", "iniToEnd");
	setTypePath(t);

}

void TCompCameraRail::update(float dt)
{

	CEntity* hplayer = (CEntity*)getEntityByName("Player");
	TCompPlayer* player = hplayer->get<TCompPlayer>();
	TCompTransform* c_transform = get<TCompTransform>();
	TCompTransform* htrans = hplayer->get<TCompTransform>();

	if (!c_transform) return;

	// CAMARA POR RAILES

	VEC3 posPlayer = htrans->getPosition();
	MAT44 world;

	if (ratioInLookAt) {
		projPos = curve_lookat->projectPoint(world, posPlayer);
		if(!manualRatio)	ratio = curve_lookat->calculateRatioFromProjPoint(projPos);
	}
	else if(ratioInGuide) {
		projPos = curve_guide_mov->projectPoint(world, posPlayer);
		if (!manualRatio)	ratio = curve_guide_mov->calculateRatioFromProjPoint(projPos);
	}
	else {
		projPos = curve_pos->projectPoint(world, posPlayer);
		if (!manualRatio)	ratio = curve_pos->calculateRatioFromProjPoint(projPos);
	}



	VEC3 newPosCam = VEC3::Zero;

	if (typeP == typePath::IniToEnd) {
		newPosCam = curve_pos->evaluate(ratio - offset);
	}
	else {
		newPosCam = curve_pos->evaluate(ratio + offset);
	}
	

	if (target_player) {
		newLookAt = VEC3(posPlayer.x, posPlayer.y + 1, posPlayer.z);
	}
	else {
		newLookAt = curve_lookat->evaluate(ratio);
	}

	VEC3 camPos;
	VEC3 lookAt;
	//Si es la primera vez no queremos interpolar, queremos que la camara este y mire donde debe
	//porque la posicion vieja puede estar en Narnia
	if (first_time) {
		first_time = false;
		camPos = newPosCam;
		lookAt = newLookAt;
	}
	else {
		//interpolamos la posicion y el lookat con la anterior posicion
		if (quicklyBlend) {
			//Cuando queremos un blendeo mas rapido usamos esta opcion
			camPos = 0.8f * oldPos + 0.2f * newPosCam;
			lookAt = 0.8f * oldLookAt + 0.2f * newLookAt;
		}
		else {
			camPos = 0.95f * oldPos + 0.05f * newPosCam;
			lookAt = 0.95f * oldLookAt + 0.05f * newLookAt;
		}
	}


	c_transform->lookAt(camPos, lookAt);
	oldLookAt = lookAt;
	oldPos = camPos;

	if (!active)	return;

	TCompCamera* c_cam = get<TCompCamera>();
	if (c_cam)
		c_cam->targetdistance = VEC3::DistanceSquared(lookAt, camPos);

	if (guide_mov) {
		CEntity* ent_camera = (CEntity*)getEntityByName("camera_direction");
		TCompDirection* comp_direc = ent_camera->get<TCompDirection>();
		TCompTransform* camtrans = ent_camera->get<TCompTransform>();

		calculateNextPosPlayer(posPlayer, htrans, player);
		player->setPointToMove(nextPosPlayer);
	}

}

void TCompCameraRail::registerMsgs()
{
}

void TCompCameraRail::setActive(bool b)
{
	active = b;

	if (active) {
		first_time = true;
	}
}

void TCompCameraRail::setTypePath(std::string t)
{
	if (t == "iniToEnd") {
		typeP = typePath::IniToEnd;
	}
	else {
		typeP = typePath::EndToIni;
	}
}

void TCompCameraRail::calculateNextPosPlayer(VEC3 posPlayer, TCompTransform* htrans, TCompPlayer* player)
{

	if (typeP == typePath::IniToEnd) {
		if (!reverseGuide) {
			nextPosPlayer = curve_guide_mov->evaluate(ratio + offsetNextPos);
			nextPosPlayer.y = posPlayer.y;
			relYawPlayer = rad2deg(htrans->getDeltaYawToAimTo(nextPosPlayer));

			if (abs(relYawPlayer) > 90)	reverseGuide = !reverseGuide;

			if (reverseGuide) {
				player->setReverseGuide(true);
				nextPosPlayer = curve_guide_mov->evaluate(ratio - offsetNextPos);
			}

		}
		else {
			nextPosPlayer = curve_guide_mov->evaluate(ratio - offsetNextPos);
			nextPosPlayer.y = posPlayer.y;
			relYawPlayer = rad2deg(htrans->getDeltaYawToAimTo(nextPosPlayer));

			if (abs(relYawPlayer) > 90)	reverseGuide = !reverseGuide;

			if (!reverseGuide) {
				player->setReverseGuide(false);
				nextPosPlayer = curve_guide_mov->evaluate(ratio + offsetNextPos);
			}
		}
	}
	else {
		if (!reverseGuide) {
			nextPosPlayer = curve_guide_mov->evaluate(ratio - offsetNextPos);
			nextPosPlayer.y = posPlayer.y;
			relYawPlayer = rad2deg(htrans->getDeltaYawToAimTo(nextPosPlayer));

			if (abs(relYawPlayer) > 90)	reverseGuide = !reverseGuide;

			if (reverseGuide) {
				player->setReverseGuide(true);
				nextPosPlayer = curve_guide_mov->evaluate(ratio + offsetNextPos);
			}

		}
		else {
			nextPosPlayer = curve_guide_mov->evaluate(ratio + offsetNextPos);
			nextPosPlayer.y = posPlayer.y;
			relYawPlayer = rad2deg(htrans->getDeltaYawToAimTo(nextPosPlayer));

			if (abs(relYawPlayer) > 90)	reverseGuide = !reverseGuide;

			if (!reverseGuide) {
				player->setReverseGuide(false);
				nextPosPlayer = curve_guide_mov->evaluate(ratio - offsetNextPos);
			}
		}
	}

}
