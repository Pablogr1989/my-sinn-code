#include "sinn/modules/module_physics_sinn.h"
#include "PxPhysicsAPI.h"
#include <components\common\comp_transform.h>
#include <sinn\characters\player\comp_player.h>

enum class typePath {IniToEnd, EndToIni};

class TCompCameraRail : public TCompBase{
	DECL_SIBLING_ACCESS();

public:

	void renderDebug();
	void debugInMenu();
	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);

	static void registerMsgs();
	std::string getCurve() { return curvePosFilename; }
	void setActive(bool b);
	void setTypePath(std::string t);

private:

	//Variables debug
	bool drawAll = false;
	bool drawCamera = false;
	bool drawCurvePosCam = false;
	bool drawCurveLookatCam = false;
	bool drawCurveGuide = false;
	bool drawPointers = false;
	bool drawRay = false;
	float sizeSphere = 1;
	VEC3 projPos = VEC3::Zero;
	bool manualRatio = false;
	bool manualPosPlayer = false;
	VEC3 posPlayer = VEC3::Zero;
	/////////////////////////////
	float ratio = 0;
	float offset = 0.07;
	bool active = false;
	bool first_time = false;
	//VARIABLES ADDED IN JSON
	std::string curvePosFilename = "";
	std::string curveLookAtFilename = "";
	std::string curveGuideMovFilename = "";

	const CCurve* curve_pos = nullptr;
	const CCurve* curve_lookat = nullptr;
	const CCurve* curve_guide_mov = nullptr;

	float offsetNextPos = 0.1;
	float offsetCamDir = -0.05;
	float offsetYCamDir = 0;

	bool target_player = false;
	bool guide_mov = false;
	VEC3 nextPosPlayer = VEC3::Zero;
	float relYawPlayer = 0;

	typePath typeP = typePath::IniToEnd;
	bool reverseGuide = true;

	bool ratioInLookAt = false;
	bool ratioInGuide = false;
	bool quicklyBlend = false;

	VEC3 newLookAt = VEC3::Zero;
	VEC3 oldLookAt = VEC3::Zero;
	VEC3 oldPos = VEC3::Zero;

	void calculateNextPosPlayer(VEC3 posPlayer, TCompTransform* htrans, TCompPlayer* player);

};


