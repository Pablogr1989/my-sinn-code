#include "mcv_platform.h"
#include "comp_cameraFirst.h"
#include "components/common/comp_transform.h"
#include "utils/utils.h"
#include "engine.h"
#include "input/input_module.h"
#include "entity/entity_parser.h"
#include "sinn/characters/player/comp_player.h"

DECL_OBJ_MANAGER("camera_first", TCompCameraFirst);



void TCompCameraFirst::debugInMenu() {
    ImGui::DragFloat("Max Speed", &_speed, 0.1f, 1.f, 100.f);
    ImGui::DragFloat("Sensitivity", &_sensitivity, 0.001f, 0.001f, 0.1f);
    ImGui::DragFloat("Inertia", &_ispeed_reduction_factor, 0.001f, 0.7f, 1.f);
    ImGui::LabelText("Curr Speed", "%f", _ispeed);
}

void TCompCameraFirst::load(const json& j, TEntityParseContext& ctx) {
    _speed = j.value("speed", _speed);
    _sensitivity = j.value("sensitivity", _sensitivity);
    _ispeed_reduction_factor = j.value("speed_reduction_factor", _ispeed_reduction_factor);
    _enabled = j.value("enabled", _enabled);
    if (j.count("key")) {
        std::string k = j["key"];
        _key_toggle_enabled = k[0];
    }

}

void TCompCameraFirst::update(float scaled_dt)
{
    hplayer = (CEntity*)getEntityByName("Player");
    TCompPlayer* player = hplayer->get<TCompPlayer>();



    auto& input = CEngine::get().getInput();

    TCompTransform* c_transform = get<TCompTransform>();
    if (!c_transform)
        return;
    //get the input from the mouse
    VEC2 mOff = input.getMouse().deltaPosition;
    HAng -= -mOff.x * _sensitivity;
    VAng -= mOff.y * _sensitivity;
    TCompTransform* htrans = hplayer->get<TCompTransform>();

    VEC3 oldPos = c_transform->getPosition();
    VEC3 newLookAt = htrans->getPosition() + htrans->getFront() + VEC3(cos(HAng), VAng, sin(HAng));
    VEC3 newPos = htrans->getPosition() + VEC3(0, 1.4, 0);
    VEC3 camPos = 0.95 * oldPos + 0.05 * newPos;
    c_transform->lookAt(camPos, newLookAt);





}
