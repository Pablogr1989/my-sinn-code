#pragma once
#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "components/common/comp_transform.h"

class TCompCameraFirst : public TCompBase
{
    DECL_SIBLING_ACCESS();

public:

    CEntity* hplayer=nullptr;
    void debugInMenu();
    void load(const json& j, TEntityParseContext& ctx);
    void update(float dt);

private:
    float HAng = 1.f;
    float VAng = 1.6f;
    float _speed = 10.f;
    float _sensitivity = 5.0f;
    float _maxPitch = (float)M_PI_2 - 1e-4f;
    VEC3  _ispeed = VEC3::Zero;
    float _ispeed_reduction_factor = 0.95f;
    bool  _enabled = true;
    int   _key_toggle_enabled = 0;
    bool first = true;
};

