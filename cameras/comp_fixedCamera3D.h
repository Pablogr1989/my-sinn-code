#pragma once
#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "sinn/modules/module_physics_sinn.h"
#include "PxPhysicsAPI.h"
#include <components\common\comp_transform.h>

class TCompFixedCamera3D : public TCompBase
{
	DECL_SIBLING_ACCESS();

public:

	void renderDebug();
	void debugInMenu();
	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);

	CHandle target = CHandle();
	std::string target_name = "";
	std::string camera_name = "";
	VEC3 lastFront = VEC3::Zero;
	float _pitchRatio = 0.70f;
	float _sensitivity = 50.f;
	int cameraState = 0;
	QUAT rotation = QUAT(0, 0, 0, 1);

	std::string getCurve() { return curveFilename; }
	void setPitch(float pitch) { _replacePitch = pitch; }
	float getPitch() { return _pitchRatio; }
	float getYaw() { return _yaw; }

private:

	//Variables debug
	bool drawAll = false;
	bool drawCamera = false;
	bool drawCurve = false;
	bool drawPointer = false;
	bool drawRay = false;
	float sizeSphere = 1;
	/////////////////////////////
	float _maxPitch = (float)M_PI_2 - 1e-4f;
	//VARIABLES ADDED IN JSON
	std::string curveFilename = "";
	const CCurve* curve = nullptr;

	VEC3 oldLookAt = VEC3::Zero;

	float _yaw = 0.f;
	float _replacePitch = 0.f;   //Pitch que auto impondremos al cambiar a otras camaras y asi hacer una transicion mas suave
	float _padSensitivity = 0.05f;

};


