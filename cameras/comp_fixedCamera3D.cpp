#include "mcv_platform.h"
#include "comp_fixedCamera3D.h"
#include <components\common\comp_camera.h>
#include <sinn\characters\player\comp_player.h>
#include "input/input_module.h"
#include <components\postfx\comp_render_focus.h>

DECL_OBJ_MANAGER("fixedCamera_3D", TCompFixedCamera3D);

void TCompFixedCamera3D::renderDebug()
{

	TCompTransform* camera_trans = get<TCompTransform>();
	VEC3 myPos = camera_trans->getPosition();
	CEntity* eTarget = (CEntity*)getEntityByName("Player");
	if (!eTarget) return;
	TCompTransform* cTargetTransform = eTarget->get<TCompTransform>();
	if (!cTargetTransform) return;

	const MAT44 rt = MAT44::CreateFromYawPitchRoll(_yaw, 0.f, 0.f);
	const MAT44 world = rt * cTargetTransform->asMatrix();


	if (drawAll || drawCurve) {
		curve->renderDebug(world, 200, { 1.f, 1.f, 0.f, 1.f });
	}

	TCompCamera* camera = get<TCompCamera>();

	//// xy between -1..1 and z betwee 0 and 1
	auto mesh = Resources.get("view_volume.mesh")->as<CMesh>();

	////// Sample several times to 'view' the z distribution along the 3d space
	if (drawAll || drawCamera) {
		const int nsamples = 10;
		for (int i = 1; i < nsamples; ++i) {
			float f = (float)i / (float)(nsamples - 1);
			MAT44 world = MAT44::CreateScale(1.f, 1.f, f) * camera->getInvertViewProjection();
			drawMesh(mesh, world, Color::Blue);
			//drawWiredSphere(oldLookAt, 0.2, Color::Blue);
		}
	}
	if (drawAll || drawRay) {
		drawLine(camera->getPosition(), camera->getPosition() + camera->getFront() * 100, Color::Red);
		drawWiredSphere(camera->getPosition() + camera->getFront() * 5, 0.2, Color::Blue);
	}

}

void TCompFixedCamera3D::debugInMenu()
{
	CEntity* targetE = target;
	ImGui::DragFloat("Sensitivity", &_sensitivity, 0.001f, 0.001f, 0.1f);
	ImGui::DragFloat("Pitch", &_pitchRatio, 0.1f, 0, 1);
	ImGui::DragFloat("Yaw", &_yaw, 0.1f, -100.f, 100.f);
	ImGui::Checkbox("Draw All", &drawAll);
	ImGui::Checkbox("Draw camera", &drawCamera);
	ImGui::Checkbox("Draw Curve", &drawCurve);
	ImGui::Checkbox("Draw Pointer", &drawPointer);
	ImGui::DragFloat("Sphere size", &sizeSphere, 0.1, 0, 10);
	ImGui::Checkbox("Draw Ray", &drawRay);
	ImGui::LabelText("Curve", "%s", curveFilename.c_str());
	curve->renderInMenu();
}

void TCompFixedCamera3D::load(const json& j, TEntityParseContext& ctx)
{

	curveFilename = j["curve"];
	curve = Resources.get(curveFilename)->as<CCurve>();
	camera_name = j.value("name", camera_name);
	_yaw = j.value("yaw", 0.0f);
}

void TCompFixedCamera3D::update(float scaled_dt)
{

	CEntity* hplayer = (CEntity*)getEntityByName("Player");
	TCompPlayer* player = hplayer->get<TCompPlayer>();
	TCompTransform* c_transform = get<TCompTransform>();
	TCompTransform* htrans = hplayer->get<TCompTransform>();

	if (!c_transform) return;

	TCompCamera* myCam = get<TCompCamera>();//para hacer los calculos de las colisiones luego
	VEC3 oldPos = c_transform->getPosition();//para interpolar

	//matrices de traslacion y rotacion de la camara, yaw es cogido del input del movimiento del raton y offset se pasa del json
	const MAT44 tr = MAT44::CreateTranslation({ 0.f, 0.f, 0.f });
	const MAT44 rt = MAT44::CreateFromYawPitchRoll(_yaw, 0.f, 0.f);

	MAT44 world;

	VEC3 curvePos, newLookAt;


	//la matriz de la transform del player (con rotacion 0,0,0 para que no siga la rotacion del target, sino que el target se mueva en funcion de la camara)
	//por la traslacion y rotacion de la camara
	world = rt * tr * MAT44::CreateScale(htrans->getScale())
		* MAT44::CreateFromQuaternion(QUAT(0, 0, 0, 0))
		* MAT44::CreateTranslation(htrans->getPosition());

	curvePos = curve->evaluate(world, _pitchRatio);


	VEC3 posPlayer = htrans->getPosition();

	newLookAt = VEC3(posPlayer.x, posPlayer.y + 1, posPlayer.z);

	//comprobar que no colisiona la camara con alguna pared o que haya algo entre ella y el target
	//VEC3 newPos = myCam->HandleCollisionZoom(curvePos, newLookAt, 0.1);
	VEC3 newPos = curvePos;

	//interpolamos la posicion y el lookat
	VEC3 camPos = 0.8f * oldPos + 0.2f * newPos;
	VEC3 lookAt = 0.8f * oldLookAt + 0.2f * newLookAt;

	c_transform->lookAt(camPos, lookAt);
	oldLookAt = lookAt;

	TCompCamera* cam = get<TCompCamera>();
	if (cam)
		cam->targetdistance = VEC3::DistanceSquared(lookAt, camPos);
}