#include <iomanip>
#include "mcv_platform.h"
#include "comp_camera2D.h"
#include "components/common/comp_transform.h"
#include "utils/utils.h"
#include "engine.h"
#include "input/input_module.h"
#include "entity/entity_parser.h"
#include "sinn/characters/player/comp_player.h"
#include "components/common/comp_camera.h"
#include <iostream>
#include <components\postfx\comp_render_focus.h>

DECL_OBJ_MANAGER("camera_2D", TCompCamera2D);

float angle = 1;
void TCompCamera2D::debugInMenu() {
    /*ImGui::DragFloat3("Look debug", &debug_look.x, 0.1, -1000, 1000, "%.2f");
    ImGui::DragFloat3("CamPos debug", &debug_camPos.x, 0.1, -1000, 1000, "%.2f");*/

    ImGui::LabelText("Dist Shadow To Camera", "%f", dis_shadow_to_camera);
    ImGui::DragFloat("Offset Shadow To Camera", &offset_shadow_to_camera);
    ImGui::DragFloat("Min distance Shadow To Camera", &min_dis_shadow_to_camera);

    ImGui::DragFloat3("Position", &debug_cam_pos.x);
    ImGui::DragFloat3("Look", &debug_cam_look.x);
    ImGui::LabelText("Movimiento Raton", "%f %f", mOff.x, mOff.y);
    ImGui::DragFloat("Horizontal Offset", &hOffset2D);
    ImGui::DragFloat("Vertical Offset", &vOffset2D);
    if (ImGui::SmallButton("Reiniciar Offsets")) {
        resetcam2D();
    }
    ImGui::DragFloat("Max Horizontal Offset", &max_hOffset2D);
    ImGui::DragFloat("Max Vertical Offset", &max_vOffset2D);
    ImGui::Checkbox("Invertir Movimiento Cam2D", &paneoInverso);
    ImGui::Checkbox("Vuelta poco a poco", &vueltaPorVelocidad);
    ImGui::LabelText("Delay timer", "%.1f", timerDelayReturn);
    ImGui::ProgressBar(timerDelayReturn / delayReturn, ImVec2(-1, 0), "Delay");
    ImGui::LabelText("Volviendo...", useVelToReduce ? "True" : "False");

  
    ImGui::DragFloat("Sensitivity", &_sensitivity, 0.001f, 0.001f, 10);
    ImGui::LabelText("Distance Center Line", "%f", distance_center_line);
    ImGui::DragFloat("Pitch raton", &_pitchRatio);
    ImGui::DragFloat("Yaw", &_yaw);
    ImGui::LabelText("Curve", "%s", curveFilename.c_str());
    curve->renderInMenu();
}

void TCompCamera2D::load(const json& j, TEntityParseContext& ctx) {
    _sensitivity = j.value("sensitivity", _sensitivity);
    curveFilename = j["curve"];
    curve = Resources.get(curveFilename)->as<CCurve>();
    _pitchRatio = j.value("pitchRatio", _pitchRatio);
    _padSensitivity = j.value("padsensitivity", _padSensitivity);
    velReduceOffset = j.value("offset_vel", velReduceOffset);

    if (j.contains("maxH_formValues")) {
        hgv = loadFloatVecN(j, "maxH_formValues", 10);
    }

    if (j.contains("maxV_formValues")) {
        vgv = loadFloatVecN(j, "maxV_formValues", 5);
    }

}

void TCompCamera2D::update(float scaled_dt)
{
    hplayer = (CEntity*)getEntityByName("Player");

    TCompPlayer* player = hplayer->get<TCompPlayer>();
    TCompTransform* c_transform = get<TCompTransform>();
    TCompTransform* htrans = hplayer->get<TCompTransform>();

    timer_desprojection += scaled_dt;
    timerDelayReturn += scaled_dt;


    VEC3 oldPos = c_transform->getPosition();
    
    if (player->getModoCamera() == CAM2D) {
        
        if(!player->isInCutscene())readInput(scaled_dt);

        VEC3 wall_point = player->getWallPoint();
        if (wall_point == VEC3::Zero) {
          //dbg("Nada, no tengo muro de proyeccion aun\n");
          return;
        }
        VEC3 wall_normal = player->getWallNormal();

        if (player->focusToShadow()) {
            if (type2DCam != type2DCam::FIXED)  resetcam2D();

            reductionOffset(scaled_dt);
            renderCam2D(wall_normal, wall_point);  

            type2DCam = type2DCam::FIXED;
        }
        else {
            if (type2DCam != type2DCam::FREE)  resetFree2DCam();

            renderFreeCam2D(scaled_dt);

            type2DCam = type2DCam::FREE;
        }
    }
    else {

        type2DCam = type2DCam::NEITHER;

        if (!without_init) {
            timer_desprojection = 0;
            without_init = true;
        }

        if (timer_desprojection <= time_to_changeCamera) {
            camPos = oldPos;
            newLook = oldLook;
        }
        else {
            float a = deg2rad(90);
            VEC3 v = htrans->getFront();
            VEC3 vDer = VEC3(v.x * cos(-a) - v.z * sin(-a), 0, v.x * sin(-a) + v.z * cos(-a));
            vDer.y = 1;

            camPos = htrans->getPosition() + vDer * VEC3(-2, 1, 1);
            newLook = htrans->getPosition() + VEC3(0, 1, 0);

        }

        debug_cam_pos = camPos;
        debug_cam_look = newLook;

    }
    c_transform->lookAt(camPos, newLook);
    oldLook = newLook;
}

void TCompCamera2D::renderDebug()
{
  /*drawWiredSphere(debug_cam_pos, 0.2, Color::Green);
  drawWiredSphere(debug_cam_look, 0.2, Color::Green);
  drawLine(debug_cam_pos, debug_cam_look,Color::Red);*/

  TCompTransform* camera_trans = get<TCompTransform>();
  VEC3 myPos = camera_trans->getPosition();
  //drawLine(myPos, myTransform, Color::Green);

  TCompTransform* cTargetTransform = hplayer->get<TCompTransform>();
  if (!cTargetTransform) return;

  const MAT44 rt = MAT44::CreateFromYawPitchRoll(_yaw, 0.f, 0.f);
  const MAT44 world = rt * cTargetTransform->asMatrix();

  curve->renderDebug(world, 200, { 1.f, 1.f, 0.f, 1.f });

  TCompCamera* camera = get<TCompCamera>();

  //// xy between -1..1 and z betwee 0 and 1
  auto mesh = Resources.get("view_volume.mesh")->as<CMesh>();

  ////// Sample several times to 'view' the z distribution along the 3d space
  const int nsamples = 10;
  for (int i = 1; i < nsamples; ++i) {
      float f = (float)i / (float)(nsamples - 1);
      MAT44 world = MAT44::CreateScale(1.f, 1.f, f) * camera->getInvertViewProjection();
      drawMesh(mesh, world, Color::White);
      
  }
  drawLine(camera->getPosition(), camera->getPosition() + camera->getFront() * 10, Color::Red);

  if (ray.size() > 0) {
      for (auto r : ray) {
          drawLine(camPos, camPos + r * 2, Color::Red);
      }
  }

}

void TCompCamera2D::renderCam2D(VEC3 wall_normal, VEC3 wall_point)
{
    TCompPlayer* player = hplayer->get<TCompPlayer>();
    TCompTransform* c_transform = get<TCompTransform>();
    TCompTransform* htrans = hplayer->get<TCompTransform>();
    TCompCamera* myCam = get<TCompCamera>();

    if (!c_transform) return;
    VEC3 oldPos = c_transform->getPosition();

    VEC3 newPos, look;

    //am I going left to right
            //we calculate the distance from the shadow to the body
    distance_shadow_body = VEC3::Distance(htrans->getPosition(), wall_point);
    distance_shadow_body = float_one_point_round(distance_shadow_body);
    
    /* if (distance_shadow_body > 30)
       distance_shadow_body *=0.8;*/
       //we try to get a line that goes from the body or shadow to wherever we want the camera to go
    s = (distance_shadow_body / 2) / sen20;
    //with that s, we get the line that goes from the center point of shadow and body to S in what should be a circle that englobes shadow, body and camera
    distance_center_line = s * cos20;
    distance_center_line = float_one_point_round(distance_center_line);
    //to make sure the camera doesn't go behind the wall we check both possible camera points and the closest to a point outside the wall will be the chosen one
    VEC3 wall_normal_2;
    wall_normal_2 = wall_normal * 2; //a point outside the wall, using its normal
    float radius = s * sen20 / sen140; //this will be the radius of the circle that englobes shadow, player and camera
    //we get the centre point between shadow and body

    float distance_centre_pointX = (htrans->getPosition().x + wall_point.x) / 2;
    float distance_centre_pointZ = (htrans->getPosition().z + wall_point.z) / 2;
    //and the perpendicular vector of the line that goes from shadow to body
    float perpendicular_distanceX = -(wall_point.z - htrans->getPosition().z) / distance_shadow_body;
    float perpendicular_distanceZ = (wall_point.x - htrans->getPosition().x) / distance_shadow_body;
    //we try to get the centre of the circle that englobes shadow, body and camera, there are two possibilities
    VEC3 centrePoint = VEC3(distance_centre_pointX, htrans->getPosition().y + 1, distance_centre_pointZ) + VEC3(perpendicular_distanceX, htrans->getPosition().y + 1, perpendicular_distanceZ) * (distance_center_line - radius);
    VEC3 centrePoint2 = VEC3(distance_centre_pointX, htrans->getPosition().y + 1, distance_centre_pointZ) - VEC3(perpendicular_distanceX, htrans->getPosition().y + 1, perpendicular_distanceZ) * (distance_center_line - radius);
    //calculate each possibility with that normalPoint
    float dis_centre1_normal = VEC3::Distance(centrePoint, wall_normal_2);
    float dis_centre2_normal = VEC3::Distance(centrePoint2, wall_normal_2);

    VEC3 myCentrePoint;
    //the closest one will be the camera that is in front of the wall
    if (dis_centre1_normal < dis_centre2_normal) {
        myCentrePoint = centrePoint;
    }
    else {
        myCentrePoint = centrePoint2;
    }
    //now we need the angle between the vectors that go shadow to centre and the normal of the wall
    VEC3 shadow_to_centre = (myCentrePoint - wall_point) / radius; //calculate line that goes shadow to centre
    VEC3 unit_wall_normal = wall_normal / wall_normal.Length();//get the normal in units of 1 and 0 (unitize)
    //to get the angle between two vectors we need to do the acosen of the dot product of such vectors
    float dotProduct = unit_wall_normal.Dot(shadow_to_centre);
    //float angle = acos(dotProduct);
    //if (isnan(angle))angle = 0;
    //now we can know the angle from the centre of the circle to shadow and camera
    //float angle_center = PI - 2 * angle;
    //the distance from shadow to camera will be the radius* sin of the centre angle divided by the sin of the angle of the shadow/camera
    /*float dis_shadow_to_camera = radius * sin(angle_center) / sin(angle);
    if (dis_shadow_to_camera <= distance_shadow_body * 1.3) dis_shadow_to_camera = distance_shadow_body * 1.3;
    if (dis_shadow_to_camera < 6)dis_shadow_to_camera = 6;
    if (distance_shadow_body > 30) dis_shadow_to_camera = 30;
    dbg("Distance shadow to body : %f \n", distance_shadow_body);*/
    // Change the distance shadow to camera for absolute value -> dis_shadow_to_camera = distance_shadow_body * 1.3;
    dis_shadow_to_camera = distance_shadow_body + offset_shadow_to_camera;
    if (dis_shadow_to_camera < min_dis_shadow_to_camera) dis_shadow_to_camera = min_dis_shadow_to_camera;

    float dif_in_y = htrans->getPosition().y - wall_point.y + 1;
    //the position of the camera will now be the distance calculated multiplied by the direction of the wall normal and the position of the shadow  

    newPos = debug_camPos = (unit_wall_normal * dis_shadow_to_camera) + wall_point + VEC3(0, dif_in_y, 0);
    look = debug_look = wall_point;

    VEC3 wallRight = VEC3::Up.Cross(wall_normal);

    calculateMovementOffset(&newPos, &look, wallRight);

    debug_cam_pos = newPos;
    debug_cam_look = look;

    //Solo una vez lo que queremos es que se cambie la posicion de la camara en un solo frame
    if (without_init) {
        without_init = false;
        camPos = newPos;
        newLook = look;
    }
    else {
        camPos = 0.9f * oldPos + 0.1f * newPos;
        newLook = 0.9f * oldLook + 0.1f * look;
    }
    TCompCamera* cam = get<TCompCamera>();
    if (cam)
      cam->targetdistance = VEC3::DistanceSquared(newLook, camPos);
}

void TCompCamera2D::renderFreeCam2D(float scaled_dt)
{
    TCompPlayer* player = hplayer->get<TCompPlayer>();
    TCompTransform* c_transform = get<TCompTransform>();
    TCompTransform* htrans = hplayer->get<TCompTransform>();

    if (!c_transform) return;

    TCompCamera* myCam = get<TCompCamera>();//para hacer los calculos de las colisiones luego
    VEC3 oldPos = c_transform->getPosition();//para interpolar

    //matrices de traslacion y rotacion de la camara, yaw es cogido del input del movimiento del raton y offset se pasa del json
    const MAT44 tr = MAT44::CreateTranslation({ 0.f, 0.f, 0.f });
    const MAT44 rt = MAT44::CreateFromYawPitchRoll(_yaw, 0.f , 0.f);

    MAT44 world;


    VEC3 curvePos, newLookAt;


    //la matriz de la transform del player (con rotacion 0,0,0 para que no siga la rotacion del target, sino que el target se mueva en funcion de la camara)
    //por la traslacion y rotacion de la camara
    world = rt * tr * MAT44::CreateScale(htrans->getScale())
        * MAT44::CreateFromQuaternion(QUAT(0, 0, 0, 0))
        * MAT44::CreateTranslation(htrans->getPosition());

    curvePos = curve->evaluate(world, _pitchRatio);


    VEC3 posPlayer = htrans->getPosition();

    newLookAt = VEC3(posPlayer.x, posPlayer.y + 1, posPlayer.z);

    //comprobar que no colisiona la camara con alguna pared o que haya algo entre ella y el target
     VEC3 newPos = myCam->HandleCollisionZoom(curvePos, newLookAt, 0.1);


    //interpolamos la posicion y el lookat
    camPos = 0.8f * oldPos + 0.2f * newPos;
    newLook = 0.8f * oldLook + 0.2f * newLookAt;

    TCompCamera* cam = get<TCompCamera>();
    if (cam)
      cam->targetdistance = VEC3::DistanceSquared(newLook, camPos);
}

void TCompCamera2D::readInput(float dt)
{
    input::CInputModule& input = CEngine::get().getInput();

    //GAMEPAD
    if (input["camera_x"].isPressed() or input["camera_y"].isPressed()) {

        mOff = VEC2(input["camera_x"].value, input["camera_y"].value);
        mOff *= _padSensitivity;
        _yaw += mOff.x * _sensitivity * 3;
        _pitchRatio += mOff.y * _sensitivity;

        if (paneoInverso) {
            hOffset2D += mOff.x * _sensitivity * 3;
            vOffset2D += mOff.y * _sensitivity * 3;
        }
        else {
            hOffset2D += -mOff.x * _sensitivity * 3;
            vOffset2D += -mOff.y * _sensitivity * 3;
        }
    }
    //MOUSE & KEYBOARD
    else {

        mOff = input.getMouse().deltaPosition * 10; // multiply the mouse input so the camera doesn't go extremeley slow
        _yaw += -mOff.x * _sensitivity * 3;
        _pitchRatio += mOff.y * _sensitivity;

        if (paneoInverso) {
            hOffset2D += -mOff.x * _sensitivity * 3;
            vOffset2D += mOff.y * _sensitivity * 3;
        }
        else {
            hOffset2D += mOff.x * _sensitivity * 3;
            vOffset2D += -mOff.y * _sensitivity * 3;
        }
    }

    //Si utilizamos gamepad comprobamos si lo estamos moviendo o no
    if (input.getActiveDevice() == input.GAMEPAD) {
        if (input["camera_x"].value == 0 && input["camera_y"].value == 0) {
            //Si queremos utilizar un tiempo para volver al centro de la pantalla
            if (vueltaPorVelocidad) {
                //Si hemos dejado de mover el joystick y aun no estamos volviendo al centro de la pantalla
                if (!inDelayReturn) {
                    timerDelayReturn = 0;
                    inDelayReturn = true;
                }
            }
            else {
                hOffset2D = 0;
                vOffset2D = 0;
            }

        }
        else {
            //Si estamos moviendo el joystick
            inDelayReturn = false;
            useVelToReduce = false;
            hOffset2D = clamp(hOffset2D, -max_hOffset2D, max_hOffset2D);
            vOffset2D = clamp(vOffset2D, -max_vOffset2D, max_vOffset2D);
        }
    }
    //Si usamos teclado y raton
    else {
        hOffset2D = clamp(hOffset2D, -max_hOffset2D, max_hOffset2D);
        vOffset2D = clamp(vOffset2D, -max_vOffset2D, max_vOffset2D);
        inDelayReturn = false;
        useVelToReduce = false;
    }

    _pitchRatio = clamp(_pitchRatio, 0.f, 1.f);

    adjustYaw();

}

void TCompCamera2D::resetFree2DCam()
{
    TCompTransform* c_transform = get<TCompTransform>();

    float p, y;
    c_transform->getEulerAngles(&y, &p, nullptr);
    _yaw = y;
    _pitchRatio = 0.5f;
}

void TCompCamera2D::resetcam2D()
{
    hOffset2D = 0.f;
    vOffset2D = 0.f;
}

void TCompCamera2D::reductionOffset(float dt)
{

    if (inDelayReturn) {
        if (timerDelayReturn >= delayReturn) {
            inDelayReturn = false;
            useVelToReduce = true;
        }
    }

    if (useVelToReduce) {
        //Movimiento horizontal
        if (hOffset2D < 0) {
            hOffset2D += velReduceOffset * dt;

            if (hOffset2D > 0) hOffset2D = 0;
        }
        else {
            hOffset2D -= velReduceOffset * dt;

            if (hOffset2D < 0) hOffset2D = 0;
        }

        //Movimiento vertical
        if (vOffset2D < 0) {
            vOffset2D += velReduceOffset * dt;

            if (vOffset2D > 0) vOffset2D = 0;
        }
        else {
            vOffset2D -= velReduceOffset * dt;

            if (vOffset2D < 0) vOffset2D = 0;
        }

        if (hOffset2D == 0 && vOffset2D == 0)    useVelToReduce = false;
    }

    //suspicius_bar -= (max_value_suspicius / time_to_reduce_suspicius) * dt;

}

void TCompCamera2D::calculateMovementOffset(VEC3 *newPos, VEC3 *newLook, VEC3 wallRight)
{

    VEC3 posWithoff = VEC3(newPos->x, newPos->y, newPos->z);
    VEC3 lookWithoff = VEC3(newLook->x, newLook->y, newLook->z);

    posWithoff += wallRight * hOffset2D;
    posWithoff.y += vOffset2D;

    lookWithoff += wallRight * hOffset2D;
    lookWithoff.y += vOffset2D;

    newPos->x = posWithoff.x;
    newPos->y = posWithoff.y;
    newPos->z = posWithoff.z;

    newLook->x = lookWithoff.x;
    newLook->y = lookWithoff.y;
    newLook->z = lookWithoff.z;
}

void TCompCamera2D::adjustYaw()
{
    if (_yaw > M_PI) {
        _yaw = -M_PI;
        return;
    }

    if (_yaw < -M_PI) {
        _yaw = M_PI;
        return;
    }
}

