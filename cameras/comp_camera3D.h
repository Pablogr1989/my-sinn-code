#pragma once
#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "sinn/modules/module_physics_sinn.h"
#include "PxPhysicsAPI.h"
#include <components\common\comp_transform.h>

class TCompCamera3D : public TCompBase
{
	DECL_SIBLING_ACCESS();
private:

	//Variables debug
	bool drawAll = false;
	bool drawCamera = false;
	bool drawCurve = false;
	bool drawPointer = false;
	bool drawRay = false;
	bool cameraFixed = false; 
	float sizeSphere = 1;
	float weightDebug = 0;
	float lc, l1, l2 = 0;
	bool using_far = false;
	bool hitCamera = false;
	/////////////////////////////

	//Variables para suavizar la transicion de la camara2D a la 3D
	float timer_projection = 0;
	float time_to_changeCamera = 0.5;
	bool without_init = true;
	/////////////////////////////
	float _maxPitch = (float)M_PI_2 - 1e-4f;
	//VARIABLES ADDED IN JSON
	float height = 1.f;
	float offset = 1.f;
	float leftOffset = 3.f;
	bool uses_camera_left = false; //when we look at the enemy we don't use the camera left

	std::string curveFilename = "";
	std::string farCurveFilename = "";
	const CCurve* curve = nullptr;
	const CCurve* farCurve = nullptr;

	VEC3 oldLookAt = VEC3::Zero;

	float _yaw = 0.f;
	float extraYaw = 0.f;
	float _replacePitch = 0.f;   //Pitch que auto impondremos al cambiar a otras camaras y asi hacer una transicion mas suave
	float _padSensitivity = 0.05f;

	void addEnemyTarget(const TMsgTargetChange& msg);
	void readInput(float dt);
	void fetchTarget();
	void adjustYaw();

	VEC3 calculateCurvePos(MAT44 world, float yawCamera, float yawPlayer);
	void calculateLimitsFarCam(float yawPlayer, float* limitCentral, float* limitLat1, float* limitLat2);
	float calculateWeightFarCam(float yawCamera, float limitCentral, float limitCentral2, float limitLat1, float limitLat2);
	bool isInRange(float limitCentral, float limitLat, float yawCamera);
	int checkQuadrant(float yaw);



public:

	void renderDebug();
	void debugInMenu();
	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);

	CHandle target = CHandle();
	std::string target_name = "";
	std::string camera_name = "";
	VEC3 lastFront = VEC3::Zero;
	float _pitchRatio = 0.70f;
	float _sensitivity = 50.f;
	int cameraState = 0;
	bool Fixed_distance = false;

	static void registerMsgs();
	std::string getCurve() { return curveFilename; }
	void setPitch(float pitch) { _replacePitch = pitch; }
	float getPitch() { return _pitchRatio; }
	float getYaw() { return _yaw; }
	void blockMovement(bool b);
	void blockMovement(bool b, float p, float y);
	void setPitchYaw(float p, float y);
	void rotateToPoint(VEC3 point);
	bool isBlockMovement() { return cameraFixed; }
};


