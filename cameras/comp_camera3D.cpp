#include "mcv_platform.h"
#include "comp_camera3D.h"
#include <components\common\comp_camera.h>
#include <sinn\characters\player\comp_player.h>
#include "input/input_module.h"
#include <components\postfx\comp_render_focus.h>

DECL_OBJ_MANAGER("camera_3D", TCompCamera3D);

void TCompCamera3D::renderDebug()
{

	TCompTransform* camera_trans = get<TCompTransform>();
	VEC3 myPos = camera_trans->getPosition();
	CEntity* eTarget = (CEntity*)getEntityByName("Player");
	if (!eTarget) return;
	TCompTransform* cTargetTransform = eTarget->get<TCompTransform>();
	if (!cTargetTransform) return;

	/*const MAT44 rt = MAT44::CreateFromYawPitchRoll(_yaw, 0.f, 0.f);
	const MAT44 world = rt * cTargetTransform->asMatrix();*/

	const MAT44 tr = MAT44::CreateTranslation({ 0.f, 0.f, offset });
	const MAT44 rt = MAT44::CreateFromYawPitchRoll(_yaw, _replacePitch, 0.f);

	MAT44 world = rt * tr * MAT44::CreateScale(cTargetTransform->getScale())
		* MAT44::CreateFromQuaternion(QUAT(0, 0, 0, 0))
		* MAT44::CreateTranslation(cTargetTransform->getPosition());


	if (drawAll || drawCurve) {
		curve->renderDebug(world, 200, { 1.f, 1.f, 0.f, 1.f });
		if (farCurve != nullptr) {
			farCurve->renderDebug(world, 200, { 1.f, 1.f, 0.f, 1.f });
		}
	}

	TCompCamera* camera = get<TCompCamera>();

	//// xy between -1..1 and z betwee 0 and 1
	auto mesh = Resources.get("view_volume.mesh")->as<CMesh>();

	////// Sample several times to 'view' the z distribution along the 3d space
	if (drawAll || drawCamera) {
		const int nsamples = 10;
		for (int i = 1; i < nsamples; ++i) {
			float f = (float)i / (float)(nsamples - 1);
			MAT44 world = MAT44::CreateScale(1.f, 1.f, f) * camera->getInvertViewProjection();
			if (hitCamera) drawMesh(mesh, world, Color::Red);
			else drawMesh(mesh, world, Color::Blue);
			//drawWiredSphere(oldLookAt, 0.2, Color::Blue);
		}
	}
	if (drawAll || drawRay) {
		drawLine(camera->getPosition(), camera->getPosition() + camera->getFront() * 100, Color::Red);
		drawWiredSphere(camera->getPosition() + camera->getFront() * 5, 0.2, Color::Blue);
	}

}

void TCompCamera3D::debugInMenu()
{
	CEntity* targetE = target;
	ImGui::DragFloat("Sensitivity", &_sensitivity, 0.001f, 0.001f, 0.1f);
	ImGui::DragFloat("leftOffset", &leftOffset, 0.1f, -100.f, 100.f);
	ImGui::DragFloat("Pitch", &_pitchRatio, 0.1f, 0, 1);
	ImGui::DragFloat("Yaw", &_yaw, 0.1f, -100.f, 100.f);	
	ImGui::Checkbox("Camera Fixed", &cameraFixed);
	ImGui::LabelText("uses_camera_lefte", uses_camera_left ? "true" : "false");
	ImGui::LabelText("Target: ", targetE->getName());
	ImGui::Checkbox("Draw All", &drawAll);
	ImGui::Checkbox("Draw camera", &drawCamera);
	ImGui::Checkbox("Draw Curve", &drawCurve);
	ImGui::Checkbox("Draw Pointer", &drawPointer);
	ImGui::DragFloat("Sphere size", &sizeSphere, 0.1, 0, 10);
	ImGui::Checkbox("Draw Ray", &drawRay);
	ImGui::LabelText("Hit Camera", hitCamera ? "true" : "false");
	ImGui::LabelText("Curve", "%s", curveFilename.c_str());
	curve->renderInMenu();
	ImGui::Checkbox("Usando Far Curve", &using_far);
	ImGui::DragFloat("Weight", &weightDebug);
	ImGui::DragFloat("Limite Central", &lc);
	ImGui::DragFloat("Limite1", &l1);
	ImGui::DragFloat("Limite2", &l2);
	ImGui::Checkbox("Fixed Distance", &Fixed_distance);
}

void TCompCamera3D::load(const json& j, TEntityParseContext& ctx)
{

	target_name = j.value("target", target_name);

	curveFilename = j["curve"];
	curve = Resources.get(curveFilename)->as<CCurve>();

	farCurveFilename = j.value("farCurve", farCurveFilename);
	if (farCurveFilename != "") {
		farCurve = Resources.get(farCurveFilename)->as<CCurve>();
	}

	offset = j.value("offset", offset);
	height = j.value("height",height);
	leftOffset = j.value("leftOffset", leftOffset);
	uses_camera_left = j.value("usesCameraLeft", uses_camera_left);
	_pitchRatio = j.value("pitchRatio", _pitchRatio);
	_sensitivity = j.value("sensitivity", _sensitivity);
	_padSensitivity = j.value("padsensitivity", _padSensitivity);
	camera_name = j.value("name", camera_name);
	Fixed_distance = j.value("fixed_distance", Fixed_distance);
}

void TCompCamera3D::update(float scaled_dt)
{

	timer_projection += scaled_dt;

	CEntity* hplayer = (CEntity*)getEntityByName("Player");
	TCompPlayer* comp_player = hplayer->get<TCompPlayer>();
	CEntity* player_entity;
	TCompTransform* player_trans;
	CEntity* hcamera = CHandle(this).getOwner();
	TCompTransform* camera_trans = get<TCompTransform>();
	CEntity* hCamDir = (CEntity*)getEntityByName("camera_direction");
	if (!camera_trans) return;

	//llamo al target cogiendo su entidad
	fetchTarget();

	player_entity = target;
	if (!player_entity) return; // a veces tarda el target en existir, por lo que me espero a que no sea nulo
	player_trans = player_entity->get<TCompTransform>();
	
	if (comp_player->getModoCamera() == CAM3D /*|| !comp_player->focusToShadow()*/) {
		//leo el input del movimiento del raton
		readInput(scaled_dt);
		without_init = true;
		//dbg("Estoy en modo camara3D\n");
	}
	else {

		if (without_init) {
			timer_projection = 0;
			without_init = false;
			//dbg("Estoy en modo camara2D y acabo de inicializar\n");
		}

		if (timer_projection >= time_to_changeCamera) {
			//dbg("Estoy en modo camara2D y ya estoy poniendo el yaw y pitch de la camara2D\n");
			CEntity* hCam2d = (CEntity*)getEntityByName("camera2D");
			TCompTransform* cam2d_trans = hCam2d->get<TCompTransform>();
			float yaw2d, pitch2d;
			cam2d_trans->getEulerAngles(&yaw2d, &pitch2d, nullptr);
			_yaw = yaw2d;
			_replacePitch = pitch2d;
		}
		else {
			//dbg("Estoy en modo camara2D pero aun no cambio la camara\n");
		}

	}


	TCompCamera* myCam = get<TCompCamera>();//para hacer los calculos de las colisiones luego
	VEC3 oldPos = camera_trans->getPosition();//para interpolar

	adjustYaw();

	//matrices de traslacion y rotacion de la camara, yaw es cogido del input del movimiento del raton y offset se pasa del json
	const MAT44 tr = MAT44::CreateTranslation({ 0.f, 0.f, offset });
	const MAT44 rt = MAT44::CreateFromYawPitchRoll(_yaw, _replacePitch, 0.f);
	_replacePitch = 0.f;
	MAT44 world;
	
	 
	VEC3 curvePos, newLookAt;
	//la matriz de la transform del player (con rotacion 0,0,0 para que no siga la rotacion del target, sino que el target se mueva en funcion de la camara)
	//por la traslacion y rotacion de la camara
	world = rt * tr * MAT44::CreateScale(player_trans->getScale())
		* MAT44::CreateFromQuaternion(QUAT(0, 0, 0, 0))
		* MAT44::CreateTranslation(player_trans->getPosition());

	//la curva se evalua en funcion de la matriz y el input vertical del movimiento del raton
	float yawcam, pitchcam;
	camera_trans->getEulerAngles(&yawcam, &pitchcam, nullptr);
	float yawCam_deg = rad2deg(yawcam);	

	float yawPlayer, pitchPlayer;
	player_trans->getEulerAngles(&yawPlayer, &pitchPlayer, nullptr);
	float yawPlayer_deg = rad2deg(yawPlayer);


	curvePos = calculateCurvePos(world, yawCam_deg, yawPlayer_deg);
	//curvePos = curve->evaluate(world, _pitchRatio);

	std::string name = hcamera->getName();
	if (name == "cameraStealthIdle3D" || name == "cameraStealthWalk3D" || name == "cameraThrowObject") {
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// PONER AL PERSONAJE UN POQUITO A LA IZQUIERDA

		TCompTransform* camDir_trans = hCamDir->get<TCompTransform>();

		newLookAt = player_trans->getPosition() - camDir_trans->getLeft() * leftOffset;
		newLookAt.y += 1;

		curvePos = curvePos - camDir_trans->getLeft() * leftOffset;

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//Comprobar que el newLookat no este detras de una pared
		float new_leftoffset = leftOffset;
			VEC3 rayStart = curvePos;
			VEC3 rayEndP = player_trans->getPosition() + VEC3(0, 1, 0);
			VEC3 rayEndL = newLookAt;
			VEC3 DirP = rayEndP - rayStart;
			float raycastLengthP = DirP.Length();
			DirP.Normalize();
			VEC3 DirL = rayEndL - rayStart;
			float raycastLengthL = DirL.Length();
			DirL.Normalize();
			VEC3 Dir = rayEndL - rayEndP;
			float raycastLength = Dir.Length();
			Dir.Normalize();
			PxRaycastBuffer hitCallP;
			PxRaycastBuffer hitCallL;
			PxQueryFilterData fd = PxQueryFilterData();
			fd.data.word0 = CModulePhysicsSinn::FilterGroup::Scenario | CModulePhysicsSinn::FilterGroup::Obstacles;
			// a result between 0 and 1 indicates a hit along the ray segment

			bool statusP = CEngine::get().getPhysics().gScene->raycast(VEC3_TO_PXVEC3(rayStart), VEC3_TO_PXVEC3(DirP), raycastLengthP, hitCallP, PxHitFlags(PxHitFlag::eDEFAULT), fd);
			bool statusL = CEngine::get().getPhysics().gScene->raycast(VEC3_TO_PXVEC3(rayStart), VEC3_TO_PXVEC3(DirL), raycastLengthL, hitCallL, PxHitFlags(PxHitFlag::eDEFAULT), fd);
			if (statusP && !statusL) {
				//La camara y el lookat estan detras de la pared
				newLookAt = rayEndP;
				//newLookAt = 0.1 * rayEndP + 0.9 * oldLookAt;
				curvePos = curvePos + camDir_trans->getLeft() * leftOffset;
				new_leftoffset = 0;
			}
			else if (statusP && statusL) {
				//Solo la camara esta detras de la pared
				if (hitCallL.hasAnyHits()) {
					if (hitCallP.hasAnyHits()) {
						const PxRaycastHit& overlapHitP = hitCallP.getAnyHit(0);
						const PxRaycastHit& overlapHitL = hitCallL.getAnyHit(0);

						//radius es el radio de la capsula del player
						float radius = 0.3f;
						float Player_Comp = DirP.Dot(DirL) * overlapHitP.distance;
						if (Player_Comp == 0) {
							new_leftoffset = 0;
						}
						else {
							new_leftoffset = std::max(((Player_Comp - overlapHitL.distance) / Player_Comp) * leftOffset, 0.0f);
						}
						//float correction = (raycastLength - overlapHit.distance) / (raycastLength - radius);
						newLookAt = rayEndP - camDir_trans->getLeft() * new_leftoffset;
						//newLookAt = rayEndP - camDir_trans->getLeft() * new_leftoffset;
						curvePos = curvePos + camDir_trans->getLeft() * leftOffset - camDir_trans->getLeft() * new_leftoffset;
					}
				}
			}
			else if (!statusP && statusL) {
				//Solo el lookat esta detras del muro
				bool status = CEngine::get().getPhysics().gScene->raycast(VEC3_TO_PXVEC3(rayEndP), VEC3_TO_PXVEC3(Dir), raycastLength, hitCallP, PxHitFlags(PxHitFlag::eDEFAULT), fd);

				if (status) {
					if (hitCallP.hasAnyHits()) {
						const PxRaycastHit& overlapHit = hitCallP.getAnyHit(0);
						//radius es el radio de la capsula del player
						float WAngle = atan(tan(myCam->getFov() / 2) * myCam->getAspectRatio());
						float Far_Left_Vector =  myCam->getNear() * (tan(WAngle));
						float radius = 0.3f;
						new_leftoffset = overlapHit.distance - Far_Left_Vector - (radius- Far_Left_Vector) * (1 - overlapHit.distance) / (1 - radius);
						//float correction = (raycastLength - overlapHit.distance) / (raycastLength - radius);
						newLookAt = rayEndP - camDir_trans->getLeft() * new_leftoffset;
						//if (camDir_trans->getLeft().Dot(newLookAt - oldLookAt) > 0) {
						//	dbg("viene de la camara");
						//	//newLookAt = 0.1 * newLookAt + 0.9 * oldLookAt;
						//}
						curvePos = curvePos + camDir_trans->getLeft() * leftOffset - camDir_trans->getLeft() * new_leftoffset;
					}
				}
			}
	}
	else {
		// PROVISIONAL -> BORRAR CUANDO SE PONGA A LA IZQUIERDA
		newLookAt = player_trans->getPosition();
		newLookAt.y += 1;
	}
	

	//comprobar que no colisiona la camara con alguna pared o que haya algo entre ella y el target
	VEC3 newPos = myCam->HandleCollisionZoom(curvePos, newLookAt, 0.3);

	if (newPos != curvePos) {
		hitCamera = true;
	}
	else {
		hitCamera = false;
	}


	//interpolamos la posicion y el lookat
	VEC3 camPos = 0.8f * oldPos + 0.2f * newPos;
	VEC3 lookAt = 0.8f * oldLookAt + 0.2f * newLookAt;
	
	camera_trans->lookAt(camPos , lookAt);
	oldLookAt = lookAt;

	TCompCamera* cam = get<TCompCamera>();
	if (cam && !Fixed_distance)
		cam->targetdistance = VEC3::DistanceSquared(lookAt, camPos);

}

void TCompCamera3D::registerMsgs()
{
	DECL_MSG(TCompCamera3D, TMsgTargetChange, addEnemyTarget);
	
}

void TCompCamera3D::blockMovement(bool b)
{
	cameraFixed = b;
}

void TCompCamera3D::blockMovement(bool b, float p, float y)
{
	cameraFixed = b;
	_yaw = y;
	_pitchRatio = p;
}

void TCompCamera3D::setPitchYaw(float p, float y)
{
	_yaw = y;
	_pitchRatio = p;
}

void TCompCamera3D::rotateToPoint(VEC3 point)
{
	TCompTransform* trans = get<TCompTransform>();
	float yaw = trans->getDeltaYawToAimTo(point);

	VEC3 v = point - trans->getPosition();
	float absYaw = atan(v.z / v.x);

	dbg("%s -> Debes tener una rotacion de %f\n", camera_name.c_str(), absYaw);

	extraYaw = yaw;
	dbg("%s -> Debes rotar %f Yaw y tienes un yaw %f\n", camera_name.c_str(), extraYaw, _yaw);
	_yaw += extraYaw;

	dbg("%s -> Ahora tienes una rotacion de %f\n", camera_name.c_str(), _yaw);
	dbg("---------------\n");
}

void TCompCamera3D::addEnemyTarget(const TMsgTargetChange& msg)
{

	//recibo el target del enemigo y me guardo su nombre
	target = msg.target;
	CEntity* auxEntity = target;
	target_name = auxEntity->getName();
}



void TCompCamera3D::readInput(float dt) {
	input::CInputModule& input = CEngine::get().getInput();

	if (cameraFixed) return;

	float sensitivity;
	VEC2 delta;
	if (input["camera_x"].isPressed() or input["camera_y"].isPressed()) {
		delta = VEC2(input["camera_x"].value, input["camera_y"].value);
		sensitivity = input.getPadSensitivity();
		//mOff *= _padSensitivity;
		//_yaw += mOff.x * _sensitivity * 3;
		//_pitchRatio += mOff.y * _sensitivity;
	}
	else {
		delta = input.getMouse().deltaPosition;
		delta.x *= -1;
		sensitivity = input.getMouseSensitivity();
	}
	_yaw += delta.x * sensitivity * dt * 3;
	_pitchRatio += delta.y * sensitivity * dt;
	_pitchRatio = clamp(_pitchRatio, 0.f, 1.f);
}


void  TCompCamera3D::fetchTarget() {
	target = getEntityByName(target_name);
}

void TCompCamera3D::adjustYaw()
{

	if (_yaw > M_PI) {
		_yaw = -M_PI;
		return;
	}

	if (_yaw < -M_PI) {
		_yaw = M_PI;
		return;
	}


}

VEC3 TCompCamera3D::calculateCurvePos(MAT44 world, float yawCamera, float yawPlayer)
{
	//Si no hay curva para alejarse, entonces devolvemos la normal
	if (farCurve == nullptr) {
		using_far = false;
		return curve->evaluate(world, _pitchRatio);
	}

	//Valores por defecto cuando el angulo del player es 0
	//Para cuando va de izquierda al centro
	float limLateral = 90;
	float limCentral = 180;
	//Para cuando va de derecha al centro
	float limLateral2 = -90;
	float limCentral2 = -180;

	if (yawPlayer != 0) {
		calculateLimitsFarCam(yawPlayer, &limCentral, &limLateral, &limLateral2);
		limCentral2 = limCentral;
	}
	float weight = calculateWeightFarCam(yawCamera, limCentral, limCentral2, limLateral, limLateral2);

	// Variables debug
	lc = limCentral;
	l1 = limLateral;
	l2 = limLateral2;
	////////////////////


	//Si el peso es negativo es que no hay que blendear
	if (weight < 0) {
		using_far = false;
		return curve->evaluate(world, _pitchRatio);
	}
	else {
		VEC3 curveStealthPos = curve->evaluate(world, _pitchRatio);
		VEC3 curveFarStealthPos = farCurve->evaluate(world, _pitchRatio);

		using_far = true;
		return VEC3::Lerp(curveStealthPos, curveFarStealthPos, weight);
	}

}

void TCompCamera3D::calculateLimitsFarCam(float yawPlayer, float* limitCentral, float* limitLat1, float* limitLat2)
{
	float c1 = 180, c2 = -180, c3 = 90, c4 = -90;

	if (yawPlayer < 0) {

		*limitCentral = c1 - std::abs(yawPlayer);
		*limitLat1 = *limitCentral - c3;
		*limitLat2 = *limitCentral - c4;
		if (*limitLat2 > c1) {
			*limitLat2 = c2 + (*limitLat2 - c1);
		}

	}
	else {

		*limitCentral = c2 + yawPlayer;
		*limitLat1 = *limitCentral - c3;
		if (*limitLat1 < c2) {
			*limitLat1 = c1 - std::abs(*limitLat1 - c2);
		}
		*limitLat2 = *limitCentral - c4;
	}
}

float TCompCamera3D::calculateWeightFarCam(float yawCamera, float limitCentral, float limitCentral2, float limitLat1, float limitLat2)
{
	float w;

	//Tenemos que hacer una regla de tres pero con dos factores extras
	//Ej ->    0% -> 90
	//       100% -> 180
	//          X -> 135
	// x = (100 * (135 - 90)) / (180 - 90) -> 50%
	
	// Se puede simplificar el calculo:
	// angulo = m x + y_0
	// angulo = 90 x + 90 = 90 (x + 1)
	// x = angulo/90 - 1
	
	// si angulo = 135
	// x = 135/90 - 1 = 0.5 = 50%

	if (isInRange(limitCentral, limitLat1, yawCamera)) {

		float c = limitCentral;
		float l = limitLat1;
		float yc = yawCamera;

		int qLat = checkQuadrant(limitLat1);
		int qCen = checkQuadrant(limitCentral);


		if (qLat == 2 && qCen == 4) {
			c = 180;
			l = 90;
			if (yawCamera > 0) {
				yc = l + (yawCamera- limitLat1);
			}
			else {
				yc = c - abs(limitCentral - yawCamera);
			}
		}
		else if (qLat == 4 && qCen == 2) {
			c = 90;
			l = 180;

			if (yawCamera > 0) {
				yc = c + (yawCamera - limitCentral);
			}
			else {
				yc = l - abs(limitLat1 - yawCamera);
			}
		}


		c -= 15;
		if (c < (-180))	c = -180;

		w = (100 * (yc - l)) / (c - l);
		w = w / 100; // Pasamos el peso a 0.0 -> 1.0

		if (w > 1)	w = 1;
		if (w < 0)	w = 0;

		weightDebug = w;

	}
	else if (isInRange(limitCentral2, limitLat2, yawCamera)) {

		float c = limitCentral2;
		float l = limitLat2;
		float yc = yawCamera;

		int qLat = checkQuadrant(limitLat2);
		int qCen = checkQuadrant(limitCentral2);


		if (qLat == 2 && qCen == 4) {
			c = 180;
			l = 90;
			if (yawCamera > 0) {
				yc = l + (yawCamera - limitLat2);
			}
			else {
				yc = c - abs(limitCentral2 - yawCamera);
			}
		}
		else if (qLat == 4 && qCen == 2) {
			c = 90;
			l = 180;

			if (yawCamera > 0) {
				yc = c + (yawCamera - limitCentral2);
			}
			else {
				yc = l - abs(limitLat2 - yawCamera);
			}
		}

		c += 15;
		if (c < (-180))	c = -180;

		w = (100 * (yc - l)) / (c - l);
		w = w / 100; // Pasamos el peso a 0.0 -> 1.0

		if (w > 1)	w = 1;
		if (w < 0)	w = 0;

		weightDebug = w;
	}
	else {
		w = -1;
	}

	return w;
}

bool TCompCamera3D::isInRange(float limitCentral, float limitLat, float yawCamera)
{

	//Primero comprobamos si el limite lateral y limite central estan en los cuadrantes 2 y 4 (da lo mismo el orden)
	// ya que estos nos llevan a comprobar los rangos de limite a 180, -180 a limite

	int qLat = checkQuadrant(limitLat);
	int qCen = checkQuadrant(limitCentral);

	float min, max;

	if (qLat == 2 && qCen == 4 || qLat == 4 && qCen == 2) {

		if (qLat == 4) {
			min = limitLat;
			max = limitCentral;
		}
		else {
			min = limitCentral;
			max = limitLat;
		}

		if ((yawCamera >= max && yawCamera <= 180) || (yawCamera >= -180 && yawCamera <= min))	return true;
		else return false;
	}
	else {
		if (limitCentral > limitLat) {
			min = limitLat;
			max = limitCentral;
		}
		else {
			min = limitCentral;
			max = limitLat;
		}

		if (yawCamera >= min && yawCamera <= max)	return true;
		else return false;

	}

	return false;
}

int TCompCamera3D::checkQuadrant(float yaw)
{

	if (yaw >= 0 && yaw <= 90)	return 1;
	if (yaw > 90 && yaw <= 180)	return 2;
	if (yaw < 0 && yaw >= -90)	return 3;
	if (yaw < -90 && yaw >= -180) return 4;
	else return 0;

}