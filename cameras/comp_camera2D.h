#pragma once
#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "components/common/comp_transform.h"

enum class type2DCam { FIXED, FREE, NEITHER };

class TCompCamera2D : public TCompBase
{
    DECL_SIBLING_ACCESS();

public:

    CEntity* hplayer = nullptr;

    void debugInMenu();
    void load(const json& j, TEntityParseContext& ctx);
    void update(float dt);
    void renderDebug();

    std::string getCurve() { return curveFilename; }
    float getPitch() { return _pitchRatio; }
    float getYaw() { return _yaw; }

private:

    type2DCam type2DCam = type2DCam::NEITHER;

    //Variables render camara
    VEC3 camPos = VEC3::Zero;
    VEC3 newLook = VEC3::Zero;

    //Atributos
    float _sensitivity = 5.0f;
    float _pitchRatio = 0.70f;
    float _maxPitch = (float)M_PI_2 - 1e-4f;

    std::vector<float> hgv, vgv; //Horizontal/Vertical grade value (Variables de la ecuacion polinomica que calcula el maximo horizontal/vertical offset)

    bool first3DTo2D = true;

    float hOffset2D = 0.f;
    float vOffset2D = 0.f;
    float max_hOffset2D = 5.f;
    float max_vOffset2D = 2.f;
    float velReduceOffset = 1;

    float dis_shadow_to_camera = 0;
    float offset_shadow_to_camera = 2;
    float min_dis_shadow_to_camera = 5;

    bool useVelToReduce = false;
    bool inDelayReturn = false;
    float timerDelayReturn = 0;
    float delayReturn = 2;

    float _yaw = 0.f;
    bool replacePitch = false;
    float _padSensitivity = 0.05f;

    const CCurve* curve = nullptr;
    std::string curveFilename = "";
    VEC3 oldLook = VEC3::Zero;

    //Variables debug
    VEC3 debug_cam_pos = VEC3::Zero;
    VEC3 debug_cam_look = VEC3::Zero;
    bool without_init = true;
    float timer_desprojection = 0;
    float time_to_changeCamera = 2;
    VEC3 debug_look = VEC3::Zero;
    VEC3 debug_camPos = VEC3::Zero;
    float mov_offset = 0.f;
    std::vector<VEC3> ray;
    VEC2 mOff = VEC2::Zero;

    bool paneoInverso = false;
    bool vueltaPorVelocidad = false;


    //CALCULATION VARIABLES

    float cos20 = 0.94f;
    float sen140 = 0.64f;
    float sen20 = 0.34f;
    float tan20 = sen20 / cos20;
    float distance_shadow_body=0.f;
    float distance_center_line=0.f;
    float s=0.f;

    void renderCam2D(VEC3 wall_normal, VEC3 wall_point);
    void renderFreeCam2D(float scaled_dt);
    void readInput(float dt);

    void resetFree2DCam();
    void resetcam2D();

    void reductionOffset(float dt);

    void calculateMovementOffset(VEC3 *newPos, VEC3 *newLook, VEC3 wallRight);

    void adjustYaw();

    float float_one_point_round(float value)
    {
        return ((float)((int)(value * 10))) / 10;
    }




};

