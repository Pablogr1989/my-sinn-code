#include "mcv_platform.h"
#include "comp_direction.h"

#include "engine.h"
#include "utils/utils.h"
#include "input/input_module.h"
#include "components/common/comp_transform.h"
#include "components/common/comp_camera.h"
#include "sinn/characters/player/comp_player.h"
#include "engine.h"
#include <components\postfx\comp_render_focus.h>
#include <sinn/cameras/comp_camera3D.h>

DECL_OBJ_MANAGER("camera_dir", TCompDirection);

void TCompDirection::renderDebug()
{
	TCompTransform* camera_trans = get<TCompTransform>();
	VEC3 myPos = camera_trans->getPosition();
	CEntity* eTarget = (CEntity*)getEntityByName("Player");
	if (!eTarget) return;
	TCompTransform* cTargetTransform = eTarget->get<TCompTransform>();
	if (!cTargetTransform) return;

	const MAT44 rt = MAT44::CreateFromYawPitchRoll(_yaw, 0.f, 0.f);
	const MAT44 world = rt * cTargetTransform->asMatrix();


	if (drawAll || drawCurve) cam_curves.find(current_curve)->second->renderDebug(world, 200, { 1.f, 1.f, 0.f, 1.f });

	TCompCamera* camera = get<TCompCamera>();

	//// xy between -1..1 and z betwee 0 and 1
	auto mesh = Resources.get("view_volume.mesh")->as<CMesh>();

	////// Sample several times to 'view' the z distribution along the 3d space
	if (drawAll || drawCamera) {
		const int nsamples = 10;
		for (int i = 1; i < nsamples; ++i) {
			float f = (float)i / (float)(nsamples - 1);
			MAT44 world = MAT44::CreateScale(1.f, 1.f, f) * camera->getInvertViewProjection();
			drawMesh(mesh, world, Color::Green);
			//drawWiredSphere(oldLookAt, 0.2, Color::Green);
		}
	}
	if (drawAll || drawRay) {
		drawLine(camera->getPosition(), camera->getPosition() + camera->getFront() * 100, Color::Red);
		drawWiredSphere(camera->getPosition() + camera->getFront() * 5, 0.2, Color::Green);
	}
}

void TCompDirection::debugInMenu()
{
	ImGui::DragFloat("Sensitivity", &_sensitivity, 0.001f, 0.001f, 0.1f);
	ImGui::DragFloat("Pitch", &_pitchRatio, 0.1f, -100.f, 100.f);
	ImGui::DragFloat("Yaw", &_yaw, 0.1f, -100.f, 100.f);
	ImGui::Checkbox("Camera Fixed", &cameraFixed);
	ImGui::Checkbox("Draw All", &drawAll);
	ImGui::Checkbox("Draw camera", &drawCamera);
	ImGui::Checkbox("Draw Curve", &drawCurve);
	ImGui::Checkbox("Draw Pointer", &drawPointer);
	ImGui::DragFloat("Sphere size", &sizeSphere, 0.1, 0, 10);
	ImGui::Checkbox("Draw Ray", &drawRay);
	ImGui::LabelText("Curva", "%s", current_curve.c_str());
	cam_curves.find(current_curve)->second->renderInMenu();
}

void TCompDirection::load(const json& j, TEntityParseContext& ctx)
{
	//std::string curveFilename = j["curve"];
	//curve = Resources.get(curveFilename)->as<CCurve>();
	std::vector<std::string> curves_filename = j["curves"].get<std::vector<std::string>>();
	for (auto e : curves_filename) {
		const CCurve* c = Resources.get(e)->as<CCurve>();
		cam_curves[e] = c;
	}
	current_curve = cam_curves.begin()->first;
	_pitchRatio = j.value("pitchRatio", _pitchRatio);
	_sensitivity = j.value("sensitivity", _sensitivity);
	_padSensitivity = j.value("padsensitivity", _padSensitivity);
	//_yaw += 0.169;
}

void TCompDirection::update(float scaled_dt)
{
	timer_projection += scaled_dt;

	CEntity* hplayer = (CEntity*)getEntityByName("Player");
	TCompPlayer* comp_player = hplayer->get<TCompPlayer>();
	CEntity* target_entity;
	TCompTransform* target_trans;
	TCompTransform* camera_trans = get<TCompTransform>();
	if (!camera_trans) return;

	//llamo al target cogiendo su entidad

	target_entity = hplayer;
	if (!target_entity) return; // a veces tarda el target en existir, por lo que me espero a que no sea nulo
	target_trans = target_entity->get<TCompTransform>();

	float pitch = 0.f; //Pitch que auto impondremos al cambiar a otras camaras y asi hacer una transicion mas suave

	if (comp_player->getModoCamera() == CAM3D) {
		//leo el input del movimiento del raton
		readInput(scaled_dt);
		without_init = true;
	}
	else {

		if (without_init) {
			timer_projection = 0;
			without_init = false;
			//dbg("Estoy en modo camara2D y acabo de inicializar\n");
		}

		if (timer_projection >= time_to_changeCamera) {
			readInput(scaled_dt);
		}
		else {
		}

	}
	//leo el input del movimiento del raton


	TCompCamera* myCam = get<TCompCamera>();//para hacer los calculos de las colisiones luego
	VEC3 oldPos = camera_trans->getPosition();//para interpolar

	//matrices de traslacion y rotacion de la camara, yaw es cogido del input del movimiento del raton y offset se pasa del json
	const MAT44 tr = MAT44::CreateTranslation({ 0.f, 0.f, 0.f });
	const MAT44 rt = MAT44::CreateFromYawPitchRoll(_yaw, pitch, 0.f);
	MAT44 world;


	VEC3 curvePos, newLookAt;
	//para crear la linea del debug
	myTransform = newLookAt;
	//la matriz de la transform del player (con rotacion 0,0,0 para que no siga la rotacion del target, sino que el target se mueva en funcion de la camara)
	//por la traslacion y rotacion de la camara
	world = rt * tr * MAT44::CreateScale(target_trans->getScale())
		* MAT44::CreateFromQuaternion(QUAT(0, 0, 0, 0))
		* MAT44::CreateTranslation(target_trans->getPosition());


	//la curva se evalua en funcion de la matriz y el input vertical del movimiento del raton
	curvePos = cam_curves.find(current_curve)->second->evaluate(world, _pitchRatio);
	//dbg("Estoy evaluando la curva %s\n", current_curve.c_str());


	VEC3 pos = target_trans->getPosition();
	newLookAt = VEC3(pos.x, pos.y + 1, pos.z);


	//comprobar que no colisiona la camara con alguna pared o que haya algo entre ella y el target
	//VEC3 newPos = myCam->HandleCollisionZoom(curvePos, target_trans->getPosition(), 0.01);
	VEC3 newPos = curvePos;


	//interpolamos la posicion y el lookat
	VEC3 camPos = 0.8f * oldPos + 0.2f * newPos;
	VEC3 lookAt = 0.8f * oldLookAt + 0.2f * newLookAt;

	camera_trans->lookAt(camPos, lookAt);


	oldLookAt = lookAt;



}

void TCompDirection::readInput(float scaled_dt)
{

	if (cameraFixed) return;

	CEntity* hplayer = (CEntity*)getEntityByName("Player");
	if (hplayer == nullptr)	return;

	TCompPlayer* comp_player = hplayer->get<TCompPlayer>();
	CEntity* camera = comp_player->getCamera();
	TCompCamera3D* camera3D = camera->get< TCompCamera3D>();

	/*if (camera3D->isBlockMovement()) {
		input::CInputModule& input = CEngine::get().getInput();
		float sensitivity;
		VEC2 delta;
		if (input["camera_x"].isPressed() or input["camera_y"].isPressed()) {
			delta = VEC2(input["camera_x"].value, input["camera_y"].value);
			sensitivity = input.getPadSensitivity();
		}
		else {
			delta = input.getMouse().deltaPosition;
			delta.x *= -1;
			sensitivity = input.getMouseSensitivity();
		}
		_yaw += delta.x * sensitivity * scaled_dt * 3;
		_pitchRatio += delta.y * sensitivity * scaled_dt;
		_pitchRatio = clamp(_pitchRatio, 0.f, 1.f);
	}
	else {
		_yaw = comp_player->getYawCamera();
		_pitchRatio = comp_player->getPitchCamera();
		//_yaw += 0.169;
	}*/

	_yaw = comp_player->getYawCamera();
	_pitchRatio = comp_player->getPitchCamera();




}

void TCompDirection::blockMovement(bool b)
{
	cameraFixed = b;
}

void TCompDirection::blockMovement(bool b, float p, float y)
{
	cameraFixed = b;
	_yaw = y;
	_pitchRatio = p;
}

void TCompDirection::setPitchYaw(float p, float y)
{
	_yaw = y;
	_pitchRatio = p;
}

void TCompDirection::rotateToPoint(VEC3 point)
{
	TCompTransform* trans = get<TCompTransform>();
	float relYaw = trans->getDeltaYawToAimTo(point);
	_yaw += relYaw;
}

void TCompDirection::setCurve(std::string new_curve)
{
	current_curve = new_curve;
}
