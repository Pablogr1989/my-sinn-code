#include "mcv_platform.h"
#include "comp_player.h"
#include "components/common/comp_render.h"
#include "components/common/comp_camera.h"
#include "components/common/comp_transform.h"
#include "modules/module_camera_mixer.h"
#include "components/common/comp_aabb.h"
#include "sinn/cameras/comp_camera3D.h"
#include "sinn/cameras/comp_camera2D.h"
#include "sinn/components/comp_cuerda.h"
#include <skeleton\comp_skeleton.h>
#include <sinn\behavior\ai\ai_controller.h>
#include "render/render_module.h"
#include "sinn/modules/module_scripting.h"
#include "sinn/modules/module_sound.h"
#include "sinn/components/comp_audio.h"
#include "sinn/characters/player/comp_direction.h"
#include "sinn/cameras/comp_cameraRail.h"
#include "components/common/comp_particles.h"
#include "particles/particles_system.h"
#include "sinn/characters/animator.h"
#include "input/input_module.h"
#include "sinn/modules/module_cutscene.h"
#include "ui/ui_module.h"
#include "ui/widgets/ui_image.h"
#include "sinn/interaction/comp_push_obj.h"
#include <sinn/components/comp_thrown_object.h>


extern CCteBuffer< CtePlayer >  cte_player;

DECL_OBJ_MANAGER("player", TCompPlayer);

void TCompPlayer::registerMsgs() {
	DECL_MSG(TCompPlayer, TMsgPush, hitBox);
	DECL_MSG(TCompPlayer, TMsgCheckpoint, saveGame);
	DECL_MSG(TCompPlayer, TMsgConstrict, onConstrict);
	DECL_MSG(TCompPlayer, TMsgDeath, onDead);
	DECL_MSG(TCompPlayer, TMsgDeathFall, onFallingDead);
	DECL_MSG(TCompPlayer, TMsgRecordPath, hitWithActor);
	DECL_MSG(TCompPlayer, TMsgPlatMove, OnPlatform);
	DECL_MSG(TCompPlayer, TMsgDamage, OnHit);
	DECL_MSG(TCompPlayer, TMsgEnterArea, onEnterArea);
	DECL_MSG(TCompPlayer, TMsgExitArea, onExitArea);
	DECL_MSG(TCompPlayer, TMsgInitFinalEvent, initFinalEvent);
}


void TCompPlayer::setInCutscene(bool n)
{
	inCutscene = n;
	if (inCutscene) {
		TCompFSM* fsm = get<TCompFSM>();
		fsm->changeState("CutScene");
	}
}

void TCompPlayer::activateSteps(bool n)
{
	stepsActivated = n;
}



void TCompPlayer::playSteps()
{
	if (!stepsActivated) return;
	if (rightOrLeftFootInFloor) {
		//PASO DERECHO
		if (last_value_rightorleft != rightOrLeftFootInFloor) {
			TCompAudio* audio = get<TCompAudio>();
			steps_event = audio->playEvent("Test1/OnStep");
			steps_event->setParameter("Material", step_area);
			if (plant.isValid()) {
				TMsgStepPlant msg;
				CEntity* e = plant;
				e->sendMsg(msg);
			}
			last_value_rightorleft = rightOrLeftFootInFloor;
		}
	}
	else {
		//PASO IZQUIERDO
		if (last_value_rightorleft != rightOrLeftFootInFloor) {
			TCompAudio* audio = get<TCompAudio>();
			//dbg("pie izquierdo \n");
			steps_event = audio->playEvent("Test1/OnStep");
			steps_event->setParameter("Material", step_area);
			if (plant.isValid()) {
				TMsgStepPlant msg;
				CEntity* e = plant;
				e->sendMsg(msg);
			}
			last_value_rightorleft = rightOrLeftFootInFloor;
		}
	}
}

void TCompPlayer::resetMovement()
{
	local_dir = VEC3::Zero;
	velocity = VEC3::Zero;
	modVelocity = 0;
}

void TCompPlayer::onConstrict(const TMsgConstrict& msgConstrict)
{

	if (!constrict) {
		constrict = true;
		attemptsToFree = msgConstrict.attemptsToFree;

		h_guardian = msgConstrict.h_sender;
		CEntity* guardian = h_guardian;
		dbg("Me ha cogido %s\n", guardian->getName());

		TCompFSM* fsm = get<TCompFSM>();
		fsm->changeState("Reposition");
	}
}

void TCompPlayer::onDead(const TMsgDeath& msgDead)
{
	CEngine::get().getCutScenes().skipCutscene();
	dead = true;
}

void TCompPlayer::onFallingDead(const TMsgDeathFall& msgDead)
{
	CEngine::get().getCutScenes().skipCutscene();
	fallingDead = true;
	TCompAudio* audio = get<TCompAudio>();
	audio->playEvent("dieFall");
}

void TCompPlayer::resetPlayer()
{
	VEC3 pos;
	QUAT rot;
	VEC3 scale;
	startPos.Decompose(scale, rot,pos);

	TCompCollider* col = get<TCompCollider>();
	col->controller->setPosition(PxExtendedVec3(pos.x, pos.y, pos.z));

	//girar al player y poner la camara justo detras del player
	TCompTransform* t = get<TCompTransform>();
	t->setRotation(rot);
	float yaw, pitch, roll;
	t->getEulerAngles(&yaw, &pitch, &roll);
	float p = 0.5;
	float y = yaw;
	bool block = false;
	getObjectManager<TCompCamera3D>()->forEach([block, p, y](TCompCamera3D* c) {
		c->blockMovement(block, p, y);
		});
	CEngine::get().getCutScenes().resetCutscene("Cinevento_Escalera_ENGINE");

	CEngine::get().getSound().playBSO("Temple");
	SoundEvent* intro = CEngine::get().getSound().getBSO();
	intro->setParameter("intro", 0);
	//dbg("Player: He reseteado al punto X:%4.f Y:%4.f Z:%4.f\n", startPos.x, startPos.y, startPos.z);
	
	stopBoxSound();

	//recreate audio and cutscene triggers so they can throw the events again
	if (audio_cutscene_triggers.size() > 0) {
		for (auto n : audio_cutscene_triggers) {
			CEntity* e = getEntityByName(n);
			TCompCollider* c = e->get<TCompCollider>();
			c->recreateTriggers();

		}
	}
	//Informo al Ai controller que debe resetear a todos los guardianes
	TMsgResetGame msg;
	getObjectManager<CEntity>()->forEach([&msg](CEntity* e) {
			e->sendMsg(msg);
		});

	//Me desproyecto(por si acaso estaba proyectado
	if (CEngine::get().getProjected()) {
		manualDesproject();
	}
	//Si estabamos en una camara por railes o especial, la desactivamos
	setCanChangeCamera(true);
	needCorrectCam = false;
	block_localDir = false;
	setInCutscene(false);

	if (getInRailCamera()) {
		setGuideMode(false);
		setInRailCamera(false);
		CEntity* ent_camera = (CEntity*)getEntityByName("camera_direction");
		TCompDirection* comp_direc = ent_camera->get<TCompDirection>();
		comp_direc->blockMovement(false);
		getObjectManager<TCompCamera3D>()->forEach([](TCompCamera3D* c) {
			c->blockMovement(false);
			});
	}

	CEntity* e = h_camera;
	CEngine::get().getCameraMixer().blendCamera(e->getName(), 0.1, &interpolators::linearInterpolator);

	resetConstrictVars();

	dead = false;
	fallingDead = false;
	reset = true;
	velocity = VEC3::Zero;
	push_box = CHandle();
	noise = 0.f;
	light = CHandle();
	wall_point = VEC3::Zero;
	wall_normal = VEC3::Zero;
	if (firstTimeResurrect && area == eventArea::GUARDIANS) {
		firstTimeResurrect = false;
		CEngine::get().getSound().playEvent("Tutorial_Quetz_019");
	}
	resurrect = true;

	if (seele_cry) {
		seele_cry->stop();
		seele_cry = nullptr;
	}
}

void TCompPlayer::hitBox(const TMsgPush& msg_push) {
	push_box = msg_push.object;
}

void TCompPlayer::convertPushBox(bool create_controller) {
	CEntity* ent_box = push_box;
	if (ent_box == nullptr) {

		stopBoxSound();
		return;
	}
	TCompCollider* box_col = ent_box->get<TCompCollider>();

	stopBoxSound();
	if (create_controller && !box_col->boxcontroller) {
		box_col->destroyActor();
		box_col->recreateController();
	}
	else if(!create_controller){
	
		box_col->destroyBoxController();
		box_col->recreateActorBox();
		push_box = CHandle();
	}
}
bool particle = false;
void TCompPlayer::movePushBox(float dt) {
	CEntity* ent_box = push_box;
	//assert(ent_box);
	if (ent_box == nullptr) return;
	TCompTransform* t_box = ent_box->get<TCompTransform>();
	VEC3 pos = t_box->getPosition();
	TCompCollider* box_col = ent_box->get<TCompCollider>();
	TCompParticles* particles = ent_box->get<TCompParticles>();
	//assert(box_col->boxcontroller);
	if (box_col->boxcontroller == nullptr) return;
	float dif = VEC3::Distance(pos, oldPos);
	CEngine::get().getPhysics().move(velocity, dt, box_col->boxcontroller, box_collisions);
	oldPos = pos;
	//dbg("particle : %s \n", particle ? "true" : "false");
	if (velocity.x != 0 && velocity.z != 0 && dif>0.01) {
		if (!particle) {
			particles->startAgain();
			particle = true;
		}

		TCompAudio* audio = get<TCompAudio>();
		if (move_box_sound)
		{
			if (move_box_sound->isPlaying()) return;
			move_box_sound = audio->playEvent("moveObject");
		}
		else {
			move_box_sound = audio->playEvent("moveObject");
		}

	}
	else {
		particle = false;

		stopBoxSound();
		

	}
	
}

bool TCompPlayer::checkPushBox()
{
	CEntity* ent_push_box = push_box;
	if (!ent_push_box) return false;
	TCompCollider* push_box_col = ent_push_box->get<TCompCollider>();
	if (!push_box_col) return false;
	if (!push_box_col->boxcontroller) return false;
	else return true;
}

bool TCompPlayer::canMoveTheBox(){
	PxRaycastBuffer hitCall;
	PxQueryFilterData fd = PxQueryFilterData();
	fd.data.word0 = CModulePhysicsSinn::FilterGroup::Projectable_Object;

	TCompTransform* player_trans = get<TCompTransform>();
	VEC3 player_front = player_trans->getFront(); // Deberia estar ya normalizado
	//player_front.Normalize(); // Deberiamos poderlo quitar
	VEC3 origin = player_trans->getPosition();
	origin.y += 0.5;
	
	bool status = CEngine::get().getPhysics().gScene->raycast(
		VEC3_TO_PXVEC3(origin), VEC3_TO_PXVEC3(player_front),
		4, hitCall, PxHitFlags(PxHitFlag::eDEFAULT), fd);
	if (!status) return status;
	if (!hitCall.hasAnyHits()) {

		stopBoxSound();
			
		return false;
	}
	CHandle h_collider;
	h_collider.fromVoidPtr(hitCall.block.actor->userData);
	TCompCollider* c_collider = h_collider;
	const json& jconfig = c_collider->jconfig;

	if (jconfig.count("controllerBox") > 0) {
		TCompTransform* box_trans = c_collider->get<TCompTransform>();
		VEC3 half_size = VEC3::Zero;
		if (jconfig.count("shapes")) {
			const json& jshapes = jconfig["shapes"];
			for (const json& jshape : jshapes) {
				std::string jgeometryType = jshape["shape"];
				if (jgeometryType == "box") {
					VEC3 aux = loadVEC3(jshape, "half_size");
					if (aux.x + aux.y + aux.z > half_size.x + half_size.y + half_size.z)
						half_size = aux;
				}
			}			
		}
		if ((1.4f / (half_size.y * 2)) > 1) {
			return true;
		}
		else {

			stopBoxSound();
			return false;
		}
	}

}

void TCompPlayer::stopBoxSound() {
	CEngine::get().getSound().forceStopEvent("moveObject");
	if (move_box_sound) {
		move_box_sound = nullptr;
	}
}

void TCompPlayer::convertShadowPushBox(bool create_controller) {
	CEntity* ent_box = push_box;
	if (ent_box == nullptr)	return;

	TCompCollider* box_col = ent_box->get<TCompCollider>();

	if (create_controller && !box_col->boxcontroller) {
		box_col->destroyActor();
		box_col->recreateController();
		ent_box->get<TCompColShadow>().destroy();
	}
	else if (!create_controller) {
		box_col->destroyBoxController();
		box_col->recreateActorBox();
		stopBoxSound();

		CEntity* ent_light = getLight();
		TCompTransform* light_trans = ent_light->get<TCompTransform>();
		box_col->create_projection(light_trans->getPosition(),light_trans->getUp(), light_trans->getLeft());
		oldPos = VEC3(0, 0, 0);
	}
}


void TCompPlayer::saveGame(const TMsgCheckpoint& msg)
{
	startPos = msg.check_point;
	last_checkpoint_name = msg.form_name;
}




void TCompPlayer::updateTimers(float dt)
{
	timer_player += dt;
	fadeTimer += dt;
	tSpeed += dt;
	current_time += dt;
	timer_debug_jump += dt;
	recordTimer += dt;
	sonidoPies += dt;
	timer_ResetProject += dt;
	timer_fEv += dt;
	timer_jump += dt;
	timer_jumpStart += dt;
	timer_damaged += dt;
	timer_animation += dt;
}

void TCompPlayer::load(const json& j, TEntityParseContext& ctx) {
	rotation_speed = j.value("rotation_speed", 10);

	cte_player.projected = false;
	cte_player.final_battle = false;

	if (j.count("pos_teleport")) {
		const json& jPositions = j["pos_teleport"];
		assert(jPositions.is_array());

		for (auto jpos : jPositions.items()) {
			const json& item = jpos.value();
			loadTeleportPos(item);
		}

	}

	cte_player.updateGPU();
	//jump_strength = j.value("jump_strength", 0.f); //+ physx::fall_velocity;
	//h_camera = getEntityByName("cameraWalk3D");
}

void TCompPlayer::debugInMenu() {

	string cameraMod = "NINGUNA";
	switch (modoCamera)
	{
	case CAM3D:
		cameraMod = "CAM3D";
		break;
	case CAM2D:
		cameraMod = "CAM2D";
		break;
	case FLYOVER:
		cameraMod = "FLYOVER";
		break;
	case EXTERNAL:
		cameraMod = "EXTERNAL";
		break;
	}

	ImGui::Text("Tipo de Camara %s", cameraMod.c_str());
	CEntity* c_cam = h_camera;
	if (c_cam != nullptr) {
		string name_cam = c_cam->getName();
		ImGui::Text("Camara: %s", name_cam.c_str());
	}

	static char nameObj[512];
	ImGui::InputText("Object name", nameObj, size(nameObj));

	if (ImGui::SmallButton("Crear")) {
		TCompTransform* mytrans = get<TCompTransform>();
		VEC3 mypos = mytrans->getPosition();

		TEntityParseContext myContext;
		string name = nameObj;
		parseScene("data/prefabs/" + name + ".json", myContext);
		CHandle h = myContext.entities_loaded[0];
		CEntity* e = h;
		TCompTransform* e_pos = e->get<TCompTransform>();
		TCompCollider* e_col = e->get<TCompCollider>();
		if (e_col->actor != nullptr)
		{
			((PxRigidDynamic*)e_col->actor)->putToSleep();
		}
		e_pos->setPosition(mypos + mytrans->getFront() * 0.5);
		if (e_col->actor != nullptr)
		{
			((PxRigidDynamic*)e_col->actor)->wakeUp();
		}
	}

	/*if(ImGui::TreeNode("Lanzar piedra")) {
		//Formula parabola
		static float tf = 1;  //Duracion del lanzamiento
		static float y0 = 1.3;  //Altura inicial
		static VEC3 pos_ruido;

		TCompTransform* mytrans = get<TCompTransform>();
		VEC3 mypos = mytrans->getPosition() + VEC3(-0.3, y0, 0.1);
		float xf = VEC3::Distance(mypos, pos_ruido);  // Distancia al punto finalr
		static VEC3 f;

		static bool sim_enemy_noise = false;
		ImGui::Checkbox("Simular ruido para guardianes", &sim_enemy_noise);
		ImGui::DragFloat3("Posicion lanzamiento", &pos_ruido.x, 0.1, -1000, 1000);
		ImGui::SameLine();
		if (ImGui::SmallButton("->")) {
			pos_ruido = mypos;
		}
		ImGui::DragFloat("Tiempo parabola", &tf, 0.1, 0.5, 100);

		vector<float> vtime;
		vector<float> vdx;
		vector<float> vdy;

		float pow2_tf = tf * tf;
		float v0x = xf / tf;
		float v0y = (0.5 * 9.8 * pow2_tf) / tf;
		float initAng = atanf(v0y / v0x);
		float v0 = v0x / cosf(initAng);

		VEC3 up_stone = VEC3(0, 1, 0);
		VEC3 front_stone = pos_ruido - mypos;
		front_stone.Normalize();

		static bool pintar_trayectoria = false;

		ImGui::Checkbox("Pintar trayectoria", &pintar_trayectoria);

		if (pintar_trayectoria) {
			for (float t = 0; t < tf; t += 0.1) {
				vtime.push_back(t);

				float pow2_t = t * t;
				float ax = t * v0x;
				float ay = v0y * t - 0.5 * 9.8 * pow2_t;
				vdy.push_back(ay);
				vdx.push_back(ax);
			}

			VEC3 last_point = VEC3::Zero;

			drawWiredSphere(mypos, 0.1, Color::Red);
			for (int i = 0; i < vtime.size(); i++) {

				if (i == 0) {
					last_point = mypos + front_stone * vdx[i] + up_stone * vdy[i];
					drawLine(mypos, last_point, Color::LimeGreen);
					drawWiredSphere(last_point, 0.1, Color::Red);
				}
				else {
					VEC3 point = mypos + front_stone * vdx[i] + up_stone * vdy[i];
					drawLine(last_point, point, Color::LimeGreen);
					drawWiredSphere(last_point, 0.1, Color::Red);
					last_point = point;
				}
			}
			//drawWiredSphere(last_point, 0.1, Color::Red);
			drawLine(last_point, pos_ruido, Color::LimeGreen);
		}
		drawWiredSphere(pos_ruido, 0.2, Color::Green);

		if (ImGui::SmallButton("Lanzar")) {

			TEntityParseContext myContext;
			parseScene("data/prefabs/thrownStone.json", myContext);
			CHandle h = myContext.entities_loaded[0];
			CEntity* e = h;
			TCompTransform* e_pos = e->get<TCompTransform>();
			TCompCollider* e_col = e->get<TCompCollider>();
			if (e_col->actor != nullptr)
			{
				((PxRigidDynamic*)e_col->actor)->putToSleep();
				e_col->actor->setGlobalPose(PxTransform(mypos.x, mypos.y, mypos.z));
			}
			TCompThrownObject* stone = e->get< TCompThrownObject>();
			VEC3 fDirec = front_stone * cos(initAng) + up_stone * sin(initAng);
			f = fDirec * v0;
			stone->throwObject(f);

			if (sim_enemy_noise) {
				CHandle boss = getEntityByName("Boss");
				TCompTransform* my_trans = get<TCompTransform>();

				TMsgSuspiciusEvent msg;
				msg.intensity = 2;
				msg.pos = pos_ruido;
				msg.type = static_cast<int> (suspicius_noise_type::STONE_NOISE);
				boss.sendMsg(msg);

			}
		}

		ImGui::TreePop();
	}*/



	ImGui::Checkbox("Debug Velocity", &debug_velocity);
	ImGui::DragFloat("Debug Velocity Value", &fake_velocity, 0.1, 0, 10);

	ImGui::DragFloat("Seele Cry Volumen", &vol_seele_cry, 0.1, 0, 1);

	ImGui::DragFloat2("pointingInit", &pointingInit.x);
	ImGui::DragFloat2("pointingAngle", &pointingAngle.x);
	ImGui::DragFloat2("pointingLimits", &pointingLimits.x);

	ImGui::Checkbox("Can Change Camera", &canChangeCamera);
	ImGui::Checkbox("Damaged", &damaged);
	ImGui::DragFloat("Timer damaged", &timer_damaged);

	ImGui::Checkbox("Not move", &not_move);

	/*static VEC3 pos1 = VEC3::Zero;
	static VEC3 pos2 = VEC3::Zero;
	static VEC3 posCam = VEC3::Zero;
	static VEC3 resPosCam = VEC3::Zero;
	static VEC3 res = VEC3::Zero;

	ImGui::DragFloat3("Posicion 1", &pos1.x, 0.1);
	ImGui::DragFloat3("Posicion 2", &pos2.x, 0.1);
	ImGui::DragFloat3("Desplazamiento", &res.x, 0.1);
	ImGui::DragFloat3("Posicion Camara", &posCam.x, 0.1);
	ImGui::DragFloat3("Posicion Camara con desplazamiento", &resPosCam.x, 0.1);

	res = pos1 - pos2;
	resPosCam = posCam + res;*/

	if (ImGui::SmallButton("CutScene State")) {
		if (isInCutscene()) {
			setInCutscene(false);
		}
		else {			
			setInCutscene(true);
		}

	}

	CEntity* ent_interact_ptr = ent_interact;
	ImGui::LabelText("Interaction Entity", "%s\n", ent_interact_ptr == nullptr ? "null" : ent_interact_ptr->getName());
	ImGui::Text("Moving 2D Obj: %s\n", haveMove2DObj ? "true" : "false");

	ImGui::Checkbox("Start Record", &startRecord);

	if (ImGui::TreeNode("Path Record"))
	{
		if (startRecord && list_steps.size() > 0) {
			int i = 1;
			for (auto step : list_steps) {
				ImGui::PushID(i);
				ImGui::LabelText("Nombre entidad:", "%s", step.actor_name.c_str());
				ImGui::LabelText("Tiempo contacto", "%f", step.abs_time);
				ImGui::LabelText("Posicion contacto", "%f %f %f", step.pos_player.x, step.pos_player.y, step.pos_player.z);
				ImGui::PopID();
				ImGui::NewLine();
				i++;
			}
		}
		ImGui::TreePop();
	}

	if (ImGui::SmallButton("Print Path Record")) {
		dbg("/---------------------------------------------------------------------------------------------------/\n");
		dbg("/t/t LIST PATH RECORD\n");
		dbg("/---------------------------------------------------------------------------------------------------/\n");
		int i = 0;
		float last_time = 0;
		for (auto step : list_steps) {
			dbg("Step %d [%s] -> Abs.Time: %f -> Diff: %f\n", i, step.actor_name.c_str(), step.abs_time, abs(step.abs_time-last_time) );
			last_time = step.abs_time;
			i++;
		}
		dbg("/---------------------------------------------------------------------------------------------------/\n");
	}

	ImGui::DragFloat("Additional Value for Time collision Up", &addValueCollisionUp, 0.1, 0, 100);
	ImGui::DragFloat("Time Before JumpDesc", &time_beforeJumpDesc, 0.1, 0, 10);
	ImGui::LabelText("Timer Before JumpDesc", "%.2f", (timer_player - time_collision_up));

	ImGui::DragFloat("Time Eternal Falling", &time_eternalFalling, 0.1, 1, 15);
	ImGui::DragFloat("Time Death by fall", &time_deathByFall, 0.1, 1, 15);

	if (ImGui::SmallButton("Trigger cajones")) {

		sendMsgCajones("obj_CajaAlmacen_001");
		sendMsgCajones("obj_CajaAlmacen_002");
		sendMsgCajones("obj_CajaAlmacen_003");
		sendMsgCajones("obj_CajaAlmacen_004");
		sendMsgCajones("obj_CajaAlmacen_005");
		sendMsgCajones("obj_CajaAlmacen_006");
		sendMsgCajones("obj_CajaAlmacen_007");
		sendMsgCajones("obj_CajaAlmacen_008");
		sendMsgCajones("obj_CajaAlmacen_009");
		sendMsgCajones("obj_CajaAlmacen_010");
		sendMsgCajones("obj_CajaAlmacen_011");
		sendMsgCajones("obj_CajaAlmacenRota_TipoA_001");
		sendMsgCajones("obj_CajaAlmacenRota_TipoA_002");
		sendMsgCajones("obj_CajaAlmacenRota_TipoA_003");
		sendMsgCajones("obj_CajaAlmacenRota_TipoA_004");
		sendMsgCajones("obj_CajaAlmacenRota_TipoA_005");
		sendMsgCajones("obj_CajaAlmacenRota_TipoA_006");
		sendMsgCajones("obj_CajaAlmacenRota_TipoA_007");
		sendMsgCajones("obj_CajaAlmacenRota_TipoA_008");
		sendMsgCajones("obj_CajaAlmacenRota_TipoA_009");
		sendMsgCajones("obj_CajaAlmacenRota_TipoA_010");
		sendMsgCajones("obj_CajaAlmacenRota_TipoA_011");
		sendMsgCajones("obj_CajaAlmacenRota_TipoA_012");
		sendMsgCajones("obj_CajaAlmacenRota_TipoA_013");
		sendMsgCajones("obj_CajaAlmacenRota_TipoA_014");	

	}

	if (ImGui::SmallButton("Change camera")) {
		if (canChangeCamera) {
			mixer.blendCamera("camara_cueva_rail", 2, &interpolators::linearInterpolator);
			canChangeCamera = false;
			CEntity* ent_camera = (CEntity*)getEntityByName("camera_direction");
			TCompDirection* comp_direc = ent_camera->get<TCompDirection>();
			comp_direc->blockMovement(true);
			getObjectManager<TCompCamera3D>()->forEach([](TCompCamera3D* c) {
				c->blockMovement(true);
				});
		}
		else {
			CEntity* cam = h_camera;
			mixer.blendCamera(cam->getName(), 2, &interpolators::linearInterpolator);
			canChangeCamera = true;
			CEntity* ent_camera = (CEntity*)getEntityByName("camera_direction");
			TCompDirection* comp_direc = ent_camera->get<TCompDirection>();
			comp_direc->blockMovement(false);
			getObjectManager<TCompCamera3D>()->forEach([](TCompCamera3D* c) {
				c->blockMovement(false);
				});
		}
	}

	ImGui::DragFloat("Rotation Speed", &rotation_speed, 1, 0, 1000);
	TCompSkeleton* skel = get<TCompSkeleton>();

	ImGui::Checkbox("Left Foot in Floor", &rightOrLeftFootInFloor);
	ImGui::DragFloat("Margen al suelo", &distStepToFloor, 0.01, 0, 100);
	


	ImGui::DragFloat("Max Horizontal Jump Height", &max_y_hor_jump, 0.1, 0, 100);
	ImGui::DragFloat("Max Vertical Jump Height", &max_y_vert_jump, 0.1, 0, 100);
	ImGui::DragFloat("Duration Jump", &time_jump, 0.1, 0, 100);
	ImGui::Checkbox("Draw Jump", &drawJump);
	if (ImGui::SmallButton("Clear Jump Draws")) {
		debug_pos_jump.clear();
	}	

	ImGui::DragFloat("Jump Strength", &jump_strength, 0.1f, 0.f, 100.f, "%.1f");
	ImGui::Checkbox("In Jump Start", &inJumpStart);
	ImGui::DragFloat("Timer Jump Start", &timer_jumpStart, 0.1, 0, 100);

	/*if (ImGui::TreeNode("Fake Jump")) {

		const char* fjstate = "";

		if (fake_jump_state == 0) {
			fjstate = "Fake Jump Start";
		}

		if (fake_jump_state == 1) {
			fjstate = "Fake Jump Up";
		}

		if (fake_jump_state == 2) {
			fjstate = "Fake Jump Desc";
		}

		if (fake_jump_state == 3) {
			fjstate = "Fake Jump End";
		}

		ImGui::Text("Fake Jump State: %s", fjstate);
		bool col = collisions.collision_down;
		bool pushUp = isPushingUp();

		ImGui::Text("Timer Jump: %.2f", timer_jump);
		ImGui::Text("Timer fake Jump: %.2f", timer_fake_jump);
		ImGui::Checkbox("Collision down", &col);
		ImGui::Checkbox("Pushin Up", &pushUp);

		static ImGuiComboFlags flags = 0;
		const char* types[] = { "cycle", "action" };

		static float jforce = 1;
		ImGui::DragFloat("Jump Strength", &jforce, 0.1, 0.4, 1);
		ImGui::DragFloat("Duration Jump Start", &time_fake_jumpStart, 0.1, 0, 100);
		ImGui::DragFloat("Duration Jump End", &time_fake_jumpEnd, 0.1, 0, 100);

		// FAKE JUMP START
		ImGui::Text("Jump Start");
		static float jsBegin = 0;
		static float jsEnd = 0;
		static float jsIn = 0;
		static float jsOut = 0;
		ImGui::DragFloat("Jump Start Begin time", &jsBegin, 0.1, 0, 100);
		ImGui::DragFloat("Jump Start End time", &jsEnd, 0.1, 0, 100);
		ImGui::DragFloat("Jump Start In Delay", &jsIn, 0.1, 0, 100);
		ImGui::DragFloat("Jump Start Out Delay", &jsOut, 0.1, 0, 100);
		static const char* jsType = types[0];
		if (ImGui::BeginCombo("combo jump start type", jsType, flags))
		{
			for (int n = 0; n < IM_ARRAYSIZE(types); n++)
			{
				bool is_selected = (jsType == types[n]);
				if (ImGui::Selectable(types[n], is_selected))
					jsType = types[n];
				if (is_selected)
					ImGui::SetItemDefaultFocus();
			}
			ImGui::EndCombo();
		}


		// FAKE JUMP UP AND DESC
		ImGui::Text("Jump Up & Jump Desc");
		static float juBegin = 0;
		static float juEnd = 0;
		static float juIn = 0;
		static float juOut = 0;
		ImGui::DragFloat("Jump Up/Desc Begin time", &juBegin, 0.1, 0, 100);
		ImGui::DragFloat("Jump Up/Desc End time", &juEnd, 0.1, 0, 100);
		ImGui::DragFloat("Jump Up/Desc In Delay", &juIn, 0.1, 0, 100);
		ImGui::DragFloat("Jump Up/Desc Out Delay", &juOut, 0.1, 0, 100);
		static const char* juType = types[0];
		if (ImGui::BeginCombo("combo jump/Desc up type", juType, flags))
		{
			for (int n = 0; n < IM_ARRAYSIZE(types); n++)
			{
				bool is_selected = (juType == types[n]);
				if (ImGui::Selectable(types[n], is_selected))
					juType = types[n];
				if (is_selected)
					ImGui::SetItemDefaultFocus();
			}
			ImGui::EndCombo();
		}

		// FAKE JUMP END
		ImGui::Text("Jump End");
		static float jeBegin = 0;
		static float jeEnd = 0;
		static float jeIn = 0;
		static float jeOut = 0;
		ImGui::DragFloat("Jump End Begin time", &jeBegin, 0.1, 0, 100);
		ImGui::DragFloat("Jump End End time", &jeEnd, 0.1, 0, 100);
		ImGui::DragFloat("Jump End In Delay", &jeIn, 0.1, 0, 100);
		ImGui::DragFloat("Jump End Out Delay", &jeOut, 0.1, 0, 100);
		static const char* jeType = types[0];
		if (ImGui::BeginCombo("combo jump End type", jeType, flags))
		{
			for (int n = 0; n < IM_ARRAYSIZE(types); n++)
			{
				bool is_selected = (jeType == types[n]);
				if (ImGui::Selectable(types[n], is_selected))
					jeType = types[n];
				if (is_selected)
					ImGui::SetItemDefaultFocus();
			}
			ImGui::EndCombo();
		}



		if (ImGui::SmallButton("Ejecutar Fake Salto")) {
			timer_fake_jump = 0;
			fake_jump_state = 0;
			fakeJump = true;
			setJumpStrength(jforce);

			TCompAnimator* anim = get<TCompAnimator>();
			anim->insertAnimInList(jsBegin, jsEnd, "jump-start", jsType, jsIn, jsOut);
			anim->insertAnimInList(juBegin, juEnd, "jump-loop", juType, juIn, juOut);
			//anim->insertAnimInList(jeBegin, jeEnd, "jump-end", jeType, jeIn, jeOut);
		}

		ImGui::TreePop();
	}*/

}

void TCompPlayer::sendMsgCajones(std::string n_ent)
{
	CEntity* cen = getEntityByName(n_ent);
	TMsgSwitchActivation msg;
	cen->sendMsg(msg);
}

bool TCompPlayer::tetris_ui_sound()
{
	if (!tetris_ui_sound_first) return false;
	CEntity* tetris = getEntityByName("obj_Tetris_TipoB");
	if (!tetris) return false;
	TCompPushObj* comp_push = tetris->get<TCompPushObj>();
	if (!comp_push) return false;
	if (comp_push->getZone() != IZ_FAR) return false;
	
	tetris_ui_sound_first = false;
	return true;
}

void TCompPlayer::renderDebug() {

	//drawAxis(tp_start.asMatrix());
	//drawAxis(tp_target.asMatrix());

	/*input::CInputModule& input = CEngine::get().getInput();
	static int mouseWheelValue = 0;

	if (input.getMouse().mouseWheel != 0) {
		if (input.getMouse().mouseWheel > 0) {
			mouseWheelValue += 1;
		}
		if (input.getMouse().mouseWheel < 0) {
			mouseWheelValue -= 1;
		}
	}

	ImGui::Text("Mouse Wheel Value: %d", mouseWheelValue);*/

	ImGui::Checkbox("Lanzamiento en dos pasos", &throwing_two_steps);

	if (render_parabole) {
		TCompTransform* mytrans = get<TCompTransform>();

		//Formula parabola
		float tf = time_parabole;  //Duracion del lanzamiento
		float y0 = 1.3;  //Altura inicial
		VEC3 pos_ruido = throwing_point;

		//Correccion (-0.3, y0, 0.1)
		VEC3 mypos = mytrans->getPosition() - mytrans->getLeft() * 0.3  + mytrans->getUp() * y0 + mytrans->getFront() * 0.1;
		float xf = VEC3::Distance(mypos, pos_ruido);  // Distancia al punto finalr
		static VEC3 f;

		vector<float> vtime;
		vector<float> vdx;
		vector<float> vdy;

		float pow2_tf = tf * tf;
		float v0x = xf / tf;
		float v0y = (0.5 * 9.8 * pow2_tf) / tf;
		float initAng = throwing_angle = atanf(v0y / v0x);
		float v0 =throwing_vel = v0x / cosf(initAng);

		VEC3 up_stone = VEC3(0, 1, 0);
		VEC3 front_stone = pos_ruido - mypos;
		front_stone.Normalize();
		front_object = front_stone;


		for (float t = 0; t < tf; t += 0.1) {
			vtime.push_back(t);

			float pow2_t = t * t;
			float ax = t * v0x;
			float ay = v0y * t - 0.5 * 9.8 * pow2_t;
			vdy.push_back(ay);
			vdx.push_back(ax);
		}

		VEC3 last_point = VEC3::Zero;

		drawWiredSphere(mypos, 0.1, Color::Red);
		for (int i = 0; i < vtime.size(); i++) {

			if (i == 0) {
				last_point = mypos + front_stone * vdx[i] + up_stone * vdy[i];
				drawLine(mypos, last_point, Color::LimeGreen);
				drawWiredSphere(last_point, 0.1, Color::Red);
			}
			else {
				VEC3 point = mypos + front_stone * vdx[i] + up_stone * vdy[i];
				drawLine(last_point, point, Color::LimeGreen);
				drawWiredSphere(last_point, 0.1, Color::Red);
				last_point = point;
			}
		}
		//drawWiredSphere(last_point, 0.1, Color::Red);
		drawLine(last_point, pos_ruido, Color::LimeGreen);
		drawWiredSphere(pos_ruido, 0.2, Color::Green);
	}

	//DO NOT TOUCH 
	//DISABLES IMGUI WHEN SPECIFIED
	if (!CApplication::get().ImGui)return;
	/*CEntity* ent_box = push_box;
	std::string box_name = "null";
	physx::PxRigidActor* actor = nullptr;
	physx::PxBoxController* boxcontroller = nullptr;
	if (ent_box != nullptr) {
		box_name = ent_box->getName();

		TCompCollider* box_col = ent_box->get<TCompCollider>();
		actor = box_col->actor;
		boxcontroller = box_col->boxcontroller;
	}*/


	//if (!rightOrLeftFootInFloor) {
	//	TCompSkeleton* skel = get<TCompSkeleton>();
	//	VEC3 posLFoot = skel->getAbsTraslationBone(175);
	//	drawWiredSphere(posLFoot, 0.05, Color::Purple);
	//}else {
	//	TCompSkeleton* skel = get<TCompSkeleton>();
	//	VEC3 posRFoot = skel->getAbsTraslationBone(180);
	//	drawWiredSphere(posRFoot, 0.05, Color::Purple);
	//}



	if (ImGui::TreeNode("Teleport to Entity...")) {

		ImGui::DragFloat("Offset", &teleportOffset);
		//ImGui::SameLine();
		static bool flat = false;
		ImGui::Checkbox("Flat", &flat);

		static ImGuiTextFilter Filter;
		ImGui::SameLine();
		Filter.Draw("Filter");

		auto om = getObjectManager<CEntity>();
		ImGui::LabelText("Num Entities", "%d", om->size());

		om->forEach([&](CEntity* e) {
			CHandle h_e(e);
			if (!flat && h_e.getOwner().isValid())
				return;
			if (Filter.IsActive() && !Filter.PassFilter(e->getName()))
				return;

			TCompTransform* trans = e->get<TCompTransform>();
			if (!trans) return;
			ImGui::PushID(e);
			if (ImGui::Button(e->getName())) {
				VEC3 pos = trans->getPosition() + trans->getFront() * teleportOffset;
				std::stringstream coords;
				coords << pos.x << ',' << pos.y << ',' << pos.z;
				CEngine::get().getScripting().ThrowEvent(CModuleScripting::Event::TELEPORT_PLAYER, coords.str(), 0);
			}
			ImGui::PopID();

			});
		ImGui::TreePop();
	}


	if (ImGui::TreeNode("Teleport to save pos...")) {

		int i = 0;
		for (auto tp : teleportPositions) {
			ImGui::PushID(i);
			ImGui::Text("%s : [%.2f %.2f %.2f]", tp.name.c_str(), tp.pos.x, tp.pos.y, tp.pos.z);
			ImGui::SameLine();
			float y = tp.pos.y + 2;
			if (ImGui::SmallButton("TP")) {
				std::stringstream coords;
				coords << tp.pos.x << ',' << y << ',' << tp.pos.z;
				CEngine::get().getScripting().ThrowEvent(CModuleScripting::Event::TELEPORT_PLAYER, coords.str(), 0);
			}
			i++;
		}

		ImGui::TreePop();
	}

	std::string sArea;

	if (area == eventArea::NEUTRAL) sArea = "Neutral";
	if (area == eventArea::GUARDIANS) sArea = "Guardians";
	if (area == eventArea::TUTORIAL2) sArea = "Tutorial2";
	if (area == eventArea::TUTORIAL10) sArea = "Tutorial10";
	if (area == eventArea::TUTORIAL11) sArea = "Tutorial11";
	ImGui::LabelText("Area:", "%s", sArea.c_str());

	std::string box_sound = "null";
	if (move_box_sound) box_sound = move_box_sound->getName();
	ImGui::LabelText("Box Sound:", "%s", box_sound.c_str());
	bool changed = false;
	changed |= ImGui::Checkbox("Projected", (bool*)&cte_player.projected);
	ImGui::Checkbox("Final battle activated", (bool*)&cte_player.final_battle);
	cte_player.updateGPU();
	
	/*TCompTransform* h_trans = get<TCompTransform>();

	drawLine(h_trans->getPosition(), h_trans->getPosition() + (local_dir * 2), Color::Green);


	/*const char* estado = "";
	switch (velState) {
		case velocityStates::STOPPED :
			estado = "STOPPED";
			break;
		case velocityStates::SPEEDINGUP:
			estado = "SPEEDINGUP";
			break;
		case velocityStates::MAXVELOCITY:
			estado = "MAXVELOCITY";
			break;
		case velocityStates::SLOWINGDOWN:
			estado = "SLOWINGDOWN";
			break;
	}
	ImGui::LabelText("Velocity State", "%s", estado);

	CEntity* ent_interact_ptr = ent_interact;
	ImGui::LabelText("Interaction Entity", "%s\n", ent_interact_ptr == nullptr ? "null" : ent_interact_ptr->getName());
	string box_sound = "null";
	if (move_box_sound) box_sound = move_box_sound->getName();
	ImGui::LabelText("Box Sound:", "%s", box_sound);
	ImGui::LabelText("Timer velocidad", "%.2f", timer_player);
	//ImGui::LabelText("Tiempo empezar a caer", "%.2f", time_ini_colDown);
	//ImGui::Checkbox("Frenada inmediata", &frenadaInmediata);
	//ImGui::Checkbox("Velocidad fija en profundiad 2D", &vel2DFija);
	/*ImGui::LabelText("Timer Jump", "%.2f", timer_debug_jump);
	ImGui::LabelText("Time Init Acc", "%.2f", time_ini_acceleration);
	ImGui::LabelText("Time Init Dec", "%.2f", time_ini_deceleration);
	ImGui::LabelText("Diferencia Time Jump", "%.2f", timer_player - time_ini_jump);*/

	//ImGui::DragFloat("Step Distance", &stepDistance, 0.01, 0, 10);

	//////////////////////////////////////////////////////////
	// DISTANCIA ENTRE DOS PUNTOS

	/*static VEC3 point1 = VEC3::Zero;
	static VEC3 point2 = VEC3::Zero;

	ImGui::DragFloat3("Point1", &point1.x, 0.1, -1000, 1000);
	ImGui::DragFloat3("Point2", &point2.x, 0.1, -1000, 1000);

	float dist1 = VEC3::Distance(point1, point2);
	float dist2 = VEC3::DistanceSquared(point1, point2);

	ImGui::LabelText("Distance", "%f", dist1);
	ImGui::LabelText("Distance Squared", "%f", sqrt(dist2));

	drawWiredSphere(point1, 0.1, Color::Red);
	drawWiredSphere(point2, 0.1, Color::Red);*/

	//////////////////////////////////////////////////////////
	
	
	/*ImGui::LabelText("", "Formula Salto");
	ImGui::LabelText("a", "%.2f", j_a);
	ImGui::LabelText("x0","%.2f", j_x0);
	ImGui::LabelText("y0", "%.2f", j_y0);
	ImGui::DragInt("Grado formula", &j_n, 2, 2, 10);*/

	/*ImGui::DragFloat3("Velocidad", &velocity.x, 0.1, 0, 100);
	ImGui::DragFloat("Jump Strength", &jump_strength, 0.1f, 0.f, 100.f, "%.1f");
	ImGui::LabelText("Is falling down", isFallingDown() ? "true" : "false");
	ImGui::LabelText("Is Pushing Up", isPushingUp() ? "true" : "false");*/

	

	//ImGui::DragFloat("Velocidad hacia la Luz", &velocity_2D_light, 0.1f, 0.f, 100.f, "%.1f");
	//ImGui::DragFloat("Offset", &velocity_2D_offset, 0.1f, 0.f, 100.f, "%.1f");
	//ImGui::DragFloat("Global Acceleration", &global_acc, 0.1f, 0.f, 100.f, "%.1f");

	/*ImGui::Text("Recording Max Distance: %.2f", jumpRecord.max_distance);
	ImGui::Text("Recording Max Height: %.2f", jumpRecord.max_height);*/


	ImGui::Checkbox("Collision Up", &collisions.collision_up);
	ImGui::Checkbox("Collision Down", &collisions.collision_down);
	ImGui::DragFloat3("Velocity vector", &last_velocity.x);
	ImGui::DragFloat("Mod Velocity", &modVelocity);



	/*
	ImGui::DragFloat("Max Fall Damage", &max_fall_damage, 0.1f, 0.f, 100.f, "%.1f");
	ImGui::DragFloat("Damage", &fall_damage, 0.1f, 0.f, 100.f, "%.1f");
	*/

	/*
	ImGui::LabelText("Box Name", box_name.c_str());
	ImGui::LabelText("Box Actor", actor ? "true" : "false");
	ImGui::LabelText("Box Controller", boxcontroller ? "true" : "false");
	ImGui::LabelText("In front of Column", in_front_column ? "true" : "false");
	ImGui::DragFloat("Jump Strength", &jump_strength, 0.1f, 0.f, 50.f, "%.1f");
	ImGui::DragInt("Modo Camera", &modoCamera);
	ImGui::LabelText("Camera Name", cameraData.name.c_str());
	ImGui::DragFloat3("Wall Point", &wall_point.x, 0.1f, -1.f, 50.f, "%.2f");
	ImGui::DragFloat3("Wall Normal", &wall_normal.x, 0.1f, -1.f, 50.f, "%.2f");
	*/

	//PARA VER EN QUE DIRECCION VA SEELE

	//TCompTransform* player_trans = get<TCompTransform>();
	//VEC3 destiny = (player_trans->getPosition()+player_trans->getFront()*4)*VEC3(1,0,1)+VEC3(0, player_trans->getPosition().y + 0.5,0);	
	//drawLine(VEC3(player_trans->getPosition().x, player_trans->getPosition().y+0.5, player_trans->getPosition().z), destiny, Color::Red);

	if (drawJump && debug_pos_jump.size() > 1) {
		for (int i = 0; i < debug_pos_jump.size()-1; i++) {
			//drawLine(debug_pos_jump[i], debug_pos_jump[i + 1], Color::Red);
			drawWiredSphere(debug_pos_jump[i], 0.1, Color::Green);
		}
	}

	/*if (startRecord && list_steps.size() > 0) {
		for (auto step : list_steps) {
			drawWiredSphere(step.pos_player, 0.05, Color::Green);
		}
	}*/

	/*if (light) {
		TCompAbsAABB* c_aabb = get<TCompAbsAABB>();
		VEC3 center = c_aabb->Center;
		TCompTransform* c_trans = get<TCompTransform>();
		VEC3 src = light->getPosition();
		VEC3 unitDir = /*c_trans->getPosition() + center - src;
		unitDir.Normalize();
		drawLine(src, src + unitDir * 200, Color::Red);
	}*/

	//ImGui::LabelText("State:", db_state.c_str());
	//ImGui::Checkbox("Collision Down", &collision_down);
	//ImGui::DragFloat3("Velocity", &current_velocity.x, 0.1f, -1.f, 50.f, "%.2f");
	//ImGui::DragFloat("Velocity Length", &dbg_velocity_lenght, 0.1f, -1.f, 50.f, "%.2f");
	//ImGui::DragFloat("Max Velocity", &max_velocity, 0.1f, -1.f, 50.f, "%.2f");
	//TCompTransform* player_trans = get<TCompTransform>();
	//VEC3 pos = player_trans->getPosition() + VEC3::Up * 0.5f;
	//drawLine(pos, pos + current_velocity, Color::Green);
	//drawLine(pos, pos - dbg_resistance, Color::Red);

	if (showWindow) {

	// VENTANA CON ESTADO DEL PERSONAJE Y GRAFICO DE RUIDO
		//ImGui::Begin("SINN", &windowStatus_active);
		ImGui::Begin("SINN", &windowStatus_active, ImGuiWindowFlags_NoBackground
			| ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse
			| ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove);
		
		/*freq = M_PI / cycle;
		float x = freq * timeGame.elapsed();
		float displayNoise = noise * sin(x) * cos(10 * x);

		qGenerated_noise.push_back(displayNoise);
		if (qGenerated_noise.size() > 100) {
			qGenerated_noise.pop_front();
		}
		int i = 0;
		for (auto it : qGenerated_noise) {
			noise_values[i] = it;
			noise_values[i + 1] = it * -1;
			i += 2;
		}*/
		//ImGui::PlotLines("", noise_values, IM_ARRAYSIZE(noise_values), 0, "", -1.5f, 1.5f, ImVec2(250, 40));
		//ImGui::Text("Velocidad actual %.2f", velocity.Length());
		//ImGui::DragFloat("Max value", &max_noise_value, 0.1, 0, 10, "%.2f");
		//ImGui::DragFloat("Variation value", &variation_value, 0.1, 0, 10, "%.2f");
		//ImGui::DragFloat("Change Point", &change_point, 0.1, 0, 10, "%.2f");
		//ImGui::Text("Distancia x Sec %.2f", distxSec);
		ImGui::SetWindowFontScale(3.f);
		ImGui::Text("%d intentos", attemptsToFree);



		ImGui::End();

	}



	//Ability to change camera sensitivity
	/*if (CApplication::get().getInPause() && modoCamera==CAM3D) {
		bool* p_open = new bool(true);
		ImGui::Begin("Controls", p_open, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove);
		ImGui::SetWindowSize(ImVec2(250, 50));
		ImVec2 size = ImGui::GetWindowSize();
		int resx = CApplication::get().getWidth();
		int resy = CApplication::get().getHeight();
		resx -= static_cast<int>(size.x);
		resy -= static_cast<int>(size.y);
		ImGui::SetWindowPos(ImVec2(static_cast<float>(resx >> 1), static_cast<float>(resy >> 1)));
		CEntity* cCam3d = h_camera;
		TCompCamera3D* cam3d = cCam3d->get<TCompCamera3D>();
		ImGui::DragFloat("Cam Sensivity", &cam3d->_sensitivity, 0.01f, 0.f, 10.0f);
		ImGui::End();
	}*/

	/*
	if (getInFrontEntity()) {
		CEntity* c_ent = ent_interact;
		string name = c_ent->getName();
		if (c_ent->get<TCompCuerda>().isValid()) {
			bool* p_open = new bool(true);
			ImGui::Begin("Rope", p_open, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove);
			ImGui::SetWindowSize(ImVec2(250, 50));
			ImVec2 size = ImGui::GetWindowSize();
			int resx = CApplication::get().getWidth();
			int resy = CApplication::get().getHeight();
			resx -= static_cast<int>(size.x);
			resy -= static_cast<int>(size.y);
			ImGui::SetWindowPos(ImVec2(static_cast<float>(resx >> 1), static_cast<float>(resy >> 1)));
			ImGui::Text("Press E to cut the rope");
			ImGui::End();
		}
		else if (c_ent->get<TCompLight>().isValid()) {
			if (getMateria()) {
				bool* p_open = new bool(true);
				ImGui::Begin("Spot", p_open, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove);
				ImGui::SetWindowSize(ImVec2(250, 50));
				ImVec2 size = ImGui::GetWindowSize();
				int resx = CApplication::get().getWidth();
				int resy = CApplication::get().getHeight();
				resx -= static_cast<int>(size.x);
				resy -= static_cast<int>(size.y);
				ImGui::SetWindowPos(ImVec2(static_cast<float>(resx >> 1), static_cast<float>(resy >> 1)));
				ImGui::Text("Press E to turn on the light");
				ImGui::End();
			}
			else {
				bool* p_open = new bool(true);
				ImGui::Begin("Spot", p_open, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove);
				ImGui::SetWindowSize(ImVec2(270, 50));
				ImVec2 size = ImGui::GetWindowSize();
				int resx = CApplication::get().getWidth();
				int resy = CApplication::get().getHeight();
				resx -= static_cast<int>(size.x);
				resy -= static_cast<int>(size.y);
				ImGui::SetWindowPos(ImVec2(static_cast<float>(resx >> 1), static_cast<float>(resy >> 1)));
				ImGui::Text("Without the matter it won't turn on");
				ImGui::End();
			}
		}
		else if (name.compare("materia")==0) {
			bool* p_open = new bool(true);
			ImGui::Begin("Materia", p_open, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove);
			ImGui::SetWindowSize(ImVec2(250, 50));
			ImVec2 size = ImGui::GetWindowSize();
			int resx = CApplication::get().getWidth();
			int resy = CApplication::get().getHeight();
			resx -= static_cast<int>(size.x);
			resy -= static_cast<int>(size.y);
			ImGui::SetWindowPos(ImVec2(static_cast<float>(resx >> 1), static_cast<float>(resy >> 1)));
			ImGui::Text("Press E to collect the matter");
			ImGui::End();

		}else{
			bool* p_open = new bool(true);
			ImGui::Begin("Column", p_open, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove);
			ImGui::SetWindowSize(ImVec2(250, 50));
			ImVec2 size = ImGui::GetWindowSize();
			int resx = CApplication::get().getWidth();
			int resy = CApplication::get().getHeight();
			resx -= static_cast<int>(size.x);
			resy -= static_cast<int>(size.y);
			ImGui::SetWindowPos(ImVec2(static_cast<float>(resx >> 1), static_cast<float>(resy >> 1)));
			ImGui::Text("Press E to push Column");
			ImGui::End();
		}
	}
	*/
}

void TCompPlayer::update(float dt){

	updateTimers(dt);

	////////////////////////////////////////////////////////////////////
	//				LUZ AMBIENTAL
	////////////////////////////////////////////////////////////////////

	if (firstTime && fadeTimer >= 3) {
		CEngine::get().getRender().returnLight(2, 1);
		firstTime = false;
		fadeTimer = 0;
	}

	////////////////////////////////////////////////////////////////////
	//				MOVIMIENTO
	////////////////////////////////////////////////////////////////////

	checkBlockLocalDir();
	calculeLastFootInFloor();
	movingPlayer(dt);

	////////////////////////////////////////////////////////////////////
	//				CAMARAS
	////////////////////////////////////////////////////////////////////


	if (init_cameras) {
		//CHandle hOutputCamera = getEntityByName("cameraWalk3D");
		CHandle hOutputCamera = getEntityByName("mixed_camera");
		CHandle hDefaultCamera = getEntityByName("cameraWalk3D");

		CEngine::get().getCameraMixer().setOutputCamera(hOutputCamera);
		CEngine::get().getCameraMixer().setDefaultCamera(hDefaultCamera);
		init_cameras = false;
	}
	correctCam();
	
	////////////////////////////////////////////////////////////////////
	//				SONIDOS
	////////////////////////////////////////////////////////////////////


	playSteps();

	if (!move_box_sound) {
		CEngine::get().getSound().forceStopEvent("moveObject");
	}
	////////////////////////////////////////////////////////////////////
	//				LLAMAS
	////////////////////////////////////////////////////////////////////

	if (!isProjected()) {
		cte_player.projected = false;
		cte_player.updateGPU();
	}

	////////////////////////////////////////////////////////////////////
	//				EVENTO FINAL
	////////////////////////////////////////////////////////////////////
	if (final_event_active) {
		updateFinalEvent();
	}

	////////////////////////////////////////////////////////////////////
	//				DEBUG
	////////////////////////////////////////////////////////////////////
	if (fakeJump) {
		updateFakeJump(dt);
	}
	/*dbg("===============================================================\n");
	dbg("Player: Mod Velocity = %.2f\n", modVelocity);
	dbg("Player: Velocity Vector = %.2f %.2f %.2f\n", last_velocity.x, last_velocity.y, last_velocity.z );
	dbg("===============================================================\n");*/
	
	
 }

void TCompPlayer::onEntityCreated()
{
	TCompTransform* trans = get<TCompTransform>();
	VEC3 pos = trans->getPosition();
	pos.y += 2;
	MAT44 mat = MAT44::CreateScale(trans->getScale())
		* MAT44::CreateFromQuaternion(trans->getRotation())
		* MAT44::CreateTranslation(pos);
	
	startPos = mat;
	playerStepPos = pos;
}

void TCompPlayer::correctCam()
{
	//Esta funcion la uso para en la entrada de zonas con camaras con railes
	//que la camara se posicione de tal forma que cuando salgamos el movimiento
	// y el blendeo no haga cosas extranyas

	if (!needCorrectCam) return;

	if (timer_player - init_time_to_correct_cam > time_to_correct_cam) {

		CEntity* ent_camera = (CEntity*)getEntityByName("camera_direction");
		TCompDirection* comp_direc = ent_camera->get<TCompDirection>();
		float p = corr_pR;
		float y = corr_y;

		comp_direc->setPitchYaw(p, y);
		getObjectManager<TCompCamera3D>()->forEach([p, y](TCompCamera3D* c) {
			c->setPitchYaw(p, y);
			});

		needCorrectCam = false;
	}

}

void TCompPlayer::setCorrectCamValues(float time, float y, float p)
{
	init_time_to_correct_cam = timer_player;
	time_to_correct_cam = time + 0.5;
	corr_y = y;
	corr_pR = p;
	needCorrectCam = true;
	
}

void TCompPlayer::blockLocalDir(float time)
{
	init_time_block_localDir = timer_player;
	time_block_localDir = time;
	block_localDir = true;
	canChangeCamera = false;
}

void TCompPlayer::checkBlockLocalDir()
{
	if (!block_localDir)	return;

	if (timer_player - init_time_block_localDir > time_block_localDir) {
		block_localDir = false;
		canChangeCamera = true;
	}
}

void TCompPlayer::calculeLastFootInFloor()
{
	if (collisions.collision_down) {
		TCompTransform* t_player = get<TCompTransform>();
		TCompSkeleton* skel = get<TCompSkeleton>();

		//Si es true significa que fue el pie derecho, si es false el izquierdo
		if (rightOrLeftFootInFloor) {
			//Como el ultimo fue el derecho, comprobamos ahora si el izquierdo toca el suelo
			if (skel->hasRightOrLeftFootInFloor(rightOrLeftFootInFloor, t_player->getPosition().y, distStepToFloor)) {
				rightOrLeftFootInFloor = false;
			}
		}
		else {
			if (skel->hasRightOrLeftFootInFloor(rightOrLeftFootInFloor, t_player->getPosition().y, distStepToFloor)) {
				rightOrLeftFootInFloor = true;
			}
		}
	}
}

void TCompPlayer::precalculateFuncJump()
{
	float t = time_jump * jump_strength;
	if(vertical_jump) j_y0 = max_y_vert_jump * jump_strength;
	else j_y0 = max_y_hor_jump * jump_strength;
	j_a = pow(2, j_n) * (j_y0 / pow(t, j_n));
	float auxRaiz = j_y0 / j_a;
	j_x0 = pow(auxRaiz, 1.0 / j_n);
}

float TCompPlayer::calculateJump(){

	// Anterior funcion de salto
	/*float new_maxY = max_y_jump * jump_strength;
	float tmax = time_jump/2.0;
	float Vy = 2 * (new_maxY / tmax);
	float grav = Vy / tmax;
	float tN = timer_player - time_ini_jump;
	float newY = Vy * tN - 0.5 * grav * tN * tN;*/

	if(drawJump) insertPosJump();

	float x = timer_jump;
	float auxPot = x - j_x0;
	float newY = -j_a * pow(auxPot, j_n) + j_y0;
	newY += y_ini_value;

	//dbg("La nueva y es : %.4f\n", newY);

	return newY;
}

void TCompPlayer::initializeJump()
{
	if (!collisions.jumping) {
		timer_jump = 0;
		collisions.jumping = true;
		collisions.collision_down = false;

		TCompTransform* player_trans = get<TCompTransform>();
		VEC3 pos = player_trans->getPosition();
		y_ini_value = pos.y;
		not_collision_up = false;

		inJumpStart = false;
	}
}

void TCompPlayer::finalizeJump()
{
	if (collisions.jumping) {
		collisions.jumping = false;
		collisions.collision_down = true;
	}
}

void TCompPlayer::changeTimeToJumpDesc()
{

	float time_pushingUp = time_jump / 2.f;

	float add = time_jump / 4.f;

	if (timer_jump < time_pushingUp) {
		timer_jump = time_pushingUp + addValueCollisionUp;
	}

}

void TCompPlayer::initJumpStart()
{
	inJumpStart = true;
	timer_jumpStart = 0;
}

bool TCompPlayer::isPushingUp()
{
	float time_pushingUp = time_jump / 2.f;

	return timer_jump < time_pushingUp;
}

bool TCompPlayer::isCollisionUp()
{
	return collisions.collision_up;
}

VEC3 TCompPlayer::rotateWithValues(float dYaw, float dPitch, float dRoll, VEC3 Pos)
{

	TCompTransform* player_trans = get<TCompTransform>();
	QUAT Rot = QUAT::CreateFromYawPitchRoll(dYaw, dPitch, dRoll);
	VEC3 Rel_Pos = player_trans->getPosition() - Pos;
	MAT44 Rot_Matrix = MAT44::CreateFromQuaternion(Rot);
	player_trans->setRotation(Rot * player_trans->getRotation());
	VEC3 Move = player_trans->getPosition();
	MAT44 Player_Mat = MAT44::CreateTranslation(Rel_Pos);
	MAT44 new_trans = Player_Mat * Rot_Matrix;
	return  VEC3(new_trans._41, new_trans._42, new_trans._43) - Rel_Pos;


}

void TCompPlayer::rotate(float dt) {

	TCompTransform* player_trans = get<TCompTransform>();
	float player_yaw = 0.f, player_pitch = 0.f;
	player_trans->getEulerAngles(&player_yaw, &player_pitch, nullptr);
	float deltaRot = 0.f, aim = 0.f;

	aim = player_trans->getDeltaYawToAimTo(player_trans->getPosition() + local_dir);
	//float near2zero = 0.5 * rotation_speed * dt;
	//if (aim != near2zero) deltaRot = aim * rotation_speed * dt;
	deltaRot = (aim) * rotation_speed * dt;
	player_yaw += deltaRot;
	player_trans->setEulerAngles(player_yaw, player_pitch, 0.f);
}

void TCompPlayer::rotateInTime(float dt) {

	if (time.elapsed() > rot_time) return;

	TCompTransform* c_trans = get<TCompTransform>();
	float yaw, pitch;
	c_trans->getEulerAngles(&yaw, &pitch, nullptr);
	float v = rot_angle * rot_time / fk * (1 / rot_time - 1 / (k * time.elapsed() + rot_time));
	yaw += v * dt;
	c_trans->setEulerAngles(yaw, 0.f, 0.f);
}

void TCompPlayer::initRotateInTime(VEC3 dst, float max_time) {
	TCompTransform* c_trans = get<TCompTransform>();
	time.reset();
	rot_angle = c_trans->getDeltaYawToAimTo(c_trans->getPosition() + dst);
	rot_time = max_time;
}

void TCompPlayer::forceRotation(VEC3 localDir) {
	if (localDir == VEC3::Zero) return;

	TCompTransform* player_trans = get<TCompTransform>();
	float player_yaw = 0.f, player_pitch = 0.f;
	player_trans->getEulerAngles(&player_yaw, &player_pitch, nullptr);

	//dbg("Forzando rotacion hacia %.4f %.4f %.4f\n", localDir.x, localDir.y, localDir.z);
	float aim = player_trans->getDeltaYawToAimTo(player_trans->getPosition() + localDir);
	//dbg("Aim %.4f\n", aim);
	player_yaw += aim;
	//dbg("Player yaw %.4f, Player pitch %.4f\n", player_yaw, player_pitch);
	player_trans->setEulerAngles(player_yaw, player_pitch, 0.f);
}

void TCompPlayer::addAnimRot()
{
	TCompSkeleton* skel = get<TCompSkeleton>();
	TCompTransform* player_trans = get<TCompTransform>();
	float animDeltaYaw = skel->getDeltaYaw();

	float player_yaw = 0.f, player_pitch = 0.f;
	player_trans->getEulerAngles(&player_yaw, &player_pitch, nullptr);

	player_yaw += animDeltaYaw;
	player_trans->setEulerAngles(player_yaw, player_pitch, 0.f);

	//dbg("Voy anyadirle %.4f de rotacion\n", animDeltaYaw);

}

void TCompPlayer::move2trans(float dt)
{
	TCompCollider* collider = get<TCompCollider>();
	TCompTransform* trans = get<TCompTransform>();
	
	//dbg("Posicion antes de mover [%.2f %.2f %.2f]\n", collider->controller->getPosition().x, collider->controller->getPosition().y, collider->controller->getPosition().z);
	VEC3 deltaMove = trans->getPosition() - PXVEC3_TO_VEC3(collider->controller->getPosition());
	//dbg("DeltaMove [%.2f %.2f %.2f]\n", deltaMove.x, deltaMove.y, deltaMove.z);
	CEngine::get().getPhysics().move(collider->controller, deltaMove, dt);
	//dbg("Posicion antes de mover [%.2f %.2f %.2f]\n", collider->controller->getPosition().x, collider->controller->getPosition().y, collider->controller->getPosition().z);
}

void TCompPlayer::movingPlayer(float dt)
{
	if (haveMove3DObj) {
		move(dt);
		movePushBox(dt);
		haveMove3DObj = false;
	}
	else if (haveMove2DObj) {
		moveObj2D(dt);
		//haveMove2DObj = false;
	}
	else if (not_move) {
		return;
	}
	else {
		move(dt);
		rotate(dt);
	}

	velocity = VEC3::Zero;
	local_dir = VEC3::Zero;
	if (isOnTopPlatform) {
		isOnTopPlatform = false;
		VEC3 Rot_Var = rotateWithValues(lastM.dYaw, lastM.dPitch, lastM.dRoll, lastM.Position);
		velocity = lastM.deltaMove + Rot_Var/dt;
		collisions.OnPlatform = true;
		move(dt);
		collisions.OnPlatform = false;
	}
}

void TCompPlayer::move(float dt) {

	TCompCollider* comp_collider = get<TCompCollider>();
	TCompTransform* comp_trans = get<TCompTransform>();

	bool old_colDown = collisions.collision_down;
	CEngine::get().getPhysics().move(velocity, dt, comp_collider->controller, collisions);
	//dbg("Move: %.2f %.2f %.2f\n", velocity.x, velocity.y, velocity.z);


	if (!collisions.collision_down && old_colDown != collisions.collision_down) {
		setIniTimeColDown();
	}

	if (collisions.collision_up) {
		//Si not_collision_up es true significa que el collider contra el que hemos chocado necesita que sigamos saltando aunque haya collision up
		//asi que ponemos el collision up a falso y este bool tambien para la siguiente vez que se detecte collision up
		if (not_collision_up) {
			collisions.collision_up = false;
			not_collision_up = false;
		}
	}
}

void TCompPlayer::moveObj2D(float dt) {
	TCompCollider* comp_collider = get<TCompCollider>();
	TCompTransform* comp_trans = get<TCompTransform>();

	bool old_colDown = collisions.collision_down;
	VEC3 pos = comp_trans->getPosition();

	//Calculamos la diferencia de la distancia a la luz entre el player y la caja
	CEntity* ent_light = getLight();
	TCompTransform* Origin = ent_light->get<TCompTransform>();
	CEntity* Box = push_box;
	TCompTransform* box_trans = Box->get<TCompTransform>();
	TCompCollider* box_Collider = Box->get<TCompCollider>();
	if (box_Collider->boxcontroller == nullptr) return;
	float dif = abs(VEC3::Distance(pos, oldPos));
	VEC3 box_pos = box_trans->getPosition();
	if (oldPos.Length() != 0) {
		
		//Movemos al player o la caja en funcion de lo que paso en el frame anterior
		if (movePlayer) {
			//Usamos la diferencia de posiciones(lo que se ha movido el player) para determinar cuanto se movera la caja
			VEC3 Velocity_Box = (pos - oldPos) / dt / diff;

			CEngine::get().getPhysics().move(Velocity_Box, dt, box_Collider->boxcontroller, box_collisions);

		}
		else {
			//Usamos la diferencia de posiciones(lo que se ha movido la caja) para determinar cuanto se movera al player
			VEC3 newVelocity = (box_trans->getPosition() - oldPos) / dt * diff;

			newVelocity = newVelocity.Dot(comp_trans->getFront()) * comp_trans->getFront();
			CEngine::get().getPhysics().move(newVelocity, dt, comp_collider->controller, collisions);

		}
	}
	if (velocity.x != 0 && velocity.z != 0/* && dif>0.1*/) {
		TCompAudio* audio = get<TCompAudio>();
		if (!move_box_sound)
		{
			move_box_sound = audio->playEvent("moveObject");
		}
	}
	else {
		stopBoxSound();
	}
	//Empujamos la caja
	if (velocity.Dot(comp_trans->getFront()) > 0) {
		//Movemos primero la caja
		VEC3 box_Pos = box_trans->getPosition();
		VEC3 Velocity_Box = (velocity)/diff;
		
		CEngine::get().getPhysics().move(Velocity_Box, dt, box_Collider->boxcontroller, box_collisions);

		//En funcion de cuanto se ha movido la caja, movemos al player
		oldPos = box_Pos;
		movePlayer = false;
	}
	//Estiramos la caja
	else if(velocity.Length() != 0){
		//Movemos primero al player
		CEngine::get().getPhysics().move(velocity, dt, comp_collider->controller, collisions);
		VEC3 newPos = PXVEC3_TO_VEC3(comp_collider->controller->getActor()->getGlobalPose().p);
		//Guardamos valores para mover la caja en el proximo frame
		oldPos = pos;
		movePlayer = true;
	}
	else {
		oldPos = VEC3::Zero;
	}



	if (!collisions.collision_down && old_colDown != collisions.collision_down) {
		setIniTimeColDown();
	}

	if (collisions.collision_damage > max_fall_damage) {
		fall_damage = collisions.collision_damage;
		dead = true;
		deadAnimation = true;
		reset = false;
	}
}

void TCompPlayer::setNoise(float v, float mod) {
	// slpoe -> inclinacion = 2
	// slope_point = 4
	noise = (max_noise_value / 2 + 2 * atan(variation_value * (v - change_point)) / PI) * mod;

	CHandle boss = getEntityByName("Boss");	 
	TCompTransform* my_trans = get<TCompTransform>();

	//dbg("Mando %.2f de ruido\n", noise);

	TMsgSuspiciusEvent msg;
	msg.intensity = noise;
	msg.pos = my_trans->getPosition();
	msg.type = static_cast<int> (suspicius_noise_type::PLAYER_NOISE);
	boss.sendMsg(msg);
}


void TCompPlayer::changeCamera(CameraData fromCamera, CameraData toCamera) {
	// Caso base
	CameraData gotoCamera = toCamera;

	if (!canChangeCamera) {
		h_camera = getEntityByName(gotoCamera.name);
		return;
	}
	
	if (flyover_active) {
		h_camera = getEntityByName(gotoCamera.name);
		return;
	}

	//CHandle h = getEntityByName(gotoCamera.name);
	mixer.blendCamera(gotoCamera.name, gotoCamera.time, &interpolators::linearInterpolator);
	h_camera = getEntityByName(gotoCamera.name);
	
}

void TCompPlayer::putFlyOverCamera()
{
	flyover_active = !flyover_active;

	if (flyover_active) {
		mixer.blendCamera("cameraFlyover", 0, &interpolators::linearInterpolator);
		//h_camera = getEntityByName("cameraFlyover");
	}
	else {
		mixer.blendCamera(cameraData.name, 0, &interpolators::linearInterpolator);
	}

}

void TCompPlayer::switchFocusToShadow()
{
	if (canChangeCamera) {
		focus2shadow = !focus2shadow;
	}
}

float TCompPlayer::getPitchCamera()
{
	CEntity* cam = (h_camera);
	if (modoCamera == CAM2D) {
		TCompCamera2D* c2d = cam->get<TCompCamera2D>();
		if (c2d == nullptr)	return 0;
		return c2d->getPitch();
	}
	else if(modoCamera == CAM3D) {
		TCompCamera3D* c3d = cam->get<TCompCamera3D>();
		if (c3d == nullptr)	return 0;
		return c3d->getPitch();
	}
	return 0;
}

float TCompPlayer::getYawCamera()
{
	CEntity* cam = (h_camera);

	if (modoCamera == CAM2D) {
		TCompCamera2D* c2d = cam->get<TCompCamera2D>();
		if (c2d == nullptr)	return 0;
		return c2d->getYaw();
	}
	else if (modoCamera == CAM3D) {
		TCompCamera3D* c3d = cam->get<TCompCamera3D>();
		if (c3d == nullptr)	return 0;
		return c3d->getYaw();
	}
	return 0;
}

void TCompPlayer::changeOtherCam(bool b, std::string nameCam, float time)
{
	canChangeCamera = b;

	//Si es una camara de tipo rail, como se va a cambiar a otra, la desactivamos
	if (other_cam.isValid()) {
		CEntity* cen = other_cam;
		if (cen->get<TCompCameraRail>().isValid()) {
			TCompCameraRail* c_cRail = cen->get<TCompCameraRail>();
			c_cRail->setActive(false);
		}
	}

	other_cam = getEntityByName(nameCam);

	//Si esta camara es de tipo rail, como la vamos a usar, la activamos
	if (other_cam.isValid()) {
		CEntity* cen = other_cam;
		if (cen->get<TCompCameraRail>().isValid()) {
			TCompCameraRail* c_cRail = cen->get<TCompCameraRail>();
			c_cRail->setActive(true);
		}
	}

	mixer.blendCamera(nameCam, time, &interpolators::linearInterpolator);

}

void TCompPlayer::addPointingAngle(VEC2 delta) {
	pointingAngle += delta;
	pointingAngle.x = std::clamp(pointingAngle.x, -pointingLimits.x, pointingLimits.x);
	pointingAngle.y = std::clamp(pointingAngle.y, -pointingLimits.y, pointingLimits.y);
}

bool TCompPlayer::setProjectedWall(VEC3 Velocity,float dt) {
	TCompAbsAABB* c_aabb = get<TCompAbsAABB>();
	VEC3 center = c_aabb->Center + (Velocity* dt);
	//TCompTransform* c_trans = get<TCompTransform>();
	CEntity* ent_light = getLight();
	TCompLight* p_light = ent_light->get<TCompLight>();
	VEC3 src = p_light->getPosition();
	VEC3 unitDir = /*c_trans->getPosition() +*/ center - src;
	unitDir.Normalize();
	
	// RAYCAST
	PxRaycastBuffer hitCall;
	PxQueryFilterData fd = PxQueryFilterData();
	// Check if there is nothing between the player and the projectable wall
	// The player is not included
	fd.data.word0 = CModulePhysicsSinn::FilterGroup::Projectable_Wall;
	bool status = CEngine::get().getPhysics().gScene->raycast(
		VEC3_TO_PXVEC3(src),
		VEC3_TO_PXVEC3(unitDir),
		200.f, hitCall, PxHitFlags(PxHitFlag::eDEFAULT), fd);
	
	if (!status) return false;
	if (!hitCall.hasAnyHits()) return false;
	PxVec3 normal = hitCall.block.normal;
	const PxRaycastHit& overlap_hit_wall = hitCall.getAnyHit(0);
	CHandle h_collider;
	h_collider.fromVoidPtr(overlap_hit_wall.actor->userData);
	TCompCollider* c_collider = h_collider;
	if (c_collider == nullptr) return false;
	
	bool IsWall = overlap_hit_wall.shape->getQueryFilterData().word0 & CModulePhysicsSinn::FilterGroup::Projectable_Wall;
	if (IsWall) {
		wall_normal = PXVEC3_TO_VEC3(hitCall.block.normal);
		wall_point = PXVEC3_TO_VEC3(hitCall.block.position);

		// SUCCESS
		return true;
	}
	return false;

	//DEBUG
	drawLine(src, src + unitDir * 200, Color::Red);
}

void TCompPlayer::setLastState(std::string s)
{
	if (strcmp("Idle", s.c_str()) == 0 || strcmp("Idle2D", s.c_str()) == 0) {
		state_before_proj = fsmStates::IDLE;
	}
	else if (strcmp("Walk", s.c_str()) == 0 || strcmp("Walk2D", s.c_str()) == 0) {
		state_before_proj = fsmStates::WALK;
	}
	else if (strcmp("Sprint", s.c_str()) == 0 || strcmp("Sprint2D", s.c_str()) == 0) {
		state_before_proj = fsmStates::SPRINT;
	}
	else if (strcmp("Stealth", s.c_str()) == 0) {
		state_before_proj = fsmStates::STEALTH;
	}
	else if (strcmp("LongJumpStart", s.c_str()) == 0 || strcmp("LongJumpStart2D", s.c_str()) == 0) {
		state_before_proj = fsmStates::LONGJUMPSTART;
	}
	else if (strcmp("LongJumpUp", s.c_str()) == 0 || strcmp("LongJumpUp2D", s.c_str()) == 0) {
		state_before_proj = fsmStates::LONGJUMPUP;
	}
	else if (strcmp("LongJumpDesc", s.c_str()) == 0 || strcmp("LongJumpDesc2D", s.c_str()) == 0) {
		state_before_proj = fsmStates::LONGJUMPDESC;
	}
	else if (strcmp("LongJumpEnd", s.c_str()) == 0 || strcmp("LongJumpEnd2D", s.c_str()) == 0) {
		state_before_proj = fsmStates::LONGJUMPDESC;
	}
	else if (strcmp("ShortJumpStart", s.c_str()) == 0 || strcmp("ShortJumpStart2D", s.c_str()) == 0) {
		state_before_proj = fsmStates::SHORTJUMPSTART;
	}
	else if (strcmp("ShortJumpUp", s.c_str()) == 0 || strcmp("ShortJumpUp2D", s.c_str()) == 0) {
		state_before_proj = fsmStates::SHORTJUMPUP;
	}
	else if (strcmp("ShortJumpDesc", s.c_str()) == 0 || strcmp("ShortJumpDesc2D", s.c_str()) == 0) {
		state_before_proj = fsmStates::SHORTJUMPDESC;
	}
	else if (strcmp("ShortJumpEnd", s.c_str()) == 0 || strcmp("ShortJumpEnd2D", s.c_str()) == 0) {
		state_before_proj = fsmStates::SHORTJUMPEND;
	}
	else if (strcmp("Falling", s.c_str()) == 0 || strcmp("Falling2D", s.c_str()) == 0) {
		state_before_proj = fsmStates::FALLING;
	}
	else if (strcmp("Dead", s.c_str()) == 0 || strcmp("FalliFallingDeadng2D", s.c_str()) == 0) {
		state_before_proj = fsmStates::DEATH;
	}
}

VEC3 TCompPlayer::getLight2Player() {
	assert(light != CHandle());
	TCompTransform* light_trans = getLight()->get<TCompTransform>();
	TCompTransform* player_trans = get<TCompTransform>();
	return player_trans->getPosition() - light_trans->getPosition();
}

std::string TCompPlayer::getStringLastState()
{

	bool inShadow = isProjected();

	switch (state_before_proj) {
	case fsmStates::IDLE:
		if (inShadow)  return "Idle2D";
		else return "Idle";
		break;
	case fsmStates::WALK:
		if (inShadow)  return "Walk2D";
		else return "Walk";
		break;
	case fsmStates::STEALTH:
		if (inShadow)  return "Idle2D";
		else return "Stealth";
		break;
	case fsmStates::SPRINT:
		if (inShadow)  return "Sprint2D";
		else return "Sprint";
		break;
	case fsmStates::LONGJUMPSTART:
		if (inShadow)  return "LongJumpStart2D";
		else return "LongJumpStart";
		break;
	case fsmStates::LONGJUMPUP:
		if (inShadow)  return "LongJumpUp2D";
		else return "LongJumpUp";
		break;
	case fsmStates::LONGJUMPDESC:
		if (inShadow)  return "LongJumpDesc2D";
		else return "LongJumpDesc";
		break;
	case fsmStates::LONGJUMPEND:
		if (inShadow)  return "LongJumpEnd2D";
		else return "LongJumpEnd";
		break;
	case fsmStates::SHORTJUMPSTART:
		if (inShadow)  return "ShortJumpStart2D";
		else return "ShortJumpStart";
		break;
	case fsmStates::SHORTJUMPUP:
		if (inShadow)  return "ShortJumpUp2D";
		else return "ShortJumpUp";
		break;
	case fsmStates::SHORTJUMPDESC:
		if (inShadow)  return "ShortJumpDesc2D";
		else return "ShortJumpDesc";
		break;
	case fsmStates::SHORTJUMPEND:
		if (inShadow)  return "ShortJumpEnd2D";
		else return "ShortJumpEnd";
		break;
	case fsmStates::FALLING:
		if (inShadow)  return "Falling2D";
		else return "Falling";
		break;
	case fsmStates::DEATH:
		return "dead";
		break;

	}
}

bool TCompPlayer::project() {

	if(timer_ResetProject >= timeResetProject)	focus2shadow = true;

	std::vector<TCompLight*> lights;
	TCompLight* p_light;
	CHandle h_player = CHandle(this).getOwner();
	getObjectManager<TCompLight>()->forEach([&lights, h_player](TCompLight* light) {
		if (light->lightOn && light->canProjectPlayer(h_player)) lights.push_back(light);
		});
	
	//assert(lights.size() < 2);
	// If we've found just 1 light, we can project the player in 2D
	if (lights.size() == 1) {
		p_light = lights[0];
		p_light->projectPlayer();
	}
	// If we've found more than one light, we have to choose one of them
	else if (lights.size() > 1) {
		// Comprobar ultima luz
		auto p = std::find(lights.begin(), lights.end(), p_light);
		if (p != lights.end()) {
			p_light = *p;
		}
		// Comprobar distancia
		else {
			p_light = lights[0];
			TCompTransform* player_trans = get<TCompTransform>();
			float dist2 = VEC3::DistanceSquared(p_light->getPosition(), player_trans->getPosition());
			for (auto comp_light : lights) {
				float it_dist2 = VEC3::DistanceSquared(comp_light->getPosition(), player_trans->getPosition());
				if (it_dist2 < dist2) {
					p_light = comp_light;
					dist2 = it_dist2;
				}
			}
		}
		p_light->projectPlayer();
	}
	// If we can't project the player, we go back to 3D
	else {
		cte_player.projected = false;
		cte_player.updateGPU();
		light = CHandle();
		return projected;
	}

	cte_player.projected =1;
	cte_player.updateGPU();
	cte_player.activate();
	projected = true;
	light = CHandle(p_light).getOwner();

	if (firstTimeProjectedCave && area==eventArea::TUTORIAL2) {
		firstTimeProjectedCave = false;
		string e_name;
		e_name.push_back('"');
		e_name.append("Camera_Tutorial_02");
		e_name.push_back('"');
		CEngine::get().getScripting().ThrowEvent(CModuleScripting::CUTSCENE,e_name,500);
	}
	if (firstTimeProjectedCave2 && area == eventArea::TUTORIAL10) {
		firstTimeProjectedCave2 = false;
		string e_name;
		e_name.push_back('"');
		e_name.append("Camera_Tutorial_10");
		e_name.push_back('"');
		CEngine::get().getScripting().ThrowEvent(CModuleScripting::CUTSCENE,e_name,500);
	}
	if (firstTimeProjectedCave3 && area == eventArea::GUARDIANS) {
		firstTimeProjectedCave3 = false;
		string e_name;
		e_name.push_back('"');
		e_name.append("Camera_Tutorial_17");
		e_name.push_back('"');
		CEngine::get().getScripting().ThrowEvent(CModuleScripting::CUTSCENE,e_name, 500);
	}
	return projected;
}

void TCompPlayer::desproject() {
	getObjectManager<TCompColShadow>()->forEach([](TCompColShadow* c) {
		TCompCollider* comp_collider = c->get<TCompCollider>();
		c->get<TCompColShadow>().destroy();
		if(comp_collider) comp_collider->recreateTriggers();
		});
	getObjectManager<TCompLight>()->forEach([](TCompLight* c) {
		c->ActivateLightLimit(false);
		});
	//getObjectManager<TCompColShadow>()->forEach([](TCompColShadow* c) {
	//	if (c->get<TCompCollider>().isValid()) {
	//		TCompCollider* comp_collider = c->get<TCompCollider>();
	//		c->get<TCompColShadow>().destroy();
	//		comp_collider->recreateTriggers();
	//	}
	//	else {
	//		c->get<TCompColShadow>().destroy();
	//	}
	//	});

	getObjectManager<TCompPictogram>()->forEach([](TCompPictogram* c) {
		c->destroyActor();
		});


	cte_player.projected = false;
	cte_player.updateGPU();
	projected = false;
	haveMove2DObj = false;
	timer_ResetProject = 0;
	light = CHandle();

	if (firstTimeDeproject && area == eventArea::TUTORIAL11) {
		firstTimeDeproject = false;
		CEngine::get().getCutScenes().startCutscene("Camera_Tutorial_11");
	}
}

bool TCompPlayer::isProjected()
{
	/*CEntity* cam = (h_camera);
	std::string cam_name = cam->getName();
	return cam_name == "camera2D";*/
	return projected;
}

void TCompPlayer::manualDesproject()
{
	desproject();
	focus2shadow = true;
	CEngine::get().setProjected(false);
	setModoCamera(CAM3D);
	SoundEvent* bso = CEngine::get().getSound().getBSO();
	bso->setParameter("projected", 0);

	ui::CWidget* barWidget = CEngine::get().getUI().getWidgetById("Vingentin_Proyectado_ch");
	ui::CImage* bar = dynamic_cast<ui::CImage*> (barWidget);
	bool visibility = false;
	bar->setVisible(visibility);

	CEngine::get().getUI().deactivateWidget("vignetting");

	TMsgPauseParticles msg;
	msg.pause = true;
	CEntity* e = getEntityByName("Footprint_controller");
	if (e) e->sendMsg(msg);

	TCompFSM* fsm = get<TCompFSM>();
	fsm->setVariable("shadow_mode", false);
	fsm->setVariable("switch_shadow", true);
}

void TCompPlayer::insertPosJump()
{
	TCompTransform* comp_trans = get<TCompTransform>();
	debug_pos_jump.push_back(comp_trans->getPosition());
}

void TCompPlayer::startDrawJump()
{
	debug_pos_jump.clear();
	timer_debug_jump = 0;
}

void TCompPlayer::loadTeleportPos(const json& j)
{

	teleportPos tp;

	tp.name = j.value("name", "");
	tp.pos = loadVEC3(j, "pos");

	teleportPositions.push_back(tp);

}

void TCompPlayer::updateFakeJump(float dt)
{
	timer_fake_jump += dt;

	static bool init_jEnd = false;

	switch (fake_jump_state)
	{
	case 0:
		if (timer_fake_jump <= time_fake_jumpStart) {
			dbg("FAKE JUMP START\n");
			return;
		}
		else {
			dbg("Finalizo FAKE JUMP START\n");
			fake_jump_state = 1;
			setJumpStrength(1);
			setVerticalJump(true);
			initializeJump();
			precalculateFuncJump();
			return;
		}
		break;
	case 1:
		if (isPushingUp()) {
			dbg("FAKE JUMP UP\n");
			TCompTransform* player_trans = get<TCompTransform>();
			VEC3 current_pos = player_trans->getPosition();
			VEC3 newVel;
			newVel.y = calculateJump() - current_pos.y;
			setVelocity(newVel);
			move(dt);
			return;
		}
		else {
			fake_jump_state = 2;
			return;
		}
		break;
	case 2:
		dbg("FAKE JUMP DESC\n");
		if (collisions.collision_down) {
			fake_jump_state = 3;
			timer_fake_jump = 0;
			return;

		}else{
			TCompTransform* player_trans = get<TCompTransform>();
			VEC3 current_pos = player_trans->getPosition();
			VEC3 newVel;
			newVel.y = calculateJump() - current_pos.y;
			setVelocity(newVel);
			move(dt);
			return;
		}
		break;
	case 3:
		if (timer_fake_jump <= time_fake_jumpEnd) {
			if (!init_jEnd) {
				init_jEnd = true;
				TCompAnimator* anim = get<TCompAnimator>();
				anim->removeCurrentAction(0.1);
				anim->playActionAnim("jump-end", false, false, false);
			}
			dbg("FAKE JUMP END\n");
			return;
		}
		else {
			setVerticalJump(false);
			finalizeJump();
			fakeJump = false;
			init_jEnd = false;
			return;
		}
		break;
	}





}

void TCompPlayer::setRebirth(float time)
{
	/*rebirth = false;
	rebirth_time = time;
	current_time = 0;*/
}

void TCompPlayer::hitWithActor(const TMsgRecordPath& msg)
{
	if (startRecord) {
		if (list_steps.size() == 0)	recordTimer = 0;

		CEntity* ent = msg.h_sender.getOwner();
		string name = ent->getName();

		for (auto step : list_steps) {
			if (name == step.actor_name) {
				return;
			}
		}

		pathRecord step;
		step.actor_name = name;
		step.abs_time = recordTimer;

		TCompTransform* trans = get<TCompTransform>();
		step.pos_player = trans->getPosition();

		list_steps.push_back(step);

		TCompCollider* col = ent->get<TCompCollider>();
		col->drawCollider = true;

		//dbg("He pisado al actor %s\n", ent->getName());
	}
}

void TCompPlayer::OnHit(const TMsgDamage& msg)
{
	if (!damaged) {
		damaged = true;
		timer_damaged = 0;
		TCompFSM* fsm = get<TCompFSM>();
		fsm->changeState("Damaged");

		TCompAudio* audio = get<TCompAudio>();
		audio->playEvent("seeleHurt");
	}
}

void TCompPlayer::onEnterArea(const TMsgEnterArea& msg)
{
	switch (msg.area)
	{
	case 1:
		area = eventArea::TUTORIAL2;
		break;
	case 2:
		area = eventArea::TUTORIAL10;
		break;
	case 3:
		area = eventArea::GUARDIANS;
		break;
	case 4:
		area = eventArea::TUTORIAL11;
		break;
	default:
		break;
	}
	
}

void TCompPlayer::InitConstrictVars()
{
	if (!initConstrict) {

		//Aqui meter el visualizar el boton de la UI y el vi�etin circular
		showWindow = true;
	}
}

void TCompPlayer::resetConstrictVars()
{

	constrict = false;

	release = false;

	timer_animation = 0;

	time_animation = 0.1;

	attemptsToFree = 0;

	h_guardian = h_null;

	showWindow = false;

	frontConstrict = false;

	not_move = false;

	//Aqui desactivar el boton de la UI
}

void TCompPlayer::initTimeAnimation(float time)
{
	time_animation = time;
	timer_animation = 0;
}

void TCompPlayer::clearExtraYRoot()
{
	extra_y_vec = VEC3::Zero;
	is_extra_Root = false;
}

void TCompPlayer::setExtraYRoot(VEC3 delta)
{
	extra_y_vec = delta;
	is_extra_Root = true;
}

void TCompPlayer::activateSeeleCry()
{
	if (!seele_cry) {
		seele_cry = CEngine::get().get().getSound().playEvent("seele_cry");
		seele_cry->setVolume(vol_seele_cry);
	}
}

void TCompPlayer::desactivatedSeeleCry()
{
	if (seele_cry) {
		seele_cry->stop();
		seele_cry = nullptr;
	}
}

void TCompPlayer::lowerCounter()
{
	if (attemptsToFree > 0 && !release) {
		//Actualizar la UI del boton
		attemptsToFree--;

		if (attemptsToFree == 0) {
			release = true;

			dbg("IMPORTANTE - MANDO MENSAJE DE RELEASE AL GUARDIAN");

			TMsgPlayerRelease msg;
			msg.h_sender = CHandle(this).getOwner();
			h_guardian.sendMsg(msg);

			showWindow = false;
		}
	}
}

void TCompPlayer::drawThrowning(bool b)
{
	render_parabole = b;
}

void TCompPlayer::updateThrowing()
{
	TCompTransform* mytrans = get<TCompTransform>();
	CEntity* ent_camera = (CEntity*)getEntityByName("camera_direction");
	TCompCamera* comp_camera = ent_camera->get<TCompCamera>();
	TCompDirection* comp_direc = ent_camera->get<TCompDirection>();

	float pitch = comp_direc->getPitchRatio();
	pitch = std::clamp(pitch, 0.0f, 0.6f);

	/*time_parabole = 2.7 - 3.9 * pitch;
	if (time_parabole > 2.7) time_parabole = 2.7;*/

	

	//float dist = 110 - 350 * pitch + 290 * pitch * pitch;
	float dist = 222.73 * pitch * pitch - 290.32 * pitch + 95.505;
	dist = std::clamp(dist, 1.5f, 15.f);

	VEC3 newFront = comp_camera->getLeft().Cross(VEC3::Up);
	throwing_point = mytrans->getPosition() + newFront * dist - mytrans->getLeft() * 0.3;
}

void TCompPlayer::setThrowObject()
{
	TCompTransform* mytrans = get<TCompTransform>();
	TEntityParseContext myContext;
	parseScene("data/prefabs/thrownStone.json", myContext);
	CHandle h = myContext.entities_loaded[0];
	CEntity* e = h;
	TCompTransform* e_pos = e->get<TCompTransform>();
	TCompCollider* e_col = e->get<TCompCollider>();
	if (e_col->actor != nullptr)
	{
		VEC3 pos_stone = mytrans->getPosition() - mytrans->getLeft() * 0.3 + mytrans->getUp() * 1.3 + mytrans->getFront() * 0.1;
		((PxRigidDynamic*)e_col->actor)->putToSleep();
		e_col->actor->setGlobalPose(PxTransform(pos_stone.x, pos_stone.y, pos_stone.z));
	}
	TCompThrownObject* stone = e->get< TCompThrownObject>();
	VEC3 fDirec = front_object * cos(throwing_angle) + VEC3::Up * sin(throwing_angle);
	VEC3 f = fDirec * throwing_vel;
	stone->throwObject(f);
}

void TCompPlayer::changeParaboleTime(int value)
{
	input::CInputModule& input = CEngine::get().getInput();

	if (input.getActiveDevice() == input.GAMEPAD) {
		if (value > 0) {
			time_parabole += 0.01;
		}
		else {
			time_parabole -= 0.01;
		}
	}
	else {
		if (value > 0) {
			time_parabole += 0.1;
		}
		else {
			time_parabole -= 0.1;
		}
	}


	time_parabole = std::clamp(time_parabole, 0.5f, 2.7f);
}

void TCompPlayer::changeParaboleTime()
{
	CEntity* ent_camera = (CEntity*)getEntityByName("camera_direction");
	TCompDirection* comp_direc = ent_camera->get<TCompDirection>();

	float pitch = comp_direc->getPitchRatio();

	if (pitch != last_pitch_value) {
		/*if (pitch < last_pitch_value) {
			time_parabole += 0.01;
		}
		else {
			time_parabole -= 0.01;
		}*/
		time_parabole = -2.2 * pitch + 2.7;
		last_pitch_value = pitch;
	}

	time_parabole = std::clamp(time_parabole, 0.5f, 2.7f);
}

void TCompPlayer::changeStepMode(int val)
{
	throwing_step = val;
	if (val == 1) {
		CEntity* camera = getCamera();
		TCompCamera3D* camera3D = camera->get< TCompCamera3D>();
		camera3D->blockMovement(false);
	}

	if (val == 2) {
		CEntity* camera = getCamera();
		TCompCamera3D* camera3D = camera->get< TCompCamera3D>();

		camera3D->blockMovement(true);
	}
}


void TCompPlayer::OnPlatform(const TMsgPlatMove& msg)
{
	//dbg("Player-> Estoy encima de una plataforma y he recibido su movimiento\n");

	isOnTopPlatform = true;

	lastM.Position = msg.Pos;
	lastM.applied = false;
	lastM.deltaMove = msg.move;
	lastM.dYaw = msg.yaw;
	lastM.dPitch = msg.pitch;
	lastM.dRoll = msg.roll;
	//velocity += msg.move;
	//last_velocity = velocity;
}

void TCompPlayer::moveWithPlatform(float dt)
{
	if (!lastM.applied) {
		/*dbg("#############################################\n");
		dbg("PLAYER\n");
		dbg("Delta Move : %f %f %f \n", lastM.deltaMove.x, lastM.deltaMove.y, lastM.deltaMove.z);
		dbg("Yaw %f Pitch %f Roll %f\n", lastM.dYaw, lastM.dPitch, lastM.dRoll);
		dbg("#############################################\n");*/

		//absoluteMove(lastM.deltaMove, dt);
		VEC3 Rot_Var = rotateWithValues(lastM.dYaw, lastM.dPitch, lastM.dRoll, lastM.Position);
		velocity = lastM.deltaMove + Rot_Var/dt;
		move(dt);
		lastM.applied = true;
	}

}

void TCompPlayer::initFinalEvent(const TMsgInitFinalEvent& msg)
{
	timer_fEv = 0;
	final_event_active = true;
	cte_player.final_battle = 1;
}

void TCompPlayer::updateFinalEvent()
{
	if (timer_fEv > fEv_time) {
		dead = true;
	}
}
