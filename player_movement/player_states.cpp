#include "mcv_platform.h"
#include "mcv_platform.h"
#include "player_states.h"
#include "entity/common_msgs.h"
#include "fsm/fsm_context.h"
#include "render/video/video_texture.h"
#include "engine.h"
#include "sinn/modules/module_scripting.h"
#include "input/input_module.h"
#include "render/render_module.h"
#include "modules/module_camera_mixer.h"
#include "ui/ui_module.h"
#include "ui/widgets/ui_image.h"
#include "components/common/comp_camera.h"
#include "components/common/comp_aabb.h"
#include "components/common/comp_transform.h"
#include "components/common/comp_render.h"
#include "sinn/interaction/comp_animation_object.h"
#include "sinn/interaction/comp_ui_interaction.h"
#include "sinn/interaction/comp_column.h"
#include "sinn/interaction/comp_push_obj.h"
#include "sinn/interaction/comp_lightbulb.h"
#include "sinn/cameras/comp_camera3D.h"
#include "sinn/cameras/comp_camera2D.h"
#include "sinn/characters/player/comp_direction.h"
#include "sinn/characters/animator.h"
#include "windows/application.h"
#include "sinn/components/comp_audio.h"
#include "sinn/components/comp_orbital_matter.h"
#include "sinn/modules/module_sound.h"

namespace fsm {



#pragma region StateData
  StateData::StateData(std::string _camera_name, float _camera_time,
    float _max_velocity, float _secondary_max_velocity, float _time_acceleration, float _time_deceleration, float _noise, std::string _animation_name, std::string _type_anim) {
    camera.name = _camera_name;
    camera.time = _camera_time;
    max_velocity = _max_velocity;
    secondary_max_velocity = _secondary_max_velocity;
    time_acceleration = _time_acceleration;
    time_deceleration = _time_deceleration;
    noise = _noise;
    animation_name = _animation_name;
    type_anim = _type_anim;
  }
#pragma endregion

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                                                          GENERAL
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma region StatePlayer
  void CStatePlayer::load(const json& jData){
    auto camera = jData["camera"];
    stateData = StateData(
      camera.value("name", ""),
      camera.value("time", 1.f),
      jData.value("max_velocity", .5f),
      jData.value("secondary_max_velocity", 0.f),        
      jData.value("time_acceleration", 2.f),
      jData.value("time_deceleration", 2.f),
      jData.value("noise", 0.1f),
      jData.value("name_anim", ""),
      jData.value("type_anim", "")
    );
  }

  void CStatePlayer::start(CContext& ctx) const {
    setState(ctx);
    startDebug(ctx);
    if (!checkState(ctx)) return;
    startAnimation(ctx);
    changeCamera(ctx);
    startState(ctx);
  }

  void CStatePlayer::update(CContext& ctx, float dt) const {
    if (console.isConsoleOn()) return; // REVISAR

    updateDebug(ctx);
    if (changeState(ctx)) return;
    updateInput(ctx);
    updateBlend(ctx);
    updateVelocity(ctx, dt);
    updateVelocityDirecction(ctx, dt);
    updateTransform(ctx, dt);
    updateState(ctx, dt);

  }

  void CStatePlayer::setState(CContext& ctx) const
  {
    TCompPlayer* player = getCompPlayer(ctx);
    std::string state = ctx.getStateName();
    player->setLastState(state);
  }

  void CStatePlayer::startDebug(CContext& ctx) const {
    int f = CApplication::get().getFrameCount();
    std::string state = ctx.getStateName();
    //dbg("Start %s in %d\n", state.c_str(), f);
    startStateDebug(ctx);
  }

  void CStatePlayer::startAnimation(CContext& ctx) const {
    if (stateData.animation_name == "") return;
    CEntity* ent = ctx.getOwner();
    TCompAnimator* anim = ent->get<TCompAnimator>();

    std::string state = ctx.getStateName();
    string velName = state + "Velocity";
    anim->setVariable(velName, stateData.max_velocity);

    if (stateData.type_anim == "action") {
      anim->playActionAnim(stateData.animation_name, false, true, false);
    }
    else {
      anim->playCycleAnim(stateData.animation_name);
    }
  }

  void CStatePlayer::changeCamera(CContext& ctx) const {
    TCompPlayer* comp_player = getCompPlayer(ctx);
    //if (comp_player->isInCutscene())return;
    CameraData fromCamera = comp_player->getCameraData();
    if (stateData.camera.name.length() == 0) return;

    comp_player->changeCamera(fromCamera, stateData.camera);
    comp_player->setCameraData(stateData.camera);
  }

  void CStatePlayer::updateInput(CContext& ctx) const {
    //Input de la flyover
    TCompPlayer* player = getCompPlayer(ctx);

    if (input["flyover_camera"].justPressed()) {
        player->putFlyOverCamera();
    }
    //
    if (getCompPlayer(ctx)->isFallingDead()) ctx.changeToState("FallingDead");
    if (getCompPlayer(ctx)->isDead()) ctx.changeToState("Dead");

    ctx.setVariable("input_move", input["move_front"].isPressed() || input["move_side"].isPressed());
    bool input_move = std::get<bool>(ctx.getVariable("input_move")->getValue());
    ctx.setVariable("input_sprint", input["sprint"].isPressed() && input_move);
    ctx.setVariable("drop_sprint", !input["sprint"].isPressed() || !input_move);
    //dbg("Update Time -> Input Jump : %d con tiempo %.2f\n", input["jump"].isPressed(), input["jump"].time);
    ctx.setVariable("pushing_up", player->isPushingUp());



    updateStateInput(ctx);
  }

  void CStatePlayer::updateVelocity(CContext& ctx, float dt) const {
      TCompPlayer* player = getCompPlayer(ctx);

      float vel = 0;
      float max_vel = 0;

      //Para hacer pruebas donde le ponemos la velocidad desde el ImGui
      if (player->isDebugVelocity()) {
          max_vel = player->getDebugVelocityValue();
      }
      else {
          if (player->getIfChangeSecVel()) {
              max_vel = stateData.secondary_max_velocity;
          }
          else {
              max_vel = stateData.max_velocity;
          }
      }



      bool shadow_mode = std::get<bool>(ctx.getVariable("shadow_mode")->getValue());
      if (shadow_mode) {
          max_vel = calculateMaxVelInput2D(ctx, max_vel);
      }
      else {
          max_vel = calculateMaxVelInput3D(max_vel);
      }

      //Actualizamos si estamos acelerando, desacelerando, etc
      updateVelocityState(ctx, max_vel);

      switch (player->getVelState()) {
      case velocityStates::STOPPED:
          //dbg("VELOCIDAD STATE : STOPPED\n");
          vel = 0;
          break;
      case velocityStates::SLOWINGDOWN:
          //dbg("VELOCIDAD STATE : SLOWINGDOWN\n");
          vel = calculatePorcDeceleration(ctx);
          break;
      case velocityStates::SPEEDINGUP:
          //dbg("VELOCIDAD STATE : SPEEDINGUP\n");
          vel = calculatePorcAcceleration(ctx);
          break;
      case velocityStates::MAXVELOCITY:
          //dbg("VELOCIDAD STATE : MAXVELOCITY\n");
          vel = calculatePorcAcceleration(ctx);
          break;
      }

      vel = vel * max_vel;

      player->setModVelocity(vel);

      //dbg("La velocidad actual es %.2f de la maxima %.2f\n", vel, max_vel);

      ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

      CEntity* ent_player = ctx.getOwner();
      TCompAbsAABB* comp_aabb = ent_player->get<TCompAbsAABB>();
      TCompTransform* player_trans = ent_player->get<TCompTransform>();
      VEC3 aabbCenter = comp_aabb->Center;
      VEC3 aabbExt = comp_aabb->Extents;

      /*dbg("El center aabb es %.5f %.5f %.5f\n", aabbCenter.x, aabbCenter.y, aabbCenter.z);
      dbg("El ext aabb es %.5f %.5f %.5f\n", aabbExt.x, aabbExt.y, aabbExt.z);*/

      player->setNoise(vel, stateData.noise);
      ctx.setVariable("velocity", vel);
  }


  void CStatePlayer::updateTransform(CContext& ctx, float dt) const {
    TCompPlayer* player = getCompPlayer(ctx);
    /*player->rotate(dt);
    player->move(dt);*/

    ctx.setVariable("collision_down", player->getCollisionDown());
  }

  void CStatePlayer::updateBlend(CContext& ctx) const {
    if (stateData.animation_name == "") return;

    CEntity* ent_player = ctx.getOwner();
    TCompAnimator* anim = ent_player->get<TCompAnimator>();
    float velocity = getCompPlayer(ctx)->getModVelocity();

    anim->setVariable("velocity", velocity);
  }

  TCompPlayer* CStatePlayer::getCompPlayer(CContext& ctx) const {
    CEntity* player_ent = ctx.getOwner();
    return player_ent->get<TCompPlayer>();
  }

  bool CStatePlayer::changeStateAction(CContext& ctx, const char* action, const char* dst) const {
    if (input[action].justPressed()) {
      ctx.changeToState(dst);
      return true;
    }
    else return false;
  }

  bool CStatePlayer::changeStateCondition(CContext& ctx, bool condition, const char* dst) const {
    if (condition) {
      ctx.changeToState(dst);
      return true;
    }
    else return false;
  }

  bool CStatePlayer::interactAction(CContext& ctx) const {

    if (!input["interact"].justPressed()) return false;

    TCompPlayer* player = getCompPlayer(ctx);
    CEntity* ent_interact = player->getInteractionEntity();
    
    if (!ent_interact) {
      CEngine::get().getSound().playEvent("interactionWrong");
      return false;
    }

    ctx.changeToState("Interaction");

    // Throw Interact Event
    stringstream event_name;
    event_name << '"' << ent_interact->getName() << '"';
    CEngine::get().getScripting().ThrowEvent(CModuleScripting::Event::INTERACT, event_name.str(), 0);

    return true;
  }

  VEC3 CStatePlayer::getLocalDir(CContext& ctx) const {
    VEC3 local_dir;
    // We need the horitzontal front of the camera
    CEntity* ent_player = ctx.getOwner();
    TCompPlayer* comp_player = ent_player->get<TCompPlayer>();
    TCompTransform* player_trans = ent_player->get<TCompTransform>();
    
    // Si hemos parado deslizamos un poquito pero en direccion al front del player
    if (comp_player->getVelState() == velocityStates::SLOWINGDOWN && !comp_player->frenadaInmediata) {
        local_dir = player_trans->getFront();
        local_dir.Normalize();
        return local_dir;
    }

    //En el caso que tengamos la direccion bloqueada, vamos hacia el punto que ibamos antes
    if (comp_player->getBlockLocalDir()) {
        VEC3 newFront = comp_player->getOldLocalDir();
        VEC3 newRight = newFront.Cross(VEC3::Up);

        local_dir = newFront * input["move_front"].value + newRight * input["move_side"].value;
        local_dir.Normalize();
        return local_dir;
    }

    // Si estamos en una camara por rail, y estamos siendo guiados por una cueva entonces el movimiento ira acorde a un punto dado
    if (comp_player->getGuideMode()) {
        VEC3 newFront;
        VEC3 newRight;
        if (comp_player->getReverseGuide()) {
            newFront = player_trans->getPosition() - comp_player->getPointToMove();            
        }
        else {
            newFront = comp_player->getPointToMove() - player_trans->getPosition();
        }
        newRight = newFront.Cross(VEC3::Up);

        //Guardamos la direccion por si se bloquea el movimiento y tenemos que seguir esta direccion
        comp_player->setOldLocalDir(newFront);


        local_dir = newFront * input["move_front"].value + newRight * input["move_side"].value;
        local_dir.Normalize();

    }else{
        // Obtenemos la camara actual para obtener su curva y aplicarla al componente de direccion
        CEntity* camera = comp_player->getCamera();
        TCompCamera3D* camera3D = camera->get< TCompCamera3D>();

        // Obtenemos el componente de direccion para obtener hacia donde esta apuntando
        CEntity* ent_camera = (CEntity*)getEntityByName("camera_direction");
        TCompCamera* comp_camera = ent_camera->get<TCompCamera>();
        TCompDirection* comp_direc = ent_camera->get<TCompDirection>();

        //Aplicamos la curva de la camara actual al componente de direccion
        if (camera3D != nullptr) {
            comp_direc->setCurve(camera3D->getCurve());
        }

        VEC3 frontCamera = comp_camera->getFront();

        VEC3 newFront = VEC3(frontCamera.x, 0.f, frontCamera.z);
        newFront.Normalize();
        VEC3 hRight = -comp_camera->getLeft();       

        //Guardamos la direccion por si se bloquea el movimiento y tenemos que seguir esta direccion
        comp_player->setOldLocalDir(newFront);


        local_dir = newFront * input["move_front"].value + hRight * input["move_side"].value;
        local_dir.Normalize();
    }

    return local_dir;
  }

  VEC3 CStatePlayer::getLocalDir3D2D(CContext& ctx) const
  {
      // We need the horitzontal front of the camera
      CEntity* ent_player = ctx.getOwner();
      TCompPlayer* comp_player = ent_player->get<TCompPlayer>();
      TCompTransform* player_trans = ent_player->get<TCompTransform>();

      // Obtenemos la camara actual para obtener su curva y aplicarla al componente de direccion
      CEntity* camera = comp_player->getCamera();
      TCompCamera2D* camera2D = camera->get< TCompCamera2D>();

      // Obtenemos el componente de direccion para obtener hacia donde esta apuntando
      CEntity* ent_camera = (CEntity*)getEntityByName("camera_direction");
      TCompCamera* comp_camera = ent_camera->get<TCompCamera>();
      TCompDirection* comp_direc = ent_camera->get<TCompDirection>();

      //Aplicamos la curva de la camara actual al componente de direccion
      comp_direc->setCurve(camera2D->getCurve());

      float front_length_camera = sqrt(comp_camera->getFront().x * comp_camera->getFront().x + comp_camera->getFront().z * comp_camera->getFront().z);
      float angle_camera_front = acos(comp_camera->getFront().x / front_length_camera);

      if (comp_camera->getFront().x > 0) {
          if (comp_camera->getFront().z > 0) {
              angle_camera_front = angle_camera_front;
          }
          else {
              angle_camera_front = 2 * PI - angle_camera_front;
          }
      }
      else {
          if (comp_camera->getFront().z > 0) {
              angle_camera_front = angle_camera_front;
          }
          else {
              angle_camera_front = 2 * PI - angle_camera_front;
          }
      }
      float front_length_target = sqrt(player_trans->getFront().x * player_trans->getFront().x + player_trans->getFront().z * player_trans->getFront().z);

      float frontX_player = front_length_target * cos(angle_camera_front);
      float frontZ_player = front_length_target * sin(angle_camera_front);



      VEC3 newFront = VEC3(frontX_player, 0.f, frontZ_player);
      VEC3 hRight = VEC3(-frontZ_player, 0.f, frontX_player);
      newFront.Normalize();

      VEC3 local_dir;
      if (comp_player->getVelState() == velocityStates::SLOWINGDOWN && !comp_player->frenadaInmediata) {
          local_dir = player_trans->getFront();
      }
      else {
          local_dir = newFront * input["move_front"].value + hRight * input["move_side"].value;
      }

      //VEC3 local_dir = newFront * input["move_front"].value + hRight * input["move_side"].value;
      local_dir.Normalize();

      //dbg("Moviendome hacia %.4f %.4f %.4f\n", local_dir.x, local_dir.y, local_dir.z);

      return local_dir;
  }

  VEC3 CStatePlayer::getLocalDirJump2D(CContext& ctx, VEC3 light_ray, VEC3 wallRight) const {
    CEntity* ent_player = ctx.getOwner();
    TCompPlayer* comp_player = ent_player->get<TCompPlayer>();
    TCompTransform* player_trans = ent_player->get<TCompTransform>();
    // input move_front is relative to light and input move_side is relative to the wall
    light_ray.Normalize();

    VEC3 local_dir;

    if (comp_player->getVelState() == velocityStates::SLOWINGDOWN && !comp_player->frenadaInmediata) {
        local_dir = player_trans->getFront();
    }
    else {
        local_dir = light_ray * input["move_front"].value + wallRight * input["move_side"].value;
    }

    return local_dir;
  }

  VEC3 CStatePlayer::getLocalDirMovingObj(CContext& ctx, CEntity* camera) const {
    // We need the horitzontal front of the camera
    TCompCamera* comp_camera = camera->get<TCompCamera>();
    VEC3 hFront = comp_camera->getFront();
    hFront.y = 0.f;
    hFront.Normalize();
    VEC3 hRight = -comp_camera->getLeft();

    VEC3 local_dir = hFront * input["move_front"].value + hRight * input["move_side"].value;
    local_dir.Normalize();


    // SNAP TO CARTESIAN DIRECTION
    ctx.setVariable("pull_back", false);
    ctx.setVariable("push_right", false);
    ctx.setVariable("push_front", false);
    ctx.setVariable("push_left", false);


    // if we are moving, exit function
    bool input_move = input["move_side"].isPressed() || input["move_front"].isPressed();
    if (!input_move) return VEC3::Zero;


    CEntity* ent_player = ctx.getOwner();
    TCompTransform* player_trans = ent_player->get<TCompTransform>();
    VEC3 pos = player_trans->getPosition();
    VEC3 dir;
    float aim = rad2deg(player_trans->getDeltaYawToAimTo(pos + local_dir));
    if (aim < -135) {
      ctx.setVariable("pull_back", true);
      dir = -player_trans->getFront();
      local_dir = dir * local_dir.Dot(dir);
    }
    else if (aim < -45) {
      ctx.setVariable("push_right", true);
      dir = -player_trans->getLeft();
      local_dir = dir * local_dir.Dot(dir);
    }
    else if (aim < 45) {
      ctx.setVariable("push_front", true);
      dir = player_trans->getFront();
      local_dir = dir * local_dir.Dot(dir);
    }
    else if (aim < 135) {
      ctx.setVariable("push_left", true);
      dir = player_trans->getLeft();
      local_dir = dir * local_dir.Dot(dir);
    }
    else {
      ctx.setVariable("pull_back", true);
      dir = -player_trans->getFront();
      local_dir = dir * local_dir.Dot(dir);
    }

    return local_dir;
  }

  VEC3 CStatePlayer::getLocalDirMovingObj2D(CContext& ctx, CEntity* camera, float dt) const {
    CEntity* ent_player = ctx.getOwner();
    TCompPlayer* comp_player = getCompPlayer(ctx);
    TCompTransform* player_trans = ent_player->get<TCompTransform>();
    CEntity* ent_light = comp_player->getLight();
    TCompLight* light = ent_light->get<TCompLight>();
    if (!light)  return VEC3::Zero;
    //assert(!light);


    // Positions
    VEC3 light_pos = light->getPosition();
    TCompAbsAABB* comp_aabb = ent_player->get<TCompAbsAABB>();
    VEC3 wall_normal = comp_player->getWallNormal();

    // Unit Vectors
    VEC3 light_unit = comp_aabb->Center - light_pos;
    light_unit.Normalize();
    VEC3 wall_unit = VEC3::Up.Cross(wall_normal);

    VEC3 inputDir = calculateDirecctionInputMoveObj2D(ctx, light_unit, wall_unit);

    VEC3 vel = inputDir * comp_player->getModVelocity();

    if (!comp_player->setProjectedWall(vel, dt)) return VEC3::Zero;
    //assert(!comp_player->setProjectedWall(vel, dt));

    return vel;
  }

  VEC3 CStatePlayer::calculateDirecctionInputMoveObj2D(CContext& ctx, VEC3 lightDir, VEC3 wallDir) const
  {
    CEntity* ent_player = ctx.getOwner();
    TCompTransform* player_trans = ent_player->get<TCompTransform>();
    TCompPlayer* comp_player = getCompPlayer(ctx);

    VEC3 dirInput = VEC3::Zero;

    ctx.setVariable("pull_back", false);
    ctx.setVariable("push_front", false);

    float tWall = comp_player->getTimeIniMoveSide();
    float tLight = comp_player->getTimeIniMoveFront();
    //Nos movemos de lado o no nos movemos
    if (input["move_side"].value == 0) {
      if (input["move_front"].value) {
        comp_player->setLocalDir(dirInput);
        return dirInput;
      } 
      dirInput = comp_player->getLocalDir();
    }
    else {
      dirInput = wallDir * input["move_side"].value;
      VEC3 front_player = player_trans->getFront();
      float res = front_player.Dot(dirInput);

      if (res >= 0) {
          ctx.setVariable("push_front", true);
      }
      else {
          ctx.setVariable("pull_back", true);
      }

      comp_player->setLocalDir(dirInput);
      comp_player->setTimeIniMoveSide();
    }
    return dirInput;
  }

  VEC3 CStatePlayer::calculateMovement3D(CContext& ctx) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
      VEC3 local_dir = getLocalDir(ctx);
      player->setLocalDir(local_dir);
      CEntity* ent_player = ctx.getOwner();
      TCompTransform* player_trans = ent_player->get<TCompTransform>();

      VEC3 current_pos = player_trans->getPosition();
      VEC3 newVel = local_dir * player->getModVelocity();

      return newVel;
  }

  VEC3 CStatePlayer::calculateMovement3D2D(CContext& ctx) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
      if (player->isInCutscene())return VEC3::Zero;
      VEC3 local_dir = getLocalDir3D2D(ctx);
      player->setLocalDir(local_dir);
      CEntity* ent_player = ctx.getOwner();
      TCompTransform* player_trans = ent_player->get<TCompTransform>();

      VEC3 newVel = local_dir * player->getModVelocity();

      return newVel;
  }

  int CStatePlayer::calculateInput2D(CContext& ctx) const
  {
      TCompPlayer* comp_player = getCompPlayer(ctx);
      float tWall = comp_player->getTimeIniMoveSide();
      float tLight = comp_player->getTimeIniMoveFront();

      int opt = 0;
      // OPC = 0 -> Mover en anterior direccion
      // OPC = 1 -> Mover en direccion move_side
      // OPC = 2 -> Mover en direccion move_front

      // Si no se esta pulsando ninguna tecla o moviendo ningun gatillo
      //assert(!(input["move_side"].value == 0 && input["move_front"].value == 0));

      if (input["move_side"].value == 0 && input["move_front"].value == 0) {
        opt = 0;
      }
      // Pulsa ambas teclas al mismo tiempo o mueve ambos gatillos el mismo valor
      else if (input["move_side"].value == input["move_front"].value) {
          // En este caso comprobamos si uno se ha pulsado/movido antes que el otro
          if (tWall == tLight || tWall > tLight) {
              opt = 1;
          }
          else {
              opt = 2;
          }
      }
      // Si la tecla o gatillo de mover en X es mayor que el de mover en Y
      else if (abs(input["move_side"].value) > abs(input["move_front"].value)) {
          opt = 1;
      }
      // Si la tecla o gatillo de mover en Y es mayor que la de mover en X
      else if (abs(input["move_side"].value) < abs(input["move_front"].value)) {
          opt = 2;
      }

      return opt;
  }

  VEC3 CStatePlayer::calculateDirecctionInput2D(CContext& ctx, VEC3 lightDir, VEC3 wallDir) const
{
    CEntity* ent_player = ctx.getOwner();
    TCompTransform* player_trans = ent_player->get<TCompTransform>();
    TCompPlayer* comp_player = getCompPlayer(ctx);

    VEC3 dirInput = VEC3::Zero;

    float tWall = comp_player->getTimeIniMoveSide();
    float tLight = comp_player->getTimeIniMoveFront();


    int opt = calculateInput2D(ctx);
    // OPC = 0 -> Mover en anterior direccion
    // OPC = 1 -> Mover en direccion move_side
    // OPC = 2 -> Mover en direccion move_front

    switch (opt) {
    case 0:
        dirInput = comp_player->getLocalDir();
        break;

    case 1:
        dirInput = wallDir * input["move_side"].value;
        comp_player->setLocalDir(dirInput);
        comp_player->setTimeIniMoveSide();
        break;

    case 2:
        dirInput = lightDir * input["move_front"].value;
        comp_player->setLocalDir(dirInput);
        comp_player->setTimeIniMoveFront();
        break;
    }
    return dirInput;
}

VEC3 CStatePlayer::calculateMovement2D(CContext& ctx, float dt) const
{
    CEntity* ent_player = ctx.getOwner();
    TCompPlayer* comp_player = getCompPlayer(ctx);
    TCompTransform* player_trans = ent_player->get<TCompTransform>();
    CEntity* ent_light = comp_player->getLight();
    TCompLight* light = ent_light->get<TCompLight>();
    if (!light)  return VEC3::Zero;
    //assert(!light);


    // Positions
    VEC3 light_pos = light->getPosition();
    TCompAbsAABB* comp_aabb = ent_player->get<TCompAbsAABB>();
    VEC3 wall_normal = comp_player->getWallNormal();

    // Unit Vectors
    VEC3 light_unit = comp_aabb->Center - light_pos;
    light_unit.Normalize();
    VEC3 wall_unit = VEC3::Up.Cross(wall_normal);

    VEC3 inputDir = calculateDirecctionInput2D(ctx, light_unit, wall_unit);

    VEC3 vel = inputDir * comp_player->getModVelocity();

    if(!comp_player->setProjectedWall(vel,dt)) return VEC3::Zero;
    //assert(!comp_player->setProjectedWall(vel, dt));

    return vel;
}

void CStatePlayer::updateVelocityState(CContext& ctx, float max_vel) const
  {
      CEntity* ent_player = ctx.getOwner();
      TCompPlayer* comp_player = ent_player->get<TCompPlayer>();
      float vel = comp_player->getModVelocity();
      bool input_move = std::get<bool>(ctx.getVariable("input_move")->getValue());
      velocityStates state = comp_player->getVelState();

      //dbg("Mi velocidad actual es %.2f y mi maxima velocidad es %.2f\n", vel, max_vel);


      // Hemos movido al personaje
      if (input_move) {
          //dbg("Estoy pulsando mover teclas\n");
          // Estamos acelerando o vamos acelerar
          if (vel < max_vel) {
              if (state == velocityStates::SLOWINGDOWN || state == velocityStates::STOPPED)    comp_player->setIniTimeAcc();
              comp_player->setVelState(velocityStates::SPEEDINGUP);
          }
          // Ya hemos llegado a la velocidad maxima
          else comp_player->setVelState(velocityStates::MAXVELOCITY);
      }
      //No hemos movido al personaje
      else {
          //dbg("==================> NO estoy pulsando mover teclas\n");
          // Si estamos totalmente parados
          if(vel < 0.1) comp_player->setVelState(velocityStates::STOPPED);
          // Si estamos desacelerando
          else {
              if (state == velocityStates::MAXVELOCITY || state == velocityStates::SPEEDINGUP)    comp_player->setIniTimeDec();
              comp_player->setVelState(velocityStates::SLOWINGDOWN);
          }
      }
  }

  float CStatePlayer::calculatePorcAcceleration(CContext& ctx) const
  {
      CEntity* ent_player = ctx.getOwner();
      TCompPlayer* comp_player = ent_player->get<TCompPlayer>();

      float inc_curve = (1.0 / 10.0);
      float tacc = stateData.time_acceleration;
      float tnow = comp_player->getPlayerTimer() - comp_player->getIniTimeAcc();
      float v100 = 1.0 - 1.0 / (tacc / inc_curve + 1.0);

      float porAcc = 2.0 - v100 - 1.0 / (tnow / inc_curve + 1.0);
      if (porAcc > 1)    porAcc = 1;

      /*dbg("Time Now: %.2f ,Timer: %.2f, TimeAcc: %.2f, Inc_Curve: %.2f\n", tnow, comp_player->getPlayerTimer(), comp_player->getIniTimeAcc(), inc_curve);
      dbg("Porcentaje aceleracion %.2f\n", porAcc);*/

      return porAcc;
  }

  float CStatePlayer::calculatePorcDeceleration(CContext& ctx) const
  {
      CEntity* ent_player = ctx.getOwner();
      TCompPlayer* comp_player = ent_player->get<TCompPlayer>();

      float inc_curve = 1;            
      if(!comp_player->isJumping()) inc_curve = (1.0 / 50.0);
      float tdec = stateData.time_deceleration;
      float tnow = comp_player->getPlayerTimer() - comp_player->getIniTimeDec();
      float v100 = 1.0 - 1.0 / (tdec / inc_curve + 1.0);

      float porDec = (2.0 - v100 - 1.0 - inc_curve / (tnow + inc_curve )) * - 1.0;
      if (porDec < 0)    porDec = 0;

      //dbg("Porcentaje deceleracion %.2f\n", porDec);

      return porDec;
  }

  float CStatePlayer::calculateMaxVelInput3D(float max_vel) const
  {
      float side_value = input["move_side"].value;
      float front_value = input["move_front"].value;

      float max_por = sqrt((side_value * side_value) + (front_value * front_value));
      if (max_por > 1)   max_por = 1;

      float newMax_vel = max_vel * max_por;

      //dbg("3D: Input side %.2f, Input front %.2f, Porcentaje %.2f, Resultado = %.2f\n", side_value, front_value, max_por, newMax_vel);

      return newMax_vel;
  }

  float CStatePlayer::calculateMaxVelInput2D(CContext& ctx, float max_vel) const
  {
      float side_value = input["move_side"].value;
      float front_value = input["move_front"].value;

      int opt = calculateInput2D(ctx);
      // OPC = 0 -> Mover en anterior direccion
      // OPC = 1 -> Mover en direccion move_side
      // OPC = 2 -> Mover en direccion move_front

      float newMax_vel = 0;
      float max_por = 0;

      switch (opt) {
      case 0:
          newMax_vel = max_vel;
          break;
      case 1:
          max_por = sqrt(side_value * side_value);
          newMax_vel = max_vel * max_por;
          break;
      case 2:
          max_por = sqrt(front_value * front_value);
          newMax_vel = max_vel * max_por;
          break;
      }

      //dbg("2D: Input side %.2f, Input front %.2f, Porcentaje %.2f, Resultado = %.2f\n", side_value, front_value, max_por, newMax_vel);

      return newMax_vel;
  }

  

#pragma endregion


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                                                          MOVE3D
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


#pragma region Move3D

  // WALK, SPRINT AND STEALTH
      
  SoundEvent* runBreath = nullptr;
  float volumeAudio = 0.f;
	void CStateMove3D::startState(CContext& ctx) const
	{
    TCompPlayer* player = getCompPlayer(ctx);
    std::string current_state = ctx.getStateName();
    /*CEntity* e_orbit = getEntityByName("orbital_matter");
    TCompOrbitalMatter* orbit = e_orbit->get<TCompOrbitalMatter>();*/
    
    if (current_state == "Walk") {
  
      player->activateSteps(true);
      //if (e_orbit)orbit->setVelocity(15);
      SoundEvent* sEvent = player->getStepsEvent();
      if (sEvent)
        sEvent->setParameter("stealth", 0);
    }
    else if (current_state == "Sprint") {
      if (!runBreath)runBreath = CEngine::get().getSound().playEvent("running");
          runBreath->setParameter("volumen",volumeAudio);
      player->activateSteps(true);
      SoundEvent* sEvent = player->getStepsEvent();
      if (sEvent)
        sEvent->setParameter("stealth", 0);
      //if (e_orbit)orbit->setVelocity(25);
    }
   
    
	}

	bool CStateMove3D::changeState(CContext& ctx) const {
    TCompPlayer* player = getCompPlayer(ctx);
    // Transitions to check
    if (changeStateAction(ctx, "shadow_mode", "CheckShadow")) {
      return true;
    }
    if (changeStateAction(ctx, "jump", "LongJumpStart")) return true;
    if (changeStateAction(ctx, "stealth", "StealthIdle")) return true;

    if (changeStateAction(ctx, "throw", "ThrowObject")) return true;
    

    //if (changeStateCondition(ctx, !player->getCollisionDown() && player->getVelocity().y < -0.8f, "JumpDesc")) return true;
    if (changeStateCondition(ctx, player->isFallingDown(), "Falling")) return true;
    if (interactAction(ctx)) return true;
    if (changeStateCondition(ctx, input["interact"].justPressed() && player->canMoveTheBox(), "MoveObj")) return true;
    return false;
  }


  void CStateMove3D::updateVelocityDirecction(CContext& ctx, float dt) const
  {

      TCompPlayer* player = getCompPlayer(ctx);
      std::string current_state = ctx.getStateName();
      ////////////////////////////////////////////////////////////////////////////////////////////////////////
      // Si estamos en cualquiera de los estados sprint, indicamos que se use la velocidad maxima secundaria
      // para cuando realicemos un salto que saltemos mas lejos
      std::string state = ctx.getStateName();
      if (state.find("Sprint") != std::string::npos) {
          //dbg("Cambiamos a la maxima velocidad secundaria\n");
          player->setChangeSecVel(true);
      }
      else {
          player->setChangeSecVel(false);
      }
      ///////////////////////////////////////////////////////////////////////////////////////////////////////
      if (current_state == "Sprint") {
        if (ctx.getTimeInState() < 2.8f) {
          if (volumeAudio < 1)volumeAudio += 0.005;
          runBreath->setParameter("volume", volumeAudio);
        }
      }
      else if (current_state == "Walk") {
        if (runBreath) {
          if (ctx.getTimeInState() < 2.8f) {
            if (volumeAudio > 0)volumeAudio -= 0.01;
            runBreath->setParameter("volume", volumeAudio);
          }
          else {
            runBreath->stop();
            volumeAudio = 0.f;
            runBreath = nullptr;
          }

        }
      }
    
      VEC3 newVel = calculateMovement3D(ctx);
      player->setVelocity(newVel);
  }
  void CStateMove3D::finish(CContext& ctx) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
      player->setVerticalJump(false);
      player->activateSteps(false);
  }

  // IDLE

  bool CStateIdle3D::changeState(CContext& ctx) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
      // Transitions to check
      if (changeStateAction(ctx, "shadow_mode", "CheckShadow")) {
          return true;
      }
      if (changeStateAction(ctx, "jump", "ShortJumpStart")) return true;
      if (changeStateAction(ctx, "stealth", "StealthIdle")) return true;
      if (changeStateAction(ctx, "throw", "ThrowObject")) return true;

      //if (changeStateCondition(ctx, !player->getCollisionDown() && player->getVelocity().y < -0.8f, "JumpDesc")) return true;
      if (changeStateCondition(ctx, player->isFallingDown(), "Falling")) return true;
      if (input["interact"].justPressed()) {
        bool b = true;
      }
      if (interactAction(ctx)) return true;
      if (changeStateCondition(ctx, input["interact"].justPressed() && player->canMoveTheBox(), "MoveObj")) return true;
      return false;
  }

  void CStateIdle3D::updateVelocityDirecction(CContext& ctx, float dt) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
      VEC3 newVel;
      if (player->isOnTopPlatform) {
          newVel = VEC3::Zero;
      }
      else {
          newVel = calculateMovement3D(ctx);
      }
      
      if (runBreath) {
        if (ctx.getTimeInState() < 2.8f) {
          if (volumeAudio > 0)volumeAudio -= 0.01;
          runBreath->setParameter("volume", volumeAudio);
        }
        else {
          runBreath->stop();
          volumeAudio = 0.f;
          runBreath = nullptr;
        }
      }
        

      player->setVelocity(newVel);

      //dbg("Update del Idle State\n");
  }

  void CStateIdle3D::updateTransform(CContext& ctx, float dt) const
  {
      TCompPlayer* player = getCompPlayer(ctx);

      /*if (player->isOnTopPlatform) {
          player->moveWithPlatform(dt);
      }
      else {
          player->rotate(dt);
          player->move(dt);
      }*/

      ctx.setVariable("collision_down", player->getCollisionDown());
  }

  void CStateIdle3D::finish(CContext& ctx) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
      player->setVerticalJump(true);
  }


#pragma endregion

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                                                          MOVE2D
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma region Move2D
  void CStateMove2D::startState(CContext& ctx) const {
      TCompPlayer* player = getCompPlayer(ctx);
      std::string state = ctx.getStateName();

      if (state.find("Walk2D") != std::string::npos) {
          //Para indicarle a las particulas de los pasos que estamos andando
          player->setWalking(true);
      }

      if (state.find("Sprint2D") != std::string::npos) {
          //Para indicarle a las particulas de los pasos que estamos corriendo
          player->setSprinting(true);
          if (!runBreath)runBreath = CEngine::get().getSound().playEvent("running");
      }
      
  }

  bool CStateMove2D::changeState(CContext& ctx) const {
    TCompPlayer* player = getCompPlayer(ctx);

    if (changeStateAction(ctx, "shadow_mode", "CheckShadow")) return true;  
    if (changeStateCondition(ctx, !getCompPlayer(ctx)->isProjected(), "CheckShadow")) {
      return true;
    }
    if (changeStateAction(ctx, "jump", "LongJumpStart2D")) return true;

    if (interactAction(ctx)) return true;
    //if (changeStateCondition(ctx, input["interact"].justPressed() && player->checkShadowPushBox(), "MoveObj2D")) return true;

    if (changeStateCondition(ctx, player->isFallingDown(), "Falling2D")) return true;

    return false;
  }

  void CStateMove2D::updateVelocityDirecction(CContext& ctx,float dt) const
  {
      ////////////////////////////////////////////////////////////////////////////////////////////////////////
      // Si estamos en cualquiera de los estados sprint, indicamos que se use la velocidad maxima secundaria
      // para cuando realicemos un salto que saltemos mas lejos
      std::string state = ctx.getStateName();
      if (state.find("Sprint") != std::string::npos) {
          //dbg("Cambiamos a la maxima velocidad secundaria\n");
          getCompPlayer(ctx)->setChangeSecVel(true);
      }
      else {
          getCompPlayer(ctx)->setChangeSecVel(false);
      }
      ///////////////////////////////////////////////////////////////////////////////////////////////////////
      if (state == "Sprint2D") {
        if (ctx.getTimeInState() < 2.8f) {
          if (volumeAudio < 1)volumeAudio += 0.005;
          runBreath->setParameter("volume",volumeAudio);
        }
      }
      else{
        if (runBreath) {
          if (ctx.getTimeInState() < 2.8f) {
            if (volumeAudio > 0)volumeAudio -= 0.01;
            runBreath->setParameter("volume", volumeAudio);
          }
          else {
            runBreath->stop();
            volumeAudio = 0.f;
            runBreath = nullptr;
          }

        }
      }
      VEC3 vel;

      if (getCompPlayer(ctx)->focusToShadow()) {
          vel = calculateMovement2D(ctx, dt);
      }
      else {
          vel = calculateMovement3D2D(ctx);
      }

      getCompPlayer(ctx)->setVelocity(vel);
  }

  void CStateMove2D::updateStateInput(CContext& ctx) const {
      if (input["vertical_camera"].justPressed()) {
          getCompPlayer(ctx)->switchFocusToShadow();
      }
  }

  void CStateMove2D::updateTransform(CContext& ctx, float dt) const {
    TCompPlayer* comp_player = getCompPlayer(ctx);

    comp_player->forceRotation(comp_player->getLocalDir());
    //comp_player->move(dt);

    ctx.setVariable("collision_down", comp_player->getCollisionDown());
  }
  void CStateMove2D::finish(CContext& ctx) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
      std::string state = ctx.getStateName();

      if (state.find("Walk2D") != std::string::npos) {
          //Para indicarle a las particulas de los pasos que ya no estamos andando
          player->setWalking(false);
      }

      if (state.find("Sprint2D") != std::string::npos) {
          //Para indicarle a las particulas de los pasos que ya no estamos corriendo
          player->setSprinting(false);
      }
  }


  bool CStateIdle2D::changeState(CContext& ctx) const
  {
    TCompPlayer* player = getCompPlayer(ctx);

    if (changeStateAction(ctx, "shadow_mode", "CheckShadow")) return true;
    if (changeStateCondition(ctx, !getCompPlayer(ctx)->isProjected(), "CheckShadow")) {
      return true;
    }
    if (changeStateAction(ctx, "jump", "ShortJumpStart2D")) return true;

    if (interactAction(ctx)) return true;
    //if (changeStateCondition(ctx, input["interact"].justPressed() && player->checkShadowPushBox(), "MoveObj2D")) return true;
    if (changeStateCondition(ctx, player->isFallingDown(), "Falling2D")) return true;

    return false;
  }

  void CStateIdle2D::updateVelocityDirecction(CContext& ctx, float dt) const
  {
      VEC3 vel;

      if (getCompPlayer(ctx)->focusToShadow()) {
          vel = calculateMovement2D(ctx, dt);
      }
      else {
          vel = calculateMovement3D2D(ctx);
      }
      
      if (runBreath!=nullptr) {
        if (ctx.getTimeInState() < 2.8f) {
          if (volumeAudio > 0)volumeAudio -= 0.01;
          runBreath->setParameter("volume", volumeAudio);
        }
        else {
          runBreath->stop();
          volumeAudio = 0.f;
          runBreath = nullptr;
        }
        
      }
        
      getCompPlayer(ctx)->setVelocity(vel);
  }

  void CStateIdle2D::updateStateInput(CContext& ctx) const
  {
      if (input["vertical_camera"].justPressed())
          getCompPlayer(ctx)->switchFocusToShadow();
  }

  void CStateIdle2D::updateTransform(CContext& ctx, float dt) const
  {
      TCompPlayer* comp_player = getCompPlayer(ctx);

      comp_player->forceRotation(comp_player->getLocalDir());
      //comp_player->move(dt);

      ctx.setVariable("collision_down", comp_player->getCollisionDown());
  }

  void CStateIdle2D::finish(CContext& ctx) const
  {
      getCompPlayer(ctx)->setVerticalJump(true);
  }


#pragma endregion

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                                                          JUMP3D
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma region Jump3D

  /******************************************************************************/
  //   JUMP START
  /*****************************************************************************/

  void CStateJumpStart3D::startState(CContext& ctx) const
  {
     
      TCompPlayer* player = getCompPlayer(ctx);
      player->initializeJump();
      player->startDrawJump();

      //dbg("He empezado a saltar\n");
      if (player->getPlant().isValid()) {
        TMsgStepPlant msg;
        CEntity* e = player->getPlant();
        e->sendMsg(msg);
      }

      CEntity* ent = ctx.getOwner();
      TCompAudio* audio = ent->get<TCompAudio>();
      audio->playEvent("startJump");
  }

  void CStateJumpStart3D::startAnimation(CContext& ctx) const {

  }

  bool CStateJumpStart3D::changeState(CContext& ctx) const {
    TCompPlayer* player = getCompPlayer(ctx);
    // Transitions to check
    if (changeStateAction(ctx, "shadow_mode", "CheckShadow")) {
      return true;
    }
    return false;
  }

  void CStateJumpStart3D::updateVelocityDirecction(CContext& ctx, float dt) const {
      TCompPlayer* player = getCompPlayer(ctx);
      CEntity* ent_player = ctx.getOwner();
      TCompTransform* player_trans = ent_player->get<TCompTransform>();
     
      if (runBreath) {
    
          runBreath->stop();
          volumeAudio = 0.f;
          runBreath = nullptr;
        
      }
        
      VEC3 newVel = calculateMovement3D(ctx);

      if (input["jump"].value) {
          float jump_strength = ((ctx.getTimeInState() * 100.0) / 0.1f) / 100.0;
          if (jump_strength > 1) jump_strength = 1;
          if (jump_strength < 0.4) jump_strength = 0.4;
          player->setJumpStrength(jump_strength);

      }

      
      player->precalculateFuncJump();

      VEC3 current_pos = player_trans->getPosition();
      newVel.y = player->calculateJump() - current_pos.y;
      player->setVelocity(newVel);

      
  }

  /******************************************************************************/
  //   JUMP UP
  /*****************************************************************************/

  void CStateJumpUp3D::startAnimation(CContext& ctx) const {
      if (stateData.animation_name == "") return;
      CEntity* ent = ctx.getOwner();
      TCompAnimator* anim = ent->get<TCompAnimator>();
      TCompPlayer* player = getCompPlayer(ctx);

      if (player->getRightFootInFloor()) {
          anim->playParcialActionBucle("jump-loop");
      }
      else {
  
          anim->playParcialActionBucle("jump-loop-inv");
      }     

  }

  bool CStateJumpUp3D::changeState(CContext& ctx) const {
      TCompPlayer* player = getCompPlayer(ctx);

      // Transitions to check
      if (player->isCollisionUp()) {
          ctx.changeToState("LongJumpDesc");
          player->changeTimeToJumpDesc();
          return true;
      }
      if (changeStateAction(ctx, "shadow_mode", "CheckShadow")) {
          return true;
      }
      return false;
  }

  void CStateJumpUp3D::updateVelocityDirecction(CContext& ctx, float dt) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
      CEntity* ent_player = ctx.getOwner();
      TCompTransform* player_trans = ent_player->get<TCompTransform>();

      VEC3 newVel = calculateMovement3D(ctx);
      VEC3 current_pos = player_trans->getPosition();
      newVel.y = player->calculateJump() - current_pos.y;

      player->setVelocity(newVel);
  }
  /******************************************************************************/
  //   JUMP DESC
  /*****************************************************************************/

  void CStateJumpDesc3D::startAnimation(CContext& ctx) const {
      CEntity* ent = ctx.getOwner();
      TCompAnimator* anim = ent->get<TCompAnimator>();
      TCompPlayer* player = getCompPlayer(ctx);

      if (!anim->hasBucleAction()) {
          if (player->getRightFootInFloor()) {
              anim->playParcialActionBucle("jump-loop");
          }
          else {

              anim->playParcialActionBucle("jump-loop-inv");
          }
      }
  }

  bool CStateJumpDesc3D::changeState(CContext& ctx) const {
      TCompPlayer* player = getCompPlayer(ctx);

      // Transitions to check
      if (changeStateAction(ctx, "shadow_mode", "CheckShadow")) {
          return true;
      }
      //Si llevamos mucho tiempo cayendo, pensamos que estamos
      if (ctx.getTimeInState() >= player->getTimeEternalFalling()) {
          ctx.changeToState("FallingDead");
          return true;
      }
      return false;
  }

  void CStateJumpDesc3D::updateVelocityDirecction(CContext& ctx, float dt) const
  {
      TCompPlayer* player = getCompPlayer(ctx);

      CEntity* ent_player = ctx.getOwner();
      TCompTransform* player_trans = ent_player->get<TCompTransform>();

      VEC3 newVel = calculateMovement3D(ctx);
      VEC3 current_pos = player_trans->getPosition();
      newVel.y = player->calculateJump() - current_pos.y;

      player->setVelocity(newVel);
  }

  void CStateJumpDesc3D::finish(CContext& ctx) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
      player->timer_FallingState = ctx.getTimeInState();
      player->finalizeJump();
  }

  /******************************************************************************/
  //   JUMP END
  /*****************************************************************************/

  void CStateJumpEnd3D::startState(CContext& ctx) const
  {
      CEntity* ent = ctx.getOwner();
      TCompAudio* audio = ent->get<TCompAudio>();
      audio->playEvent("jumpEnd");
  }

  void CStateJumpEnd3D::startAnimation(CContext& ctx) const {
      if (stateData.animation_name == "") return;
      CEntity* ent = ctx.getOwner();
      TCompAnimator* anim = ent->get<TCompAnimator>();

      anim->removeBucleAction("jump-loop");
      anim->removeBucleAction("jump-loop-inv");

      TCompSkeleton* skel = ent->get<TCompSkeleton>();

      if (skel->getLegForward()) {
          anim->playActionAnim("jump-end-moving-inv", false, false, false);          
      }
      else {
          anim->playActionAnim("jump-end-moving", false, false, false);
      }     

  }

  bool CStateJumpEnd3D::changeState(CContext& ctx) const {
      TCompPlayer* player = getCompPlayer(ctx);

      // Si hemos estado cayendo en Jump Desc durante mas del tiempo predefinido, morimos
      if (player->timer_FallingState >= player->getTimeDeathByFall()) {
          ctx.changeToState("Dead");
          return true;
      }
      // Transitions to check
      if (changeStateAction(ctx, "shadow_mode", "CheckShadow")) {
          return true;
      }

      bool moving = input["move_front"].isPressed() || input["move_side"].isPressed();
      if (!moving) {
          CEntity* ent = ctx.getOwner();
          TCompAnimator* anim = ent->get<TCompAnimator>();
          anim->removeAction("jump-end-moving", 0.1);
          anim->removeAction("jump-end-moving-inv", 0.1);
          ctx.changeToState("Idle");
          return true;
      }

      if (changeStateAction(ctx, "jump", "LongJumpStart")) {
          CEntity* ent = ctx.getOwner();
          TCompAnimator* anim = ent->get<TCompAnimator>();
          anim->removeAction("jump-end-moving", 0.1);
          anim->removeAction("jump-end-moving-inv", 0.1);
          return true;
      }

      return false;
  }
  void CStateJumpEnd3D::updateVelocityDirecction(CContext& ctx, float dt) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
      VEC3 newVel = calculateMovement3D(ctx);
      player->setVelocity(newVel);
  }
  void CStateJumpEnd3D::finish(CContext& ctx) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
      if (player->getPlant().isValid()) {
        TMsgStepPlant msg;
        CEntity* e = player->getPlant();
        e->sendMsg(msg);
      }
      
  }
#pragma endregion

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //                                                          IDLE JUMP3D
    //
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma region IdleJump3D

  /******************************************************************************/
  //   JUMP START
  /*****************************************************************************/
  void CStateIdleJumpStart3D::startState(CContext& ctx) const
  {
      TCompPlayer* player = getCompPlayer(ctx);

      if (!player->isInJumpStart()) {
          if (player->getPlant().isValid()) {
              TMsgStepPlant msg;
              CEntity* e = player->getPlant();
              e->sendMsg(msg);
          }
          player->initJumpStart();
      }
  }

  void CStateIdleJumpStart3D::startAnimation(CContext& ctx) const
  {
      TCompPlayer* player = getCompPlayer(ctx);

      if (!player->isInJumpStart()) {
          if (stateData.animation_name == "") return;
          CEntity* ent = ctx.getOwner();

          TCompAnimator* anim = ent->get<TCompAnimator>();
          anim->playActionAnim(stateData.animation_name, false, true, false);
      }

  }

  bool CStateIdleJumpStart3D::changeState(CContext& ctx) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
      // Transitions to check
      if (changeStateAction(ctx, "shadow_mode", "CheckShadow")) {
          return true;
      }

      if (player->getTimeInJumpStart() > 0.3) {
          ctx.changeToState("ShortJumpUp");
          return true;
      }

      return false;
  }

  void CStateIdleJumpStart3D::updateVelocityDirecction(CContext& ctx, float dt) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
      CEntity* ent_player = ctx.getOwner();
      TCompTransform* player_trans = ent_player->get<TCompTransform>();

      if (runBreath) {

          runBreath->stop();
          volumeAudio = 0.f;
          runBreath = nullptr;

      }

      if (input["jump"].value) {
          float jump_strength = ((player->getTimeInJumpStart() * 100.0) / 0.3f) / 100.0;
          if (jump_strength > 1) jump_strength = 1;
          if (jump_strength < 0.4) jump_strength = 0.4;
          player->setJumpStrength(jump_strength);
      }
  }
  /******************************************************************************/
  //   JUMP UP
  /*****************************************************************************/
  void CStateIdleJumpUp3D::startState(CContext& ctx) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
      player->initializeJump();
      player->startDrawJump();
      player->precalculateFuncJump();
      ctx.setVariable("pushing_up", player->isPushingUp());

      CEntity* ent = ctx.getOwner();
      TCompAudio* audio = ent->get<TCompAudio>();
      audio->playEvent("startJump");
  }

  void CStateIdleJumpUp3D::startAnimation(CContext& ctx) const
  {
      if (stateData.animation_name == "") return;
      CEntity* ent = ctx.getOwner();
      TCompAnimator* anim = ent->get<TCompAnimator>();


      //anim->removeCurrentAction(0.1);
      anim->playParcialActionBucle("jump-loop");
  }

  bool CStateIdleJumpUp3D::changeState(CContext& ctx) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
      // Transitions to check
      if (player->isCollisionUp()) {
          ctx.changeToState("ShortJumpDesc");
          player->changeTimeToJumpDesc();
          return true;
      }
      if (changeStateAction(ctx, "shadow_mode", "CheckShadow")) {
          return true;
      }
      return false;
  }

  void CStateIdleJumpUp3D::updateVelocityDirecction(CContext& ctx, float dt) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
      CEntity* ent_player = ctx.getOwner();
      TCompTransform* player_trans = ent_player->get<TCompTransform>();

      VEC3 newVel = calculateMovement3D(ctx);
      VEC3 current_pos = player_trans->getPosition();
      newVel.y = player->calculateJump() - current_pos.y;

      player->setVelocity(newVel);
  }
  /******************************************************************************/
  //   JUMP DESC
  /*****************************************************************************/

  void CStateIdleJumpDesc3D::startAnimation(CContext& ctx) const
  {
  }

  bool CStateIdleJumpDesc3D::changeState(CContext& ctx) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
      // Transitions to check
      if (changeStateAction(ctx, "shadow_mode", "CheckShadow")) {
          return true;
      }
      //Si llevamos mucho tiempo cayendo, pensamos que estamos
      if (ctx.getTimeInState() >= player->getTimeEternalFalling()) {
          ctx.changeToState("FallingDead");
          return true;
      }
      return false;
  }

  void CStateIdleJumpDesc3D::updateVelocityDirecction(CContext& ctx, float dt) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
      CEntity* ent_player = ctx.getOwner();
      TCompTransform* player_trans = ent_player->get<TCompTransform>();

      VEC3 newVel = calculateMovement3D(ctx);
      VEC3 current_pos = player_trans->getPosition();
      newVel.y = player->calculateJump() - current_pos.y;

      player->setVelocity(newVel);
  }

  void CStateIdleJumpDesc3D::finish(CContext& ctx) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
      player->timer_FallingState = ctx.getTimeInState();
      player->setVerticalJump(false);
      player->finalizeJump();
  }

  /******************************************************************************/
  //   JUMP END
  /*****************************************************************************/

  void CStateIdleJumpEnd3D::startState(CContext& ctx) const
  {
      CEntity* ent = ctx.getOwner();
      TCompAudio* audio = ent->get<TCompAudio>();
      audio->playEvent("jumpEnd");
  }

  void CStateIdleJumpEnd3D::startAnimation(CContext& ctx) const
  {
      if (stateData.animation_name == "") return;
      CEntity* ent = ctx.getOwner();
      TCompAnimator* anim = ent->get<TCompAnimator>();

      anim->removeBucleAction("jump-loop");
      anim->playActionAnim(stateData.animation_name, false, false, false);

  }

  bool CStateIdleJumpEnd3D::changeState(CContext& ctx) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
      // Si hemos estado cayendo en Jump Desc durante mas del tiempo predefinido, morimos
      if (player->timer_FallingState >= player->getTimeDeathByFall()) {
          ctx.changeToState("Dead");
          return true;
      }
      // Transitions to check
      if (changeStateAction(ctx, "shadow_mode", "CheckShadow")) {
          return true;
      }

      if (input["move_front"].isPressed() || input["move_side"].isPressed()) {
          CEntity* ent = ctx.getOwner();
          TCompAnimator* anim = ent->get<TCompAnimator>();
          anim->removeAction("jump-end",0.1);
          ctx.changeToState("Walk");
          return true;
      }

      if (changeStateAction(ctx, "jump", "ShortJumpStart")) {
          CEntity* ent = ctx.getOwner();
          TCompAnimator* anim = ent->get<TCompAnimator>();
          anim->removeAction("jump-end", 0.1);
          return true;
      }
      return false;
  }

  void CStateIdleJumpEnd3D::updateVelocityDirecction(CContext& ctx, float dt) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
      VEC3 newVel = VEC3::Zero;
      player->setVelocity(newVel);
  }

  void CStateIdleJumpEnd3D::finish(CContext& ctx) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
      if (player->getPlant().isValid()) {
          TMsgStepPlant msg;
          CEntity* e = player->getPlant();
          e->sendMsg(msg);
      }
  }


#pragma endregion

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                                                          JUMP2D
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma region Jump2D

  /******************************************************************************/
  //   JUMP2D START
  /*****************************************************************************/


  void CStateJumpStart2D::startAnimation(CContext& ctx) const {

  }

  void CStateJumpStart2D::startState(CContext& ctx) const {
      TCompPlayer* player = getCompPlayer(ctx);

      player->initializeJump();
      player->startDrawJump();

      CEntity* ent = ctx.getOwner();
      TCompAudio* audio = ent->get<TCompAudio>();
      audio->playEvent("startJump");
    
  }

  bool CStateJumpStart2D::changeState(CContext& ctx) const {
      TCompPlayer* player = getCompPlayer(ctx);

      if (changeStateAction(ctx, "shadow_mode", "CheckShadow")) {
          return true;
      }
      if (changeStateCondition(ctx, !getCompPlayer(ctx)->isProjected(), "CheckShadow")) {
        return true;
      }
      return false;
  }

  void CStateJumpStart2D::updateStateInput(CContext& ctx) const {
    if (input["vertical_camera"].justPressed())
      getCompPlayer(ctx)->switchFocusToShadow();
  }

  void CStateJumpStart2D::updateTransform(CContext& ctx, float dt) const {
    TCompPlayer* comp_player = getCompPlayer(ctx);

    comp_player->forceRotation(comp_player->getLocalDir());
    //comp_player->move(dt);
    ctx.setVariable("collision_down", comp_player->getCollisionDown());
  }

  void CStateJumpStart2D::updateVelocityDirecction(CContext& ctx, float dt) const
  {
      TCompPlayer* comp_player = getCompPlayer(ctx);
      CEntity* ent_player = ctx.getOwner();
      TCompTransform* player_trans = ent_player->get<TCompTransform>();
      
      if (runBreath) {
       
          runBreath->stop();
          volumeAudio = 0.f;
          runBreath = nullptr;
        
      }
        
      VEC3 newVel;
      if (getCompPlayer(ctx)->focusToShadow()) {
          newVel = calculateMovement2D(ctx, dt);
      }
      else {
          newVel = calculateMovement3D2D(ctx);
      }

      if (input["jump"].value) {
          float jump_strength = ((ctx.getTimeInState() * 100.0) / 0.1f) / 100.0;
          if (jump_strength > 1) jump_strength = 1;
          if (jump_strength < 0.4) jump_strength = 0.4;
          comp_player->setJumpStrength(jump_strength);
      }

      comp_player->precalculateFuncJump();

      VEC3 current_pos = player_trans->getPosition();
      newVel.y = comp_player->calculateJump() - current_pos.y;

      comp_player->setCollisionDown(false);

      getCompPlayer(ctx)->setVelocity(newVel);
  }

 /******************************************************************************/
//   JUMP2D UP
/*****************************************************************************/

  void CStateJumpUp2D::startAnimation(CContext& ctx) const
  {
      if (stateData.animation_name == "") return;
      CEntity* ent = ctx.getOwner();
      TCompAnimator* anim = ent->get<TCompAnimator>();
      TCompPlayer* player = getCompPlayer(ctx);

      if (player->getRightFootInFloor()) {
          anim->playParcialActionBucle("jump-loop");
      }
      else {

          anim->playParcialActionBucle("jump-loop-inv");
      }
  }

  bool CStateJumpUp2D::changeState(CContext& ctx) const
  {
      TCompPlayer* player = getCompPlayer(ctx);

      if (player->isCollisionUp()) {
          ctx.changeToState("LongJumpDesc2D");
          player->changeTimeToJumpDesc();
          return true;
      }
      // Transitions to check
      if (changeStateAction(ctx, "shadow_mode", "CheckShadow")) {
          return true;
      }
      if (changeStateCondition(ctx, !getCompPlayer(ctx)->isProjected(), "CheckShadow")) {
        return true;
      }
      return false;
  }

  void CStateJumpUp2D::updateTransform(CContext& ctx, float dt) const
  {
      TCompPlayer* comp_player = getCompPlayer(ctx);

      comp_player->forceRotation(comp_player->getLocalDir());
      //comp_player->move(dt);
      ctx.setVariable("collision_down", comp_player->getCollisionDown());
  }

  void CStateJumpUp2D::updateVelocityDirecction(CContext& ctx, float dt) const
  {
      TCompPlayer* comp_player = getCompPlayer(ctx);
      CEntity* ent_player = ctx.getOwner();
      TCompTransform* player_trans = ent_player->get<TCompTransform>();

      VEC3 newVel;
      if (getCompPlayer(ctx)->focusToShadow()) {
          newVel = calculateMovement2D(ctx, dt);
      }
      else {
          newVel = calculateMovement3D2D(ctx);
      }

      VEC3 current_pos = player_trans->getPosition();
      newVel.y = comp_player->calculateJump() - current_pos.y;

      getCompPlayer(ctx)->setVelocity(newVel);
  }

  void CStateJumpUp2D::updateStateInput(CContext& ctx) const
  {
      if (input["vertical_camera"].justPressed())   getCompPlayer(ctx)->switchFocusToShadow();
  }

/******************************************************************************/
//   JUMP2D DESC
/*****************************************************************************/

  void CStateJumpDesc2D::startAnimation(CContext& ctx) const
  {
      CEntity* ent = ctx.getOwner();
      TCompAnimator* anim = ent->get<TCompAnimator>();
      TCompPlayer* player = getCompPlayer(ctx);

      if (!anim->hasBucleAction()) {
          if (player->getRightFootInFloor()) {
              anim->playParcialActionBucle("jump-loop");
          }
          else {

              anim->playParcialActionBucle("jump-loop-inv");
          }
      }
  }

  bool CStateJumpDesc2D::changeState(CContext& ctx) const
  {
      TCompPlayer* player = getCompPlayer(ctx);

      if (ctx.getTimeInState() >= player->getTimeEternalFalling()) {
          ctx.changeToState("FallingDead");
          return true;
      }
      // Transitions to check
      if (changeStateAction(ctx, "shadow_mode", "CheckShadow")) {
          return true;
      }
      if (changeStateCondition(ctx, !getCompPlayer(ctx)->isProjected(), "CheckShadow")) {
        return true;
      }
      return false;
  }

  void CStateJumpDesc2D::updateTransform(CContext& ctx, float dt) const
  {
      TCompPlayer* comp_player = getCompPlayer(ctx);

      comp_player->forceRotation(comp_player->getLocalDir());
      //comp_player->move(dt);
      ctx.setVariable("collision_down", comp_player->getCollisionDown());
  }

  void CStateJumpDesc2D::updateVelocityDirecction(CContext& ctx, float dt) const
  {
      TCompPlayer* comp_player = getCompPlayer(ctx);
      CEntity* ent_player = ctx.getOwner();
      TCompTransform* player_trans = ent_player->get<TCompTransform>();

      VEC3 newVel;
      if (getCompPlayer(ctx)->focusToShadow()) {
          newVel = calculateMovement2D(ctx, dt);
      }
      else {
          newVel = calculateMovement3D2D(ctx);
      }

      VEC3 current_pos = player_trans->getPosition();
      newVel.y = comp_player->calculateJump() - current_pos.y;

      getCompPlayer(ctx)->setVelocity(newVel);
  }

  void CStateJumpDesc2D::updateStateInput(CContext& ctx) const
  {
      if (input["vertical_camera"].justPressed())   getCompPlayer(ctx)->switchFocusToShadow();
  }

  void CStateJumpDesc2D::finish(CContext& ctx) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
      player->timer_FallingState = ctx.getTimeInState();
      player->finalizeJump();
  }

/******************************************************************************/
//   JUMP2D END
/*****************************************************************************/

  void CStateJumpEnd2D::startAnimation(CContext& ctx) const
  {
      if (stateData.animation_name == "") return;
      CEntity* ent = ctx.getOwner();
      TCompAnimator* anim = ent->get<TCompAnimator>();

      anim->removeBucleAction("jump-loop");
      anim->removeBucleAction("jump-loop-inv");

      TCompSkeleton* skel = ent->get<TCompSkeleton>();

      if (skel->getLegForward()) {
          anim->playActionAnim("jump-end-moving-inv", false, false, false);
      }
      else {
          anim->playActionAnim("jump-end-moving", false, false, false);
      }
  }

  void CStateJumpEnd2D::startState(CContext& ctx) const
  {
      CEntity* ent = ctx.getOwner();
      TCompAudio* audio = ent->get<TCompAudio>();
      audio->playEvent("jumpEnd");
  }


  bool CStateJumpEnd2D::changeState(CContext& ctx) const
  {
      if (getCompPlayer(ctx)->timer_FallingState >= getCompPlayer(ctx)->getTimeDeathByFall()) {
          ctx.changeToState("Dead");
          return true;
      }
      // Transitions to check
      if (changeStateAction(ctx, "shadow_mode", "CheckShadow")) {
          return true;
      }
      if (changeStateCondition(ctx, !getCompPlayer(ctx)->isProjected(), "CheckShadow")) {
        return true;
      }
      
      bool moving = input["move_front"].isPressed() || input["move_side"].isPressed();
      if (!moving) {
          CEntity* ent = ctx.getOwner();
          TCompAnimator* anim = ent->get<TCompAnimator>();
          anim->removeAction("jump-end-moving", 0.1);
          anim->removeAction("jump-end-moving-inv", 0.1);
          ctx.changeToState("Idle2D");
          return true;
      }

      if (changeStateAction(ctx, "jump", "LongJumpStart2D")) {
          CEntity* ent = ctx.getOwner();
          TCompAnimator* anim = ent->get<TCompAnimator>();
          anim->removeAction("jump-end-moving", 0.1);
          anim->removeAction("jump-end-moving-inv", 0.1);
          return true;
      }

      return false;
  }

  void CStateJumpEnd2D::updateTransform(CContext& ctx, float dt) const
  {
      TCompPlayer* comp_player = getCompPlayer(ctx);

      comp_player->forceRotation(comp_player->getLocalDir());
      //comp_player->move(dt);
      ctx.setVariable("collision_down", comp_player->getCollisionDown());
  }

  void CStateJumpEnd2D::updateVelocityDirecction(CContext& ctx, float dt) const
  {
      TCompPlayer* comp_player = getCompPlayer(ctx);
      CEntity* ent_player = ctx.getOwner();
      TCompTransform* player_trans = ent_player->get<TCompTransform>();

      VEC3 newVel;
      if (getCompPlayer(ctx)->focusToShadow()) {
          newVel = calculateMovement2D(ctx, dt);
      }
      else {
          newVel = calculateMovement3D2D(ctx);
      }

      VEC3 current_pos = player_trans->getPosition();
      newVel.y = comp_player->calculateJump() - current_pos.y;

      comp_player->setVelocity(newVel);
  }

  void CStateJumpEnd2D::updateStateInput(CContext& ctx) const
  {
      if (input["vertical_camera"].justPressed())   getCompPlayer(ctx)->switchFocusToShadow();
  }

  void CStateJumpEnd2D::finish(CContext& ctx) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
    
  }


#pragma endregion

#pragma region IdleJump2D

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                                                          IDLE JUMP2D
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/******************************************************************************/
//   IDLE JUMP2D START
/*****************************************************************************/

  void CStateIdleJumpStart2D::startState(CContext& ctx) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
      if (!player->isInJumpStart()) {
          if (player->getPlant().isValid()) {
              TMsgStepPlant msg;
              CEntity* e = player->getPlant();
              e->sendMsg(msg);
          }
          player->initJumpStart();
      }

  }

  void CStateIdleJumpStart2D::startAnimation(CContext& ctx) const
  {
      if (stateData.animation_name == "") return;

      TCompPlayer* player = getCompPlayer(ctx);
      if (!player->isInJumpStart()) {
          CEntity* ent = ctx.getOwner();

          TCompAnimator* anim = ent->get<TCompAnimator>();
          anim->playActionAnim(stateData.animation_name, false, true, false);
      }
  }

  bool CStateIdleJumpStart2D::changeState(CContext& ctx) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
      // Transitions to check
      if (changeStateAction(ctx, "shadow_mode", "CheckShadow")) {
          return true;
      }
      if (changeStateCondition(ctx, !getCompPlayer(ctx)->isProjected(), "CheckShadow")) {
          return true;
      }

      if (player->getTimeInJumpStart() > 0.3) {
          ctx.changeToState("ShortJumpUp2D");
          return true;
      }
      return false;
  }

  void CStateIdleJumpStart2D::updateStateInput(CContext& ctx) const {
      if (input["vertical_camera"].justPressed())
          getCompPlayer(ctx)->switchFocusToShadow();
  }

  void CStateIdleJumpStart2D::updateTransform(CContext& ctx, float dt) const {
      TCompPlayer* comp_player = getCompPlayer(ctx);

      comp_player->forceRotation(comp_player->getLocalDir());
      //comp_player->move(dt);
      ctx.setVariable("collision_down", comp_player->getCollisionDown());
  }

  void CStateIdleJumpStart2D::updateVelocityDirecction(CContext& ctx, float dt) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
      CEntity* ent_player = ctx.getOwner();
      TCompTransform* player_trans = ent_player->get<TCompTransform>();

      if (runBreath) {

          runBreath->stop();
          volumeAudio = 0.f;
          runBreath = nullptr;

      }

      if (input["jump"].value) {
          float jump_strength = ((player->getTimeInJumpStart() * 100.0) / 0.3f) / 100.0;
          if (jump_strength > 1) jump_strength = 1;
          if (jump_strength < 0.4) jump_strength = 0.4;
          player->setJumpStrength(jump_strength);
      }
  }

  /******************************************************************************/
//   IDLE JUMP2D UP
/*****************************************************************************/

  void CStateIdleJumpUp2D::startState(CContext& ctx) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
      player->initializeJump();
      player->startDrawJump();
      player->precalculateFuncJump();
      ctx.setVariable("pushing_up", player->isPushingUp());

      CEntity* ent = ctx.getOwner();
      TCompAudio* audio = ent->get<TCompAudio>();
      audio->playEvent("startJump");
  }

  void CStateIdleJumpUp2D::startAnimation(CContext& ctx) const
  {
      if (stateData.animation_name == "") return;
      CEntity* ent = ctx.getOwner();
      TCompAnimator* anim = ent->get<TCompAnimator>();

      anim->playActionAnim(stateData.animation_name, false, true, false);
  }

  bool CStateIdleJumpUp2D::changeState(CContext& ctx) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
      // Transitions to check
      if (player->isCollisionUp()) {
          //dbg("Hay collision up asi que cambio a JumpDesc\n");
          ctx.changeToState("ShortJumpDesc2D");
          player->changeTimeToJumpDesc();
          return true;
      }
      // Transitions to check
      if (changeStateAction(ctx, "shadow_mode", "CheckShadow")) {
          return true;
      }
      if (changeStateCondition(ctx, !getCompPlayer(ctx)->isProjected(), "CheckShadow")) {
          return true;
      }
      return false;
  }

  void CStateIdleJumpUp2D::updateTransform(CContext& ctx, float dt) const
  {
      TCompPlayer* comp_player = getCompPlayer(ctx);

      comp_player->forceRotation(comp_player->getLocalDir());
      //comp_player->move(dt);
      ctx.setVariable("collision_down", comp_player->getCollisionDown());
  }

  void CStateIdleJumpUp2D::updateStateInput(CContext& ctx) const
  {
      if (input["vertical_camera"].justPressed())   getCompPlayer(ctx)->switchFocusToShadow();
  }

  void CStateIdleJumpUp2D::updateVelocityDirecction(CContext& ctx, float dt) const
  {
      TCompPlayer* comp_player = getCompPlayer(ctx);
      CEntity* ent_player = ctx.getOwner();
      TCompTransform* player_trans = ent_player->get<TCompTransform>();

      VEC3 newVel;
      if (comp_player->focusToShadow()) {
          newVel = calculateMovement2D(ctx, dt);
      }
      else {
          newVel = calculateMovement3D2D(ctx);
      }

      VEC3 current_pos = player_trans->getPosition();
      newVel.y = comp_player->calculateJump() - current_pos.y;

      comp_player->setVelocity(newVel);
  }

  /******************************************************************************/
//   IDLE JUMP2D DESC
/*****************************************************************************/

  void CStateIdleJumpDesc2D::startAnimation(CContext& ctx) const
  {

  }

  bool CStateIdleJumpDesc2D::changeState(CContext& ctx) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
      if (ctx.getTimeInState() >= player->getTimeEternalFalling()) {
          ctx.changeToState("FallingDead");
          return true;
      }
      // Transitions to check
      if (changeStateAction(ctx, "shadow_mode", "CheckShadow")) {
          return true;
      }
      if (changeStateCondition(ctx, !getCompPlayer(ctx)->isProjected(), "CheckShadow")) {
          return true;
      }
      return false;
  }

  void CStateIdleJumpDesc2D::updateVelocityDirecction(CContext& ctx, float dt) const
  {
      TCompPlayer* comp_player = getCompPlayer(ctx);
      CEntity* ent_player = ctx.getOwner();
      TCompTransform* player_trans = ent_player->get<TCompTransform>();

      VEC3 newVel;
      if (getCompPlayer(ctx)->focusToShadow()) {
          newVel = calculateMovement2D(ctx, dt);
      }
      else {
          newVel = calculateMovement3D2D(ctx);
      }

      VEC3 current_pos = player_trans->getPosition();
      newVel.y = comp_player->calculateJump() - current_pos.y;

      getCompPlayer(ctx)->setVelocity(newVel);
  }

  void CStateIdleJumpDesc2D::updateStateInput(CContext& ctx) const
  {
      if (input["vertical_camera"].justPressed())   getCompPlayer(ctx)->switchFocusToShadow();
  }

  void CStateIdleJumpDesc2D::updateTransform(CContext& ctx, float dt) const
  {
      TCompPlayer* comp_player = getCompPlayer(ctx);

      comp_player->forceRotation(comp_player->getLocalDir());
      //comp_player->move(dt);
      ctx.setVariable("collision_down", comp_player->getCollisionDown());
  }

  void CStateIdleJumpDesc2D::finish(CContext& ctx) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
      player->timer_FallingState = ctx.getTimeInState();
      player->setVerticalJump(false);
      player->finalizeJump();
  }

  /******************************************************************************/
//   IDLE JUMP2D END
/*****************************************************************************/

  void CStateIdleJumpEnd2D::startState(CContext& ctx) const
  {
      CEntity* ent = ctx.getOwner();
      TCompAudio* audio = ent->get<TCompAudio>();
      audio->playEvent("jumpEnd");
  }

  void CStateIdleJumpEnd2D::startAnimation(CContext& ctx) const
  {
      if (stateData.animation_name == "") return;
      CEntity* ent = ctx.getOwner();
      TCompAnimator* anim = ent->get<TCompAnimator>();

      anim->removeBucleAction("jump-loop");
      anim->playActionAnim(stateData.animation_name, false, false, false);
  }

  bool CStateIdleJumpEnd2D::changeState(CContext& ctx) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
      // Si hemos estado cayendo en Jump Desc durante mas del tiempo predefinido, morimos
      if (player->timer_FallingState >= player->getTimeDeathByFall()) {
          ctx.changeToState("Dead");
          return true;
      }
      // Transitions to check
      if (changeStateAction(ctx, "shadow_mode", "CheckShadow")) {
          return true;
      }
      if (changeStateCondition(ctx, !getCompPlayer(ctx)->isProjected(), "CheckShadow")) {
          return true;
      }

      if (input["move_front"].isPressed() || input["move_side"].isPressed()) {
          CEntity* ent = ctx.getOwner();
          TCompAnimator* anim = ent->get<TCompAnimator>();
          anim->removeAction("jump-end", 0.1);
          ctx.changeToState("Walk2D");
          return true;
      }

      if (changeStateAction(ctx, "jump", "ShortJumpStart2D")) {
          CEntity* ent = ctx.getOwner();
          TCompAnimator* anim = ent->get<TCompAnimator>();
          anim->removeAction("jump-end", 0.1);
          return true;
      }
      return false;
  }

  void CStateIdleJumpEnd2D::updateVelocityDirecction(CContext& ctx, float dt) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
      VEC3 newVel = VEC3::Zero;
      player->setVelocity(newVel);
  }

  void CStateIdleJumpEnd2D::updateStateInput(CContext& ctx) const
  {
      if (input["vertical_camera"].justPressed())   getCompPlayer(ctx)->switchFocusToShadow();
  }

  void CStateIdleJumpEnd2D::updateTransform(CContext& ctx, float dt) const
  {
      TCompPlayer* comp_player = getCompPlayer(ctx);

      comp_player->forceRotation(comp_player->getLocalDir());
      //comp_player->move(dt);
      ctx.setVariable("collision_down", comp_player->getCollisionDown());
  }



  void CStateIdleJumpEnd2D::finish(CContext& ctx) const
  {
      TCompPlayer* player = getCompPlayer(ctx);
      player->setVerticalJump(false);
      player->finalizeJump();

      if (player->getPlant().isValid()) {
          TMsgStepPlant msg;
          CEntity* e = player->getPlant();
          e->sendMsg(msg);
      }
  }

#pragma endregion

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                                                          CHECKSHADOW
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma region CheckShadow
  void CStateCheckShadow::startState(CContext& ctx) const {
    //TCompLight* light;
    //dbg("Chequeando cambio de realidad\n");

    bool shadow_mode = std::get<bool>(ctx.getVariable("shadow_mode")->getValue());

    // If shadow_mode == false we came from 3D
    // We need to check if we can project the player


    //////////////////////////
    // 3D TO 2D
    /////////////////////////
    if (!shadow_mode) {
      bool status = getCompPlayer(ctx)->project();
      shadow_mode = status;
      ctx.setVariable("switch_shadow", status);

      // Si podemos proyectarnos...
      if (shadow_mode) {
        CEngine::get().setProjected(true);
        CEntity* ent_player = ctx.getOwner();
        TCompPlayer* comp_player = ent_player->get<TCompPlayer>();
        TCompTransform* player_trans = ent_player->get<TCompTransform>();

        if (!comp_player->isInCutscene()) {
          switchToSameState(ctx);
          changeCamera(ctx);
        }


        CEngine::get().getUI().activateWidget("vignetting");

        SoundEvent* bso = CEngine::get().getSound().getBSO();
        if(bso)bso->setParameter("projected", 1);

/*        ui::CWidget* barWidget = CEngine::get().getUI().getWidgetById("Vingentin_Proyectado_ch");
        ui::CImage* bar = dynamic_cast<ui::CImage*> (barWidget);
        bool visibility = true;
        bar->setVisible(visibility)*/;
       /* ui::CWidget* barWidget = CEngine::get().getUI().getWidgetById("Vingentin_Proyectado_ch");
        ui::CImage* bar = dynamic_cast<ui::CImage* > (barWidget);
        bool visibility = true;
        bar->setVisible(visibility);*/
        TMsgPauseParticles msg;
        msg.pause = false;
        CEntity* e = getEntityByName("Footprint_controller");
        if (e) e->sendMsg(msg);        

        TCompAudio* player_audio = ent_player->get<TCompAudio>();
        player_audio->playEvent("project");
        ///////////////////////////////////////////////////////////////////////

        comp_player->setProjectedWall();
        
        VEC3 wall_normal = comp_player->getWallNormal();
        VEC3 wallRight = VEC3::Up.Cross(wall_normal);

        float player_yaw = 0.f, player_pitch = 0.f;
        player_trans->getEulerAngles(&player_yaw, &player_pitch, nullptr);
        float aim = player_trans->getDeltaYawToAimTo(player_trans->getPosition() + wall_normal);
        float side = aim < 0.f ? 1.f : -1.f;

        comp_player->forceRotation(wallRight * side);

        int enumCam;
        if (shadow_mode) enumCam = CAM2D;
        else enumCam = CAM3D;
        getCompPlayer(ctx)->setModoCamera(enumCam);
       

        //dbg("Me proyecto\n");

      }
      else {
          //ctx.changeToState("Idle");
          CEntity* ent_player = ctx.getOwner();
          TCompAudio* player_audio = ent_player->get<TCompAudio>();
          player_audio->playEvent("interactionWrong");
          switchToSameState(ctx);
      }

    }

    // If shadow_mode == true we came from 2D
    // We need desproject player and go to Walk

     //////////////////////////
    // 2D TO 3D
    /////////////////////////
    else {
      getCompPlayer(ctx)->desproject();
      getCompPlayer(ctx)->setPlant(CHandle());
      CEngine::get().setProjected(false);
      shadow_mode = false;
      ctx.setVariable("switch_shadow", true);
      //ctx.changeToState("Desproject");
      if (!getCompPlayer(ctx)->isInCutscene()) {
        switchToSameState(ctx);
        changeCamera(ctx);
      }
      SoundEvent* bso = CEngine::get().getSound().getBSO();
      if(bso)bso->setParameter("projected", 0);
      CEntity* ent_player = ctx.getOwner();
      TCompAudio* player_audio = ent_player->get<TCompAudio>();
      player_audio->playEvent("project");

      ui::CWidget* barWidget = CEngine::get().getUI().getWidgetById("Vingentin_Proyectado_ch");
      ui::CImage* bar = dynamic_cast<ui::CImage*> (barWidget);
      bool visibility = false;
      bar->setVisible(visibility);

      CEngine::get().getUI().deactivateWidget("vignetting");

      TMsgPauseParticles msg;
      msg.pause = true;
      CEntity* e = getEntityByName("Footprint_controller");
      if (e) e->sendMsg(msg);

      int enumCam;
      if (shadow_mode) enumCam = CAM2D;
      else enumCam = CAM3D;
      getCompPlayer(ctx)->setModoCamera(enumCam);
     
      //dbg("Me desproyecto\n");
    }

    ctx.setVariable("shadow_mode", shadow_mode);



  }
  void CStateCheckShadow::switchToSameState(CContext& ctx) const
  {
    TCompPlayer* player = getCompPlayer(ctx);

    string newState = player->getStringLastState();
    ctx.changeToState(newState.c_str());

  }
#pragma endregion

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                                                          STEALTH
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma region Stealth
  bool CStateStealth::changeState(CContext& ctx) const {
      TCompPlayer* player = getCompPlayer(ctx);
      // Transitions to check
      if (changeStateAction(ctx, "shadow_mode", "CheckShadow")) {
          return true;
      }
      if (player->getVelState()!=velocityStates::STOPPED) {

        player->activateSteps(true);
        SoundEvent* sEvent = player->getStepsEvent();
        if (sEvent)
          sEvent->setParameter("stealth", 1);
      }
    if (changeStateAction(ctx, "stealth", "Walk")) return true;
    if (changeStateAction(ctx, "sprint", "Sprint")) return true;
    if (changeStateAction(ctx, "jump", "ShortJumpStart")) return true;

    if (changeStateCondition(ctx, player->isFallingDown(), "FallingStealth")) return true;
    if (interactAction(ctx)) return true;
    return false;
  }
  void CStateStealth::updateVelocityDirecction(CContext& ctx, float dt) const
  {

      if (runBreath) {
        if (ctx.getTimeInState() < 2.8f) {
          if (volumeAudio > 0)volumeAudio -= 0.01;
          runBreath->setParameter("volumen",volumeAudio);
        }
        else {
          runBreath->stop();
          volumeAudio = 0.f;
          runBreath = nullptr;
        }

      }
      VEC3 newVel = calculateMovement3D(ctx);
      getCompPlayer(ctx)->setVelocity(newVel);
  }
#pragma endregion

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                                                          MOVEOBJ
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma region MoveObj
  bool CStateMoveObj::checkState(CContext& ctx) const {
    TCompPlayer* comp_player = getCompPlayer(ctx);

    // Player tiene la informacion de si ha entrado en el trigger de una box
    CEntity* ent_box = comp_player->getPushBox();
    if (!ent_box) {
      /*if (comp_player->getMoveBoxSound()) {
        comp_player->getMoveBoxSound()->stop();
        comp_player->nullMoveBoxSound();
      }*/
      ctx.changeToState("Idle");
      return false;
    }

    // Comprobamos con un raycast si el player puede mover la box
    if (!comp_player->canMoveTheBox()) {
      /*if (comp_player->getMoveBoxSound()) {
        comp_player->getMoveBoxSound()->stop();
        comp_player->nullMoveBoxSound();
      }*/
      ctx.changeToState("Idle");
      return false;
    }

    // Podemos mover la box
    comp_player->convertPushBox(true);
    return true;
  }

  void CStateMoveObj::startState(CContext& ctx) const {
    TCompPlayer* comp_player = getCompPlayer(ctx);
    CEntity* ent_box = comp_player->getPushBox();

    // Get box side
    VEC3 side = VEC3::Zero;
    TCompTransform* box_trans = ent_box->get<TCompTransform>();
    CEntity* ent_player = ctx.getOwner();
    TCompTransform* player_trans = ent_player->get<TCompTransform>();
    VEC3 box_pos = box_trans->getPosition();
    VEC3 player_pos = player_trans->getPosition();
    float minDist2 = VEC3::DistanceSquared(player_pos, box_pos);
    float dist2 = minDist2;

    dist2 = VEC3::DistanceSquared(player_pos, box_pos + box_trans->getFront());
    if (dist2 < minDist2) {
      side = box_trans->getFront();
      minDist2 = dist2;
    }
    dist2 = VEC3::DistanceSquared(player_pos, box_pos - box_trans->getFront());
    if (dist2 < minDist2) {
      side = -box_trans->getFront();
      minDist2 = dist2;
    }
    dist2 = VEC3::DistanceSquared(player_pos, box_pos + box_trans->getLeft());
    if (dist2 < minDist2) {
      side = box_trans->getLeft();
      minDist2 = dist2;
    }
    dist2 = VEC3::DistanceSquared(player_pos, box_pos - box_trans->getLeft());
    if (dist2 < minDist2) {
      side = -box_trans->getLeft();
      minDist2 = dist2;
    }

    // Rotate player
    comp_player->forceRotation(-side);
  }

  bool CStateMoveObj::changeState(CContext& ctx) const {
    TCompPlayer* player = getCompPlayer(ctx);
    if (input["interact"].justPressed()) {
      //dbg("Suelto la caja\n");
      /*if (player->getMoveBoxSound()) {
        player->getMoveBoxSound()->stop();
        player->nullMoveBoxSound();
      }*/
      player->convertPushBox(false);
      ctx.changeToState("Idle");
      return true;
    }
    if (player->canMoveTheBox() == false) {
      //dbg("La caja se ha soltado!\n");
     /* if (player->getMoveBoxSound()) {
        player->getMoveBoxSound()->stop();
        player->nullMoveBoxSound();
      }*/
      
      player->convertPushBox(false);
      ctx.changeToState("Idle");
      return true;
    }
    if (changeStateCondition(ctx, !player->getCollisionDown() && player->getVelocity().y < -0.8f, "Falling")) {
      /*if (player->getMoveBoxSound()) {
        player->getMoveBoxSound()->stop();
        player->nullMoveBoxSound();
      }*/
      player->convertPushBox(false);
      return true;
    }
    return false;
  }

  void CStateMoveObj::updateTransform(CContext& ctx, float dt) const {
    TCompPlayer* player = getCompPlayer(ctx);
    player->setMove3DObj(true);
    //player->move(dt);
    //player->movePushBox(dt);
  }

  void CStateMoveObj::updateVelocityDirecction(CContext& ctx, float dt) const {
    if (runBreath) {
      if (ctx.getTimeInState() < 2.8f) {
        if (volumeAudio > 0)volumeAudio -= 0.01;
        runBreath->setParameter("volume", volumeAudio);
      }
      else {
        runBreath->stop();
        volumeAudio = 0.f;
        runBreath = nullptr;
      }
    }
      TCompPlayer* player = getCompPlayer(ctx);
      VEC3 local_dir = getLocalDirMovingObj(ctx, player->getCamera());
      VEC3 newVel = local_dir * player->getModVelocity();
      player->setVelocity(newVel);
  }
#pragma endregion

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                                                            DEATHs & REBIRTH
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma region Death
  void CStateDeath::startAnimation(CContext& ctx) const {
  }

  void CStateDeath::startState(CContext& ctx) const {
    CEngine::get().getRender().fadeBlack(time_fadeOut);
    getCompPlayer(ctx)->resetMovement();
    if(runBreath)runBreath->stop();
    volumeAudio = 0.f;
    runBreath = nullptr;
    //getCompPlayer(ctx)->setRebirth(time_rebirth);
  }

  void CStateDeath::update(CContext& ctx, float dt) const {
    //updateTransform(ctx, dt);
  }
#pragma endregion

#pragma region DeathFall
  void CStateDeathFall::startAnimation(CContext& ctx) const
  {
      if (stateData.animation_name == "") return;

      CEntity* ent = ctx.getOwner();
      TCompAnimator* anim = ent->get<TCompAnimator>();

      anim->removeAllBucleActions();

      if (stateData.type_anim == "action") {
          anim->playActionAnim(stateData.animation_name, false, false, false);
      }
      else {
          anim->playCycleAnim(stateData.animation_name);
      }
  }

  void CStateDeathFall::startState(CContext& ctx) const
  {
    CEngine::get().getSound().playEvent("dieFall");
      //Finalizamos el salto para que podamos caer con la gravedad
      getCompPlayer(ctx)->finalizeJump();

      CEngine::get().getRender().fadeBlack(time_fadeOut);
  }

  void CStateDeathFall::update(CContext& ctx, float dt) const {
      updateTransform(ctx, dt);
  }
#pragma endregion

#pragma region Rebirth

  void CStateRebirth::startState(CContext& ctx) const
  {
      getCompPlayer(ctx)->resetPlayer();
  }

  void CStateRebirth::finish(CContext& ctx) const
  {
      CEngine::get().getRender().returnLight(time_fadeIn, 3);
      CEngine::get().getUI().activateWidget("blackFadeIn");
      CEngine::get().getUI().getWidgetById("blackFadeIn")->makeChildsFadeOut(0.2, 0.4, false);
      CEngine::get().getScripting().execDelayed("resurrect()", 250);
  }
#pragma endregion
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //
  //                                                          MOVEOBJ2D
  //
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma region MoveObj2D
  bool CStateMoveObj2D::checkState(CContext& ctx) const {
    

    TCompPlayer* comp_player = getCompPlayer(ctx);

    // Player tiene la informacion de si ha entrado en el trigger de una box
    CEntity* ent_box = comp_player->getPushBox();
    if (!ent_box) {
      ctx.changeToState("Idle2D");
      return false;
    }

    // Podemos mover la box
    comp_player->convertShadowPushBox(true);

    return true;
  }

  bool CStateMoveObj2D::changeState(CContext& ctx) const
  {
    TCompPlayer* player = getCompPlayer(ctx);
    // Suelto la caja
    if (input["interact"].justPressed()) {
      player->convertShadowPushBox(false);
      player->setMove2DObj(false);
      ctx.changeToState("Idle2D");
      return true;
    }

    // Me separo demasiado de la caja
    /*
    if (player->canMoveTheBox() == false) {
      player->convertShadowPushBox(false);
      ctx.changeToState("Idle2D");
      return true;
    }
    */

    // Me caigo
    if (changeStateCondition(ctx, !player->getCollisionDown() && player->getVelocity().y < -0.8f, "Falling2D")) {
      player->convertShadowPushBox(false);
      return true;
    }
    //me doy con pinchos
    if (changeStateCondition(ctx, !getCompPlayer(ctx)->isProjected(), "CheckShadow")) {
      return true;
    }
    return false;
  }

  void CStateMoveObj2D::updateVelocityDirecction(CContext& ctx, float dt) const {
    TCompPlayer* player = getCompPlayer(ctx);
    VEC3 local_dir = getLocalDirMovingObj2D(ctx, player->getCamera(), dt);
    VEC3 newVel = local_dir * player->getModVelocity();
    player->setVelocity(newVel);
  }

  void CStateMoveObj2D::updateTransform(CContext& ctx, float dt) const {
    TCompPlayer* player = getCompPlayer(ctx);
    //Mueve el jugador y la caja
    //player->moveObj2D(dt);
    player->setMove2DObj(true);
    
  }

#pragma endregion


#pragma region Interaction
  void CStateInteraction::startState(CContext& ctx) const {
    std::string animation_name = "";
    CEntity* player = ctx.getOwner();
    TCompPlayer* comp_player = player->get<TCompPlayer>();
    TCompAnimator* anim = player->get<TCompAnimator>();
    
    CEntity* e_interact = comp_player->getInteractionEntity();
    int interactionType = comp_player->getInteractionType();

    switch (interactionType) {
    case COLUMN: animation_name = "tackle"; break;
    case LIGHTBULB: {
      TCompLightbulb* lightbulb = e_interact->get<TCompLightbulb>();
      if(lightbulb->isPointer()) ctx.changeToState("FocusPointing");
      break;
    }
    case PUSH_OBJ:
      if(CEngine::get().getProjected()) ctx.changeToState("MoveObj2D");
      break;
    default: animation_name = "interact"; break;
    }


    // ANIMATION
    comp_player->setInteractAnim(animation_name);
    anim->playActionAnim(animation_name, false, false, false, false);


    // TELEPORT
    float duration = anim->getTimeAnimation(animation_name) * 0.2f;
    TCompTransform* player_trans = player->get<TCompTransform>();

    CTransform target_trans;
    if (e_interact->get<TCompPictogram>().isValid()) {
      comp_player->setTeleportVars(*player_trans, *player_trans, duration);
      return;
    }
    TCompPushObj* comp_push_obj = e_interact->get<TCompPushObj>();
    if (comp_push_obj && CEngine::get().getProjected() ) {
      comp_push_obj->setPlayerTransform();
      return;
    }
    
    TCompCollider* interact_collider = e_interact->get<TCompCollider>();
    if (!interact_collider) return;
    target_trans = interact_collider->getTriggerNearTrans();

    // TARGET POSITION
    VEC3 pos;
    bool shadow_mode = std::get<bool>(ctx.getVariable("shadow_mode")->getValue());
    if (shadow_mode) {
      pos = player_trans->getPosition();
    }
    else {
      pos = target_trans.getPosition();
      pos.y = player_trans->getPosition().y;
    }
    target_trans.setPosition(pos);

    // TARGET ROTATION
    QUAT rot180 = QUAT::CreateFromAxisAngle(VEC3::Up, deg2rad(180.f));
    target_trans.setRotation(target_trans.getRotation() * rot180);

    comp_player->setTeleportVars(*player_trans, target_trans, duration);
  }

  bool CStateInteraction::changeState(CContext& ctx) const {
    CEntity* player = ctx.getOwner();
    TCompPlayer* comp_player = player->get<TCompPlayer>();
    TCompAnimator* anim = player->get<TCompAnimator>();
    std::string current_anim = comp_player->getInteractAnim();

    if (anim->isActionAnimStop(current_anim)) {
      bool shadow_mode = std::get<bool>(ctx.getVariable("shadow_mode")->getValue());
      //std::string new_state = shadow_mode ? "Idle2D" : "Idle";
      ctx.changeToState(shadow_mode ? "Idle2D" : "Idle");
      return true;
    }
    //if (changeStateCondition(ctx,  "Idle")) return true;

    return false;
  }

  void CStateInteraction::updateInput(CContext& ctx) const {
    //Input de la flyover
    TCompPlayer* player = getCompPlayer(ctx);
    if (input["flyover_camera"].justPressed()) player->putFlyOverCamera();
    if (getCompPlayer(ctx)->isFallingDead()) ctx.changeToState("FallingDead");
    if (getCompPlayer(ctx)->isDead()) ctx.changeToState("Dead");
  }

  void CStateInteraction::updateState(CContext& ctx, float dt) const {
    TCompPlayer* comp_player = getCompPlayer(ctx);
    float t = ctx.getTimeInState();

    CTransform start, target;
    float tmax;
    comp_player->getTeleportVars(start, target, tmax);
    if (t > tmax) return;

    CEntity* player = ctx.getOwner();
    TCompTransform* player_trans = player->get<TCompTransform>();

    float ratio = t / tmax;
    VEC3 pos = lerp(start.getPosition(), target.getPosition(), ratio);
    QUAT rot = QUAT::Slerp(start.getRotation(), target.getRotation(), ratio);
    player_trans->setPosition(pos);
    player_trans->setRotation(rot);
    
    comp_player->move2trans(dt);
  }

  void CStateInteraction::finish(CContext& ctx) const {
    TCompPlayer* comp_player = getCompPlayer(ctx);
    comp_player->setInteractAnim("");
  }

#pragma endregion



#pragma region CutSceneState
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                                                          CUTSCENE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  void CStateCutscene::startState(CContext& ctx) const
  {
      TCompPlayer* comp_player = getCompPlayer(ctx);

      if (runBreath)runBreath->stop();
      volumeAudio = 0.f;
      runBreath = nullptr;
      if (comp_player->getResurrect()) {
        comp_player->setModVelocity(0.f);
        comp_player->setVelocity(VEC3::Zero);
      }else
      switch (comp_player->getLastState())
      {
      case IDLE:
          break;
      case WALK:
          comp_player->setLastState("Idle");
          comp_player->setModVelocity(0.f);
          comp_player->setVelocity(VEC3::Zero);
          break;
      case SPRINT:
          comp_player->setLastState("Idle");
          comp_player->setModVelocity(0.f);
          comp_player->setVelocity(VEC3::Zero);
          break;
      case STEALTH:
          comp_player->setLastState("Idle");
          comp_player->setModVelocity(0.f);
          comp_player->setVelocity(VEC3::Zero);
          break;
      case LONGJUMPSTART:
          comp_player->setLastState("Idle");
          comp_player->finalizeJump();
          comp_player->setModVelocity(0.f);
          comp_player->setVelocity(VEC3::Zero);
          comp_player->setInJumpStart(false);
          break;
      case LONGJUMPUP:
          comp_player->setLastState("LongJumpDesc");
          comp_player->changeTimeToJumpDesc();
          break;
      case LONGJUMPDESC:
          break;
      case LONGJUMPEND:
          comp_player->setLastState("Idle");
          comp_player->finalizeJump();
          comp_player->setModVelocity(0.f);
          comp_player->setVelocity(VEC3::Zero);
          break;
      case SHORTJUMPSTART:
          comp_player->setLastState("Idle");
          comp_player->finalizeJump();
          comp_player->setModVelocity(0.f);
          comp_player->setVelocity(VEC3::Zero);
          comp_player->setInJumpStart(false);
          break;
      case SHORTJUMPUP:
          comp_player->setLastState("ShortJumpDesc");
          comp_player->changeTimeToJumpDesc();
          break;
      case SHORTJUMPDESC:
          break;
      case SHORTJUMPEND:
          comp_player->setLastState("Idle");
          comp_player->finalizeJump();
          comp_player->setModVelocity(0.f);
          comp_player->setVelocity(VEC3::Zero);
          break;
      default:
          break;
      }

      startAnimation(ctx);    

  }

  void CStateCutscene::startAnimation(CContext& ctx) const
  {
      CEntity* player = ctx.getOwner();
      TCompPlayer* comp_player = getCompPlayer(ctx);
      TCompAnimator* anim = player->get<TCompAnimator>();
      if (comp_player->getResurrect()) {
        anim->removeCurrentAction(0.1);
        anim->playActionAnim("resurrect",false, false, false);
        anim->setVariable("velocity", 0);
      }else
      switch (comp_player->getLastState())
      {
      case IDLE:
          anim->removeCurrentAction(0.1);
          anim->playCycleAnim("idle");
          anim->setVariable("velocity", 0);
          return;
          break;
      }
  }

  void CStateCutscene::update(CContext& ctx, float dt) const
  {
      TCompPlayer* comp_player = getCompPlayer(ctx);
      CEntity* ent_player = ctx.getOwner();

      switch (comp_player->getLastState())
      {
      case LONGJUMPDESC:
          if (updateJump(ctx, dt)) {
              TCompAnimator* anim = ent_player->get<TCompAnimator>();
              anim->removeBucleAction("jump-loop");
              anim->removeBucleAction("jump-loop-inv");
              anim->playActionAnim("jump-end", false, false, false);
              comp_player->setLastState("ShortJumpEnd");
          }
          break;
      case SHORTJUMPDESC:
          if (updateJump(ctx, dt)) {
              TCompAnimator* anim = ent_player->get<TCompAnimator>();
              anim->removeBucleAction("jump-loop");
              anim->playActionAnim("jump-end", false, false, false);
              comp_player->setLastState("ShortJumpEnd");
          }
          break;
      case SHORTJUMPEND:
          TCompAnimator* anim = ent_player->get<TCompAnimator>();
          if (anim->isActionAnimStop("ShortJumpEnd")) {
              anim->setVariable("velocity", 0);
              anim->playCycleAnim("idle");
              comp_player->setLastState("Idle");
          }
          break;
      }

      if (changeState(ctx)) return;

  }

  bool CStateCutscene::updateJump(CContext& ctx, float dt) const
  {
      TCompPlayer* comp_player = getCompPlayer(ctx);
      CEntity* ent_player = ctx.getOwner();
      TCompTransform* player_trans = ent_player->get<TCompTransform>();

      VEC3 newVel = VEC3::Zero;
      VEC3 current_pos = player_trans->getPosition();
      newVel.y = comp_player->calculateJump() - current_pos.y;

      comp_player->setModVelocity(0.f);
      comp_player->setVelocity(newVel);
      /*comp_player->rotate(dt);
      comp_player->move(dt);*/

      ctx.setVariable("collision_down", comp_player->getCollisionDown());

      return comp_player->getCollisionDown();
  }

  bool CStateCutscene::changeState(CContext& ctx) const
  {
      TCompPlayer* comp_player = getCompPlayer(ctx);
      comp_player->setResurrect(false);
      if (!comp_player->isInCutscene()) {
        dbg("me salgo de cinem�tica \n");
          if (comp_player->isProjected()) {
              ctx.changeToState("Idle2D");
          }
          else {
            CEntity* ent_camera = (CEntity*)getEntityByName("camera_direction");
            TCompDirection* comp_direc = ent_camera->get<TCompDirection>();
            comp_direc->blockMovement(false);
            getObjectManager<TCompCamera3D>()->forEach([](TCompCamera3D* c) {
              c->blockMovement(false);
              });
              ctx.changeToState("Idle");
          }
          return true;
      }

      return false;
  }

#pragma endregion

#pragma region Damaged
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                                                          DAMAGED
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


  void CStateDamaged::updateJump(CContext& ctx, float dt) const
  {
      TCompPlayer* comp_player = getCompPlayer(ctx);
      CEntity* ent_player = ctx.getOwner();
      TCompTransform* player_trans = ent_player->get<TCompTransform>();

      VEC3 newVel = VEC3::Zero;
      VEC3 current_pos = player_trans->getPosition();
      newVel.y = comp_player->calculateJump() - current_pos.y;

      comp_player->setModVelocity(0.f);
      comp_player->setVelocity(newVel);
      //comp_player->rotate(dt);
      //comp_player->move(dt);
  }


  bool CStateDamaged::updateJumpUp(CContext& ctx, float dt) const
  {
      updateJump(ctx, dt);

      return getCompPlayer(ctx)->isPushingUp();
  }

  bool CStateDamaged::updateJumpDesc(CContext& ctx, float dt) const
  {
      updateJump(ctx, dt);

      //Como es posible que choquemos contra los pinchos (y por lo tanto tengamos collisionDown), esperaremos un tiempo antes de actualizar el collisionDown
      if (ctx.getTimeInState() > 0.1) {
          dbg("Ya puedo comprobar si estoy en collisionDown\n");
          TCompPlayer* comp_player = getCompPlayer(ctx);
          ctx.setVariable("collision_down", comp_player->getCollisionDown());
          return getCompPlayer(ctx)->getCollisionDown();
      }

      return false;
  }

  void CStateDamaged::startState(CContext& ctx) const
  {
      TCompPlayer* comp_player = getCompPlayer(ctx);

      string last_state = comp_player->getStringLastState();
      dbg("Acabo de entrar en damage y el ultimo estado es %s\n", last_state.c_str());
  }
  void CStateDamaged::startAnimation(CContext& ctx) const
  {
  }
  void CStateDamaged::update(CContext& ctx, float dt) const
  {
      TCompPlayer* comp_player = getCompPlayer(ctx);
      CEntity* ent_player = ctx.getOwner();

      if (comp_player->isProjected()) {
          comp_player->manualDesproject();
          return;
      }

      switch (comp_player->getLastState())
      {
      case FALLING:
          //dbg("DAMAGED FALLING\n");
          if (updateJumpDesc(ctx, dt)) {
              TCompAnimator* anim = ent_player->get<TCompAnimator>();
              anim->removeBucleAction("jump-loop");
              anim->removeBucleAction("jump-loop-inv");
              anim->playActionAnim("jump-end", false, false, false);
              comp_player->setLastState("ShortJumpEnd");
          }
          break;
      case LONGJUMPUP:
          //dbg("DAMAGED LONG JUMP UP\n");
          if (updateJumpUp(ctx, dt)) {
              comp_player->setLastState("LongJumpDesc");
          }
          break;
      case LONGJUMPDESC:
          //dbg("DAMAGED LONG JUMP DESC\n");
          if (updateJumpDesc(ctx, dt)) {
              TCompAnimator* anim = ent_player->get<TCompAnimator>();
              anim->removeBucleAction("jump-loop");
              anim->removeBucleAction("jump-loop-inv");
              anim->playActionAnim("jump-end", false, false, false);
              comp_player->setLastState("ShortJumpEnd");
          }
          break;
      case SHORTJUMPUP:
          //dbg("DAMAGED SHORT JUMP UP\n");
          if (updateJumpUp(ctx, dt)) {
              comp_player->setLastState("ShortJumpDesc");
          }
          break;
      case SHORTJUMPDESC:
          //dbg("DAMAGED SHOR JUMP DESC\n");
          if (updateJumpDesc(ctx, dt)) {
              TCompAnimator* anim = ent_player->get<TCompAnimator>();
              anim->removeBucleAction("jump-loop");
              anim->playActionAnim("jump-end", false, false, false);
              comp_player->setLastState("ShortJumpEnd");
          }
          break;
      case SHORTJUMPEND:
          //dbg("DAMAGED SHOR JUMP END\n");
          TCompAnimator* anim = ent_player->get<TCompAnimator>();
          if (anim->isActionAnimStop("ShortJumpEnd")) {
              anim->setVariable("velocity", 0);
              anim->playCycleAnim("idle");
              comp_player->setLastState("Idle");
          }
          break;
      }

      changeState(ctx);
  }
  bool CStateDamaged::changeState(CContext& ctx) const
  {
      TCompPlayer* comp_player = getCompPlayer(ctx);

      if (comp_player->finishedTimeDamaged()) {
          string last_state = comp_player->getStringLastState();
          ctx.changeToState(last_state.c_str());
          comp_player->setDamaged(false);
      }

      if (comp_player->isFallingDead()) {
          ctx.changeToState("FallingDead");
          comp_player->setDamaged(false);
      }
      if (comp_player->isDead()) {
          comp_player->setDamaged(false);
          ctx.changeToState("Dead");
      }

      return false;
  }

#pragma endregion

#pragma region Caught-Constrict

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                                                          CONSTRCIT - CAUGHT
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
//                    REPOSITION
/////////////////////////////////////////////////////////////


  void CStateReposition::startState(CContext& ctx) const
  {
      TCompPlayer* comp_player = getCompPlayer(ctx);
      CEntity* ent_player = ctx.getOwner();
      TCompAnimator* anim = ent_player->get<TCompAnimator>();
      anim->removeCurrentAction(0.0f);

      CHandle guardian = comp_player->getHandleConstrictGuardian();
      CEntity* ent_guardian = guardian;
      TCompTransform* trans_guardian = ent_guardian->get<TCompTransform>();
      TCompTransform* trans_player = ent_player->get<TCompTransform>();
      VEC3 pos_trasl = trans_guardian->getPosition() + trans_guardian->getFront() * 1.5;
      QUAT rot_trasl;

      VEC3 front_guardian = trans_guardian->getFront();
      VEC3 front_seele = trans_player->getFront();
      float res = front_guardian.Dot(front_seele);

      if (res >= 0) {
          rot_trasl = trans_guardian->getRotation();
      }
      else {
          QUAT rot180 = QUAT(0, -1, 0, 0);
          rot_trasl = trans_guardian->getRotation() * rot180;
          comp_player->setFrontConstrict(true);
      }

      CTransform trans_target;
      trans_target.setPosition(pos_trasl);
      trans_target.setRotation(rot_trasl);

      comp_player->setNotMove(true);
      comp_player->setTeleportVars(*trans_player, trans_target, 0.1);
      comp_player->setModVelocity(0.f);
      comp_player->setVelocity(VEC3::Zero);
      comp_player->setLocalDir(trans_player->getFront());


  }
  void CStateReposition::update(CContext& ctx, float dt) const
  {

      CEntity* player = ctx.getOwner();
      TCompPlayer* comp_player = getCompPlayer(ctx);
      float t = ctx.getTimeInState();

      CTransform start, target;
      float tmax_reposition;
      comp_player->getTeleportVars(start, target, tmax_reposition);

      if (t < tmax_reposition) {

          TCompTransform* player_trans = player->get<TCompTransform>();
          TCompCollider* player_col = player->get<TCompCollider>();

          float ratio = t / tmax_reposition;
          VEC3 pos = lerp(start.getPosition(), target.getPosition(), ratio);
          //dbg("Posicion Lerp en %.2f ratio [%.2f %.2f %.2f]\n",ratio, pos.x, pos.y, pos.z);
          QUAT rot = QUAT::Slerp(start.getRotation(), target.getRotation(), ratio);
          player_trans->setPosition(pos);
          player_trans->setRotation(rot);

          player_col->controller->setFootPosition(PxExtendedVec3(pos.x, pos.y, pos.z));

          //comp_player->move2trans(dt);
      }
      else {
          changeState(ctx);
      }


  }
  bool CStateReposition::changeState(CContext& ctx) const
  {
      TCompPlayer* comp_player = getCompPlayer(ctx);

      if (comp_player->isFinishedTimeAnimation()) {
          ctx.changeToState("StartConstrict");
      }

      return false;
  }

  void CStateReposition::finish(CContext& ctx) const
  {
      CEntity* player = ctx.getOwner();
      TCompPlayer* comp_player = getCompPlayer(ctx);
      TCompTransform* trans_player = player->get<TCompTransform>();
      TCompCollider* col_player = player->get<TCompCollider>();

      CHandle guardian = comp_player->getHandleConstrictGuardian();
      CEntity* ent_guardian = guardian;
      TCompTransform* trans_guardian = ent_guardian->get<TCompTransform>();
      VEC3 pos_trasl = trans_guardian->getPosition() + trans_guardian->getFront() * 1.5;
      QUAT rot_trasl;

      if (comp_player->getFrontConstrict()) {
          QUAT rot180 = QUAT(0, -1, 0, 0);
          rot_trasl = trans_guardian->getRotation() * rot180;
      }
      else {
          rot_trasl = trans_guardian->getRotation();
      }

      trans_player->setPosition(pos_trasl);
      trans_player->setRotation(rot_trasl);

      col_player->controller->setFootPosition(PxExtendedVec3(pos_trasl.x, pos_trasl.y, pos_trasl.z));

  }

  /////////////////////////////////////////////////////////////
  //                    START CONSTRICT   
  /////////////////////////////////////////////////////////////

  void CStateStartConstrict::startState(CContext& ctx) const
  {
      CEntity* player = ctx.getOwner();
      TCompPlayer* comp_player = getCompPlayer(ctx);
      TCompTransform* trans_player = player->get<TCompTransform>();
      TCompAnimator* anim = player->get<TCompAnimator>();
      CTransform trans_start;
      CTransform trans_target;

      CHandle guardian = comp_player->getHandleConstrictGuardian();
      CEntity* ent_guardian = guardian;
      TCompTransform* trans_guardian = ent_guardian->get<TCompTransform>();

      float time;

      VEC3 start_pos = trans_player->getPosition();
      trans_start.setPosition(start_pos);

      if (comp_player->getFrontConstrict()) {
          time = anim->getTimeAnimation("frontStartConstrictIni");
          anim->playActionAnim("frontStartConstrictIni", true, false, false, true);
          QUAT start_rot = trans_player->getRotation();
          trans_start.setRotation(start_rot);
          trans_target.setRotation(trans_guardian->getRotation());

          VEC3 end_pos = start_pos - trans_player->getFront() * 0.12;
          trans_target.setPosition(end_pos);
      }
      else {
          time = anim->getTimeAnimation("backStartConstrictIni");
          anim->playActionAnim("backStartConstrictIni", true, false, false, true);

          VEC3 end_pos = start_pos + trans_player->getFront() * 0.12;
          trans_target.setPosition(end_pos);
      }

      comp_player->setTeleportVars(trans_start, trans_target, 0.3);

      comp_player->initTimeAnimation(time);
  }

  void CStateStartConstrict::update(CContext& ctx, float dt) const
  {
      CEntity* player = ctx.getOwner();
      TCompPlayer* comp_player = getCompPlayer(ctx);
      TCompTransform* trans_player = player->get<TCompTransform>();
      TCompCollider* col_player = player->get<TCompCollider>();

      float t = ctx.getTimeInState();
      CTransform start, target;
      float tmax;
      comp_player->getTeleportVars(start, target, tmax);


      if (t <= tmax) {
          float ratio = t / tmax;
          VEC3 pos = lerp(start.getPosition(), target.getPosition(), ratio);

          if (comp_player->getFrontConstrict()) {
              QUAT rot = QUAT::Slerp(start.getRotation(), target.getRotation(), ratio);
              trans_player->setRotation(rot);
          }

          trans_player->setPosition(pos);
          col_player->controller->setFootPosition(PxExtendedVec3(pos.x, pos.y, pos.z));


      }
      else {
          changeState(ctx);
      }


  }

  bool CStateStartConstrict::changeState(CContext& ctx) const
  {
      TCompPlayer* comp_player = getCompPlayer(ctx);
      if (comp_player->isFinishedTimeAnimation()) {
          ctx.changeToState("Constrict");
      }
      return false;
  }

  void CStateStartConstrict::finish(CContext& ctx) const
  {
      CEntity* player = ctx.getOwner();
      TCompPlayer* comp_player = getCompPlayer(ctx);
      TCompTransform* trans_player = player->get<TCompTransform>();
      TCompCollider* col_player = player->get<TCompCollider>();

      CTransform start, target;
      float tmax;
      comp_player->getTeleportVars(start, target, tmax);


      if (comp_player->getFrontConstrict()) {
          trans_player->setRotation(target.getRotation());
      }

      trans_player->setPosition(target.getPosition());
      col_player->controller->setFootPosition(PxExtendedVec3(target.getPosition().x, target.getPosition().y, target.getPosition().z));
  }


  /////////////////////////////////////////////////////////////
  //                    CONSTRICT
  /////////////////////////////////////////////////////////////


  void CStateConstrict::startState(CContext& ctx) const
  {
  }

  void CStateConstrict::startAnimation(CContext& ctx) const
  {
      CEntity* ent_player = ctx.getOwner();
      TCompAnimator* anim = ent_player->get<TCompAnimator>();
      anim->removeCurrentAction();
      anim->playActionAnim("constrictHug", true, false, false, false);

  }

  void CStateConstrict::update(CContext& ctx, float dt) const
  {

      float t = ctx.getTimeInState();

      if (t >= 2) {
          TCompPlayer* comp_player = getCompPlayer(ctx);
          comp_player->activateSeeleCry();
          if (input["jump"].justPressed()) {
              comp_player->lowerCounter();
          }
      }

      CEntity* player = ctx.getOwner();
      TCompPlayer* comp_player = getCompPlayer(ctx);
      TCompTransform* trans_player = player->get<TCompTransform>();
      CHandle guardian = comp_player->getHandleConstrictGuardian();
      CEntity* ent_guardian = guardian;
      TCompTransform* trans_guardian = ent_guardian->get<TCompTransform>();

      if (trans_guardian->getPosition().y - trans_player->getPosition().y != 0) {
          VEC3 delta = VEC3::Zero;
          delta.y = trans_guardian->getPosition().y - trans_player->getPosition().y;

          comp_player->setExtraYRoot(delta);
          TCompSkeleton* skel_player = player->get<TCompSkeleton>();
          skel_player->setCorrectRoot(true);
          skel_player->setCorrectMoveRoot(delta);
      }

      changeState(ctx);
  }

  bool CStateConstrict::changeState(CContext& ctx) const
  {
      CEntity* player = ctx.getOwner();
      TCompPlayer* comp_player = getCompPlayer(ctx);
      TCompSkeleton* skel_player = player->get<TCompSkeleton>();

      if (comp_player->isRelease()) {
          ctx.changeToState("Release");
          comp_player->desactivatedSeeleCry();
      }

      if (getCompPlayer(ctx)->isDead()) {
          comp_player->resetConstrictVars();
          skel_player->clearCorrectRoot();
          skel_player->setCorrectRoot(false);
          ctx.changeToState("Dead");
      }

      return false;

  }

/////////////////////////////////////////////////////////////
//                    RELEASE
/////////////////////////////////////////////////////////////

  void CStateRelease::startState(CContext& ctx) const
  {
      CEntity* player = ctx.getOwner();
      TCompPlayer* comp_player = getCompPlayer(ctx);
      TCompSkeleton* skel_player = player->get<TCompSkeleton>();
      TCompCollider* col_player = player->get<TCompCollider>();
      TCompTransform* trans_player = player->get<TCompTransform>();
      CTransform trans_start, trans_root_start;
      CTransform trans_target, trans_root_target;

      comp_player->setLocalDir(trans_player->getFront());

      VEC3 start_pos_root = trans_player->getPosition();
      VEC3 end_pos_root = start_pos_root - trans_player->getFront() * 0.422;
      if (comp_player->isExtraYRoot()) {
          VEC3 delta = comp_player->getExtraYRoot();
          skel_player->setCorrectMoveRoot((end_pos_root - start_pos_root) + delta);
      }
      else {
          skel_player->setCorrectRoot(true);
          skel_player->clearCorrectRoot();
          skel_player->setCorrectMoveRoot(end_pos_root - start_pos_root);
      }

      trans_root_start.setPosition(end_pos_root - start_pos_root);
      trans_root_target.setPosition(VEC3::Zero);

      trans_start.setPosition(trans_player->getPosition());
      trans_target.setPosition(trans_player->getPosition() + trans_player->getFront() * 1.607);

      comp_player->setTeleportVars(trans_start, trans_target, 0.5);
      comp_player->setTPRootVars(trans_root_start, trans_root_target, 0.5);

  }

  void CStateRelease::startAnimation(CContext& ctx) const
  {
      CEntity* ent_player = ctx.getOwner();
      TCompPlayer* comp_player = getCompPlayer(ctx);
      TCompAnimator* anim = ent_player->get<TCompAnimator>();

      anim->removeCurrentAction(0.1);
      anim->playActionAnim("constrictHugScape", true, false, false, true);
      float time = anim->getTimeAnimation("constrictHugScape");
      comp_player->initTimeAnimation(time);


  }

  void CStateRelease::update(CContext& ctx, float dt) const
  {

      float t = ctx.getTimeInState();

      if (t >= 0 && t <= 0.5) {
          CEntity* player = ctx.getOwner();
          TCompPlayer* comp_player = getCompPlayer(ctx);
          TCompCollider* col_player = player->get<TCompCollider>();
          TCompTransform* trans_player = player->get<TCompTransform>();
          TCompSkeleton* skel_player = player->get<TCompSkeleton>();
          CTransform start, target;
          float tmax;
          comp_player->getTPRootVars(start, target, tmax);
          float ratio = t / 0.5;
          VEC3 pos_root = lerp(start.getPosition(), target.getPosition(), ratio);
          if (comp_player->isExtraYRoot()) {
              VEC3 delta = comp_player->getExtraYRoot();
              skel_player->setCorrectMoveRoot(pos_root + delta);
          }
          else {
              skel_player->setCorrectMoveRoot(pos_root);
          }


      }
      if (t > 0.5 && t <= 1.0) {
          CEntity* player = ctx.getOwner();
          TCompPlayer* comp_player = getCompPlayer(ctx);
          TCompCollider* col_player = player->get<TCompCollider>();
          TCompTransform* trans_player = player->get<TCompTransform>();
          TCompSkeleton* skel_player = player->get<TCompSkeleton>();
          CTransform start, target;
          float tmax;

          if(comp_player->isNotMove()){
              comp_player->setNotMove(false);

              if (comp_player->isExtraYRoot()) {
                  VEC3 delta = comp_player->getExtraYRoot();
                  CTransform trans_root_start, trans_root_target;
                  trans_root_start.setPosition(delta);
                  trans_root_target.setPosition(VEC3::Zero);
              }
          }

          comp_player->getTeleportVars(start, target, tmax);
          float max = 1.0 - 0.5;
          float ratio_seele = (t - 0.5) / max;
          //Movemos al player
          VEC3 pos_ori_seele = trans_player->getPosition();
          VEC3 pos_seele = lerp(start.getPosition(), target.getPosition(), ratio_seele);
          VEC3 delta = pos_seele - pos_ori_seele;
          /*trans_player->setPosition(pos_seele);
          col_player->controller->setFootPosition(PxExtendedVec3(pos_seele.x, pos_seele.y, pos_seele.z));*/
          comp_player->setVelocity(delta);

          if (comp_player->isExtraYRoot()) {
              CTransform start_root, target_root;
              comp_player->getTPRootVars(start_root, target_root, tmax);

              VEC3 pos_root = lerp(start_root.getPosition(), target_root.getPosition(), ratio_seele);
              skel_player->setCorrectMoveRoot(pos_root);
          }

      }


      changeState(ctx);
  }

  bool CStateRelease::changeState(CContext& ctx) const
  {
      TCompPlayer* comp_player = getCompPlayer(ctx);

      if (comp_player->isFinishedTimeAnimation()) {
          comp_player->setRelease(false);
          comp_player->resetConstrictVars();
          comp_player->setNotMove(false);
          ctx.changeToState("Idle");
      }
      return false;
  }

  void CStateRelease::finish(CContext& ctx) const
  {
      CEntity* player = ctx.getOwner();
      TCompPlayer* comp_player = getCompPlayer(ctx);
      TCompTransform* trans_player = player->get<TCompTransform>();
      TCompCollider* col_player = player->get<TCompCollider>();
      TCompSkeleton* skel_player = player->get<TCompSkeleton>();
      TCompAnimator* anim = player->get<TCompAnimator>();
      CTransform start, target;
      float tmax;

      anim->removeCurrentAction();
      skel_player->clearCorrectRoot();
      skel_player->setCorrectRoot(false);

      comp_player->getTeleportVars(start, target, tmax);

      /*trans_player->setPosition(target.getPosition());
      col_player->controller->setFootPosition(PxExtendedVec3(target.getPosition().x, target.getPosition().y, target.getPosition().z));*/
      VEC3 pos_ori_seele = trans_player->getPosition();
      VEC3 pos_seele = target.getPosition();
      VEC3 delta = pos_seele - pos_ori_seele;

      comp_player->setVelocity(delta);
  }


#pragma endregion

#pragma region FocusPointing

  void CStateFocusPointing::startState(CContext& ctx) const {
    TCompPlayer* comp_player = getCompPlayer(ctx);
    CEntity* e_interact = comp_player->getInteractionEntity();
    assert(e_interact);

    CEntity* e_cam = comp_player->getCamera();
    TCompLightbulb* c_lightbulb = e_interact->get<TCompLightbulb>(); 
    CEntity* e_spot = c_lightbulb->getSpot();
    
    TCompTransform* cam_trans = e_cam->get<TCompTransform>();
    TCompTransform* malla_trans = e_interact->get<TCompTransform>();
    TCompTransform* spot_trans = e_spot->get<TCompTransform>();

    VEC3 relPos(-0.15f, 0.75f, -0.6f);
    VEC3 pos = malla_trans->getPosition() +
      spot_trans->getFront() * relPos.z + spot_trans->getUp() * relPos.y + spot_trans->getLeft() * relPos.x;
    cam_trans->lookAt(pos, pos + malla_trans->getFront());
    //malla_trans->setRotation(cam_trans->getRotation());
    //spot_trans->setRotation(cam_trans->getRotation());

    float yaw, pitch, roll;
    cam_trans->getEulerAngles(&yaw, &pitch, nullptr);
    comp_player->setPointingInit(VEC2(yaw, pitch));
    malla_trans->getEulerAngles(&yaw, &pitch, &roll);
    comp_player->setPointingInitMalla(VEC3(yaw, pitch, roll));
  }

  void CStateFocusPointing::updateState(CContext& ctx, float dt) const {
    TCompPlayer* comp_player = getCompPlayer(ctx);
    CEntity* e_interact = comp_player->getInteractionEntity();
    assert(e_interact);

    CEntity* e_cam = comp_player->getCamera();
    TCompLightbulb* c_lightbulb = e_interact->get<TCompLightbulb>();
    CEntity* e_spot = c_lightbulb->getSpot();

    TCompTransform* cam_trans = e_cam->get<TCompTransform>();
    TCompTransform* malla_trans = e_interact->get<TCompTransform>();
    TCompTransform* spot_trans = e_spot->get<TCompTransform>();
    VEC2 angles = getInputYawPitch(ctx, dt);

    //VEC2 init_angles = comp_player->getPointingInit();
    //cam_trans->setEulerAngles(angles.x + init_angles.x, angles.y + init_angles.y, 0.f);
   
    VEC3 init_malla = comp_player->getPointingInitMalla();
    malla_trans->setEulerAngles(angles.x + init_malla.x, angles.y + init_malla.y, init_malla.z);

    spot_trans->setEulerAngles(angles.x + init_malla.x, angles.y + init_malla.y, 0.f);

    //La camara se actualiza en funcion de malla_trans
    VEC3 relPos(-0.15f, 0.75f, -0.6f);
    VEC3 pos = malla_trans->getPosition() +
      spot_trans->getFront() * relPos.z + spot_trans->getUp() * relPos.y + spot_trans->getLeft() * relPos.x;
    cam_trans->lookAt(pos, pos + malla_trans->getFront() * 25.f);

  }

  VEC2 CStateFocusPointing::getInputYawPitch(CContext& ctx, float dt) const {
    input::CInputModule& input = CEngine::get().getInput();

    float sensitivity;
    VEC2 delta;
    if (input["camera_x"].isPressed() or input["camera_y"].isPressed()) {
      delta = VEC2(input["camera_x"].value, input["camera_y"].value);
      sensitivity = input.getPadSensitivity();
    }
    else {
      delta = input.getMouse().deltaPosition;
      delta.x *= -1;
      sensitivity = input.getMouseSensitivity();
    }
    
    VEC2 angles = delta * sensitivity * dt;
    angles.x *= 3.f;
    //angles.y = std::clamp(angles.y, 0.f, 1.f);

    TCompPlayer* comp_player = getCompPlayer(ctx);
    comp_player->addPointingAngle(angles);

    return comp_player->getPointingAngle();
  }

#pragma endregion
  void CStateThrowObject::startState(CContext& ctx) const
  {
      CEntity* player = ctx.getOwner();
      TCompPlayer* comp_player = getCompPlayer(ctx);

      comp_player->drawThrowning(true);
  }
  void CStateThrowObject::startAnimation(CContext& ctx) const
  {
  }
  void CStateThrowObject::update(CContext& ctx, float dt) const
  {

      if (getCompPlayer(ctx)->getTwoStepsMode()) {
          if (getCompPlayer(ctx)->getThrowingStep() == 1) {
              getCompPlayer(ctx)->updateThrowing();
          }
          if (getCompPlayer(ctx)->getThrowingStep() == 2) {
              getCompPlayer(ctx)->changeParaboleTime();
          }
      }
      else {
          getCompPlayer(ctx)->updateThrowing();
      }
      updateInput(ctx);


  }
  void CStateThrowObject::updateInput(CContext& ctx) const
  {

      if (getCompPlayer(ctx)->getTwoStepsMode()) {
          if (getCompPlayer(ctx)->getThrowingStep() == 1) {
              if (input["throw"].justPressed()) {
                  getCompPlayer(ctx)->changeStepMode(2);
                  return;
              }
          }
          if (getCompPlayer(ctx)->getThrowingStep() == 2) {
              if (input["throw"].justPressed()) {
                  getCompPlayer(ctx)->changeStepMode(1);
                  getCompPlayer(ctx)->setThrowObject();
                  changeState(ctx);
                  return;
              }
          }
      }
      else {
          if (input["shadow_mode"].isPressed()) {
              changeState(ctx);
          }

          if (!input["throw"].isPressed() && ctx.getTimeInState() > 1) {
              getCompPlayer(ctx)->setThrowObject();
              changeState(ctx);
          }

          if (input.getActiveDevice() == input.GAMEPAD) {
              if (input["move_front"].value != 0) {
                  if (input["move_front"].value > 0) {
                      getCompPlayer(ctx)->changeParaboleTime(1);
                  }
                  if (input["move_front"].value < 0) {
                      getCompPlayer(ctx)->changeParaboleTime(-1);
                  }
              }
          }
          else {
              if (input.getMouse().mouseWheel != 0) {
                  if (input.getMouse().mouseWheel > 0) {
                      getCompPlayer(ctx)->changeParaboleTime(1);
                  }
                  if (input.getMouse().mouseWheel < 0) {
                      getCompPlayer(ctx)->changeParaboleTime(-1);
                  }
              }
          }
      }

  }
  bool CStateThrowObject::changeState(CContext& ctx) const
  {

      ctx.changeToState("Idle");

      CEntity* player = ctx.getOwner();
      TCompPlayer* comp_player = getCompPlayer(ctx);

      comp_player->drawThrowning(false);

      return false;
  }
}