#pragma once
#include "player.fwd.h"
#include "components/common/comp_base.h"
#include "entity/common_msgs.h"
#include "entity/entity.h"
#include "engine.h"
#include "sinn/modules/module_physics_sinn.h"
#include "geometry/interpolators.h"
#include "sound/soundEvent.h"

enum class eventArea{NEUTRAL,TUTORIAL2,TUTORIAL10,GUARDIANS, TUTORIAL11};
enum  stepArea{TEMPLE,CAVE};

class TCompPlayer : public TCompBase {

#pragma region Component
private:
  DECL_SIBLING_ACCESS();
  void updateTimers(float dt);

public:
  void load(const json& j, TEntityParseContext& ctx);
  void update(float dt);
  void debugInMenu();
  void renderDebug();
  void onEntityCreated();

  static void registerMsgs();

#pragma endregion

#pragma region Movement
private:
  SoundEvent* eventoRight = nullptr;
  SoundEvent* eventoLeft = nullptr;

  float sonidoPies = 0;
  float lastTimeSonidoPies = 0;
  float diferenciaPasos = 0;
  bool inCutscene = false;
  bool in_sprint = false;
  bool in_walk = false;
  float timer_player = 0.f;

  float time_ini_acceleration = 0.f;  // Tiempo cuando empezo acelerar
  float time_ini_deceleration = 0.f;   // Tiempo cuando empezo a desacelerar
  float time_ini_colDown = 0.f;  //Tiempo cuando empezo a caer
  float time_beforeFallingDown = 0.1f;  //Tiempo que esperaremos antes de caer cuando collision down es false
  float time_collision_up = 0.f; //Tiempo cuando he chocado con algo de tipo not_collision_up
  float time_beforeJumpDesc = 0.1f; // Tiempo que esperaremos antes de caer cuando hemos saltado y hemos chocado con algo
  float y_ini_value = 0.f;  // Valor de y al comenzar el salto

  velocityStates velState = velocityStates::STOPPED;

  VEC3 velocity = VEC3::Zero;
  VEC3 pre_vel = VEC3::Zero;
  float modVelocity = 0.f;
  VEC3 local_dir = VEC3::Zero;

  float rotation_speed = 0.f;
  float jump_strength = 0.f;

  float max_y_hor_jump = 1.8f;
  float max_y_vert_jump = 1.8f;
  //float max_jump_velocity = 1.0f;
  float time_jump = 0.8f;
  float timer_jump = 0.f;
  float addValueCollisionUp = 0.2;  //Este valor se le a�ade a la funcion que al chocar contra el techo empecemos a descender, el valor indica en que punto de la parabola acabaremos
  bool inJumpStart = false;
  float timer_jumpStart = 0;

  bool changeSecVel = false;
  bool vertical_jump = false;
  bool not_collision_up = false;
  bool old_collision_up = false;

  // Variables formulas salto
  float j_a = 0.f;
  float j_x0 = 0.f;
  float j_y0 = 0.f;
  int j_n = 2;  // Grado de la ecuacion del salto
  ///////////////////////////


  //STEP MANAGER
  VEC3 playerStepPos = VEC3::Zero;
  bool stepsActivated = false;
  SoundEvent* steps_event = nullptr;
  bool last_value_rightorleft = false;  // Se usa para reproducir solo una vez el sonido del paso
  bool rightOrLeftFootInFloor = false; // Indica cual de los dos pies ha tocado la ultima vez el suelo en una animacion
  float distStepToFloor = 0.01; //Margen que tiene de maximo el pie para que se detecte que esta pisando el suelo
  
  CModulePhysicsSinn::MoveCollisions collisions;
  bool dead = false;
  bool fallingDead = false;
  bool deadAnimation = false;
  bool firstTime = true;
  float fadeTimer = 0;
  bool showWindow = false;

  float max_fall_damage = 25.f;
  float fall_damage = 0.f;

  // Rotate in Time
  CTimer time;
  float rot_angle = 0.f;
  float rot_time = 0.f;
  const float k = 3.f;
  const float fk = 0.26988f;
  float global_acc = 1.f;
  //collect each audio and cutscene trigger that we've gone through so we can reset them when we die
  std::vector<std::string> audio_cutscene_triggers;
public:
  void setInCutscene(bool n);
  bool isInCutscene() { return inCutscene; };
  bool isWalking() { return in_walk; };
  bool isSprinting() { return in_sprint; };
  void setWalking(bool walk) { in_walk = walk; };
  void setSprinting(bool sprint) { in_sprint = sprint; };
  //add audio and cutscene triggers to recreate when dead
  void addTrigger(std::string handle) { audio_cutscene_triggers.push_back(handle); };

  void activateSteps(bool n);
  float step_area = stepArea::CAVE;
  void setStepArea(stepArea a) { step_area = a; }
  void playSteps();
  SoundEvent* getStepsEvent() { return steps_event; };

  const VEC3 getVelocity() { return velocity; }
  void setVelocity(VEC3 value) { last_velocity = velocity = value; }
  const float getModVelocity() { return modVelocity; }
  void setModVelocity(float value) { modVelocity = value; }
  const VEC3 getLocalDir() { return local_dir; }
  void setLocalDir(VEC3 value) { local_dir = value; }
  void resetMovement();




  float getIniTimeAcc() { return time_ini_acceleration; }
  float getIniTimeDec() { return time_ini_deceleration; }
  void setIniTimeAcc() { time_ini_acceleration = timer_player; }
  void setIniTimeDec() { time_ini_deceleration = timer_player; }

  velocityStates getVelState() { return velState; }
  void setVelState(velocityStates s) { velState = s; }
  float getPlayerTimer() { return timer_player; }

  float getIniTimeColDown(){ return time_ini_colDown; }
  void setIniTimeColDown() { time_ini_colDown = timer_player; }
  bool isFallingDown() { return !collisions.collision_down && timer_player - time_ini_colDown > time_beforeFallingDown; }
  void calculeLastFootInFloor();
  bool getRightFootInFloor() { return rightOrLeftFootInFloor; }

  void precalculateFuncJump();
  float calculateJump();
  void initializeJump();
  void finalizeJump();
  void changeTimeToJumpDesc();
  bool isJumping() { return collisions.jumping; }
  bool isPushingUp();
  void setInJumpStart(bool b) { inJumpStart = b; }
  ////////////////////////////////////////////////////////////////
  // Metodos para que el JumpStart en no se inicie cada vez que cambiamos de 2D a 3D
  void initJumpStart();
  bool isInJumpStart() { return inJumpStart; }
  float getTimeInJumpStart() { return timer_jumpStart; }
  ////////////////////////////////////////////////////////////////
  bool isCollisionUp();


  void setChangeSecVel(bool value) { changeSecVel = value; }
  bool getIfChangeSecVel() { return changeSecVel; }
  void setVerticalJump(bool value) { vertical_jump = value; }
  bool getVerticalJump() { return vertical_jump; }
  /*void setMaxJumpVelocity(float vel) { max_jump_velocity = vel; }
  float getMaxJumpVelocity() { return max_jump_velocity; }*/

  const bool getCollisionDown() { return collisions.collision_down; }
  void setCollisionDown(bool value) { collisions.collision_down = value; }
  void setCollisionUp(bool value) { collisions.collision_up = value; }
  void setJumpStrength(float value) { jump_strength = value; }
  const float getJumpStrenght() { return jump_strength; }
  void setNotCollisionUp(bool value) { not_collision_up = true; }

  VEC3 rotateWithValues(float dYaw, float dPitch, float dRoll, VEC3 Pos);
  void rotate(float dt);
  void rotateInTime(float dt);
  void initRotateInTime(VEC3 dst, float time);
  void forceRotation(VEC3 local_dir);
  void addAnimRot();
  void move2trans(float dt);
  void movingPlayer(float dt);
  void move(float dt);
  void moveObj2D(float dt);
#pragma endregion

#pragma region Noise
private:
  //Variables sistema generacion de ruido
  bool stealth_mode = false;
  float noise = 0.f;
  float max_noise_value = 2;
  float variation_value = 1.5;
  float change_point = 2;
  ////////////////////////////////////

  //VARIABLES PARA MOSTRAR EN EL MENU DE STATUS
  std::deque<float> qGenerated_noise; // Cola con el ruido que vamos generando
  bool windowStatus_active = false;
  CTimer timeGame = CTimer();
  bool have_been_detected = false; // Variable booleana para saber cuando hemos recibido mensaje del ruido que el enemigo detecta de nosotros
  bool detected = false; // Variable booleana para saber cuando hemos sido detectados
  float cycle = 0.1f; // segundos que pasaran para hacer un ciclo de onda
  float freq = (float)M_PI / cycle;
  float noise_values[200];

public:
  void setNoise(float velocity, float mod);
  float getNoise() { return noise; }
#pragma endregion

#pragma region Cameras
private:
  CHandle h_camera = CHandle();
  CHandle other_cam = CHandle();

  CameraData cameraData;
  int modoCamera = CAM3D;
  CModuleCameraMixer& mixer = CEngine::get().getCameraMixer();
  bool flyover_active = false;

  bool init_cameras = true;
  bool focus2shadow = true;

  bool canChangeCamera = true;  // Determina si podemos o no cambiar la camara en el mixer


  VEC2 pointingInit;
  VEC3 pointingInitMalla;
  VEC2 pointingAngle;
  VEC2 pointingLimits = VEC2(deg2rad(45.f), deg2rad(30.f));


  ///////////////////////////////////////
//Variables camaras con railes
  bool guided_mov = false;   //Modo que guia el movimiento del personaje en vez hacerlo con la camara
  VEC3 point_to_move = VEC3::Zero;  // Punto al que hay que moverse en el modo guiado
  bool reverse_guide = false; // Bool que indica si estamos yendo en sentido contrario a la guia (si pulsamos S)
  bool inRailCamera = false;
  bool needCorrectCam = false;
  float init_time_to_correct_cam = -1;  //Tiempo en el que se cambio a una camara rail, o de una camara rail
  float time_to_correct_cam = 1;  // Tiempo que ha de pasar cuando se cambie una camara por rail para corregir el angulo de la camara
  float corr_y = 0, corr_pR = 0;
  VEC3 old_localDir = VEC3::Zero; // Variable donde guardamos el viejo valor del local dir para cuando necesitamos hacer que el player se mueva en el mismo sentido de antes durante X tiempo
  float init_time_block_localDir = 0;
  float time_block_localDir = 0;
  bool block_localDir = false;
  ///////////////////////////////////////

public:
  CHandle getCamera() {
    return h_camera;
  }

  CameraData getCameraData() { return cameraData; }
  void setCameraData(CameraData _camera) { cameraData = _camera; }
  void changeCamera(CameraData fromCamera, CameraData toCamera);
  void putFlyOverCamera();

  int getModoCamera() { return modoCamera; }
  void setModoCamera(int _modoCamera) { modoCamera = _modoCamera; }

  bool focusToShadow() { return focus2shadow; }
  //void switchFocusToShadow() { focus2shadow = !focus2shadow; }
  void switchFocusToShadow();

  float getPitchCamera();
  float getYawCamera();

  void setCanChangeCamera(bool b) { canChangeCamera = b; }
  void changeOtherCam(bool b, std::string nameCam, float time);

  VEC2 getPointingInit() { return pointingInit; }
  void setPointingInit(VEC2 vec) { pointingInit = vec; }
  VEC3 getPointingInitMalla() { return pointingInitMalla; }
  void setPointingInitMalla(VEC3 vec) { pointingInitMalla = vec; }
  VEC2 getPointingAngle() { return pointingAngle; }
  void addPointingAngle(VEC2 delta);

  ///////////////////////////////////////
//Funciones camaras con railes
  void setGuideMode(bool b) { guided_mov = b; }
  void setPointToMove(VEC3 p) { point_to_move = p; }
  VEC3 getPointToMove() { return point_to_move; }
  bool getGuideMode() { return guided_mov; }
  void setReverseGuide(bool b) { reverse_guide = b; }
  bool getReverseGuide() { return reverse_guide; }
  void setInRailCamera(bool b) { inRailCamera = b; }
  bool getInRailCamera() { return inRailCamera; }
  void correctCam();
  void setCorrectCamValues(float time, float y, float p);
  void blockLocalDir(float time);
  void checkBlockLocalDir();
  bool getBlockLocalDir() { return block_localDir; }
  void setOldLocalDir(VEC3 vector) { old_localDir = vector; }
  VEC3 getOldLocalDir() { return old_localDir; }

  /////////////////////////////////////////////////

#pragma endregion

#pragma region Projection
private:
  CHandle light;
  VEC3 wall_point = VEC3::Zero;
  VEC3 wall_normal = VEC3::Zero;

  float velocity_2D_offset = 30.f;
  float velocity_2D_light = 7.f;

  float time_ini_moveSide = 0.f;
  float time_ini_moveFront = 0.f;

  bool projected = false;

  fsmStates state_before_proj = fsmStates::IDLE;

public:
  VEC3 oldInputDir2D = VEC3::Zero;
  float timeResetProject = 2;
  float timer_ResetProject = 0;


  bool project();
  void desproject();

  bool isProjected();
  void manualDesproject();

  VEC3 getWallPoint() { return wall_point; }
  VEC3 getWallNormal() { return wall_normal; }
  bool setProjectedWall(VEC3 Velocity = VEC3(0, 0, 0), float dt = 0);

  CEntity* getLight() { return light; }
  VEC3 getLight2Player();
  
  float getVelocity2D_Offset() { return velocity_2D_offset; }
  float getVelocity2D_Light() { return velocity_2D_light; }

  void setTimeIniMoveSide() { time_ini_moveSide = timer_player; }
  void setTimeIniMoveFront() { time_ini_moveFront = timer_player; }
  float getTimeIniMoveSide() { return time_ini_moveSide; }
  float getTimeIniMoveFront() { return time_ini_moveFront; }

  void setLastState(std::string s);
  fsmStates getLastState() { return state_before_proj; }
  std::string getStringLastState();
#pragma endregion

#pragma region Interaction
private:
  CHandle ent_interact = CHandle();
  int interact_type = UNDEFINED;
  std::string interact_anim = "";

  CHandle materia = CHandle();
  CHandle plant = CHandle();

  float tp_time;
  CTransform tp_start;
  CTransform tp_target;

public:
  CHandle getInteractionEntity() { return ent_interact; }
  int getInteractionType() { return interact_type; }
  void setInteractionVars(CHandle h_ent, int type) {
    ent_interact = h_ent;
    interact_type = type;
  }
  std::string getInteractAnim() { return interact_anim; }
  void setInteractAnim(std::string anim) { interact_anim = anim; }

  void getTeleportVars(CTransform& start, CTransform& target, float& time) {
    start = tp_start; target = tp_target; time = tp_time;
  }
  void setTeleportVars(CTransform start, CTransform target, float time) {
    tp_start = start; tp_target = target; tp_time = time;
    //dbg("Voy a posicionar de [%.2f %.2f %.2f] a [%.2f %.2f %.2f] en %.2f s\n", start.getPosition().x, start.getPosition().y, start.getPosition().z, target.getPosition().x, target.getPosition().y, target.getPosition().z, time);
  }

  CHandle getMateria() { return materia; };
  void setMateria(CHandle _materia) { materia = _materia; };

  void setPlant(CHandle h) { plant = h; };
  CHandle getPlant() { return plant; };

  void sendMsgCajones(std::string n_ent);

  bool tetris_ui_sound();
  bool tetris_ui_sound_first = true;

#pragma endregion

#pragma region Move Box
private:
  CHandle push_box = CHandle();
  CModulePhysicsSinn::MoveCollisions box_collisions;
  SoundEvent* move_box_sound = nullptr;
  MAT44 startPos ;
  std::string last_checkpoint_name = "checkpoint0";
  //bool intoTrigger = false;

  //Parametros para MoveObj2D
  VEC3 oldPos = VEC3(0,0,0);
  bool movePlayer;
  float diff = 0;

  bool haveMove2DObj = false;
  bool haveMove3DObj = false;


public:
  void hitBox(const TMsgPush& msg_push);
  CHandle getPushBox() { return push_box; };
  bool canMoveTheBox();
  
  SoundEvent* getMoveBoxSound() { return move_box_sound; };
  void stopBoxSound();

  void nullMoveBoxSound() { move_box_sound = nullptr; };
  void convertShadowPushBox(bool create_controller);
  void convertPushBox(bool controller);
  void movePushBox(float dt);
  bool checkPushBox();

  void setMove3DObj(bool b) { haveMove3DObj = b; }
  void setMove2DObj(bool b) { haveMove2DObj = b; }
  bool getMove2DObj() { return haveMove2DObj; }
  void setDiff(float b) { diff = b; }


#pragma endregion

#pragma region Save and Death
private:
  float current_time = 0.f;
  bool rebirth = true;
  bool reset = false;
  float time_eternalFalling = 15;
  float time_deathByFall = 2;

public:
  MAT44 getCheckPointPos() { return startPos; };
  void saveGame(const TMsgCheckpoint& msg);
  
  void onConstrict(const TMsgConstrict& msgConstrict);
  void onDead(const TMsgDeath& msgDead);
  void onFallingDead(const TMsgDeathFall& msgDead);
  bool isDead() { return dead; }
  bool isFallingDead() { return fallingDead; }
  bool isDeadAnimation() { return deadAnimation; }
  void resetPlayer();
  bool resurrect = false;
  void setResurrect(bool f) { resurrect = f; }
  bool getResurrect() { return resurrect; }
  float timer_FallingState = 0;

  float isRebirth() { return rebirth; }
  void setRebirth(float time);

  float getTimeEternalFalling() { return time_eternalFalling; }
  float getTimeDeathByFall() { return time_deathByFall; }


#pragma endregion

#pragma region OnPlatform

  bool isOnTopPlatform = false;

  lastMovement lastM;

  void OnPlatform(const TMsgPlatMove& msg);
  void moveWithPlatform(float dt);

#pragma endregion

#pragma region finalEvent

  bool final_event_active = false;
  float timer_fEv = 0;
  float fEv_time = 120;

  void initFinalEvent(const TMsgInitFinalEvent& msg);
  void updateFinalEvent();


#pragma endregion

#pragma region Debug / Test
public:
  float teleportOffset = 2.f;
  std::vector<teleportPos> teleportPositions;

  float tSpeed = 0.f;
  VEC3 ini = VEC3::Zero;
  VEC3 fin = VEC3::Zero;
  float distxSec = 0.f;
  float distMaxWalk = 0.f;
  std::string db_state = "";
  VEC3 dbg_resistance = VEC3::Zero;
  float dbg_velocity_lenght = 0.f;
  VEC3 pointToMove = VEC3::Zero;
  bool drawJump = false;
  float timer_debug_jump = 0;
  bool frenadaInmediata = false;
  bool vel2DFija = true;
  VEC3 last_velocity = VEC3::Zero;

  bool debug_velocity = false;
  float fake_velocity = 0.f;

  bool fakeJump = false;
  int fake_jump_state = 0; // 0 - JumpStart, 1 - JumpUp, 2 - JumpDesc, 3 - JumpEnd
  float timer_fake_jump = 0;
  float time_fake_jumpStart = 0.5;
  float time_fake_jumpEnd = 1.2;

  std::vector<VEC3> debug_pos_jump;
  void insertPosJump();
  void startDrawJump();
  void loadTeleportPos(const json& j);

  void updateFakeJump(float dt);

  bool isDebugVelocity() { return debug_velocity; };
  float getDebugVelocityValue() { return fake_velocity; }

#pragma endregion

#pragma region PathDebug

  std::vector<pathRecord> list_steps;

  bool startRecord = false;
  float recordTimer = 0;

  void hitWithActor(const TMsgRecordPath& msg);



#pragma endregion

#pragma region OnHit
  void OnHit(const TMsgDamage& msg);

  float timer_damaged = 0;
  float time_damage = 0.3;
  bool damaged = false;
  void setDamaged(bool b) { damaged = b; }
  bool isDamaged() { return damaged; }
  bool finishedTimeDamaged() { return timer_damaged >= time_damage; }

#pragma endregion

#pragma region Events
  bool firstTimeProjectedCave = true;
  bool firstTimeProjectedCave2 = true;
  bool firstTimeProjectedCave3 = true;
  bool firstTimeDeproject = true;
  bool firstTimeResurrect = true;
  eventArea area = eventArea::NEUTRAL;
  void onEnterArea(const TMsgEnterArea& msg);
  void onExitArea(const TMsgExitArea& msg) {
    area = eventArea::NEUTRAL;
  }
#pragma endregion

#pragma region Caught-Constrict

  private:

      SoundEvent* seele_cry;
      float vol_seele_cry = 1.f;

      CHandle h_guardian;
      CHandle h_null;

      float tp_root_time;

      CTransform tp_root_start;
      CTransform tp_root_target;

      VEC3 extra_y_vec = VEC3::Zero;
      bool is_extra_Root = false;

      bool not_move = false;
      bool initConstrict = false;
      bool constrict = false;
      bool release = false;
      float last_time = 0.f;
      bool frontConstrict = false;

      float timer_animation = 0;
      float time_animation = 0.1;

      int attemptsToFree = 0;

  public:

      void setNotMove(bool b) { not_move = b; }
      bool isNotMove() { return not_move; }
      bool isConstrict() { return constrict; }
      void setConstrict(bool b) { constrict = b; }
      bool isRelease() { return release; }
      void setRelease(bool b) { release = b; }
      void InitConstrictVars();
      void resetConstrictVars();
      CHandle getHandleConstrictGuardian() { return h_guardian; }
      void initTimeAnimation(float time);
      bool isFinishedTimeAnimation() { return timer_animation > time_animation; }
      void setFrontConstrict(bool b) { frontConstrict = b; }
      bool getFrontConstrict() { return frontConstrict; }

      void getTPRootVars(CTransform& start, CTransform& target, float& time) {
          start = tp_root_start; target = tp_root_target; time = tp_root_time;
      }
      void setTPRootVars(CTransform start, CTransform target, float time) {
          tp_root_start = start; tp_root_target = target; tp_root_time = time;
      }

      void clearExtraYRoot();
      void setExtraYRoot(VEC3 delta);
      VEC3 getExtraYRoot() { return extra_y_vec; }
      bool isExtraYRoot() {return is_extra_Root;}

      void activateSeeleCry();
      void desactivatedSeeleCry();


      void lowerCounter();

#pragma endregion  

#pragma region ThrowObject

      private:
          bool render_parabole = false;
          VEC3 throwing_point = VEC3::Zero;
          float time_parabole = 1;
          VEC3 front_object;
          float throwing_angle = 0;
          float throwing_vel = 0;

          bool throwing_two_steps = false;
          int throwing_step = 1;
          float last_pitch_value = 0;

      public:          
          void drawThrowning(bool b);
          void updateThrowing();
          void setThrowObject();
          void changeParaboleTime(int value);
          void changeParaboleTime();
          bool getTwoStepsMode() { return throwing_two_steps; }
          void changeStepMode(int val);
          int getThrowingStep() { return throwing_step; }

#pragma endregion

};
