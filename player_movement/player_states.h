#pragma once

#include "fsm/fsm_state.h"
#include "engine.h"
#include "comp_player.h"
#include "sinn/modules/module_console.h"

namespace fsm {

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                                                          GENERAL
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  struct StateData {
    CameraData camera;
    float max_velocity = 0.f;
    float secondary_max_velocity = 0.f;
    float time_acceleration = 2.f;   //Tiempo que tarda hasta acelerar maxima velocidad
    float time_deceleration = 1.f;   //Tiempo que tarda hasta desacelerar a maxima velocidad
    float noise = 0.f;
    std::string animation_name = "";
    std::string type_anim = "";

    StateData() {};
    StateData(std::string _camera_name, float _camera_time,
      float _max_velocity, float _secondary_max_velocity, float _time_acceleration, float _time_deceleration,
      float _noise, std::string _animation_name, std::string _type_anim);
  };


  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                                                          STATE PLAYER
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


  class CStatePlayer : public IState {
  public:
    virtual void load(const json& jData);
    virtual void start(CContext& ctx) const;
    virtual void update(CContext& ctx, float dt) const;
    virtual void finish(CContext& ctx) const {}
    virtual void renderInMenu() const {}


  protected:
    // start
    virtual void setState(CContext& ctx) const;
    virtual void startDebug(CContext& ctx) const;
    virtual void startStateDebug(CContext& ctx) const {}
    virtual bool checkState(CContext& ctx) const { return true; }
    virtual void startAnimation(CContext& ctx) const;
    virtual void changeCamera(CContext& ctx) const;
    virtual void startState(CContext& ctx) const {}
    
    // update
    virtual void updateDebug(CContext& ctx) const {}
    virtual bool changeState(CContext& ctx) const { return false; }
    virtual void updateInput(CContext& ctx) const;
    virtual void updateStateInput(CContext& ctx) const {}
    virtual void updateBlend(CContext& ctx) const;
    virtual void updateVelocity(CContext& ctx, float dt) const;
    virtual void updateVelocityDirecction(CContext& ctx, float dt) const {}
    virtual void updateTransform(CContext& ctx, float dt) const;
    virtual void updateState(CContext& ctx, float dt) const {}

    StateData stateData;
    input::CInputModule& input = CEngine::get().getInput();
    CModuleConsole& console = CEngine::get().getConsole();

    // Change State
    bool changeStateAction(CContext& ctx, const char* action, const char* dst) const;
    bool changeStateCondition(CContext& ctx, bool condition, const char* dst) const;
    bool interactAction(CContext& ctx) const;
    //void throwInteractEvent(CContext& ctx, CEntity* ent_interact) const;

    TCompPlayer* getCompPlayer(CContext& ctx) const;
    VEC3 getLocalDir(CContext& ctx) const;
    VEC3 getLocalDir3D2D(CContext& ctx) const;
    VEC3 getLocalDirJump2D(CContext& ctx, VEC3 light, VEC3 wallRight) const;
    VEC3 getLocalDirMovingObj(CContext& ctx, CEntity* camera) const;
    VEC3 getLocalDirMovingObj2D(CContext& ctx, CEntity* camera, float dt) const;
    VEC3 calculateDirecctionInputMoveObj2D(CContext& ctx, VEC3 lightDir, VEC3 wallDir) const;

    VEC3 calculateMovement3D(CContext& ctx) const;
    VEC3 calculateMovement3D2D(CContext& ctx) const;

    int calculateInput2D(CContext& ctx) const;
    VEC3 calculateDirecctionInput2D(CContext& ctx, VEC3 lightDir, VEC3 wallDir) const;
    VEC3 calculateMovement2D(CContext& ctx, float dt) const;

    void updateVelocityState(CContext& ctx, float max_vel) const;
    float calculatePorcAcceleration(CContext& ctx) const;
    float calculatePorcDeceleration(CContext& ctx) const;
    float calculateMaxVelInput3D(float max_vel) const;
    float calculateMaxVelInput2D(CContext& ctx, float max_vel) const;
  

  };

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                                                          MOVE3D
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  class CStateMove3D : public CStatePlayer {
  public:
    //start
    void startState(CContext& ctx) const override;
    // update
    bool changeState(CContext& ctx) const override;
    void updateVelocityDirecction(CContext& ctx, float dt) const override;
    //finish
    virtual void finish(CContext& ctx) const override;
  };

  class CStateIdle3D : public CStatePlayer {
  public:
      // update
      bool changeState(CContext& ctx) const override;
      void updateVelocityDirecction(CContext& ctx, float dt) const override;
      void updateTransform(CContext& ctx, float dt) const override;
      //finish
      virtual void finish(CContext& ctx) const override;
  };

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                                                          MOVE2D
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  class CStateMove2D : public CStatePlayer {
  public:
    // start
    void startState(CContext& ctx) const override;
    // update
    bool changeState(CContext& ctx) const override;
    void updateStateInput(CContext& ctx) const override;
    void updateVelocityDirecction(CContext& ctx, float dt) const override;
    void updateTransform(CContext& ctx, float dt) const override;
    
    virtual void finish(CContext& ctx) const override;
  };

  class CStateIdle2D : public CStatePlayer {
  public:
    // update
    bool changeState(CContext& ctx) const override;
    void updateStateInput(CContext& ctx) const override;
    void updateVelocityDirecction(CContext& ctx, float dt) const override;
    void updateTransform(CContext& ctx, float dt) const override;

    virtual void finish(CContext& ctx) const override;
  };

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //
  //                                                          JUMP3D
  //
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////
  //                    EN MOVIMIENTO
  /////////////////////////////////////////////////////////////

  class CStateJumpStart3D : public CStatePlayer {
  public:
    // start
    void startState(CContext& ctx) const override;
    void startAnimation(CContext& ctx) const override;
    // update
    bool changeState(CContext& ctx) const override;
    void updateVelocityDirecction(CContext& ctx, float dt) const override;
  };

  class CStateJumpUp3D : public CStatePlayer {
  public:
      // start
      void startAnimation(CContext& ctx) const override;
      // update
      bool changeState(CContext& ctx) const override;
      void updateVelocityDirecction(CContext& ctx, float dt) const override;
  };

  class CStateJumpDesc3D : public CStatePlayer {
  public:
      // start
      void startAnimation(CContext& ctx) const override;
      // update

      bool changeState(CContext& ctx) const override;
      void updateVelocityDirecction(CContext& ctx, float dt) const override;
      void finish(CContext& ctx) const override;
  };

  class CStateJumpEnd3D : public CStatePlayer {
  public:
      // start
      void startState(CContext& ctx) const override;
      void startAnimation(CContext& ctx) const override;
      // update
      bool changeState(CContext& ctx) const override;
      void updateVelocityDirecction(CContext& ctx, float dt) const override;
      //Finish
      void finish(CContext& ctx) const override;
  };


  /////////////////////////////////////////////////////////////
  //                    EN IDLE
  /////////////////////////////////////////////////////////////

  class CStateIdleJumpStart3D : public CStatePlayer {
  public:
      // start
      void startState(CContext& ctx) const override;
      void startAnimation(CContext& ctx) const override;
      // update
      bool changeState(CContext& ctx) const override;
      void updateVelocityDirecction(CContext& ctx, float dt) const override;
  };

  class CStateIdleJumpUp3D : public CStatePlayer {
  public:
      // start
      void startState(CContext& ctx) const override;
      void startAnimation(CContext& ctx) const override;
      // update
      bool changeState(CContext& ctx) const override;
      void updateVelocityDirecction(CContext& ctx, float dt) const override;
  };

  class CStateIdleJumpDesc3D : public CStatePlayer {
  public:
      void startAnimation(CContext& ctx) const override;
      bool changeState(CContext& ctx) const override;
      void updateVelocityDirecction(CContext& ctx, float dt) const override;
      void finish(CContext& ctx) const override;
  };

  class CStateIdleJumpEnd3D : public CStatePlayer {
  public:
      // start
      void startState(CContext& ctx) const override;
      void startAnimation(CContext& ctx) const override;
      // update
      bool changeState(CContext& ctx) const override;
      void updateVelocityDirecction(CContext& ctx, float dt) const override;
      //Finish
      void finish(CContext& ctx) const override;
  };

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                                                          JUMP2D
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////
  //                    EN MOVIMIENTO
  /////////////////////////////////////////////////////////////

  // JumpStart
  class CStateJumpStart2D : public CStatePlayer {
  public:
    // start
    void startAnimation(CContext& ctx) const override;
    void startState(CContext& ctx) const override;
    // update
    bool changeState(CContext& ctx) const override;
    void updateTransform(CContext& ctx, float dt) const override;
    void updateVelocityDirecction(CContext& ctx, float dt) const override;
    void updateStateInput(CContext& ctx) const override;
  };

  //Jump Up
  class CStateJumpUp2D : public CStatePlayer {
  public:
      // start
      void startAnimation(CContext& ctx) const override;
      // update
      bool changeState(CContext& ctx) const override;
      void updateTransform(CContext& ctx, float dt) const override;
      void updateVelocityDirecction(CContext& ctx, float dt) const override;
      void updateStateInput(CContext& ctx) const override;
  };


  //Jump Desc
  class CStateJumpDesc2D : public CStatePlayer {
  public:
      // start
      void startAnimation(CContext& ctx) const override;
      // update
      bool changeState(CContext& ctx) const override;
      void updateTransform(CContext& ctx, float dt) const override;
      void updateVelocityDirecction(CContext& ctx, float dt) const override;
      void updateStateInput(CContext& ctx) const override;
      void finish(CContext& ctx) const override;
  };

  // Jump End
  class CStateJumpEnd2D : public CStatePlayer {
  public:
      // start
      void startAnimation(CContext& ctx) const override;
      void startState(CContext& ctx) const override;
      // update
      bool changeState(CContext& ctx) const override;
      void updateTransform(CContext& ctx, float dt) const override;
      void updateVelocityDirecction(CContext& ctx, float dt) const override;
      void updateStateInput(CContext& ctx) const override;
      //Finish
      void finish(CContext& ctx) const override;
  };


  /////////////////////////////////////////////////////////////
  //                    EN IDLE
  /////////////////////////////////////////////////////////////

  class CStateIdleJumpStart2D : public CStatePlayer {
  public:
      // start
      void startState(CContext& ctx) const override;
      void startAnimation(CContext& ctx) const override;
      // update
      bool changeState(CContext& ctx) const override;
      void updateStateInput(CContext& ctx) const;
      void updateTransform(CContext& ctx, float dt) const;
      void updateVelocityDirecction(CContext& ctx, float dt) const override;
  };

  class CStateIdleJumpUp2D : public CStatePlayer {
  public:
      // start
      void startState(CContext& ctx) const override;
      void startAnimation(CContext& ctx) const override;
      // update
      bool changeState(CContext& ctx) const override;
      void updateTransform(CContext& ctx, float dt) const;
      void updateStateInput(CContext& ctx) const;
      void updateVelocityDirecction(CContext& ctx, float dt) const override;
  };

  class CStateIdleJumpDesc2D : public CStatePlayer {
  public:
      void startAnimation(CContext& ctx) const override;
      bool changeState(CContext& ctx) const override;
      void updateVelocityDirecction(CContext& ctx, float dt) const override;
      void updateStateInput(CContext& ctx) const;
      void updateTransform(CContext& ctx, float dt) const;
      void finish(CContext& ctx) const override;
  };

  class CStateIdleJumpEnd2D : public CStatePlayer {
  public:
      // start
      void startState(CContext& ctx) const override;
      void startAnimation(CContext& ctx) const override;
      // update
      bool changeState(CContext& ctx) const override;
      void updateVelocityDirecction(CContext& ctx, float dt) const override;
      void updateStateInput(CContext& ctx) const;
      void updateTransform(CContext& ctx, float dt) const;
      //Finish
      void finish(CContext& ctx) const override;
  };

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                                                          CHECKSHADOW
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  class CStateCheckShadow : public CStatePlayer {
  public:
    // start
    void startState(CContext& ctx) const override;
    void switchToSameState(CContext& ctx) const;
  };

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                                                          STEALTH
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  class CStateStealth : public CStatePlayer {
  public:
    // update
    bool changeState(CContext& ctx) const override;
    void updateVelocityDirecction(CContext& ctx, float dt) const override;
  };

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                                                          MOVE OBJECTS
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  class CStateMoveObj : public CStatePlayer {
  public:
    // start
    bool checkState(CContext& ctx) const override; // Las condiciones deberian comprobarse ANTES de cambiar de estado
    void startState(CContext& ctx) const override;
    // update
    bool changeState(CContext& ctx) const override;
    void updateVelocityDirecction(CContext& ctx, float dt) const override;
    void updateTransform(CContext& ctx, float dt) const override;
  };

  class CStateMoveObj2D : public CStatePlayer {
  public:
    // start
    bool checkState(CContext& ctx) const override;      // La he dejado comentada y que devuelva true
    void startState(CContext& ctx) const override {};   // Por que cara estamos cogiendo la box y forzar rotacion del player
    // update
    bool changeState(CContext& ctx) const override;     // A que estados podemos ir al salir de ese estado
    void updateVelocityDirecction(CContext& ctx, float dt) const override;
    void updateTransform(CContext& ctx, float dt) const override;     
  };

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                                                          DEATHS & REBIRTH
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  class CStateDeath : public CStatePlayer {
  public:
    float time_fadeOut = 2;
    // start
    void startAnimation(CContext& ctx) const override;
    void startState(CContext& ctx) const override;
    // update
    void update(CContext& ctx, float dt) const override;
  };

  class CStateDeathFall : public CStatePlayer {
  public:
      float time_fadeOut = 0.2;
      // start
      void startAnimation(CContext& ctx) const override;
      void startState(CContext& ctx) const override;
      void update(CContext& ctx, float dt) const;
  };

  class CStateRebirth : public CStatePlayer {
  public:
      float time_fadeIn = 2;
      // start
      void startState(CContext& ctx) const override;

      //finish
      void finish(CContext& ctx) const override;
  };

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                                                          No INPUT
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  class CStateInteraction : public CStatePlayer {
  public:
    // start
    void startState(CContext& ctx) const override;
    // update
    bool changeState(CContext& ctx) const override;
    void updateInput(CContext& ctx) const override;
    void updateState(CContext& ctx, float dt) const override;
    // finish
    void finish(CContext& ctx) const override;
  };

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                                                          CUTSCENE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  class CStateCutscene : public CStatePlayer {
  private:
      bool updateJump(CContext& ctx, float dt) const;
  public:
      // start
      void startState(CContext& ctx) const override;
      void startAnimation(CContext& ctx) const override;
      // update
      void update(CContext& ctx, float dt) const override;
      bool changeState(CContext& ctx) const override;
  };


  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                                                          DAMAGED
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  class CStateDamaged : public CStatePlayer {
  private:
      void updateJump(CContext& ctx, float dt) const;
      bool updateJumpUp(CContext& ctx, float dt) const;
      bool updateJumpDesc(CContext& ctx, float dt) const;
  public:
      // start
      void startState(CContext& ctx) const override;
      void startAnimation(CContext& ctx) const override;
      // update
      void update(CContext& ctx, float dt) const override;
      bool changeState(CContext& ctx) const override;
  };


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                                                          CONSTRCIT - CAUGHT
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    /////////////////////////////////////////////////////////////
    //                    REPOSITION
    /////////////////////////////////////////////////////////////

  class CStateReposition : public CStatePlayer {
  private:
      float last_time = 0.f;
  public:
      // start
      void startState(CContext& ctx) const override;
      // update
      void update(CContext& ctx, float dt) const override;
      bool changeState(CContext& ctx) const override;
      //finish
      void finish(CContext& ctx) const override;
  };


  /////////////////////////////////////////////////////////////
  //                    START CONSTRICT
/////////////////////////////////////////////////////////////

  class CStateStartConstrict : public CStatePlayer {
  private:
  public:
      // start
      void startState(CContext& ctx) const override;
      // update
      void update(CContext& ctx, float dt) const override;
      bool changeState(CContext& ctx) const override;
      //finish
      void finish(CContext& ctx) const override;
  };


    /////////////////////////////////////////////////////////////
    //                    CONSTRICT
    /////////////////////////////////////////////////////////////

  class CStateConstrict : public CStatePlayer {
  private:
  public:
      // start
      void startState(CContext& ctx) const override;
      void startAnimation(CContext& ctx) const override;
      // update
      void update(CContext& ctx, float dt) const override;
      bool changeState(CContext& ctx) const override;
  };

    /////////////////////////////////////////////////////////////
    //                    RELEASE
    /////////////////////////////////////////////////////////////

  class CStateRelease : public CStatePlayer {
  private:
  public:
      // start
      void startState(CContext& ctx) const override;
      void startAnimation(CContext& ctx) const override;
      // update
      void update(CContext& ctx, float dt) const override;
      bool changeState(CContext& ctx) const override;
      //finish
      void finish(CContext& ctx) const override;
  };

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                                          Pointing
    //
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    class CStateFocusPointing : public CStatePlayer {
    public:
        // start
        void startState(CContext& ctx) const override;
        // update
        bool changeState(CContext& ctx) const override { return false; }
        void updateInput(CContext& ctx) const override {}
        void updateState(CContext& ctx, float dt) const override;
        // finish
        void finish(CContext& ctx) const override {}

        VEC2 getInputYawPitch(CContext& ctx, float dt) const;
    };

    /////////////////////////////////////////////////////////////
//                    THROW OBJECT
/////////////////////////////////////////////////////////////

    class CStateThrowObject : public CStatePlayer {
    private:
    public:
        // start
        void startState(CContext& ctx) const override;
        void startAnimation(CContext& ctx) const override;
        // update
        void update(CContext& ctx, float dt) const override;
        void updateInput(CContext& ctx) const override;
        bool changeState(CContext& ctx) const override;
    };

}
