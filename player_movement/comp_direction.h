#pragma once
#include "components/common/comp_base.h"
#include "entity/entity.h"

#include "sinn/modules/module_physics_sinn.h"
#include "PxPhysicsAPI.h"

class TCompDirection : public TCompBase
{

	DECL_SIBLING_ACCESS();


public:
	void renderDebug();
	void debugInMenu();
	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);
	void setCurve(std::string new_curve);
	void blockMovement(bool b);
	void blockMovement(bool b, float p, float y);
	void setPitchYaw(float p, float y);
	void setPitch(float p) { _pitchRatio = p; }
	void setYaw(float y) { _yaw = y; }
	void rotateToPoint(VEC3 point);
	float getPitchRatio() { return _pitchRatio; }
	float getYaw() { return _yaw; }

	CHandle target = CHandle();
	VEC3 lastFront = VEC3::Zero;
	float _pitchRatio = 0.70f;
	float _sensitivity = 50.f;
	int cameraState = 0;


private:

	//Variables debug
	bool drawAll = false;
	bool drawCamera = false;
	bool drawCurve = false;
	bool drawPointer = false;
	bool drawRay = false;
	float sizeSphere = 1;
	/////////////////////////////


	//Variables para suavizar la transicion de la camara2D a la 3D
	float timer_projection = 0;
	float time_to_changeCamera = 0.5;
	bool without_init = true;
	/////////////////////////////
	float _maxPitch = (float)M_PI_2 - 1e-4f;

	VEC3 myTransform = VEC3::Zero;
	std::map<std::string, const CCurve*> cam_curves;
	std::string current_curve = "";
	//const CCurve* curve = nullptr;

	bool cameraFixed = false;

	bool first = true;
	VEC3 oldLookAt = VEC3::Zero;

	float _yaw = 0.f;
	float _padSensitivity = 0.05f;
	void readInput(float dt);


};

