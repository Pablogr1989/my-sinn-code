#pragma once
#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "entity/entity_parser.h"
#include "components/common/comp_collider.h"
#include <Cal3D\mixer.h>
#include "sinn/modules/module_sound.h"

using namespace std;

struct infoFinalEvent {
	float timer_event = 0.f;
	int phase = 0;
	int count_down = 180;
	CTimer time;

	void update(float dt);
};

class baseEvent {
public:
	float ini_time = -1.f;
	float end_time = -1.f;
	int phase = -1;
	bool active = false;
	bool finished = false;
	string type;

	virtual void update(float dt, infoFinalEvent* info) {}
};

class animEvent : public baseEvent {
public:
	string entity_name;
	string ani_name;
	bool ani_lock;
	string typeAnim;
	virtual void update(float dt, infoFinalEvent* info) override;
};

class changePhaseEvent : public baseEvent {
public:
	vector<string> entities_names;
	int change_to_phase = 0;
	virtual void update(float dt, infoFinalEvent* info) override;
};

class removeEvent : public baseEvent {
public:
	string entity_name;
	virtual void update(float dt, infoFinalEvent* info) override;
};

class spawnEvent : public baseEvent {
public:
	string prefabDir;
	VEC3 pos = VEC3::Zero;
	virtual void update(float dt, infoFinalEvent* info) override;
};

class relativeMovEvent : public baseEvent {
public:
	VEC3 mov = VEC3::Zero;
	VEC3 vel = VEC3::Zero;
	bool teleport = false;
	string entity_name;
	virtual void update(float dt, infoFinalEvent* info) override;
};

class absoluteMovEvent : public baseEvent {
public:
	VEC3 pos = VEC3::Zero;
	VEC3 vel = VEC3::Zero;
	bool teleport = false;
	string entity_name;
	virtual void update(float dt, infoFinalEvent* info) override;
};

class shakeCamEvent : public baseEvent {
public:
	float roll = 0.f;
	int numWaves = 0;
	virtual void update(float dt, infoFinalEvent* info) override;
};

class activateEntityEvent : public baseEvent {
public:
	vector<string> entities_names;
	virtual void update(float dt, infoFinalEvent* info) override {}
};

class cutSceneEvent : public baseEvent {
public:
	string cutSceneName;
	virtual void update(float dt, infoFinalEvent* info) override {}
};

class playSoundEvent : public baseEvent {
public:
	string soundEventName;
	virtual void update(float dt, infoFinalEvent* info) override {}
};

class playBSOEvent : public baseEvent {
public:
	string bsoEventName;
	virtual void update(float dt, infoFinalEvent* info) override {}
};

class changeGameStateEvent : public baseEvent {
public:
	string gameStateName;
	virtual void update(float dt, infoFinalEvent* info) override {}
};

class TCompFinalEvent : public TCompBase
{
public:
	void activate();
	void update(float dt);
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	void renderDebug();

	static void registerMsgs();

private:
	DECL_SIBLING_ACCESS();

	infoFinalEvent info;

	vector<baseEvent*> list_events;
	vector<changePhaseEvent> list_changePhaseEvents;

	//VARIABLES
	//////////////
	bool activated = false;
	//Debug

	bool windowBattle_active = true;


	//FUNCIONES PRIVADAS
	//////////////

	void activateOnSwitch(const TMsgSwitchActivation& msg);
	void loadChangePhase(const json& j);
	void loadAnimEvent(const json& j);
	void loadRemoveEvent(const json& j);
	void loadSpawnEvent(const json& j);
	void loadRelativeMovEvent(const json& j);
	void loadAbsoluteMovEvent(const json& j);
	void loadShakeCamEvent(const json& j);
	void loadActivateEvent(const json& j);
	void loadCutSceneEvent(const json& j);
	void loadPlaySoundEvent(const json& j);
	void loadPlayBSOEvent(const json& j);
	void loadChangeGameStateEvent(const json& j);

	float ProgressBar(const char* optionalPrefixText, float value, const float minValue, const float maxValue, const char* format, const ImVec2& sizeOfBarWithoutTextInPixels, const ImVec4& colorLeft, const ImVec4& colorRight, const ImVec4& colorBorder);





};