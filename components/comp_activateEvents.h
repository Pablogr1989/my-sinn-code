#pragma once
#include "components/common/comp_base.h"
#include "comp_interaction.h"
#include "entity/entity.h"

class TCompEventsActivator : public TCompInteraction
{
	DECL_SIBLING_ACCESS();

public:
	INIT_OWNER
	static void registerMsgs();
	void loadChild(const json& j, TEntityParseContext& ctx);
	void onChildInteraction(const TMsgOnTrigger& msg);


private:
	std::vector<std::string> ent_switched;

};