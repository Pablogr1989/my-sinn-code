#include "components/common/comp_base.h"
#include "comp_interaction.h"
#include "entity/entity.h"

class TCompActiveGuardians : public TCompBase
{
	DECL_SIBLING_ACCESS();

public:
	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);
	void renderDebug() {}
	void debugInMenu();
	void onEntityCreated() {};

	static void registerMsgs();

private:
	std::vector<std::string> guardians;
	std::vector<CHandle> handle_guardians;
	float countdown = 0;
	float timer_to_active = 0;
	bool finished = false;
	bool active = true;

	void onEnterTrigger(const TMsgOnTrigger& msg);
};

