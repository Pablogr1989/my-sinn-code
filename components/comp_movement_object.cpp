#include "mcv_platform.h"
#include "comp_movement_object.h"
#include "sinn/components/comp_audio.h"
DECL_OBJ_MANAGER("movement_object", TCompMovementObject);

void TCompMovementObject::load(const json& j, TEntityParseContext& ctx)
{

	// TIPO DE DISPARADOR AUTOMATICO, INTERACCION O ENTRANDO EN UN TRIGGER
	string sType = j.value("init_type", "auto");
	for (auto& c : sType)
	{
		c = tolower(c);
	}

	if (sType == "interaction") type = triggerType::INTERACTION;
	else if (sType == "enter_trigger") type = triggerType::ENTER_TRIGGER;
	else if (sType == "remote") type = triggerType::REMOTE;
	else type = triggerType::AUTO;


	in_loop = j.value("in_loop", false);
	if (!in_loop) {
		one_time = j.value("one_time", true);
	}



	if (j.count("moves") > 0) {
		const json& jmoves = j["moves"];
		int i = 0;
		for (const json& jmove : jmoves) {
			loadMovement(jmove, i);
			i++;
		}
	}
}


void TCompMovementObject::loadMovement(const json& j, int id)
{

	if (!j.contains("move_type") || !j.contains("value")) return;

	TMovement move;

	string sMovType = j.value("move_type", "pos");
	for (auto& c : sMovType)
	{
		c = tolower(c);
	}
	move.audio_event = j.value("audio", "");

	if (sMovType == "pos") move.type = movementType::POS;
	else if (sMovType == "mov") move.type = movementType::MOV;
	else if (sMovType == "yaw") move.type = movementType::YAW;
	else if (sMovType == "pitch") move.type = movementType::PITCH;
	else if (sMovType == "roll") move.type = movementType::ROLL;
	else if (sMovType == "rot") move.type = movementType::ROT;

	switch (move.type)
	{
	case movementType::POS:
		move.vec_value = loadVEC3(j, "value");
		break;
	case movementType::MOV:
		move.vec_value = loadVEC3(j, "value");
		break;
	case movementType::YAW:
		move.float_value = j.value("value", 0);
		break;
	case movementType::PITCH:
		move.float_value = j.value("value", 0);
		break;
	case movementType::ROLL:
		move.float_value = j.value("value", 0);
		break;
	case movementType::ROT:
		move.float_value = j.value("value", 0);
		break;
	}

	string timeOrFrames = j.value("timeOrFrame", "time");
	move.timeorframe = timeOrFrames == "time";

	if (move.timeorframe) {
		move.t_ini = j.value("ini", 0.0);
		move.t_fin = j.value("fin", 1.0);
	}
	else {
		move.f_ini = j.value("ini", 0);
		move.f_fin = j.value("fin", 60);
	}

	move.id = id;

	original_list.push_back(move);
}

void TCompMovementObject::update(float dt)
{
	last.deltaMove = VEC3::Zero;
	if (!initialized) {
		if (CApplication::get().getFramePerSecond() <= 10)	return;
		if (type == triggerType::AUTO)	initMovement();
		initialized = true;
		if (get<TCompPictogram>().isValid()) {
			pictogram = true;
		}
		TCompTransform* Orig_trans = get<TCompTransform>();
		Orig_Pos = Orig_trans->getPosition();
		Orig_Rot = Orig_trans->getRotation();
	}

	if (active) {
		if (pictogram) {
			TCompPictogram* picto = get<TCompPictogram>();
			if (picto->actor == nullptr) { playerOnTopMe = false; last.reset(); return; }
		}
		infoTime.update(dt);

		TCompCollider* col = get<TCompCollider>();
		TCompTransform* trans = get<TCompTransform>();		

		if (mov_actives <= 0) {
			if (in_loop) {
				mov_list = original_list;
				mov_actives = mov_list.size();
				infoTime.init();
			}
			else active = false;
		}

		for (auto& mov : mov_list) {
			lastMovement laux;

			if (mov.finished)	continue;  //Si ya ha terminado no necesitamos hacer nada con ella

			if (mov.active) {
				laux = mov.update(infoTime, dt, trans, col);  // Si esta activa actualizamos su movimiento
			}
			else if (mov.checkActivate(infoTime, dt, trans)) {
				laux = mov.update(infoTime, dt, trans, col);  //Si no esta actualizada comprobamos si deberia empezar y en ese caso actualizamos su movimiento
			}

			if (isFinished(mov, infoTime)){
				mov.finished = true;
				mov_actives--;  // Comprobamos si ya deberia terminar
			}

			last.deltaMove += laux.deltaMove;
			last.dYaw += laux.dYaw;
			last.dPitch += laux.dPitch;
			last.dRoll += laux.dRoll;
		}

		if (playerOnTopMe) {
			/*dbg("#############################################\n");
			dbg("PLATAFORMA\n");
			dbg("Delta Move : %f %f %f \n", last.deltaMove.x, last.deltaMove.y, last.deltaMove.z);
			dbg("Yaw %f Pitch %f Roll %f\n", last.dYaw, last.dPitch, last.dRoll);
			dbg("#############################################\n");
			
			dbg("Plataforma -> Le estoy enviando al player mi movimiento\n");*/
			if (CEngine::get().getProjected()) {
				TMsgPlatMove msg;

				VEC3 Pos;
				getObjectManager<TCompPlayer>()->forEach([&Pos](TCompPlayer* player) {
					CEntity* e = CHandle(player).getOwner();
					TCompTransform* trans = e->get<TCompTransform>();
					Pos = trans->getPosition();
					});
				TCompTransform* Origin;
				getObjectManager<TCompLight>()->forEach([&Origin](TCompLight* light) {
					if (light->CheckIfProjected()) {
						CEntity* e = CHandle(light).getOwner();
						TCompTransform* trans = e->get<TCompTransform>();
						Origin = trans;
					}
					});

				TCompTransform* c_trans = get<TCompTransform>();
				VEC3 dist_player = (Origin->getPosition() - Pos);
				VEC3 dist_obj = (Origin->getPosition() - c_trans->getPosition());
				float diff = dist_player.Dot(Origin->getFront()) / dist_obj.Dot(Origin->getFront());
				msg.Pos = c_trans->getPosition();
				msg.move = last.deltaMove * diff;
				msg.yaw = last.dYaw;
				msg.pitch = last.dPitch;
				msg.roll = last.dRoll;
				CEntity* player = h_player.getOwner();
				
				player->sendMsg(msg);

				playerOnTopMe = false;			
			}
			else {
				TCompTransform* c_trans = get<TCompTransform>();
				TMsgPlatMove msg;
				msg.Pos = c_trans->getPosition();
				msg.move = last.deltaMove;
				msg.yaw = last.dYaw;
				msg.pitch = last.dPitch;
				msg.roll = last.dRoll;
				CEntity* player = h_player.getOwner();
				player->sendMsg(msg);

				playerOnTopMe = false;
			}
		}

		last.reset();

		infoTime.last_frame = infoTime.current_frame;		
	}
}

void TCompMovementObject::renderDebug()
{
}

void TCompMovementObject::debugInMenu()
{

	switch (type)
	{
	case triggerType::AUTO:
		ImGui::LabelText("Init type", "AUTO");
		break;
	case triggerType::INTERACTION:
		ImGui::LabelText("Init type", "INTERACTION");
		break;
	case triggerType::ENTER_TRIGGER:
		ImGui::LabelText("Init type", "ENTER TRIGGER");
		break;
	case triggerType::REMOTE:
		ImGui::LabelText("Init type", "REMOTE");
		break;
	default:
		break;
	}
	ImGui::Checkbox("In loop", &in_loop);

	ImGui::Checkbox("Iniciado", &active);
	ImGui::LabelText("Timer Object", "%f", infoTime.timer_object);
	ImGui::NewLine();

	ImGui::LabelText("Last move", "");
	ImGui::LabelText("Delta Move", "%f %f %f", last.deltaMove.x, last.deltaMove.y, last.deltaMove.z);
	ImGui::LabelText("Delta yaw", "%f", last.dYaw);
	ImGui::LabelText("Delta pitch", "%f", last.dPitch);
	ImGui::LabelText("Delta roll", "%f", last.dRoll);

	for (auto mov : mov_list) {

		switch (mov.type)
		{
		case movementType::POS:
			ImGui::LabelText("Move type", "POS");
			break;
		case movementType::MOV:
			ImGui::LabelText("Move type", "MOV");
			break;
		case movementType::YAW:
			ImGui::LabelText("Move type", "YAW");
			break;
		case movementType::PITCH:
			ImGui::LabelText("Move type", "PITCH");
			break;
		case movementType::ROLL:
			ImGui::LabelText("Move type", "ROLL");
			break;
		case movementType::ROT:
			ImGui::LabelText("Move type", "ROT�");
			break;
		}

		ImGui::Checkbox("Active", &mov.active);
		ImGui::Checkbox("Finished", &mov.finished);
		ImGui::LabelText("Vec Value", "%f %f %f", mov.vec_value.x, mov.vec_value.y, mov.vec_value.z);
		ImGui::LabelText("Float value", "%f", mov.float_value);
		ImGui::LabelText("Original Time", "T_Ini %f T_Fin %f", mov.t_ini, mov.t_fin);
		ImGui::LabelText("Original Frames", "F_Ini %d F_Fin %d", mov.f_ini, mov.f_fin);
		ImGui::LabelText("Final Time", "Ini %f Fin %f", mov.ti, mov.tf);
		ImGui::LabelText("Mov Speed", "%f %f %f", mov.mov_speed.x, mov.mov_speed.y, mov.mov_speed.z );
		ImGui::LabelText("Float Speed", "%f", mov.rot_speed);
		ImGui::NewLine();		
	}
	ImGui::NewLine();
	if (ImGui::SmallButton("Iniciar")) {
		initMovement();
	}


}

void TCompMovementObject::onEntityCreated()
{
}

void TCompMovementObject::registerMsgs()
{
	DECL_MSG(TCompMovementObject, TMsgInteract, activateWithInteract);
	DECL_MSG(TCompMovementObject, TMsgActivateAniObject, activateOnEnterTrigger);
	DECL_MSG(TCompMovementObject, TMsgSwitchActivation, activateOnSwitch);
	DECL_MSG(TCompMovementObject, TMsgOnContactMovObject, isPlayerOnTopMe );
	DECL_MSG(TCompMovementObject, TMsgResetGame, resetState);
	DECL_MSG(TCompMovementObject, TMsgSaveState, SaveState);

}

bool TCompMovementObject::isFinished(TMovement mov, TObjectTimes iT)
{
	bool finished = false;

	if (mov.timeorframe) {
		finished = iT.timer_object > mov.t_fin;
	}
	else {
		finished = iT.current_frame > (iT.ini_frame + mov.f_fin);
	}

	return finished;
}

void TCompMovementObject::initMovement()
{
	bool init = true;

	// Si no esta activo, significa que ya esta haciendo el movimiento
	if (!active) {
		
		TCompAudio* audio = get<TCompAudio>();
		if (audio) {
			string name = audio->getEventName();
			audio->playEvent(name);
		}//Si no es la primera vez, comprobamos si se permite mas de una vez
		if (!first_time) {
			if (one_time)	init = false;
		}else first_time = false;

		if (init) {
			infoTime.init();
			active = true;

			calculateTimes();  // Calculamos con el framerate actual los tiempos de los movimientos que vayan en frames

			mov_list = original_list;
			mov_actives = mov_list.size();
		}
	}
}

void TCompMovementObject::calculateTimes()
{

	unsigned int fps = CApplication::get().getFramePerSecond();
	if (fps == 0) fps = 1;

	for (auto& mov : original_list) {
		if (mov.timeorframe) {
			// Si ya tenemos el tiempo en segundos, no necesitamos calcular nada
			mov.ti = mov.t_ini;
			mov.tf = mov.t_fin;
		}
		else {
			mov.ti = mov.f_ini / fps;
			mov.tf = mov.f_fin / fps;
		}
	}

}

void TCompMovementObject::activateWithInteract(const TMsgInteract& msg)
{
	if (type == triggerType::INTERACTION) {
		initMovement();
		if(one_time)	removeTriggers(msg.h_sender);
	}
}

void TCompMovementObject::activateOnEnterTrigger(const TMsgActivateAniObject& msg)
{
	if (type == triggerType::ENTER_TRIGGER && msg.trigger_enter) {
		initMovement();
		if(one_time)	removeTriggers(msg.h_sender);
	}
}

void TCompMovementObject::isPlayerOnTopMe(const TMsgOnContactMovObject& msg)
{
	CEntity* ent = msg.h_player.getOwner();
	if (ent->get<TCompPlayer>().isValid()) {
		playerOnTopMe = true;
		h_player = msg.h_player;
		//dbg("Plataforma -> El player esta encima de mi\n");
	}
}

void TCompMovementObject::removeTriggers(CHandle ent)
{
	CEntity* cent = ent;

	TCompCollider* col = cent->get<TCompCollider>();
	if (col->actor) {
		const PxU32 numShapes = col->actor->getNbShapes();
		std::vector<PxShape*> shapes;
		shapes.resize(numShapes);
		col->actor->getShapes(&shapes[0], numShapes);

		for (auto shape : shapes) {
			const char* shName = shape->getName();
			PxShapeFlags flags = shape->getFlags();
			if (flags & PxShapeFlag::eTRIGGER_SHAPE) {
				col->actor->detachShape(*shape);
			}
		}
	}
}

void TCompMovementObject::activateOnSwitch(const TMsgSwitchActivation& msg)
{
	initMovement();
}

///////////////////////////////////////////////////////////////////////////////
//					TIMERS OBJECT INFORMATION
///////////////////////////////////////////////////////////////////////////////

void TCompMovementObject::TObjectTimes::init()
{
	timer_object = 0;
	ini_frame = CApplication::get().getFrameCount();
}

void TCompMovementObject::TObjectTimes::update(float dt)
{
	timer_object += dt;
	current_frame = CApplication::get().getFrameCount();
	fps = CApplication::get().getFramePerSecond();
}

///////////////////////////////////////////////////////////////////////////////
//					MOVEMENT INFORMATION
///////////////////////////////////////////////////////////////////////////////

bool TCompMovementObject::TMovement::checkActivate(TObjectTimes iT, float dt, TCompTransform* trans)
{
	if (iT.timer_object >= ti)	active = true;

	if (active) {

		float total_time = tf - ti;

		VEC3 current_pos;

		switch (type)
		{
		case movementType::POS:
			current_pos = trans->getPosition();
			mov_speed = (vec_value - current_pos) / total_time;
			break;
		case movementType::MOV:
			mov_speed = vec_value / total_time;
			break;
		case movementType::YAW:
			rot_speed = float_value / total_time;
			break;
		case movementType::PITCH:
			rot_speed = float_value / total_time;
			break;
		case movementType::ROLL:
			rot_speed = float_value / total_time;
			break;
		case movementType::ROT:
			rot_speed = float_value / total_time;
			break;
		}
	}

	return active;
}

lastMovement TCompMovementObject::TMovement::update(TObjectTimes iT, float dt, TCompTransform* trans, TCompCollider* col)
{

	lastMovement last;

	float real_dt = dt;

	if (iT.timer_object > tf) {
		float curr_time = iT.timer_object - dt;
		real_dt = tf - curr_time;
		if (real_dt <= 0) {
			return last;
		}
	}

	VEC3 deltaMove;
	float dYaw = 0, dPitch = 0, dRoll = 0;

	switch (type)
	{
	case movementType::POS:
		deltaMove = mov_speed * real_dt;
		last.deltaMove = mov_speed;
		updateMovement(deltaMove, trans, col);
		break;
	case movementType::MOV:
		last.deltaMove = mov_speed;
		deltaMove = mov_speed * real_dt;
		updateMovement(deltaMove, trans, col);
		break;
	case movementType::YAW:
		dYaw = rot_speed * real_dt;
		last.dYaw = deg2rad(dYaw);
		updateRotation(dYaw, dPitch, dRoll, trans, col);
		break;
	case movementType::PITCH:
		dPitch = rot_speed * real_dt;
		last.dPitch = deg2rad(dPitch);
		updateRotation(dYaw, dPitch, dRoll, trans, col);
		break;
	case movementType::ROLL:
		dRoll = rot_speed * real_dt;
		last.dRoll = deg2rad(dRoll);
		updateRotation(dYaw, dPitch, dRoll, trans, col);
		break;
	case movementType::ROT:
		dYaw = deg2rad(rot_speed) * real_dt;

		float yaw, pitch, roll;
		trans->getEulerAngles(&yaw, &pitch, &roll);

		updateRotationAxis(dYaw, trans, col);

		float new_yaw, new_pitch, new_roll;
		trans->getEulerAngles(&new_yaw, &new_pitch, &new_roll);
		last.dRoll = new_roll - roll;
		last.dYaw = new_yaw - yaw;
		last.dPitch = new_pitch - pitch;
		break;
	}

	return last;
}

void TCompMovementObject::TMovement::updateMovement(VEC3 deltaMove, TCompTransform* trans, TCompCollider* col)
{
	VEC3 pos = trans->getPosition();
	pos += deltaMove;
	trans->setPosition(pos);

	if (col != nullptr && col->actor)	col->actor->setGlobalPose(PxTransform(VEC3_TO_PXVEC3(trans->getPosition()), QUAT_TO_PXQUAT(trans->getRotation())));

}

void TCompMovementObject::TMovement::updateRotation(float dYaw, float dPitch, float dRoll, TCompTransform* trans, TCompCollider* col)
{

	float yaw, pitch, roll;

	trans->getEulerAngles(&yaw, &pitch, &roll);
	yaw += deg2rad(dYaw);
	pitch += deg2rad(dPitch);
	roll += deg2rad(dRoll);
	trans->setEulerAngles(yaw, pitch, roll);


	if (col != nullptr && col->actor)	col->actor->setGlobalPose(PxTransform(VEC3_TO_PXVEC3(trans->getPosition()), QUAT_TO_PXQUAT(trans->getRotation())));

}

void TCompMovementObject::TMovement::updateRotationAxis(float dYaw, TCompTransform* trans, TCompCollider* col)
{

	trans->setRotation(trans->getRotation() * QUAT::CreateFromAxisAngle(trans->getUp(), dYaw));



	if (col != nullptr && col->actor)	col->actor->setGlobalPose(PxTransform(VEC3_TO_PXVEC3(trans->getPosition()), QUAT_TO_PXQUAT(trans->getRotation())));

}

void TCompMovementObject::resetState(const TMsgResetGame& msg)
{
	if(in_loop){
		mov_list = original_list;
		mov_actives = mov_list.size();
		infoTime.init();
		TCompTransform* trans = get<TCompTransform>();
		trans->setPosition(Orig_Pos);
		trans->setRotation(Orig_Rot);
	}
	else if (!one_time) {
		mov_list = reset_list;
		infoTime = reset_Time;
		active = reset_active;
		mov_actives = mov_list.size();
		TCompTransform* trans = get<TCompTransform>();
		trans->setPosition(Orig_Pos);
		trans->setRotation(Orig_Rot);
		TCompCollider* col = get<TCompCollider>();
		if (col != nullptr && col->actor)	col->actor->setGlobalPose(PxTransform(VEC3_TO_PXVEC3(trans->getPosition()), QUAT_TO_PXQUAT(trans->getRotation())));
	}


	
}

//To be properly tested and implemented
void TCompMovementObject::SaveState(const TMsgSaveState& msg)
{
	if (!one_time) {
		reset_list = mov_list;
		reset_Time = infoTime;
		reset_active = active;
		TCompTransform* trans = get<TCompTransform>();
		Orig_Pos = trans->getPosition();
		Orig_Rot = trans->getRotation();
	}

}