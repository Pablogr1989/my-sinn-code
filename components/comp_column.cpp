#include "mcv_platform.h"
#include "comp_column.h"
#include "input/input_module.h"
#include "engine.h"
#include "components/common/comp_transform.h"
#include "components/common/comp_aabb.h"
#include "sinn/characters/enemies/comp_enemy3D.h"
#include "sinn/modules/module_physics_sinn.h"
#include "sinn/characters/animator.h"
#include <windows\application.h>
#include "sinn/components/comp_audio.h"
#include "entity/entity_parser.h"
#include "comp_ui_interaction.h"
#include "components/common/comp_particles.h"

DECL_OBJ_MANAGER("column", TCompColumn);
using namespace physx;

void TCompColumn::registerMsgs() {
	DECL_MSG(TCompColumn, TMsgOnTrigger, onTrigger);
	DECL_MSG(TCompColumn, TMsgOnTriggerNear, onTriggerNear);
	DECL_MSG(TCompColumn, TMsgOnInteraction, onInteraction);
	DECL_MSG(TCompColumn, TMsgResetGame, resetState);
	DECL_MSG(TCompColumn, TMsgSaveState, SaveState);
	DECL_MSG(TCompColumn, TMsgDestroyStairs, destroyStair);
}

void TCompColumn::loadChild(const json& j, TEntityParseContext& ctx) {
	column_name = j.value("columnEntity", "null");
	column_name.append(ctx.root_name);
	id_bone = j.value("id_bone", 0);

	destroy_stair = j.value("stair", false);
	if (!destroy_stair) return;
	time = j.value("time", time);;
	velocity = j.value("velocity", velocity);
	distanceTrigger = j.value("distanceTrigger", distanceTrigger);
}

void TCompColumn::updateChild(float dt) {
	if (!initialize) {
		initialize = true;
		h_animCol = getEntityByName(column_name);
	}

	if (column_name == "null") return;

	if (resetting) {
		setReset(dt);
		return;
	}

	timer_fall += dt;

	if (falling && timer_fall < anim_time) {
		if (!push_sound && timer_fall > 1.4) {
			TCompAudio* audio = get<TCompAudio>();
			if (audio) audio->playEvent("pushColumn");
			push_sound = true;
		}
		if (!falling_sound && timer_fall > 1.6) {
			fallingSoundAndParticles();
		}
		if (!particle && timer_fall > (anim_time - 0.25)) {

			std::string name = entityOwner()->getName();
			if (name._Starts_with("collider_viga")) {
				CEntity* p = getEntityByName("impact_viga");
				if (!p)return;
				TCompParticles* particleFell = p->get<TCompParticles>();
				if (particleFell && !particle) {
					particleFell->startAgain();
					particle = true;
				}
			}
			if (name._Starts_with("collider_columna_escaleras")) {
				CEntity* p = getEntityByName("impact_columna");
				if (!p)return;
				TCompParticles* particleFell = p->get<TCompParticles>();
				if (particleFell && !particle) {
					particleFell->startAgain();
					particle = true;
				}
			}

		}

		CEntity* e = h_animCol;
		TCompAnimator* anim = e->get<TCompAnimator>();
		moveColumn();

		if (timer_fall >= (anim_time / 2) && !removeIdle) {
			//skel->setApplyCyclePitchRotation(true);


			anim->removeCycleAnim("idle");
			removeIdle = true;
		}
		float t = timer_fall + 0.4;

		if (t >= anim_time && !sound) {
			sound = true;
			TCompAudio* audio = get<TCompAudio>();
			if (audio) {
				audio->getSoundEvent()->stop(true);
				audio->playEvent("columnFell");
			}
			std::string name = entityOwner()->getName();
			if (name._Starts_with("collider_columna_escaleras")) {
				CEngine::get().getSound().stopBSO();
			}
		}
	}
	else {
		if (falling) {

			CEntity* e = h_animCol;
			TCompAnimator* anim = e->get<TCompAnimator>();
			anim->playCycleAnim("idleFall");

			falling = false;
			fallen = true;
		}
		else {
			CEntity* e = h_animCol;
			TCompSkeleton* skel = e->get<TCompSkeleton>();
			TCompTransform* trans = get<TCompTransform>();
			TCompCollider* col = get<TCompCollider>();

			trans->setPosition(skel->getAbsTraslationBone(id_bone));
			if (col->actor)	col->actor->setGlobalPose(PxTransform(VEC3_TO_PXVEC3(trans->getPosition()), QUAT_TO_PXQUAT(trans->getRotation())));
		}
	}


	if (startDestroy) {

		if (current_time < 0.0f) {
			current_time += dt;
		}
		//The destruction has began(Overlap of a sphere)
		else if (current_time < time) {
			PxOverlapBufferN<100> hit;            // [out] Overlap results
			PxReal radius = velocity * (current_time - 1.0f);
			current_time += dt;
			PxSphereGeometry overShape = PxSphereGeometry(radius);  // [in] shape to test for overlaps
			PxQueryFilterData fd;
			fd.data.word0 = CModulePhysicsSinn::FilterGroup::Escalera;
			bool status = CEngine::get().getPhysics().gScene->overlap(overShape, shapePose, hit, fd);
			if (status) {
				for (PxU32 i = 0; i < hit.getNbAnyHits(); i++)
				{
					const PxOverlapHit& overlapHit = hit.getAnyHit(i);

					CHandle h_collider;
					h_collider.fromVoidPtr(overlapHit.actor->userData);
					TCompCollider* c_collider = h_collider;
					if (c_collider != nullptr)
					{
						if (overlapHit.shape->getQueryFilterData().word0 & CEngine::get().getPhysics().Escalera) {
							CEntity* e = h_collider.getOwner();
							TMsgFall msg;
							e->sendMsg(msg);

							////Wake up actor(send message so it wakes up on update)
							//PxRigidDynamic* actor = c_collider->actor->is<PxRigidDynamic>();
							//actor->setRigidBodyFlag(PxRigidBodyFlag::eKINEMATIC, false);
							//actor->wakeUp();
							////ToWakeUp.push_back(h_collider);
							////h_collider.getOwner().destroy();
						}
					}
				}
			}
		}
	}
}

void TCompColumn::debugInMenuChild() {
	ImGui::Text("Anim_time : %.4f", anim_time);
	ImGui::Text("Timer fall : %.4f", timer_fall);
	ImGui::Checkbox("Falling", &falling);

	if (ImGui::SmallButton("Tirar Columna")) {
		onChildInteraction();

		CEntity* materia = getEntityByName("materia");
		if (materia != NULL) {
			TCompTransform* m_trans = materia->get<TCompTransform>();
			original_yPos = m_trans->getPosition().y;
		}
	}

	if (ImGui::SmallButton("Eliminar trigger")) {
		TCompCollider* col = get<TCompCollider>();
		if (col->actor) {
			const PxU32 numShapes = col->actor->getNbShapes();
			std::vector<PxShape*> shapes;
			shapes.resize(numShapes);
			col->actor->getShapes(&shapes[0], numShapes);

			for (auto shape : shapes) {
				const char* shName = shape->getName();
				PxShapeFlags flags = shape->getFlags();
				if (flags & PxShapeFlag::eTRIGGER_SHAPE) {
					col->actor->detachShape(*shape);
				}
			}
		}
	}

	if (ImGui::SmallButton("Reiniciar columna")) {
		resetting = true;
		current_time = 0.0f;
		timer_resetting = 0.0f;

		CEntity* e = h_animCol;
		TCompAnimator* anim = e->get<TCompAnimator>();
		anim->removeCurrentAction();
		anim->removeAllCycles();
		anim->playCycleAnim("idle");
	}
}

void TCompColumn::onChildInteraction(){
	CEntity* e = h_animCol;

	TCompAnimator* anim = e->get<TCompAnimator>();
	TCompSkeleton* skel = e->get<TCompSkeleton>();

	anim->playCycleAnim("idle");
	anim->playActionAnim("fall", true, false, false);
	
	if(s)s->stop();
	
	anim_time = anim->getTimeAnimation("fall");
	timer_fall = 0;
	falling = true;
	removeIdle = false;
	sound = false;
	falling_sound = false;
	push_sound = false;
	particle = false;
	TCompCollider* comp_collider = get<TCompCollider>();
	comp_collider->destroyTriggers();
	
}



void TCompColumn::moveColumn()
{
	CEntity* e = h_animCol;
	TCompSkeleton* skel = e->get<TCompSkeleton>();
	TCompTransform* trans = get<TCompTransform>();
	TCompCollider* col = get<TCompCollider>();

	VEC3 pos = skel->getAbsTraslationBone(id_bone);
	QUAT rot = skel->getAbsRotationBone(id_bone);

	trans->setPosition(pos);

	MAT44 mtx = MAT44::CreateFromQuaternion(rot) * MAT44::CreateTranslation(pos);

	VEC3 v = returnVector(skel->getTypeFront(), mtx);
	VEC3 left = returnVector(skel->getTypeLeft(), mtx);


	float yaw, pitch, roll = 0.f;
	///////////////////////////////////////
	//trans->getEulerAngles(&yaw, &pitch, nullptr);
	yaw = atan2f(v.x, v.z);
	float mdo = sqrtf(v.x * v.x + v.z * v.z);
	pitch = atan2f(-v.y, mdo);

	VEC3 roll_zero_left = VEC3(0, 1, 0).Cross(v);
	VEC3 roll_zero_up = VEC3(v).Cross(roll_zero_left);
	VEC3 my_real_left = left;
	my_real_left.Normalize();
	roll_zero_left.Normalize();
	roll_zero_up.Normalize();
	float rolled_left_on_up = my_real_left.Dot(roll_zero_up);
	float rolled_left_on_left = my_real_left.Dot(roll_zero_left);
	roll = atan2f(rolled_left_on_up, rolled_left_on_left);

	//////////////////////////////////////////
	trans->setEulerAngles(yaw, pitch, roll);

	VEC3 aux = trans->getPosition();

	//dbg("Column: yaw %f pitch %f roll %f pos %f,%f,%f\n", yaw, pitch, roll, aux.x, aux.y, aux.z);

	col->actor->setGlobalPose(PxTransform(VEC3_TO_PXVEC3(trans->getPosition()), QUAT_TO_PXQUAT(trans->getRotation())));
}

VEC3 TCompColumn::returnVector(CalMixer::vecT type, MAT44 mtx)
{
	switch (type) {
	case CalMixer::vecT::FRONT:
		return -mtx.Forward();
	case CalMixer::vecT::NFRONT:
		return mtx.Forward();
	case CalMixer::vecT::LEFT:
		return -mtx.Left();
	case CalMixer::vecT::NLEFT:
		return mtx.Left();
	case CalMixer::vecT::UP:
		return mtx.Up();
	case CalMixer::vecT::NUP:
		return -mtx.Up();
	}
}

void TCompColumn::resetState(const TMsgResetGame& msg)
{
	if (!old_fallen) {
		resetting = true;
		current_time = 0.0f;
		timer_resetting = 0.0f;
		CEntity* e = h_animCol;
		TCompAnimator* anim = e->get<TCompAnimator>();
		TCompCollider* col = get<TCompCollider>();
		reactivate();
		col->recreateTriggers();
		anim->removeCurrentAction();
		anim->removeAllCycles();
		anim->playCycleAnim("idle");
		startDestroy = false;
		sound = false;
		falling_sound = false;
		particle = false;
		push_sound = false;
		//TCompUIInteraction* UI = get<TCompUIInteraction>();
		//UI->Reactivate();
	}

}

void TCompColumn::setReset(float dt)
{
	timer_resetting += dt;

	if (timer_resetting >= time_resetting) {
		resetting = false;
		return;
	}

	moveColumn();

}

//To be properly tested and implemented
void TCompColumn::SaveState(const TMsgSaveState& msg)
{
	if (fallen) {
		destroy = true;
	}
	old_fallen = fallen;

}

void TCompColumn::destroyStair(const TMsgDestroyStairs& msg)
{
	if (destroy_stair) {
		startDestroy = true;
		TCompTransform* trans = get<TCompTransform>();
		shapePose = toPxTransform(CTransform(*trans));
		shapePose.p += VEC3_TO_PXVEC3(VEC3(-4.612, 0, 5.300));
	}
}

void TCompColumn::fallingSoundAndParticles()
{
	TCompAudio* audio = get<TCompAudio>();
	if (audio) {
		audio->playEvent("columnFalling");
		audio->playEvent("columnRocks");
	}

	string aux = entityOwner()->getName();
	string name = "dust_rocks_" + aux;
	CEntity* particles = getEntityByName(name);
	TCompParticles* p = particles->get<TCompParticles>();
	p->startAgain();
	falling_sound = true;
}
