#pragma once
#include "components/common/comp_base.h"
#include "comp_interaction.h"
#include "entity/entity.h"

enum class switchChCamModes { EXIT, ENTER, TEMP };

class TCompChangeCamera : public TCompInteraction
{
	DECL_SIBLING_ACCESS();

public:
	INIT_OWNER
	static void registerMsgs();
	void loadChild(const json& j, TEntityParseContext& ctx);
	void updateChild(float dt);
	void debugInMenuChild();

	void setType() { type = CHANGE_CAMERA; }

	void changeCamera(const TMsgOnTrigger& msg);
	void activateOnSwitch(const TMsgSwitchActivation& msg);

	void activateOnProjection(const TMsgProject& msg);


private:
	switchChCamModes swMode = switchChCamModes::EXIT;

	//Propiedades generales
	float time;
	std::string target_camera;
	bool block_mov = false;
	bool active = true;

	//Propiedades tipo EXIT/ENTER
	bool lock_camera;
	std::string typePath;
	bool guide_mov = false;
	float yaw = 0, pitchRatio = 0;
	bool block_withYawPitch = false;
	float time_block_dir = 0;
	bool correctCam = false;
	float corr_y, corr_pR;
	bool reset = false;

	//Propiedades tipo TEMP
	float time_cooldown = 0;
	bool finished = true;
	float timer_cooldown = 0;

	//Propiedades projection
	bool first = true;
	void changeEnterExitCamera();
	void changeTempCamera();

};