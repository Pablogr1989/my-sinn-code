#pragma once
#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "entity/entity_parser.h"
#include "components/common/comp_collider.h"
#include <Cal3D\mixer.h>

using namespace std;

class TCompAnimationObject : public TCompBase
{
	DECL_SIBLING_ACCESS();

public:

	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);
	void renderDebug();
	void debugInMenu();
	void onEntityCreated();
	static void registerMsgs();

private:
	
	// OPCIONES
	triggerType type = triggerType::AUTO;  // Tipo de evento que inicia la animacion
	CHandle mesh_handle; // Es la entidad que tiene la animacion
	string mesh_name = ""; // Nombre de la entidad
	string ani_name = "";  // El nombre de la animacion que se reproducira
	float ani_time = 0; // Duracion de la animacion que va a reproducir el objeto en cuestion
	string move_bone_name = "";
	int move_bone_id = 0;

	bool withOutMovement = false;
	bool finished = false;

	bool in_loop = true;  // Si la animacion se reproducira en bucle o ser� un action
	bool action_lock = false;  // Si la animacion action se quedara congelada una vez termine
	bool one_time = true;

	// VARIABLES
	bool initialized = false;
	bool activated = false;
	VEC3 last_pos = VEC3::Zero;
	float last_yaw = 0;
	float last_pitch = 0;
	float last_roll = 0;
	float timer_anim = 0;

	float time_to_reactivate = 0;
	float timer_reactivate = 0;


	void activate();

	VEC3 returnVector(CalMixer::vecT type, MAT44 mtx);
	void getRootEuerAngles(float* yaw, float* pitch, float* roll);
	VEC3 getRootPos();

	void activateWithInteract(const TMsgInteract& msg);
	void activateOnEnterTrigger(const TMsgActivateAniObject& msg);
	void activateOnSwitch(const TMsgSwitchActivation& msg);
	void removeTriggers(CHandle ent);

};

