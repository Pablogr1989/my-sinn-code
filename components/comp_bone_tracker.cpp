#include "mcv_platform.h"
#include "comp_bone_tracker.h"
#include "sinn/characters/animator.h"

DECL_OBJ_MANAGER("bone_tracker", TCompBoneTracker);

void TCompBoneTracker::load(const json& j, TEntityParseContext& ctx)
{
	mesh_name = j.value("skel_name_entity", "");
	assert(mesh_name != "");

	move_bone_name = j.value("movement_bone_name", "");

	if (j.count("parent")) {
		parent = true;
		name_parent = j.value("parent", name_parent);
	}

	if (j.count("auto")) {
		activated = true;
	}
}

void TCompBoneTracker::update(float dt)
{

	if (!initialized) {
		initialized = true;

		mesh_handle = getEntityByName(mesh_name);

		CEntity* e = mesh_handle;
		TCompSkeleton* skel = e->get<TCompSkeleton>();

		if (move_bone_name != "") {
			move_bone_id = skel->getIdBone(move_bone_name);
		}

		if (parent) {
			TCompTransform* h_my_transform = get<TCompTransform>();

			CHandle h_parent = getEntityByName(name_parent);
			CEntity* e_parent = h_parent;
			TCompTransform* h_parent_transform = e_parent->get<TCompTransform>();

			// My parent world transform
			TCompTransform* c_parent_transform = h_parent_transform;
			if (!c_parent_transform)
				return;

			// My Sibling comp transform
			TCompTransform* c_my_transform = h_my_transform;
			assert(c_my_transform);

			// Combine the current world transform with my 
			c_my_transform->set(c_parent_transform->combineWith(*c_my_transform));
		}

	}

	if (!activated) {
		last_pos = getRootPos();
		getRootEuerAngles(&last_yaw, &last_pitch, &last_roll);
	}

	if (activated) {

		//Obtenemos la informacion de la posicion y rotacion actual
		VEC3 newPos = getRootPos();
		float newYaw, newPitch, newRoll;
		getRootEuerAngles(&newYaw, &newPitch, &newRoll);

		//Calcaulamos el desplazamiento y rotacion desde el anterior frame
		VEC3 deltaMove = last_pos - newPos;
		float dyaw = last_yaw - newYaw;
		float dpitch = last_pitch - newPitch;
		float droll = last_roll - newRoll;

		TCompTransform* trans = get<TCompTransform>();

		float yaw, pitch, roll;
		VEC3 pos = trans->getPosition();

		trans->getEulerAngles(&yaw, &pitch, &roll);
		yaw -= dyaw;
		pitch -= dpitch;
		roll -= droll;

		pos -= deltaMove;

		trans->setPosition(pos);
		trans->setEulerAngles(yaw, pitch, roll);
		if (get<TCompCollider>().isValid()) {
			TCompCollider* col = get<TCompCollider>();
			PxTransform PxTrans = toPxTransform(*trans);
			col->actor->setGlobalPose(PxTrans);
		}

		last_pos = newPos;
		last_yaw = newYaw;
		last_pitch = newPitch;
		last_roll = newRoll;

	}

}

void TCompBoneTracker::renderDebug()
{
}

void TCompBoneTracker::debugInMenu()
{
	ImGui::LabelText("Nombre entidad", mesh_name.c_str());
	ImGui::LabelText("Nombre move bone", move_bone_name.c_str());
	ImGui::Checkbox("Activado", &activated);

	if (ImGui::SmallButton("Activar")) {
		if (!activated) activate();
	}

}

void TCompBoneTracker::onEntityCreated()
{
}

void TCompBoneTracker::registerMsgs()
{
	DECL_MSG(TCompBoneTracker, TMsgInteract, activateWithInteract);
	DECL_MSG(TCompBoneTracker, TMsgActivateAniObject, activateOnEnterTrigger);
	DECL_MSG(TCompBoneTracker, TMsgSwitchActivation, activateOnSwitch);
}

void TCompBoneTracker::activate()
{

	if (activated)	return;

	activated = true;

	CEntity* e = mesh_handle;

	timer_anim = 0;

}

VEC3 TCompBoneTracker::returnVector(CalMixer::vecT type, MAT44 mtx)
{
	switch (type) {
	case CalMixer::vecT::FRONT:
		return -mtx.Forward();
	case CalMixer::vecT::NFRONT:
		return mtx.Forward();
	case CalMixer::vecT::LEFT:
		return -mtx.Left();
	case CalMixer::vecT::NLEFT:
		return mtx.Left();
	case CalMixer::vecT::UP:
		return mtx.Up();
	case CalMixer::vecT::NUP:
		return -mtx.Up();
	}
}

void TCompBoneTracker::getRootEuerAngles(float* yaw, float* pitch, float* roll)
{
	CEntity* e = mesh_handle;
	TCompSkeleton* skel = e->get<TCompSkeleton>();

	VEC3 pos = skel->getAbsTraslationBone(move_bone_id);
	QUAT rot = skel->getAbsRotationBone(move_bone_id);

	MAT44 mtx = MAT44::CreateFromQuaternion(rot) * MAT44::CreateTranslation(pos);

	VEC3 v = returnVector(skel->getTypeFront(), mtx);
	VEC3 left = returnVector(skel->getTypeLeft(), mtx);

	*yaw = atan2f(v.x, v.z);
	float mdo = sqrtf(v.x * v.x + v.z * v.z);
	*pitch = atan2f(-v.y, mdo);

	VEC3 roll_zero_left = VEC3(0, 1, 0).Cross(v);
	VEC3 roll_zero_up = VEC3(v).Cross(roll_zero_left);
	VEC3 my_real_left = left;
	my_real_left.Normalize();
	roll_zero_left.Normalize();
	roll_zero_up.Normalize();
	float rolled_left_on_up = my_real_left.Dot(roll_zero_up);
	float rolled_left_on_left = my_real_left.Dot(roll_zero_left);
	*roll = atan2f(rolled_left_on_up, rolled_left_on_left);
}

VEC3 TCompBoneTracker::getRootPos()
{
	CEntity* e = mesh_handle;
	TCompSkeleton* skel = e->get<TCompSkeleton>();

	return skel->getAbsTraslationBone(move_bone_id);
}

void TCompBoneTracker::activateWithInteract(const TMsgInteract& msg)
{
	activate();
}

void TCompBoneTracker::activateOnEnterTrigger(const TMsgActivateAniObject& msg)
{
	activate();
}

void TCompBoneTracker::activateOnSwitch(const TMsgSwitchActivation& msg) {
	activate();
}



