#pragma once
#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "entity/entity_parser.h"
#include <Cal3D\mixer.h>

using namespace std;

class TCompBoneTracker : public TCompBase
{
	DECL_SIBLING_ACCESS();

public:

	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);
	void renderDebug();
	void debugInMenu();
	void onEntityCreated();
	static void registerMsgs();

private:

	// OPCIONES
	CHandle mesh_handle; // Es la entidad que tiene la animacion
	string mesh_name = ""; // Nombre de la entidad

	string move_bone_name = "";
	int move_bone_id = 0;

	// VARIABLES
	bool initialized = false;
	bool activated = false;
	VEC3 last_pos = VEC3::Zero;
	float last_yaw = 0;
	float last_pitch = 0;
	float last_roll = 0;
	float timer_anim = 0;

	bool parent = false;
	string name_parent = "";

	void activate();

	VEC3 returnVector(CalMixer::vecT type, MAT44 mtx);
	void getRootEuerAngles(float* yaw, float* pitch, float* roll);
	VEC3 getRootPos();

	void activateWithInteract(const TMsgInteract& msg);
	void activateOnEnterTrigger(const TMsgActivateAniObject& msg);
	void activateOnSwitch(const TMsgSwitchActivation& msg);
};

