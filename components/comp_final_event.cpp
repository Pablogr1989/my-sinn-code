#include "mcv_platform.h"
#include "comp_final_event.h"
#include "components/common/comp_transform.h"
#include "sinn/modules/module_physics_sinn.h"
#include "sinn/characters/animator.h"
#include "engine.h"
#include "modules/module_camera_mixer.h"
#include "sinn/modules/module_cutscene.h"

DECL_OBJ_MANAGER("final_event", TCompFinalEvent);


void TCompFinalEvent::update(float dt) {

	if (!activated)	return;

	info.update(dt);

	for (auto& e : list_events) {

		float real_dt = dt; // Delta time calculado por si nos hemos pasado del tiempo final del evento

		//Si es un evento que ya ha acabado , de otra fase o que aun no debe empezar, no lo actualizamos
		if (e->finished || e->phase != info.phase || info.timer_event < e->ini_time)	continue;

		//Si el timer es mayor que el tiempo donde deberia acabar el evento, obtenemos la diferencia y actualizamos por ultima vez
		if (info.timer_event > e->end_time && e->end_time > 0) {
			float curr_time = info.timer_event - dt;
			real_dt = e->end_time - curr_time;
			if (real_dt <= 0) {
				continue;
			}
			e->finished = true;
		}

		string type = e->type;

		if (type == "animEvent") {
			animEvent* ev = dynamic_cast<animEvent*>(e);
			ev->update(real_dt, &info);
		}

		if (type == "removeEvent") {
			removeEvent* ev = dynamic_cast<removeEvent*>(e);
			ev->update(real_dt, &info);
		}

		if (type == "relativeMovEvent") {
			relativeMovEvent* ev = dynamic_cast<relativeMovEvent*>(e);
			ev->update(real_dt, &info);
		}

		if (type == "absoluteMovEvent") {
			absoluteMovEvent* ev = dynamic_cast<absoluteMovEvent*>(e);
			ev->update(real_dt, &info);
		}

		if (type == "shakeCamEvent") {
			shakeCamEvent* ev = dynamic_cast<shakeCamEvent*>(e);
			ev->update(real_dt, &info);
		}

		if (type == "spawnEvent") {
			spawnEvent* ev = dynamic_cast<spawnEvent*>(e);
			ev->update(real_dt, &info);
		}

		if (type == "activateEntityEvent") {
			activateEntityEvent* ev = dynamic_cast<activateEntityEvent*>(e);
			for (auto name : ev->entities_names) {
				CEntity* ent = getEntityByName(name);

				dbg("Tiempo %.2f : Voy activar la entidad %s\n", ev->ini_time, name.c_str());

				TMsgSwitchActivation msg;
				msg.h_sender = CHandle(this).getOwner();
				ent->sendMsg(msg);
			}

			ev->finished = true;
		}

		if (type == "cutSceneEvent") {
			cutSceneEvent* ev = dynamic_cast<cutSceneEvent*>(e);
			CEngine::get().getCutScenes().startCutscene(ev->cutSceneName);
			ev->finished = true;
		}

		if (type == "playSoundEvent") {
			playSoundEvent* ev = dynamic_cast<playSoundEvent*>(e);
			CEngine::get().getSound().playEvent(ev->soundEventName);
			ev->finished = true;
		}

		if (type == "playBSOEvent") {
			playBSOEvent* ev = dynamic_cast<playBSOEvent*>(e);
			CEngine::get().getSound().playBSO(ev->bsoEventName);
			ev->finished = true;
		}

		if (type == "changeGameStateEvent") {
			changeGameStateEvent* ev = dynamic_cast<changeGameStateEvent*>(e);
			CEngine::get().getModules().changeToGamestate(ev->gameStateName);
			ev->finished = true;
		}

	}

	for (auto& e : list_changePhaseEvents) {
		if (e.phase == info.phase)	e.update(dt, &info);
	}
}

void TCompFinalEvent::load(const json& j, TEntityParseContext& ctx)
{

	if (j.count("events")) {
		const json& jevents = j["events"];
		assert(jevents.is_array());

		for (auto e : jevents.items()) {
			const json& item = e.value();
			string type = item["type"];
			assert(type != "");

			if (type == "changePhase") {
				loadChangePhase(item);
			}

			if (type == "animEvent") {
				loadAnimEvent(item);
			}

			if (type == "removeEvent") {
				loadRemoveEvent(item);
			}

			if (type == "relativeMovEvent") {
				loadRelativeMovEvent(item);
			}

			if (type == "absoluteMovEvent") {
				loadAbsoluteMovEvent(item);
			}

			if (type == "shakeCamEvent") {
				loadShakeCamEvent(item);
			}

			if (type == "spawnEvent") {				
				loadSpawnEvent(item);
			}

			if (type == "activateEntityEvent") {
				loadActivateEvent(item);
			}

			if (type == "cutSceneEvent") {
				loadCutSceneEvent(item);
			}

			if (type == "playSoundEvent") {
				loadPlaySoundEvent(item);
			}

			if (type == "playBSOEvent") {
				loadPlayBSOEvent(item);
			}

			if (type == "changeGameStateEvent") {
				loadChangeGameStateEvent(item);
			}
			
		}
	}

}

void TCompFinalEvent::loadChangePhase(const json& j)
{
	changePhaseEvent cpEvent;

	cpEvent.type = j.value("type", "");
	cpEvent.phase = j.value("phase", -1);
	cpEvent.change_to_phase = j.value("change_to_phase", -1);
	cpEvent.ini_time = j.value("ini_time", -1);


	if (j.count("entities")) {
		const json& jentities = j["entities"];
		assert(jentities.is_array());
		vector<string> entities;
		for (auto e : jentities.items()) {
			string ent = e.value();
			entities.push_back(ent);
		}
		cpEvent.entities_names = entities;
	}

	list_changePhaseEvents.push_back(cpEvent);

}

void TCompFinalEvent::loadAnimEvent(const json& j)
{

	animEvent* aEvent = new animEvent();

	aEvent->type = j.value("type", "");
	aEvent->phase = j.value("phase", -1);
	aEvent->ini_time = j.value("ini_time", -1.0);
	aEvent->end_time = j.value("end_time", -1.0);
	aEvent->entity_name = j.value("entity_name", "");
	aEvent->ani_name = j.value("ani_name", "");
	aEvent->typeAnim = j.value("typeAnim", "action");
	if (aEvent->typeAnim == "action") {
		aEvent->ani_lock = j.value("lock", false);
	}

	list_events.push_back(aEvent);
}

void TCompFinalEvent::loadRemoveEvent(const json& j)
{
	removeEvent* remEvent = new removeEvent();

	remEvent->type = j.value("type", "");
	remEvent->phase = j.value("phase", -1);
	remEvent->ini_time = j.value("ini_time", -1.0);
	remEvent->entity_name = j.value("entity_name", "");

	list_events.push_back(remEvent);
}

void TCompFinalEvent::loadSpawnEvent(const json& j)
{
	spawnEvent* sEvent = new spawnEvent();

	sEvent->type = j.value("type", "");
	sEvent->ini_time = j.value("ini_time", -1.0);
	sEvent->phase = j.value("phase", -1);
	sEvent->prefabDir = j.value("prefabDir", "");
	sEvent->pos = loadVEC3(j, "pos");

	list_events.push_back(sEvent);
}

void TCompFinalEvent::loadRelativeMovEvent(const json& j)
{
	relativeMovEvent* relEvent = new relativeMovEvent();

	relEvent->type = j.value("type", "");
	relEvent->phase = j.value("phase", -1);
	relEvent->ini_time = j.value("ini_time", -1.0);
	relEvent->end_time = j.value("end_time", -1.0);
	relEvent->mov = loadVEC3(j, "mov");
	relEvent->entity_name = j.value("entity_name", "");

	list_events.push_back(relEvent);

}

void TCompFinalEvent::loadAbsoluteMovEvent(const json& j)
{
	absoluteMovEvent* absEvent = new absoluteMovEvent();

	absEvent->type = j.value("type", "");
	absEvent->phase = j.value("phase", -1);
	absEvent->ini_time = j.value("ini_time", -1.0);
	absEvent->end_time = j.value("end_time", -1.0);
	absEvent->pos = loadVEC3(j, "pos");
	absEvent->entity_name = j.value("entity_name", "");

	list_events.push_back(absEvent);
}

void TCompFinalEvent::loadShakeCamEvent(const json& j)
{
	shakeCamEvent* skCamEvent = new shakeCamEvent();

	skCamEvent->type = j.value("type", "");
	skCamEvent->phase = j.value("phase", -1);
	skCamEvent->ini_time = j.value("ini_time", -1.0);
	skCamEvent->end_time = j.value("end_time", -1.0);
	skCamEvent->roll = j.value("roll", -1.0);
	skCamEvent->numWaves = j.value("numWaves", 0);

	list_events.push_back(skCamEvent);

}

void TCompFinalEvent::loadActivateEvent(const json& j)
{
	activateEntityEvent* actEvent = new activateEntityEvent();

	actEvent->type = j.value("type", "");
	actEvent->phase = j.value("phase", -1);
	actEvent->ini_time = j.value("ini_time", -1.0);

	const json& jentities = j["entities"];
	assert(jentities.is_array());
	vector<string> entities;
	for (auto e : jentities.items()) {
		string ent = e.value();
		entities.push_back(ent);
	}
	actEvent->entities_names = entities;

	list_events.push_back(actEvent);
}

void TCompFinalEvent::loadCutSceneEvent(const json& j)
{
	cutSceneEvent* event = new cutSceneEvent();

	event->type = j.value("type", "");
	event->phase = j.value("phase", -1);
	event->ini_time = j.value("ini_time", -1.0);
	event->cutSceneName = j.value("cutSceneName", "");
	list_events.push_back(event);
}

void TCompFinalEvent::loadPlaySoundEvent(const json& j)
{
	playSoundEvent* event = new playSoundEvent();

	event->type = j.value("type", "");
	event->phase = j.value("phase", -1);
	event->ini_time = j.value("ini_time", -1.0);
	event->soundEventName = j.value("soundEventName", "");
	list_events.push_back(event);
}

void TCompFinalEvent::loadPlayBSOEvent(const json& j)
{
	playBSOEvent* event = new playBSOEvent();

	event->type = j.value("type", "");
	event->phase = j.value("phase", -1);
	event->ini_time = j.value("ini_time", -1.0);
	event->bsoEventName = j.value("bsoEventName", "");
	list_events.push_back(event);
}

void TCompFinalEvent::loadChangeGameStateEvent(const json& j)
{
	changeGameStateEvent* event = new changeGameStateEvent();

	event->type = j.value("type", "");
	event->phase = j.value("phase", -1);
	event->ini_time = j.value("ini_time", -1.0);
	event->gameStateName = j.value("gameStateName", "");
	list_events.push_back(event);
}

void TCompFinalEvent::debugInMenu()
{
	ImGui::Checkbox("Activated", &activated);
	ImGui::LabelText("", "Fase %d", info.phase);
	ImGui::LabelText("Timer", "%.2f", info.timer_event);


	if (ImGui::TreeNode("Events")) {

		for (auto e : list_events) {
			ImGui::PushID(e);

			string type = e->type;
			ImGui::LabelText("Event: ", "%s", type.c_str());
			ImGui::LabelText("Ini time", "%.2f", e->ini_time);
			ImGui::LabelText("End time", "%.2f", e->end_time);
			ImGui::LabelText("Fase", "%.2f", e->phase);
			ImGui::Checkbox("Active", &e->active);
			ImGui::Checkbox("Finished", &e->finished);

			if (type == "animEvent") {
			}

			if (type == "removeEvent") {
				removeEvent* ev = dynamic_cast<removeEvent*>(e);
				ImGui::LabelText("Entity name to remove", "%s", ev->entity_name.c_str());
			}

			if (type == "relativeMovEvent") {
				relativeMovEvent* ev = dynamic_cast<relativeMovEvent*>(e);
				VEC3 mov = ev->mov;
				ImGui::LabelText("Mov", "%.2f %.2f %.2f", mov.x, mov.y, mov.z);
				VEC3 vel = ev->vel;
				ImGui::LabelText("Vel", "%.2f %.2f %.2f", vel.x, vel.y, vel.z);
			}

			if (type == "absoluteMovEvent") {
				absoluteMovEvent* ev = dynamic_cast<absoluteMovEvent*>(e);
				VEC3 pos = ev->pos;
				ImGui::LabelText("Pos", "%.2f %.2f %.2f", pos.x, pos.y, pos.z);
				VEC3 vel = ev->vel;
				ImGui::LabelText("Vel", "%.2f %.2f %.2f", vel.x, vel.y, vel.z);
			}

			if (type == "shakeCamEvent") {
				shakeCamEvent* ev = dynamic_cast<shakeCamEvent*>(e);
				ImGui::LabelText("Roll", "%.2f", ev->roll);
				ImGui::LabelText("Num Waves", "%d", ev->numWaves);
			}

			if (type == "activateEntityEvent") {
				activateEntityEvent* ev = dynamic_cast<activateEntityEvent*>(e);
				for (auto name : ev->entities_names) {
					ImGui::LabelText("", "%s", name.c_str());
				}
			}
			if (type == "cutSceneEvent") {
				cutSceneEvent* ev = dynamic_cast<cutSceneEvent*>(e);
				ImGui::LabelText("CutScene Name", "%s", ev->cutSceneName.c_str());
			}

			if (type == "playSoundEvent") {
				playSoundEvent* ev = dynamic_cast<playSoundEvent*>(e);
				ImGui::LabelText("Sound Name", "%s", ev->soundEventName.c_str());
			}

			if (type == "playBSOEvent") {
				playBSOEvent* ev = dynamic_cast<playBSOEvent*>(e);
				ImGui::LabelText("BSOEvent Name", "%s", ev->bsoEventName.c_str());
			}

			if (type == "changeGameStateEvent") {
				changeGameStateEvent* ev = dynamic_cast<changeGameStateEvent*>(e);
				ImGui::LabelText("GameState Name", "%s", ev->gameStateName.c_str());
			}

			ImGui::PopID();


		}			

		ImGui::TreePop();
	}


}

void TCompFinalEvent::renderDebug()
{

	if (!activated)	return;

	ImGui::Begin("Final Event", &windowBattle_active, ImGuiWindowFlags_NoBackground | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove);
	int min = static_cast<int>(info.count_down / 60);
	int sec = info.count_down % 60;
	ImGui::LabelText("", "\t\t\t   %d : %d", min, sec);
	/*static float window_scale = 1.0f;
	if (ImGui::DragFloat("window scale", &window_scale, 0.005f, 0.3f, 2.0f, "%.2f"))   // scale only this window*/
	ImGui::SetWindowFontScale(2);
	ImGui::End();

}

void TCompFinalEvent::registerMsgs()
{
	DECL_MSG(TCompFinalEvent, TMsgSwitchActivation, activateOnSwitch);
}

void TCompFinalEvent::activate()
{
	if (activated)	return;

	info.timer_event = 0;
	activated = true;
}

void TCompFinalEvent::activateOnSwitch(const TMsgSwitchActivation& msg)
{

	if (!activated)	return;

	CEntity* e = msg.h_sender;
	std::string name = e->getName();


	for (auto& ev : list_changePhaseEvents) {
		for (auto ent : ev.entities_names) {
			dbg("Es %s == a %s ?\n", name.c_str(), ent.c_str());
			dbg("La phase general es %d y la del evento %d", info.phase, ev.phase);
			if (!ev.finished && ent.compare(name) == 0 && info.phase == ev.phase) {
				info.phase = ev.change_to_phase;
				CHandle h = msg.h_sender;
				h.destroy();
				ev.finished = true;
			}
		}
	}

	/*if (name == "initFinalEvent") {
		activate();
		CHandle h = msg.h_sender;
		h.destroy();
	}*/

}

float TCompFinalEvent::ProgressBar(const char* optionalPrefixText, float value, const float minValue, const float maxValue, const char* format, const ImVec2& sizeOfBarWithoutTextInPixels, const ImVec4& colorLeft, const ImVec4& colorRight, const ImVec4& colorBorder) {
	if (value < minValue) value = minValue;
	else if (value > maxValue) value = maxValue;
	const float valueFraction = (maxValue == minValue) ? 1.0f : ((value - minValue) / (maxValue - minValue));
	const bool needsPercConversion = strstr(format, "%%") != NULL;

	ImVec2 size = sizeOfBarWithoutTextInPixels;
	if (size.x <= 0) size.x = ImGui::GetWindowWidth() * 0.25f;
	if (size.y <= 0) size.y = ImGui::GetTextLineHeightWithSpacing(); // or without

	const ImFontAtlas* fontAtlas = ImGui::GetIO().Fonts;

	if (optionalPrefixText && strlen(optionalPrefixText) > 0) {
		//ImGui::AlignFirstTextHeightToWidgets();
		ImGui::Text(optionalPrefixText);
		ImGui::SameLine();
	}

	if (valueFraction > 0) {
		ImGui::Image(fontAtlas->TexID, ImVec2(size.x * valueFraction, size.y), fontAtlas->TexUvWhitePixel, fontAtlas->TexUvWhitePixel, colorLeft, colorBorder);
	}
	if (valueFraction < 1) {
		if (valueFraction > 0) ImGui::SameLine(0, 0);
		ImGui::Image(fontAtlas->TexID, ImVec2(size.x * (1.f - valueFraction), size.y), fontAtlas->TexUvWhitePixel, fontAtlas->TexUvWhitePixel, colorRight, colorBorder);
	}
	ImGui::SameLine();

	ImGui::Text(format, needsPercConversion ? (valueFraction * 100.f + 0.0001f) : value);
	return valueFraction;
}

void animEvent::update(float dt, infoFinalEvent* info){

	if (!active) {
		CEntity* ent = getEntityByName(entity_name);

		TCompAnimator* anim = ent->get<TCompAnimator>();
		anim->removeAllCycles();
		anim->removeCurrentAction();
		if (typeAnim == "action") {
			anim->playActionAnim(ani_name, ani_lock, false, false);
		}
		else {
			anim->playCycleAnim(ani_name);
		}
		active = true;
	}

}

void changePhaseEvent::update(float dt, infoFinalEvent* info){
	//Si el tiempo de inicio y de fin es mayor que -1 significa que se cambia de fase por tiempo y no por activacion externa
	if (ini_time >= 0) {
		finished = true;
		info->phase = change_to_phase;
		info->timer_event = 0.f;
	}
}

void removeEvent::update(float dt, infoFinalEvent* info){
	finished = true;
	CHandle h_entity = getEntityByName(entity_name);
	h_entity.destroy();
}

void spawnEvent::update(float dt, infoFinalEvent* info){

	TEntityParseContext myContext;
	parseScene(prefabDir, myContext);
	CHandle h = myContext.entities_loaded[0];
	CEntity* e = h;
	TCompTransform* trans = e->get<TCompTransform>();
	trans->setPosition(pos);

	finished = true;

}

void relativeMovEvent::update(float dt, infoFinalEvent* info){

	//Si no esta activo, entonces calculamos el vector de velocidad
	if (!active) {
		if (end_time < 0)	teleport = true;
		else {
			float total_time = end_time - ini_time;
			vel = mov / total_time;
		}
		active = true;
	}

	if (active) {
		VEC3 deltaMove;
		//Si tiene la opcion teleport marcada, significa que haremos el movimiento en un solo frame
		if (teleport) {
			deltaMove = mov;
			finished = true;
		}
		else {
			deltaMove = vel * dt;
		}


		CEntity* ent = getEntityByName(entity_name);
		TCompTransform * trans = ent->get<TCompTransform>();
		TCompCollider* col = ent->get<TCompCollider>();

		VEC3 pos = trans->getPosition();
		pos += deltaMove;
		trans->setPosition(pos);

		if (col != nullptr && col->actor)	col->actor->setGlobalPose(PxTransform(VEC3_TO_PXVEC3(trans->getPosition()), QUAT_TO_PXQUAT(trans->getRotation())));
	}


}

void shakeCamEvent::update(float dt, infoFinalEvent* info){

	float total_time = end_time - ini_time;

	CModuleCameraMixer& mixer = CEngine::get().getCameraMixer();
	mixer.initShake(deg2rad(roll), total_time, numWaves);

	finished = true;
}

void absoluteMovEvent::update(float dt, infoFinalEvent* info){

	CEntity* ent = getEntityByName(entity_name);
	TCompTransform* trans = ent->get<TCompTransform>();
	TCompCollider* col = ent->get<TCompCollider>();

	//Si no esta activo, entonces calculamos el vector de velocidad
	if (!active) {
		if (end_time < 0)	teleport = true;
		else {
			float total_time = end_time - ini_time;
			VEC3 old_pos = trans->getPosition();
			vel = (pos - old_pos) / total_time;
		}
		active = true;
	}

	if (active) {
		VEC3 deltaMove;
		//Si tiene la opcion teleport marcada, significa que haremos el movimiento en un solo frame
		if (teleport) {
			trans->setPosition(pos);
			finished = true;
		}
		else {
			VEC3 newPos = trans->getPosition();
			deltaMove = vel * dt;
			newPos += deltaMove;
			trans->setPosition(newPos);
		}		
		

		if (col != nullptr && col->actor)	col->actor->setGlobalPose(PxTransform(VEC3_TO_PXVEC3(trans->getPosition()), QUAT_TO_PXQUAT(trans->getRotation())));
	}

}

void infoFinalEvent::update(float dt)
{
	timer_event += dt;

	if (time.elapsed() >= 1) {
		count_down -= 1;
		time.reset();
	}
}
