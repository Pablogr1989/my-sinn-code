#pragma once
#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "entity/entity_parser.h"
#include "windows/application.h"
#include "components/common/comp_transform.h"
#include "sinn/modules/module_physics_sinn.h"
#include "sinn/characters/player/comp_player.h"
#include "mcv_platform.h"

enum class movementType { POS, MOV, YAW, PITCH, ROLL, ROT};
using namespace std;


class TCompMovementObject : public TCompBase
{
	DECL_SIBLING_ACCESS();

public:

	// Estructura general para almacenar los valores de tiempo del objeto
	struct TObjectTimes {
		float timer_object = 0;
		//float last_time = 0;

		unsigned int ini_frame = 0;
		unsigned int last_frame = 0;
		unsigned int current_frame = 0;
		int fps = 60;

		void init();
		void update(float dt);
	};

	// Estructura para cada tipo de movimiento que tenga el objeto
	struct TMovement {

		// Variables que se leen del json
		bool timeorframe = true;
		movementType type = movementType::POS;
		VEC3 vec_value = VEC3::Zero;
		float float_value = 0;
		float t_ini = 0, t_fin = 0;
		unsigned int f_ini = 0, f_fin = 0;
		string audio_event = "";
		//Variables que se calculan
		int id = 0;
		VEC3 mov_speed = VEC3::Zero;
		float rot_speed = 0;
		float ti = 0, tf = 0;

		bool active = false;
		bool finished = false;

		lastMovement update(TObjectTimes iT, float dt, TCompTransform* trans, TCompCollider* col);
		void updateMovement(VEC3 deltaMove, TCompTransform* trans, TCompCollider* col);
		void updateRotation(float dYaw, float dPitch, float dRoll, TCompTransform* trans, TCompCollider* col);
		void updateRotationAxis(float dYaw, TCompTransform* trans, TCompCollider* col);
		bool checkActivate(TObjectTimes iT, float dt, TCompTransform* trans);
	};

	void load(const json& j, TEntityParseContext& ctx);
	void loadMovement(const json& jp, int id);
	void update(float dt);
	void renderDebug();
	void debugInMenu();
	void onEntityCreated();
	static void registerMsgs();

	bool isFinished(TMovement mov, TObjectTimes iT);

private:

	TObjectTimes infoTime;
	std::vector<TMovement> mov_list;
	int mov_actives = 0;

	std::vector<TMovement> original_list;
	triggerType type = triggerType::AUTO;  // Tipo de evento que inicia la animacion
	bool in_loop = true;  // Si el movimiento sera en bucle

	bool one_time = true;
	bool first_time = true;
	bool pictogram = false;

	bool active = false;
	bool initialized = false;

	bool playerOnTopMe = false;
	CHandle h_player;
	lastMovement last;

	//reset Values
	VEC3 Orig_Pos;
	QUAT Orig_Rot;
	std::vector<TMovement> reset_list;
	TObjectTimes reset_Time;
	bool reset_active;



	void initMovement();
	void calculateTimes();
	void activateWithInteract(const TMsgInteract& msg);
	void activateOnEnterTrigger(const TMsgActivateAniObject& msg);
	void isPlayerOnTopMe(const TMsgOnContactMovObject& msg);
	void removeTriggers(CHandle ent);
	void activateOnSwitch(const TMsgSwitchActivation& msg);

	void resetState(const TMsgResetGame& msg);

	void SaveState(const TMsgSaveState& msg);

};

