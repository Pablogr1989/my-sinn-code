#include "mcv_platform.h"
#include "comp_change_camera.h"
#include "engine.h"
#include "modules/module_camera_mixer.h"
#include "sinn/cameras/comp_camera3D.h"
#include "sinn/characters/player/comp_direction.h"

DECL_OBJ_MANAGER("change_camera", TCompChangeCamera);


void TCompChangeCamera::registerMsgs() {
	DECL_MSG(TCompChangeCamera, TMsgOnTrigger, changeCamera);
	DECL_MSG(TCompChangeCamera, TMsgSwitchActivation, activateOnSwitch);
	DECL_MSG(TCompChangeCamera, TMsgProject, activateOnProjection);
}

void TCompChangeCamera::loadChild(const json& j, TEntityParseContext& ctx) {
	assert(j.count("target_camera"));

	target_camera = j.value("target_camera", "");
	time = j.value("time", 1.f);
	block_mov = j.value("block_mov", block_mov);

	std::string type = j.value("type", "exit");
	if (type == "exit") {
		swMode = switchChCamModes::EXIT;
	}
	else if(type == "enter") {
		swMode = switchChCamModes::ENTER;
	}
	else if (type == "temp") {
		swMode = switchChCamModes::TEMP;
	}

	if (swMode == switchChCamModes::EXIT || swMode == switchChCamModes::ENTER) {
		assert(j.count("lock_camera"));
		lock_camera = j.value("lock_camera", false);

		guide_mov = j.value("guide_mov", guide_mov);
		reset = j.value("reset", reset);



		if (j.count("yaw") && j.count("pRatio")) {
			block_withYawPitch = true;
			yaw = j.value("yaw", yaw);
			pitchRatio = j.value("pRatio", pitchRatio);
		}



		if (j.value("iniToEnd", true)) {
			typePath = "iniToEnd";
		}
		else {
			typePath = "endToIni";
		}

		if (j.count("corr_y") && j.count("corr_pR")) {
			corr_y = j.value("corr_y", 0.f);
			corr_pR = j.value("corr_pR", 0.f);
			correctCam = true;
		}

		time_block_dir = j.value("time_block_dir", time_block_dir);

	}
	else {
		time_cooldown = j.value("time_cooldown", 1.f);
	}
}

void TCompChangeCamera::updateChild(float dt) {
	if (!finished && swMode == switchChCamModes::TEMP) {
		timer_cooldown += dt;


		if (timer_cooldown >= time_cooldown) {
			finished = true;
			TCompPlayer* comp_player = getPlayer()->get<TCompPlayer>();
			if (block_mov)			comp_player->setInCutscene(false);
			comp_player->setCanChangeCamera(true);
			//comp_player->setModoCamera(enumCameras::CAM3D);
		}

	}

}

void TCompChangeCamera::debugInMenuChild() {
	ImGui::Checkbox("Activo", &active);
	ImGui::LabelText("", "PROPERTIES");
	ImGui::LabelText("Target camera", "%s", target_camera.c_str());
	ImGui::Checkbox("Lock Camera", &lock_camera);
	ImGui::DragFloat("Time Transition", &time, 0.1, 0, 100);
	ImGui::Checkbox("Block movement", &block_mov);
	ImGui::DragFloat("Yaw", &yaw, 0.1, 0, 1);
	ImGui::DragFloat("Pitch ratio", &pitchRatio, 0.1, 0, 1);
	ImGui::NewLine();

}



void TCompChangeCamera::changeCamera(const TMsgOnTrigger& msg) {

	if (!active)	return;

	if (!msg.trigger_enter)	return;

	if (swMode == switchChCamModes::EXIT || swMode == switchChCamModes::ENTER)	changeEnterExitCamera();
	else changeTempCamera();

}

void TCompChangeCamera::activateOnSwitch(const TMsgSwitchActivation& msg)
{
	if (!active)	return;

	if (swMode == switchChCamModes::EXIT || swMode == switchChCamModes::ENTER)	changeEnterExitCamera();
	else changeTempCamera();
}

void TCompChangeCamera::activateOnProjection(const TMsgProject& msg)
{
	if (!active)	return;
	if (first) {
		first = false;
		return;
	}

	//if projected, enter.
	if (msg.project)
		swMode = switchChCamModes::ENTER;
	else
		swMode = switchChCamModes::EXIT;

	changeEnterExitCamera();
}

void TCompChangeCamera::changeEnterExitCamera()
{
	CEntity* ent_player = getEntityByName("Player");
	TCompPlayer* comp_player = ent_player->get<TCompPlayer>();

	if (comp_player->getInRailCamera() && swMode == switchChCamModes::ENTER )	return;

	if (!comp_player->getInRailCamera() && swMode == switchChCamModes::EXIT)	return;

	CEntity* ent_camera = (CEntity*)getEntityByName("camera_direction");
	TCompDirection* comp_direc = ent_camera->get<TCompDirection>();

	if (reset && comp_player->getInRailCamera()) {
		bool block = false;

		// Si existe pitch y yaw, colocamos todas las camaras con esa posicion y bloqueamos o no dependiendo de block_move
		if (block_withYawPitch) {
			float p = pitchRatio;
			float y = yaw;
			comp_direc->blockMovement(block, p, y);
			getObjectManager<TCompCamera3D>()->forEach([block, p, y](TCompCamera3D* c) {
				c->blockMovement(block, p, y);
				});
		}
		else {
			comp_direc->blockMovement(block);
			getObjectManager<TCompCamera3D>()->forEach([block](TCompCamera3D* c) {
				c->blockMovement(block);
				});
		}

		if (!block_mov) {
			comp_player->setGuideMode(guide_mov);
		}

		comp_player->setInRailCamera(!comp_player->getInRailCamera());

		//Cambiamos a la camara correspondiente y le indicamos si podemos cambiar de camara con los estados del personaje
		comp_player->changeOtherCam(!false, "cameraWalk3D", time);

		if (correctCam)	comp_player->setCorrectCamValues(time, corr_y, corr_pR);

		/*if (swMode == switchChCamModes::EXIT) {
		}*/

		//Bloqueamos el movimiento 1 segundo por si has entrado con la camara alreves
		if (time_block_dir > 0)	comp_player->blockLocalDir(time_block_dir);
	}
	else {


		bool block = block_mov;


		// Si existe pitch y yaw, colocamos todas las camaras con esa posicion y bloqueamos o no dependiendo de block_move
		if (block_withYawPitch) {
			float p = pitchRatio;
			float y = yaw;
			comp_direc->blockMovement(block, p, y);
			getObjectManager<TCompCamera3D>()->forEach([block, p, y](TCompCamera3D* c) {
				c->blockMovement(block, p, y);
				});
		}
		else {
			comp_direc->blockMovement(block);
			getObjectManager<TCompCamera3D>()->forEach([block](TCompCamera3D* c) {
				c->blockMovement(block);
				});
		}

		if (!block_mov) {
			comp_player->setGuideMode(guide_mov);
		}

		comp_player->setInRailCamera(!comp_player->getInRailCamera());

		//Cambiamos a la camara correspondiente y le indicamos si podemos cambiar de camara con los estados del personaje
		comp_player->changeOtherCam(!lock_camera, target_camera, time);

		if (correctCam)	comp_player->setCorrectCamValues(time, corr_y, corr_pR);

		/*if (swMode == switchChCamModes::EXIT) {
		}*/

		//Bloqueamos el movimiento 1 segundo por si has entrado con la camara alreves
		if (time_block_dir > 0)	comp_player->blockLocalDir(time_block_dir);
	}
}

void TCompChangeCamera::changeTempCamera()
{
	CEntity* ent_player = getEntityByName("Player");
	TCompPlayer* comp_player = ent_player->get<TCompPlayer>();

	if (block_mov) {
		comp_player->setInCutscene(true);
	}

	comp_player->changeOtherCam(false, target_camera, time);
	//comp_player->setModoCamera(enumCameras::EXTERNAL);

	finished = false;
	timer_cooldown = 0;
}
