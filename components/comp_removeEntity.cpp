#include "mcv_platform.h"
#include "comp_removeEntity.h"

DECL_OBJ_MANAGER("removeEntity", TCompRemoveEntity);

void TCompRemoveEntity::registerMsgs() {
	DECL_MSG(TCompRemoveEntity, TMsgOnTrigger, checkOnTrigger);
	DECL_MSG(TCompRemoveEntity, TMsgOnTriggerNear, onTriggerNear);
	DECL_MSG(TCompRemoveEntity, TMsgOnInteraction, onInteraction);
}

void TCompRemoveEntity::loadChild(const json& j, TEntityParseContext& ctx) {
	auto& ent_array = j["ent_to_remove"];
	assert(ent_array.size() > 0);
	for (auto& ent_name : ent_array) ent_to_remove.push_back(ent_name);

	countdown = j.value("countdown", countdown);
	int_OnTrigger = interactOnNear;

	//interaction_type = j.value("interaction_type", "remove_entities");
	//if (interaction_type == "onTrigger") {
	//	int_OnTrigger = true;
	//}
}

void TCompRemoveEntity::updateChild(float dt) {
	if (finished)	return;

	timer_to_remove += dt;

	if (timer_to_remove >= countdown && handle_to_remove.size() > 0) {
		for (auto& ent : handle_to_remove) {
			ent.destroy();
		}
		handle_to_remove.clear();
		finished = true;
	}
}

void TCompRemoveEntity::onEntityCreatedChild() {
	if (type == UNDEFINED) type = REMOVE_ENTITIES;

	//if (!int_OnTrigger) {
	//	if (interaction_type != "remove_entities") {
	//		setType(interaction_type);
	//	}
	//	else type = REMOVE_ENTITIES;
	//	setOwner(CHandle(this).getOwner());
	//}
}


void TCompRemoveEntity::onChildInteraction()
{
	if (ent_to_remove.size() <= 0) {
		finished = true;
		return;
	}

	if (countdown > 0) {
		for (auto ent : ent_to_remove) {
			CHandle h_ent = getEntityByName(ent);
			handle_to_remove.push_back(h_ent);
		}
		timer_to_remove = 0;
	}
	else {
		for (auto ent : ent_to_remove) {
			CHandle h_ent = getEntityByName(ent);
			h_ent.destroy();
		}
		ent_to_remove.clear();
		finished = true;
	}
}

void TCompRemoveEntity::checkOnTrigger(const TMsgOnTrigger& msg) {
	if (int_OnTrigger) {
		if (!msg.trigger_enter) return;
		onChildInteraction();
	}
	else {
		onTrigger(msg);
	}
}
