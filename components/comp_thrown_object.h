#pragma once
#include "components/common/comp_base.h"
#include "entity/entity.h"


using namespace std;

class TCompThrownObject : public TCompBase
{
private:
	DECL_SIBLING_ACCESS();

	//Debug
	VEC3 init_pos = VEC3::Zero;

	float timer_stopped = 0;
	float time_stopped = 0.0001;
	float num_veces = 0;

public:
	void renderDebug();
	void debugInMenu();
	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);
	void onEntityCreated();

	void throwObject(VEC3 force);


};