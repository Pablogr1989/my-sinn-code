#include "mcv_platform.h"
#include "comp_animation_object.h"
#include "sinn/characters/animator.h"

DECL_OBJ_MANAGER("animation_object", TCompAnimationObject);

void TCompAnimationObject::load(const json& j, TEntityParseContext& ctx)
{

	// TIPO DE DISPARADOR AUTOMATICO, INTERACCION O ENTRANDO EN UN TRIGGER
	string sType = j.value("init_type", "auto");
	for (auto& c : sType)
	{
		c = tolower(c);
	}
	
	if (sType == "interaction") type = triggerType::INTERACTION;
	else if (sType == "enter_trigger") type = triggerType::ENTER_TRIGGER;
	else type = triggerType::AUTO;

	mesh_name = j.value("skel_name_entity", "");
	assert(mesh_name != "");

	move_bone_name = j.value("movement_bone_name", "");

	ani_name = j.value("ani_name", "");

	in_loop = j.value("in_loop", false);

	if (!in_loop) {
		one_time = j.value("one_time", one_time);
		if (!one_time) {
			time_to_reactivate = j.value("time_to_reactivate", time_to_reactivate);
		}
	}

	action_lock = j.value("action_lock", false);
}

void TCompAnimationObject::update(float dt)
{

	if (!initialized) {
		initialized = true;

		if (mesh_name != "this") {
			mesh_handle = getEntityByName(mesh_name);
		}
		else {
			mesh_handle = CHandle(this).getOwner();
			withOutMovement = true;
		}


		CEntity* e = mesh_handle;
		TCompSkeleton* skel = e->get<TCompSkeleton>();


		skel->setApplyActionMovement(false);
		skel->setApplyCycleMovement(false);

		if (type == triggerType::AUTO) {
			activate();
		}
		else {
			//Dejamos puesta la animacion de idle en lock para que no haya ningun movimiento extra�o
			TCompAnimator* anim = e->get<TCompAnimator>();
			anim->removeAllCycles();
			anim->playActionAnim("idle", true, false, false);
		}

		if (move_bone_name != "") {
			move_bone_id = skel->getIdBone(move_bone_name);
		}

		//assert(mesh_handle == nullptr);
		/*last_pos = getRootPos();
		getRootEuerAngles(&last_yaw, &last_pitch, &last_roll);*/


	}

	timer_anim += dt;
	timer_reactivate += dt;

	if (!activated) {
		last_pos = getRootPos();
		getRootEuerAngles(&last_yaw, &last_pitch, &last_roll);
	}

	if (activated){		

		if (timer_anim > ani_time && !in_loop) {
			if (!finished) {
				//Si ya ha terminado entonces lo notificamos y reiniciamos el timer para poder volver a activarlo
				finished = true;
				timer_reactivate = 0;

				if (one_time && action_lock) return;

				CEntity* e = mesh_handle;
				TCompAnimator* anim = e->get<TCompAnimator>();
				anim->playActionAnim("idle", true, false, false);
			}


			if (timer_reactivate >= time_to_reactivate) {
				activated = false;
				return;
			}
		}

		if (withOutMovement)	return;

		//Obtenemos la informacion de la posicion y rotacion actual
		VEC3 newPos = getRootPos();
		float newYaw, newPitch, newRoll;
		getRootEuerAngles(&newYaw, &newPitch, &newRoll);

		//Calcaulamos el desplazamiento y rotacion desde el anterior frame
		VEC3 deltaMove = last_pos - newPos;
		float dyaw = last_yaw - newYaw;
		float dpitch = last_pitch - newPitch;
		float droll = last_roll - newRoll;

		TCompTransform* trans = get<TCompTransform>();

		float yaw, pitch, roll;
		VEC3 pos = trans->getPosition();

		trans->getEulerAngles(&yaw, &pitch, &roll);
		yaw -= dyaw;
		pitch -= dpitch;
		roll -= droll;

		pos -= deltaMove;

		trans->setPosition(pos);
		trans->setEulerAngles(yaw, pitch, roll);

		TCompCollider* col = get<TCompCollider>();
		if(col != nullptr && col->actor)	col->actor->setGlobalPose(PxTransform(VEC3_TO_PXVEC3(trans->getPosition()), QUAT_TO_PXQUAT(trans->getRotation())));

		/*dbg("Timer Anim %f\n", timer_anim);
		dbg("Last pos root %f %f %f yaw %f pitch %f roll %f\n", last_pos.x, last_pos.y, last_pos.z, last_yaw, last_pitch, last_roll);
		dbg("New pos root %f %f %f yaw %f pitch %f roll %f\n", newPos.x, newPos.y, newPos.z, newYaw, newPitch, newRoll);	
		dbg("Delta pos %f %f %f yaw %f pitch %f roll %f\n", deltaMove.x, deltaMove.y, deltaMove.z, dyaw, dpitch, droll);
		dbg("This pos %f %f %f yaw %f pitch %f roll %f\n\n", pos.x, pos.y, pos.z, yaw, pitch, roll);*/


		last_pos = newPos;
		last_yaw = newYaw;
		last_pitch = newPitch;
		last_roll = newRoll;

	}

}

void TCompAnimationObject::renderDebug()
{
}

void TCompAnimationObject::debugInMenu()
{

	string lbType = "";
	switch (type)
	{
	case triggerType::AUTO:
		lbType = "Auto";
		break;
	case triggerType::INTERACTION:
		lbType = "Interaction";
		break;
	case triggerType::ENTER_TRIGGER:
		lbType = "Enter";
		break;
	default:
		break;
	}
	ImGui::LabelText("Tipo de disparador", lbType.c_str());
	ImGui::LabelText("Nombre entidad", mesh_name.c_str());
	ImGui::LabelText("Nombre animacion", ani_name.c_str());
	ImGui::LabelText("Nombre move bone", move_bone_name.c_str());
	ImGui::Checkbox("Anim en bucle", &in_loop);
	ImGui::Checkbox("Anim action con lock", &action_lock);
	ImGui::LabelText("Tiempo animacion", "%f", timer_anim);
	ImGui::Checkbox("Activado", &activated);
	ImGui::Checkbox("One time", &one_time);
	ImGui::DragFloat("Time to Reactivate", &time_to_reactivate, 0.1, 0.f, 100.f);
	ImGui::LabelText("Timer reactivate", "%f", timer_reactivate);

	if (ImGui::SmallButton("Activar")) {
		if(!activated) activate();
	}

}

void TCompAnimationObject::onEntityCreated()
{
}

void TCompAnimationObject::registerMsgs()
{
	DECL_MSG(TCompAnimationObject, TMsgInteract, activateWithInteract);
	DECL_MSG(TCompAnimationObject, TMsgActivateAniObject, activateOnEnterTrigger);
	DECL_MSG(TCompAnimationObject, TMsgSwitchActivation, activateOnSwitch);
}

void TCompAnimationObject::activate()
{

	if (activated)	return;

	activated = true;

	CEntity* e = mesh_handle;

	TCompAnimator* anim = e->get<TCompAnimator>();
	TCompSkeleton* skel = e->get<TCompSkeleton>();

	anim->removeAllCycles();
	anim->removeCurrentAction();

	if (in_loop) {
		anim->playCycleAnim(ani_name);
	}
	else {
		anim->playActionAnim(ani_name, action_lock, false, false);
	}

	ani_time = anim->getTimeAnimation(ani_name);

	timer_anim = 0;
	finished = false;

}

VEC3 TCompAnimationObject::returnVector(CalMixer::vecT type, MAT44 mtx)
{
	switch (type) {
	case CalMixer::vecT::FRONT:
		return -mtx.Forward();
	case CalMixer::vecT::NFRONT:
		return mtx.Forward();
	case CalMixer::vecT::LEFT:
		return -mtx.Left();
	case CalMixer::vecT::NLEFT:
		return mtx.Left();
	case CalMixer::vecT::UP:
		return mtx.Up();
	case CalMixer::vecT::NUP:
		return -mtx.Up();
	}
}

void TCompAnimationObject::getRootEuerAngles(float* yaw, float* pitch, float* roll)
{
	CEntity* e = mesh_handle;
	TCompSkeleton* skel = e->get<TCompSkeleton>();

	VEC3 pos = skel->getAbsTraslationBone(move_bone_id);
	QUAT rot = skel->getAbsRotationBone(move_bone_id);

	MAT44 mtx = MAT44::CreateFromQuaternion(rot) * MAT44::CreateTranslation(pos);

	VEC3 v = returnVector(skel->getTypeFront(), mtx);
	VEC3 left = returnVector(skel->getTypeLeft(), mtx);

	*yaw = atan2f(v.x, v.z);
	float mdo = sqrtf(v.x * v.x + v.z * v.z);
	*pitch = atan2f(-v.y, mdo);

	VEC3 roll_zero_left = VEC3(0, 1, 0).Cross(v);
	VEC3 roll_zero_up = VEC3(v).Cross(roll_zero_left);
	VEC3 my_real_left = left;
	my_real_left.Normalize();
	roll_zero_left.Normalize();
	roll_zero_up.Normalize();
	float rolled_left_on_up = my_real_left.Dot(roll_zero_up);
	float rolled_left_on_left = my_real_left.Dot(roll_zero_left);
	*roll = atan2f(rolled_left_on_up, rolled_left_on_left);
}

VEC3 TCompAnimationObject::getRootPos()
{
	CEntity* e = mesh_handle;
	TCompSkeleton* skel = e->get<TCompSkeleton>();

	return skel->getAbsTraslationBone(move_bone_id);
}

void TCompAnimationObject::activateWithInteract(const TMsgInteract& msg)
{
	if (type == triggerType::INTERACTION) {
		activate();
		if (one_time)	removeTriggers(msg.h_sender);
	}
}

void TCompAnimationObject::activateOnEnterTrigger(const TMsgActivateAniObject& msg)
{
	if (type == triggerType::ENTER_TRIGGER && msg.trigger_enter) {
		activate();
		if (one_time)	removeTriggers(msg.h_sender);
	}
}

void TCompAnimationObject::activateOnSwitch(const TMsgSwitchActivation& msg) {
	activate();
	//if (one_time)	removeTriggers(msg.h_sender);
}

void TCompAnimationObject::removeTriggers(CHandle ent)
{
	CEntity* cent = ent;

	TCompCollider* col = cent->get<TCompCollider>();
	if (col->actor) {
		const PxU32 numShapes = col->actor->getNbShapes();
		std::vector<PxShape*> shapes;
		shapes.resize(numShapes);
		col->actor->getShapes(&shapes[0], numShapes);

		for (auto shape : shapes) {
			const char* shName = shape->getName();
			PxShapeFlags flags = shape->getFlags();
			if (flags & PxShapeFlag::eTRIGGER_SHAPE) {
				col->actor->detachShape(*shape);
			}
		}
	}
}


