#include "mcv_platform.h"
#include "comp_activateEvents.h"

DECL_OBJ_MANAGER("events_activator", TCompEventsActivator);

void TCompEventsActivator::loadChild(const json& j, TEntityParseContext& ctx) {
	auto ent_array = j["ent_switched"];
	assert(ent_array.size() > 0);
	for (auto& ent_name : ent_array) ent_switched.push_back(ent_name);
}

void TCompEventsActivator::registerMsgs() {
	DECL_MSG(TCompEventsActivator, TMsgOnTrigger, onChildInteraction);
}

void TCompEventsActivator::onChildInteraction(const TMsgOnTrigger& msg) {
	for (std::string ent_name : ent_switched) {
		CEntity* ent = getEntityByName(ent_name);
		assert(ent != nullptr);
		TMsgSwitchActivation msg;
		msg.h_sender = owner;
		ent->sendMsg(msg);
	}
}
