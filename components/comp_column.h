#pragma once
#include "components/common/comp_base.h"
#include "comp_interaction.h"
#include "entity/entity.h"

#include "sinn/modules/module_physics_sinn.h"
#include "PxPhysicsAPI.h"
#include <Cal3D\mixer.h>


class TCompColumn: public TCompInteraction
{
	DECL_SIBLING_ACCESS();

public:
	INIT_OWNER
	static void registerMsgs();
	void loadChild(const json& j, TEntityParseContext& ctx);
	void updateChild(float dt);
	void debugInMenuChild();
	void onChildInteraction();

	void setType() {
		type = COLUMN;
	}


private:
	void moveColumn();
	VEC3 returnVector(CalMixer::vecT type, MAT44 mtx);
	void resetState(const TMsgResetGame& msg);
	void setReset(float dt);
	void SaveState(const TMsgSaveState& msg);
	void destroyStair(const TMsgDestroyStairs& msg);
	void fallingSoundAndParticles();

	PxTransform shapePose;
	PxBoxGeometry overlapShape;
	SoundEvent* s = nullptr;
	CHandle h_animCol;
	std::string column_name;
	std::list<CHandle> ToWakeUp;
	int id_bone;
	bool particle = false;

	float anim_time = 0;
	float timer_fall = 0;
	bool falling = false;
	bool removeIdle = false;
	bool initialize = false;
	bool materiaFall = false;

	// Stairs destruction part
	bool fallen = false;
	bool destroy_stair = false;
	float current_time = 0.0f;
	float time = 0.0f;
	float velocity = 0.0f;
	float distanceTrigger = 0.0f;
	bool push_sound = false;
	//state parameters
	bool old_fallen = false;
	bool destroy = false;

	float y_fall = 7;
	float original_yPos = 0;

	CTimer timer = CTimer();
	CHandle thisColumn = CHandle();
	bool startDestroy = false;
	bool push = false;

	bool resetting = false;
	float time_resetting = 1;
	float timer_resetting = 0;

	bool first = true;
	bool finished = false;
	bool sound = false;
	bool falling_sound = false;

};

