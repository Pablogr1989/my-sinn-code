#include "mcv_platform.h"
#include "comp_thrown_object.h"
#include <components/common/comp_collider.h>
#include "components/common/comp_transform.h"
#include "sinn/modules/module_physics_sinn.h"
#include <engine.h>
#include <sinn/characters/enemies/comp_enemy3D.h>

DECL_OBJ_MANAGER("thrown_object", TCompThrownObject);

void TCompThrownObject::renderDebug()
{
}

void TCompThrownObject::debugInMenu()
{

	TCompCollider* compCollider = get<TCompCollider>();
	TCompTransform* mytrans = get<TCompTransform>();

	if (ImGui::SmallButton("Parar Movimiento")) {
		TCompCollider* compCollider = get<TCompCollider>();
		((PxRigidDynamic*)compCollider->actor)->clearForce(PxForceMode::eIMPULSE);
		((PxRigidDynamic*)compCollider->actor)->putToSleep();
	}

	if (ImGui::SmallButton("Pos Player")) {
		CEntity* cplayer = getEntityByName("Player");
		TCompTransform* tplayer = cplayer->get<TCompTransform>();
		VEC3 pos_player = tplayer->getPosition();
		pos_player.y += 0.7;
		compCollider->actor->setGlobalPose(PxTransform(pos_player.x, pos_player.y, pos_player.z));
	}

	if (ImGui::SmallButton("Pos Inicial")) {
		VEC3 pos = VEC3(0, 0.7, 0);
		compCollider->actor->setGlobalPose(PxTransform(pos.x, pos.y, pos.z));
	}

	/*static bool withFriction = false;

	if (ImGui::Checkbox("with Friction", &withFriction)) {
		if (compCollider->actor != nullptr)
		{
			((PxRigidDynamic*)compCollider->actor)->setRigidBodyFlag(PxRigidBodyFlag::eENABLE_CCD_FRICTION, withFriction);
		}
	}*/


	//////////////////////////////////////////////////////////////
	//TIRAR PIEDRA
	//////////////////////////////////////////////////////////////
	VEC3 my_pos = mytrans->getPosition();
	static VEC3 pos_ruido;
	static float int_ruido = 2;

	ImGui::DragFloat("Intensidad ruido", &int_ruido, 0.1, 2);
	ImGui::DragFloat3("Posicion ruido", &pos_ruido.x, 0.1, -1000, 1000);
	ImGui::SameLine();
	if (ImGui::SmallButton("->")) {
		pos_ruido = my_pos;
	}
	/*if (ImGui::SmallButton("Simular piedra")) {
		CHandle boss = getEntityByName("Boss");
		TCompTransform* my_trans = get<TCompTransform>();

		dbg("Mando %.2f de ruido\n", int_ruido);

		TMsgSuspiciusEvent msg;
		msg.intensity = int_ruido;
		msg.pos = pos_ruido;
		msg.type = static_cast<int> (suspicius_noise_type::STONE_NOISE);
		boss.sendMsg(msg);

	}*/


	//Formula parabola
	static float tf = 1;  //Duracion del lanzamiento
	static float y0 = 0.5;  //Altura inicial
	static float velX = 1;  // Velocidad adicional en el eje X
	VEC3 mypos = my_pos;
	mypos.y += y0;
	float xf = VEC3::Distance(mypos, pos_ruido);  // Distancia al punto finalr
	static VEC3 f;

	ImGui::DragFloat("Tiempo parabola", &tf, 0.1, 0.5, 100);
	ImGui::DragFloat("Velocidad adicional en X", &velX, 0.1, 1, 100);
	ImGui::DragFloat("Altura inicial", &y0, 0.1, 0.5, 10);
	ImGui::Text("Distancia al punto: %.2f", xf);
	ImGui::Text("VALORES FORMULA");

	vector<float> vtime;
	vector<float> vdx;
	vector<float> vdy;

	float pow2_tf = tf * tf;
	float v0x = xf / tf;
	float v0y = (-y0 + 0.5 * 9.8 * pow2_tf) / tf;
	float initAng = atanf(v0y / v0x);
	float v0 = v0x / cosf(initAng);


	ImGui::Text("Velocidad original: %.2f", v0);
	ImGui::Text("Velocidad original X: %.2f", v0x);
	ImGui::Text("Velocidad original Y: %.2f", v0y);
	ImGui::Text("Angulo inicial (radianes): %.2f", initAng);
	ImGui::Text("Angulo inicial: %.2f", rad2deg(initAng));

	//ImGui::DragFloat3("Force Direction", &fDirec.x, 0.1, 0, 1);
	//ImGui::DragFloat("Force", &force, 0.1, 0, 100);
	//drawLine(mytrans->getPosition(), mytrans->getPosition() + fDirec * 2, Color::Blue);

	static bool pintar_trayectoria = false;
	ImGui::Checkbox("Pintar trayectoria", &pintar_trayectoria);

	VEC3 up_stone = VEC3(0, 1, 0);
	VEC3 front_stone = pos_ruido - mypos;
	front_stone.Normalize();

	if (pintar_trayectoria) {
		for (float t = 0; t < tf; t += 0.1) {
			vtime.push_back(t);

			float pow2_t = t * t;
			float ax = t * v0x;
			float ay = y0 + v0y * t - 0.5 * 9.8 * pow2_t;
			vdy.push_back(ay);
			vdx.push_back(ax);
		}

		static VEC3 last_point = VEC3::Zero;

		for (int i = 0; i < vtime.size(); i++) {

			if (i == 0) {
				last_point = mypos + front_stone * vdx[i] + up_stone * vdy[i];
				drawLine(mypos, last_point, Color::LimeGreen);
			}
			else {
				VEC3 point = mypos + front_stone * vdx[i] + up_stone * vdy[i];
				drawLine(last_point, point, Color::LimeGreen);
				drawWiredSphere(last_point, 0.1, Color::Red);
				last_point = point;
			}
		}
		drawWiredSphere(last_point, 0.1, Color::White);
		drawLine(last_point, pos_ruido, Color::LimeGreen);
	}
	drawWiredSphere(pos_ruido, 0.2, Color::Green);

	if (ImGui::SmallButton("Empujar")) {

		if (compCollider->actor != nullptr)
		{

			VEC3 fDirec = front_stone * cos(initAng) + VEC3(0, 1, 0) * sin(initAng);

			((PxRigidDynamic*)compCollider->actor)->clearForce(PxForceMode::eIMPULSE);
			compCollider->actor->setGlobalPose(PxTransform(mypos.x, mypos.y, mypos.z));
			f = fDirec * v0;
			((PxRigidDynamic*)compCollider->actor)->addForce(VEC3_TO_PXVEC3(f), PxForceMode::eIMPULSE);
		}
	}

	/*float t_y_max = v0y / 9.8;
	float t_pow2 = t_y_max * t_y_max;
	float y_max = y0 + v0y * t_y_max - 0.5 * 9.8 * (t_pow2);
	VEC3 linearVel = front_throw * v0x + up_stone * v0y;
	ImGui::Text("Max Y: %.2f en t(%.2f)", y_max, t_y_max);
	ImGui::Text("Linear Velocity Empujar [%.2f %.2f %.2f]", linearVel.x, linearVel.y, linearVel.z);
	drawLine(mytrans->getPosition(), mytrans->getPosition() + linearVel * 2, Color::Red);*/

	static float restitution = 0.3;
	static float staticFric = 0.2;
	static float dynamicFric = 0.3;

	ImGui::DragFloat("Restution", &restitution, 0.1, 0, 100);
	ImGui::DragFloat("staticFric", &staticFric, 0.1, 0, 100);
	ImGui::DragFloat("dynamicFric", &dynamicFric, 0.1, 0, 100);


	if (ImGui::SmallButton("Cambiar material")) {
		const PxU32 numShapes = compCollider->actor->getNbShapes();
		std::vector<PxShape*> shapes;
		shapes.resize(numShapes);
		compCollider->actor->getShapes(&shapes[0], numShapes);

		for (auto shape : shapes) {
			PxMaterial* mat;
			shape->getMaterials(&mat, 1);
			mat->setRestitution(restitution);
			mat->setStaticFriction(staticFric);
			mat->setDynamicFriction(dynamicFric);
		}
	}

	/////////////////////////////////////////////////////////////////

}

void TCompThrownObject::load(const json& j, TEntityParseContext& ctx)
{
}

void TCompThrownObject::update(float dt)
{

	//if (being_thrown) {
	//	TCompCollider* compCollider = get<TCompCollider>();
	//	TCompTransform* mytrans = get<TCompTransform>();

	//	if (compCollider->actor != nullptr)
	//	{			
	//		PxVec3 lvel = ((PxRigidDynamic*)compCollider->actor)->getLinearVelocity();

	//		float mod_lvel = sqrt(lvel.x * lvel.x + lvel.y * lvel.y + lvel.z * lvel.z);

	//		dbg("Mod: %.2f -> Linear Velocity [%.2f %.2f %.2f]\n", last_mod_lvel, lvel.x, lvel.y, lvel.z);

	//		if (abs(mod_lvel - last_mod_lvel) > threshold_slope && mod_lvel <= threshold_slope) {

	//			/*if (!first_slope) {
	//				first_slope = true;
	//				timer_throw = 0;
	//				last_mod_lvel = mod_lvel;
	//				return;
	//			}
	//			else {
	//				if (timer_throw < time_throw) return;
	//			}*/

	//			VEC3 origin;
	//			VEC3 uniDir;
	//			float dist;
	//			PxRaycastBuffer hitCall;
	//			PxQueryFilterData fd = PxQueryFilterData();
	//			fd.data.word0 = CModulePhysicsSinn::FilterGroup::RFilter;

	//			bool status = CEngine::get().getPhysics().gScene->raycast(VEC3_TO_PXVEC3(origin),VEC3_TO_PXVEC3(uniDir), dist, hitCall, PxHitFlags(PxHitFlag::eDEFAULT), fd);

	//			being_thrown = false;
	//			bouncing = true;

	//			vec_force = front_throw * vel_x * perc_x + VEC3(0, 1, 0) * vel_y * perc_y;
	//			vec_force = last_lvel * perc_x;
	//			((PxRigidDynamic*)compCollider->actor)->setLinearVelocity(PxVec3(vec_force.x, vec_force.y, vec_force.z));
	//			((PxRigidDynamic*)compCollider->actor)->addForce(VEC3_TO_PXVEC3(vec_force), PxForceMode::eIMPULSE);

	//			dbg("Le he a�adido [%.2f %.2f %.2f] de fuerza cuando la Linear Velocity [%.2f %.2f %.2f]\n", vec_force.x, vec_force.y, vec_force.z, lvel.x, lvel.y, lvel.z);

	//			return;
	//		}
	//		else {
	//			last_lvel = VEC3(lvel.x, lvel.y, lvel.z);
	//			last_mod_lvel = mod_lvel;
	//		}
	//	}

	//}

	//if (bouncing) {
	//	TCompCollider* compCollider = get<TCompCollider>();
	//	TCompTransform* mytrans = get<TCompTransform>();

	//	return;
	//}


	TCompCollider* compCollider = get<TCompCollider>();
	TCompTransform* mytrans = get<TCompTransform>();

	timer_stopped += dt;

	if (compCollider->actor != nullptr)
	{
		PxVec3 lvel = ((PxRigidDynamic*)compCollider->actor)->getLinearVelocity();

		float mod_lvel = sqrt(lvel.x * lvel.x + lvel.y * lvel.y + lvel.z * lvel.z);

		if (mod_lvel < 0.8) {
			if (timer_stopped > time_stopped && num_veces == 2) {
				CHandle boss = getEntityByName("Boss");
				TCompTransform* my_trans = get<TCompTransform>();

				TMsgSuspiciusEvent msg;
				msg.intensity = 2;
				msg.pos = mytrans->getPosition();
				msg.type = static_cast<int> (suspicius_noise_type::STONE_NOISE);
				boss.sendMsg(msg);


				CHandle my = CHandle(this).getOwner();
				my.destroy();

				//((PxRigidDynamic*)compCollider->actor)->putToSleep();
			}
			else {
				num_veces++;
			}
		}
		else {
			timer_stopped = 0;
		}

	}


}

void TCompThrownObject::onEntityCreated()
{
	TCompCollider* compCollider = get<TCompCollider>();
	TCompTransform* mytrans = get<TCompTransform>();

	const PxU32 numShapes = compCollider->actor->getNbShapes();
	std::vector<PxShape*> shapes;
	shapes.resize(numShapes);
	compCollider->actor->getShapes(&shapes[0], numShapes);

	for (auto shape : shapes) {
		PxMaterial* mat;
		shape->getMaterials(&mat, 1);
		mat->setRestitution(0.3);
		mat->setStaticFriction(0.2);
		mat->setDynamicFriction(0.3);
	}

	init_pos = mytrans->getPosition();
}

void TCompThrownObject::throwObject(VEC3 force)
{
	TCompCollider* compCollider = get<TCompCollider>();
	if (compCollider->actor != nullptr)
	{
		((PxRigidDynamic*)compCollider->actor)->addForce(VEC3_TO_PXVEC3(force), PxForceMode::eIMPULSE);
	}
}
