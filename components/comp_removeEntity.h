#pragma once
#include "components/common/comp_base.h"
#include "comp_interaction.h"
#include "entity/entity.h"

class TCompRemoveEntity : public TCompInteraction
{
	DECL_SIBLING_ACCESS();

public:
	INIT_OWNER
	static void registerMsgs();
	void loadChild(const json& j, TEntityParseContext& ctx);
	void updateChild(float dt);
	void onEntityCreatedChild();
	void onChildInteraction();
	
	void checkOnTrigger(const TMsgOnTrigger& msg);

private:
	std::vector<std::string> ent_to_remove;
	std::vector<CHandle> handle_to_remove;
	float countdown = 0.f;
	float timer_to_remove = 0;
	bool finished = false;
	bool int_OnTrigger = false;




};

