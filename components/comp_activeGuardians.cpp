#include "mcv_platform.h"
#include "comp_activeGuardians.h"

DECL_OBJ_MANAGER("activeGuardians", TCompActiveGuardians);

void TCompActiveGuardians::load(const json& j, TEntityParseContext& ctx)
{
	auto ent_array = j["guardians"];
	if (ent_array.empty()) {
		finished = true;
		return;
	}
	assert(ent_array.size() > 0);

	for (auto& ent_name : ent_array) guardians.push_back(ent_name);

	countdown = j.value("countdown", 0.f);

	active = j.value("active", active);
}

void TCompActiveGuardians::update(float dt)
{
	if (finished)	return;

	timer_to_active += dt;

	if (timer_to_active >= countdown && handle_guardians.size() > 0) {
		for (auto ent : handle_guardians) {
			TMsgActiveGuardian msg;
			msg.active = active;
			ent.sendMsg(msg);
		}
		handle_guardians.clear();
		finished = true;
	}
}

void TCompActiveGuardians::debugInMenu()
{
	for (auto guard : guardians) {
		ImGui::Text("Guardian : %s", guard.c_str());
	}

	if (ImGui::SmallButton("Active")) {
		for (auto ent : guardians) {
			CHandle h_ent = getEntityByName(ent);
			TMsgActiveGuardian msg;
			msg.active = active;
			h_ent.sendMsg(msg);
		}
	}
}

void TCompActiveGuardians::registerMsgs()
{
	DECL_MSG(TCompActiveGuardians, TMsgOnTrigger, onEnterTrigger);
}

void TCompActiveGuardians::onEnterTrigger(const TMsgOnTrigger& msg)
{
	if (!msg.trigger_enter) return;

	if (guardians.size() <= 0) {
		finished = true;
		return;
	}

	if (countdown > 0) {
		for (auto ent : guardians) {
			CHandle h_ent = getEntityByName(ent);
			handle_guardians.push_back(h_ent);
		}
		timer_to_active = 0;
	}
	else {
		for (auto ent : guardians) {
			CHandle h_ent = getEntityByName(ent);
			TMsgActiveGuardian msg;
			msg.active = active;
			h_ent.sendMsg(msg);
		}
		finished = true;
	}

}
